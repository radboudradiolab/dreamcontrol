#!/usr/bin/env python3
import datetime
import json
import logging
import os
from typing import List

import dream_config as cfg
from dream_dataclasses import (CameraClientInfo, CameraServerMode, DataProduct,
                               DomeState, DomeTargetState, Errors, HeaterState,
                               LedState, PeltierState, RubinClientInfo,
                               Warnings)
from dream_electronics import Hardware
from dream_utils import (Singleton, json_default, json_object_hook,
                         send_slack_message)

log = logging.getLogger(cfg.LOGGER_NAME)


class DreamState(object, metaclass=Singleton):
    '''A singleton class representing the state of the station. It mostly
       contains the flags used by the various control loops to signal each other
       what should happen. It also serves to persist some state variables to a
       file which makes restart/reboots transparent.
    '''

    @property
    def dome_last_closed(self):
        return self._dome_last_closed

    @dome_last_closed.setter
    def dome_last_closed(self, value: datetime.datetime):
        self._dome_last_closed = value
        #log.info(f'Saving last dome close to state file: {value}')
        self.sync()

    def sync(self):
        with open(cfg.STATE_FILE, 'w') as f:
            json.dump({
                'dome_last_closed': self._dome_last_closed.isoformat(),
                'dome_ok': self.rubin.dome_ok
            }, f)


    def __init__(self) -> None:
        log.info('Initializing Dream state')

        self.problems: set[Errors|Warnings] = set()

        self.target_mode: CameraServerMode = CameraServerMode.AUTO
        self.mode: CameraServerMode = CameraServerMode.IDLE

        hw = Hardware()
        self.dome_state: DomeState = (DomeState.CLOSED if hw.get_dome_closed_switch() else
                                      DomeState.OPEN if hw.get_dome_open_switch() else DomeState.STOP)

        self.dome_target_state: DomeTargetState = DomeTargetState.AUTO
        self.dome_last_move: datetime.datetime = datetime.datetime.now(datetime.timezone.utc)

        # flag from rubin
        self.rubin: RubinClientInfo = RubinClientInfo(
            ip_address = 'unknown',
            last_request_time = datetime.datetime.fromtimestamp(0, tz=datetime.timezone.utc),
            last_request_id = '--',
            weather_ok = False,
            weather_updated = datetime.datetime.fromtimestamp(0, tz=datetime.timezone.utc),
            dome_ok = False)

        # the last closed time is persisted to disk
        try:
            with open(cfg.STATE_FILE, 'r') as f:
                db = json.load(f)
                self._dome_last_closed = datetime.datetime.fromisoformat(db['dome_last_closed'])
                self.rubin.dome_ok = db.get('dome_ok', False)
        except (FileNotFoundError, json.JSONDecodeError, ValueError):
            self._dome_last_closed = datetime.datetime.now(tz=datetime.timezone.utc)
            self.rubin.dome_ok = False
            self.sync()
            log.error(f'Dream state file absent or malformed')

        # data products also need to be loaded from disk
        self.new_data_products: List[DataProduct] = []
        self.restore_data_products()

        self.led_state: LedState = LedState.OFF
        self.led_target_state: LedState = LedState.AUTO

        # In order to reply to cameras that want to know if the last image is
        # valid, we need to know what we were doing. The minimal state needed to
        # do that is the last blink number and the last time we stopped
        # interfering (ON/FLASH). Blinks are always fully blinked or fully
        # missed so we can save the number and when we receive a notify of a new
        # image we compare the seq number. For any imcoming image we must also
        # check that the flasher or on-override were not on so we set a 'ready'
        # flag when the leds are set to off or blink. An image may arrive
        # shortly after the next blink has already started so we also need to
        # store the previous blink number. Just in case we store the last 5.
        self.recent_led_blinks: List[int] = []  # seq # of the last 5 blinks.
        self.leds_ready_since: datetime.datetime = datetime.datetime.now(datetime.timezone.utc)

        # Heaters and peltiers are automatic, but we can override, which only
        # happens from the gui:
        self.heater_target_state: HeaterState = HeaterState.AUTO
        self.heater_state: HeaterState = HeaterState.UNKNOWN

        self.peltier_target_state: PeltierState = PeltierState.AUTO
        self.peltier_state: PeltierState = PeltierState.UNKNOWN

        self.camerainfo: dict[str, CameraClientInfo] = {}

        # in gui mode (or if configured via config) sttart everything in IDLE/OFF mode
        if not cfg.AUTOSTART:
            log.info('Running in GUI mode: disabling all AUTO modes by default')
            self.target_mode = CameraServerMode.IDLE
            self.dome_target_state = DomeTargetState.STOP
            self.led_target_state = LedState.OFF
            self.heater_target_state = HeaterState.OFF
            self.peltier_target_state = PeltierState.OFF


        # internal problem flags
        self.env_ok: bool = False
        self.ups_ok: bool = False

    def get_go_nogo(self) -> bool:
        '''Return True if we should observe, False if we should close and/or do calibrations.'''
        now = datetime.datetime.now(datetime.timezone.utc)
        client_stale = now - self.rubin.last_request_time > datetime.timedelta(seconds=cfg.RUBIN_TIMEOUT)
        weather_too_old = now - self.rubin.weather_updated > datetime.timedelta(seconds=cfg.RUBIN_TIMEOUT)

        self.set_problem(Warnings.RUBIN_CLIENT_STALE, client_stale)
        self.set_problem(Warnings.RUBIN_WEATHER_TOO_OLD, weather_too_old)
        self.set_problem(Warnings.RUBIN_BAD_WEATHER, not self.rubin.weather_ok)
        self.set_problem(Warnings.RUBIN_NOGO, not self.rubin.dome_ok)

        return (
            (not client_stale) and
            (not weather_too_old) and
            self.rubin.dome_ok and
            self.rubin.weather_ok
        )

    def set_problem(self, which: Warnings | Errors, yesno=True):
        '''Add the warning if yesno, else discard it. Raises no exceptions. Just
        makes sure the warning is in/out of the set depending on yesno.'''
        if yesno:
            if which not in self.problems:
                if which in Errors:
                    timestamp = datetime.datetime.now().strftime('%Y-%m-%d %X')
                    log.warning(f'Error condition: {which.value}')
                    send_slack_message(f'[{timestamp}] Error condition: {which.value}')
                self.problems.add(which)
        else:
            if which in self.problems:
                if which in Errors:
                    timestamp = datetime.datetime.now().strftime('%Y-%m-%d %X')
                    log.warning(f'Error resolved: {which.value}')
                    send_slack_message(f'[{timestamp}] Error resolved: {which.value}')
                self.problems.discard(which)



    def restore_data_products(self):
        try:
            self.new_data_products = []
            with open('/Software/new_data_products.json', 'r') as f:
                for dpline in f.readlines():
                    dp = json.loads(dpline, object_hook=json_object_hook)
                    self.new_data_products.append(dp)
        except (FileNotFoundError, json.JSONDecodeError) as e:
            log.error('Data products index absent or malformed')
            # default to an empty list
            return []

    def add_data_product(self, new: DataProduct):
        log.info(f'Data product received: {new}')
        self.new_data_products.append(new)

        with open('/Software/new_data_products.json', 'a') as f:
            dpline = json.dumps(new, default=json_default)
            f.write(dpline + '\n') # json already escapes newlines if they are present inside strings


    def clear_data_products(self, which: List[DataProduct]):
        os.truncate('/Software/new_data_products.json', 0)
        for product in which:
            self.new_data_products.remove(product)
