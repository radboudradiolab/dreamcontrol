#!/usr/bin/env python3

import asyncio
import logging
from rich.logging import RichHandler
from rich.traceback import install

# some nicer log and traceback messages
install(show_locals=True)
FORMAT = '%(message)s'
logging.basicConfig(
    level='NOTSET', format=FORMAT, datefmt='[%X]', handlers=[RichHandler()]
)
log = logging.getLogger(__name__)


async def worker(N: int = 1):
    try:
        while True:
            log.info(f'worker {N} in progress...')
            await asyncio.sleep(1)
    except asyncio.CancelledError as e:
        log.info(f'cleaning up worker {N} after cancel...')
        await asyncio.sleep(0.5)
        log.info(f'worker {N} finished')
        raise e
    #finally:
    #    log.info(f'worker {N} finally finished')




async def main():
    task1 = asyncio.create_task(worker(1))
    log.info('task 1 started, control now sleeping')
    await asyncio.sleep(5)
    log.info('cancelling task 1')
    task1.cancel()
    log.info('waiting for task1 to finish')
    await task1

    log.info('task 1 finished, continuing with task2')

    task2 = asyncio.create_task(worker(2))
    log.info('task 2 started, control now sleeping')
    await asyncio.sleep(5)
    log.info('cancelling task 2')
    task2.cancel()


if __name__ == '__main__':
    asyncio.run(main())
