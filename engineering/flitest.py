import time

import numpy as np

import pyfli

import matplotlib.pyplot as plt

 

devarray = np.zeros(6600*4,dtype='int')

exparray = np.zeros(6600*4,dtype='int')

timarray = np.zeros(6600*4,dtype='float')

timarray2 = np.zeros(6600*4,dtype='float')

counter = 0

devs=pyfli.FLIList('usb','camera')

hcam1 = pyfli.FLIOpen(devs[0][0],'usb','camera')

t0 = time.clock()

pyfli.setExposureTime(hcam1,6400.0)

pyfli.startVideoMode(hcam1)

t1 = time.clock()

print("Status Begin: "+str(pyfli.getDeviceStatus(hcam1) & 3))

stat = pyfli.getExposureStatus(hcam1)

print("Stat: "+str(stat))

for iii in range(6600):
    
    time.sleep(0.001)

    exp = pyfli.getExposureStatus(hcam1)

    dev = pyfli.getDeviceStatus(hcam1)& 3

    t1 = time.clock()

    devarray[counter] = dev

    exparray[counter] = exp

    timarray[counter] = t1-t0

    counter = counter+1

#pyfli.startVideoMode(hcam1)

image0 = pyfli.grabVideoFrame(hcam1)

for iii in range(6500,6500*2):

    time.sleep(0.001)

    exp = pyfli.getExposureStatus(hcam1)

    dev = pyfli.getDeviceStatus(hcam1)& 3

    t1 = time.clock()

    devarray[counter] = dev

    exparray[counter] = exp

    timarray[counter] = t1-t0

    counter = counter+1

image1 = pyfli.grabVideoFrame(hcam1)

for iii in range(6500*2,6500*3):

    time.sleep(0.001)

    exp = pyfli.getExposureStatus(hcam1)

    dev = pyfli.getDeviceStatus(hcam1)& 3

    t1 = time.clock()

    devarray[counter] = dev

    exparray[counter] = exp

    timarray[counter] = t1-t0

    counter = counter+1

image2 = pyfli.grabVideoFrame(hcam1)

for iii in range(6500*3,6500*4):

    time.sleep(0.001)

    exp = pyfli.getExposureStatus(hcam1)

    dev = pyfli.getDeviceStatus(hcam1)& 3

    t1 = time.clock()

    devarray[counter] = dev

    exparray[counter] = exp

    timarray[counter] = t1-t0

    counter = counter+1

image3 = pyfli.grabVideoFrame(hcam1)


pyfli.stopVideoMode(hcam1)

import pylab

pylab.plot(timarray,exparray,".")
pylab.plot([6.4,6.4],[0,7000],"-")
pylab.plot([6.4+1.5,6.4+1.5],[0,7000],"-")

pylab.plot([2*6.4,2*6.4],[0,7000],"-")
pylab.plot([2*6.4+1.5,2*6.4+1.5],[0,7000],"-")

pylab.plot([3*6.4,3*6.4],[0,7000],"-")
pylab.plot([3*6.4+1.5,3*6.4+1.5],[0,7000],"-")

pylab.show()