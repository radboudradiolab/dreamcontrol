#!/usr/bin/env python3

import asyncio
import zmq
import zmq.asyncio
import time
from rich import print
import random

start = time.time()
def ts():
    dt = time.time() - start
    return f'{dt:.3f}: '

ctx = zmq.asyncio.Context()

async def request_handler(name: str, socket: zmq.asyncio.Socket):
    for i in range(10):

        print(f'{ts()}req sent to {name}')

        await asyncio.sleep(1)



last_updated = {}

async def router():
    socket = ctx.socket(zmq.ROUTER)
    socket.bind('tcp://*:9000')

    while True:
        client, _, request, i = await socket.recv_multipart()
        print(f'{ts()}incoming req from {client}: {request} {i}')
        last_updated[client] = time.time()

        if client == b'client 2' and i == b'5':
            print(f'{ts()}intentionally skipping reply 5 from client 2 to simulate what would happen')
            continue
        await socket.send_multipart([client, b'', b'bar', i])


async def connection_checker():
    while True:
        # check all connections:
        toremove = []
        for client, update in last_updated.items():
            if update <= time.time() - 3.0:
                print(f'{ts()}{client} seems to have timed out')
                toremove.append(client)
        for client in toremove:
            del last_updated[client]

        if len(last_updated) == 0:
            print(f'{ts()}connetion checker: nothing to do')
            await asyncio.sleep(1)
            continue

        # wait until the next one
        delay = 3.0 + min(last_updated.values()) - time.time()
        print(f'{ts()}connetion checker sleeping for {delay}')
        await asyncio.sleep(delay)



async def req(name: str, reps: int = 10):

    i = 0
    while True:
        socket = ctx.socket(zmq.REQ)
        socket.setsockopt(zmq.IDENTITY, name.encode())
        socket.connect('tcp://localhost:9000')
        print(f'{name}: Socket connected!')

        while True:
            i = i + 1
            if i > reps:
                print(f'{ts()}finished. Bye.')
                return
            await socket.send_multipart([b'foo', str(i).encode()])
            #print(f'{ts()}req {i} sent by {name}')

            try:
                rep = await asyncio.wait_for(socket.recv_multipart(), 3.0)
                print(f'{ts()}rep received by {name}: {rep[1]}')
            except asyncio.TimeoutError as e:
                print(f'{ts()}Timeout {name}. Assuming server is dead. Reconnecting...')
                break

            delay = 1 + random.random()
            #print(f'{ts()}{name} sleeping for {delay}')
            await asyncio.sleep(delay)


    socket.close()





async def main():
    central = router()
    client1 = req('client 1')
    client2 = req('client 2')

    await asyncio.gather(central, client1, client2, connection_checker())

if __name__ == '__main__':
    asyncio.run(main())
