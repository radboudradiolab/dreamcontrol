#!/usr/bin/env python3

class SNMPDevice(object):
    '''Wrapper class with shared methods to connect to SNMP devices.
    '''
    def __init__(self, ip_address: str, username: str, password: str, timeout: int = 5, debug : bool = False, name: str = '', version: int = 3):
        '''Initilization of the SNMP client

        Args:
            ip_address (str): the IP address of the PDU
            username (str): username for authentication
            password (str): for authentication

        Keyword args:
            timeout (int): time out for reading the PDU buffer
            debug
        '''
        self.ip_address = ip_address
        self.name = name or 'SNMP device at {ip_address}'.format(ip_address=ip_address)
        self.username = username
        self.password = password
        self.timeout = timeout
        self.debug = debug
        self.version = version
        self._lock = threading.Lock()

        self.connect()

    def connect(self):
        '''Create an SNMP session to communicate with the device.
        SNMP is UDP based, so no real connection is maintained.
        It is therefore also not necessary to call a close method.
        '''
        if self.debug:
            return
        with self._lock:
            self.snmp = SNMPSession(
                hostname=self.ip_address,
                version=self.version,
                security_level='auth_with_privacy' if self.username != '' else 'no_auth_or_privacy',
                security_username=self.username,
                auth_protocol='SHA',
                auth_password=self.password,
                privacy_protocol='AES',
                privacy_password=self.password,
                timeout=self.timeout,
                community='public'
            )

    def set(self, oids: Union[List[str], str], values: Union[List[str], str], datatype: str = 'INTEGER'):
        # deal with single value as if it's a list of size 1
        if type(oids) != list:
            oids = [oids]
        if type(values) != list:
            values = [values]
        if len(oids) != len(values):
            raise ValueError("oids and values must be same length")

        # in debug
        if self.debug:
            with self._lock:
                for oid, value in zip(oids, values):
                    self.dummy_state[oid] = value

        else: # if not debug
            # create a same-length list of type strings
            for oid, value in zip(oids, values):
                self._try_snmp(partial(self.snmp.set, oid, value, datatype))


    def get(self, oids: Union[List[str], str]):
        if self.debug:
            if type(oids) == list:
                return [self.dummy_state[oid] for oid in oids]
            else:
                return self.dummy_state[oids]
        else: # if not debug
            var = self._try_snmp(partial(self.snmp.get, oids))
            if type(oids) == list:
                return [v.value for v in var]
            else:
                return var.value


    def _try_snmp(self, callback : Callable[[], Any], retries : int = 3) -> Any:
        # On at least one occasion a SystemError was observed on the first SNMP get request
        # so we wrap our SNMP actions in 3 retries
        with self._lock:
            for attempt in range(retries):
                try:
                    return callback()
                except SystemError as e:
                    print("SystemError caught on SNMP attempt {count} to {name}".format(count=attempt+1, name=self.name))
                    if attempt == retries - 1:
                        print('This is bad')
                        raise e
                    else:
                        print('Retrying in 1...')
                        time.sleep(1)
