#!/usr/bin/env python3
# quick experiment to determine the accuracy of the clock in the fli camera in
# video mode.

import pyfli
import time
import os
import numpy as np
from threading import Thread
import sys

np.random.seed(42)

# constants
#EXPOSURETIME = 6.382525225
#EXPOSURETIME = 6.400000000
#EXPOSURETIME  = 6.435
EXPOSURE_MIN =  6.382525225 - 0.0035
EXPOSURE_MAX =  6.382525225 + 0.0035
EXPOSURE_STEP = 0.000050
NUM_EXPOSURES = 200 # repeats per exposure setting
LOGFILE = 'drift_log.csv'
with open(LOGFILE, 'w') as f:
    f.write('time,exposuretime,num,drift,zeros,overflows\n')

# open camera
devs = pyfli.FLIList('usb','camera')
cam = pyfli.FLIOpen(devs[0][0],'usb','camera')

#def start_video_mode(exposuretime:int) -> int:
#    print(f"Starting video mode with exposuretime={exposuretime}")
#    pyfli.setExposureTime(cam, exposuretime * 1000.)
#    pyfli.startVideoMode(cam)
#    # remember start time
#    starttime = time.time()
#    while time.time() < starttime + 1.0:
#        if pyfli.getExposureStatus(cam) > 0:
#            print(f'{time.time()}: exposure started at {starttime}')
#            return starttime
#        time.sleep(0.001)
#
#    # if we reach this point, exposure did not start
#    # so we retry
#    pyfli.
#    print(f'{time.time()}: ERROR: exposure did not start')


def camera_status_text(status : int) -> str:
    if status == pyfli.CAMERA_STATUS_UNKNOWN:
        return 'UNKNOWN'
    dataready = 'DATA_READY+' if status & pyfli.CAMERA_DATA_READY else ''

    if status & pyfli.CAMERA_STATUS_MASK == pyfli.CAMERA_STATUS_IDLE:
        return dataready+'IDLE'
    if status & pyfli.CAMERA_STATUS_MASK == pyfli.CAMERA_STATUS_WAITING_FOR_TRIGGER:
        return dataready+'WAITING_FOR_TRIGGER'
    if status & pyfli.CAMERA_STATUS_MASK == pyfli.CAMERA_STATUS_READING_CCD:
        return dataready+'READING_CCD'
    if status & pyfli.CAMERA_STATUS_MASK == pyfli.CAMERA_STATUS_EXPOSING:
        return dataready+'EXPOSING'


kill_monitor=False
def monitor_device_status():
    oldstatus = 0
    #kill_monitor = False
    exp_start = 0
    while not kill_monitor:
        newstatus = pyfli.getDeviceStatus(cam) & (pyfli.CAMERA_STATUS_MASK | pyfli.CAMERA_DATA_READY)
        if oldstatus != newstatus:
            print(f'{time.time()-exp_start:.3f}: FLI status changed {camera_status_text(oldstatus)} -> {camera_status_text(newstatus)}')
            if newstatus & pyfli.CAMERA_STATUS_EXPOSING:
                exp_start = time.time()
        oldstatus = newstatus
        time.sleep(0.0001)
    print('Monitoring thread stopped')

#mon = Thread(target=monitor_device_status)
#mon.start()

exposuretimes = np.arange(EXPOSURE_MIN, EXPOSURE_MAX, EXPOSURE_STEP)
np.random.shuffle(exposuretimes)
for i, exposuretime in enumerate(exposuretimes):
    print(f"Progress report: {i}/{len(exposuretimes)}")

    # start video mode

    # something fishy is happening when you disable and enable video mode
    # quickly after each other. So we need to make sure it is off before we
    # start it. If python is forcibly killed it is not guaranteed that video
    # mode is stopped, so it is also important to do this at the start of the
    # cycle and not at the end.
    pyfli.stopVideoMode(cam)
    print("Waiting for camera IDLE state ", end='')
    while pyfli.getDeviceStatus(cam) & pyfli.CAMERA_STATUS_MASK != pyfli.CAMERA_STATUS_IDLE:
        print('.', end='', flush=True)
        time.sleep(0.1)
    print('done')

    pyfli.setExposureTime(cam, exposuretime * 1000.)
    pyfli.startVideoMode(cam)

    # remember start time
    starttime = time.time()

    print(f"Video mode starting at {starttime}")

    # discard anything that was in the buffers
    #pyfli.grabVideoFrame(cam)
    #pyfli.grabVideoFrame(cam)
    print("Ready")

    # take repeated exposures to find slope
    for i in range(NUM_EXPOSURES):
        # wait for exposure
        print(f"{time.time()}: Getting exposure {i} at exposuretime {exposuretime}")
        #remaining = pyfli.getExposureStatus(cam)
        #print(f"{time.time()}: sleeping for {remaining} ms")
        #time.sleep(max(0, (remaining - 10) / 1000.))
        #while pyfli.getExposureStatus(cam) > 0:
        #    print('.', end='')
        #print(f"\n{time.time()}: sleep done")

        #print("Waiting for exposure to start ", end='')
        #while True:
        #    status = pyfli.getDeviceStatus(cam)
        #    if (status & pyfli.CAMERA_STATUS_MASK) == pyfli.CAMERA_STATUS_EXPOSING:
        #        break
        #    #print(camera_status_text(status))
        #    print('.', end='', flush=True)
        #    time.sleep(0.01)
        #print('done')

        #print("Waiting for data ready ", end='')
        while not (pyfli.getDeviceStatus(cam) & pyfli.CAMERA_DATA_READY):
            #print('.', end='', flush=True)
            time.sleep(0.001)
        #print('done')
        #print("Exposure done")

        # detect overflow, means image was done
        #if remaining == 0:
        donetime = time.time()
        print(f"Exposure done at {donetime}")


        #print(f'{donetime}: Exposure done!')
        # download but discard image
        image = pyfli.grabVideoFrame(cam)
        # some stats
        numzeros = np.sum(image == 0)
        nsaturated = np.sum(image > 65530)
        # calculate drift
        intended_donetime = starttime + (i+1) * exposuretime
        drift = donetime - intended_donetime
        #drift = (delta + exposuretime/2) % exposuretime - (exposuretime/2)
        print(f"{time.time()}: exposure {i: 2d} at {exposuretime} drift: {drift*1000.:.3f} (ms) zeros: {numzeros} sat: {nsaturated}")
        with open(LOGFILE, 'a') as log:
            log.write(f"{time.time()},{exposuretime},{i},{drift},{numzeros},{nsaturated}\n")

    #pyfli.stopVideoMode(cam)
    pyfli.cancelExposure(cam) # also stops video mode
