#!/usr/bin/env python3

import asyncio
import datetime
import json
import logging
from itertools import count
import sys

import aiohttp
import pytz
from textual.app import App, ComposeResult
from textual.containers import Horizontal, Vertical
from textual.widgets import DataTable, Footer, Header, RichLog, Static, Switch

sys.path.append('..')
import dream_config as cfg
from dream_logging import RichLogHandler, setup_logging
from dream_state import Warnings
from dream_utils import fix_iers_table_download

cfg.LOG_PATH = '.'
log = logging.getLogger(cfg.LOGGER_NAME)
fix_iers_table_download()

''' Design notes:

1) There are two notification channels because I didn't want to spam others
while developing. The channel is chosen on the fly based on the state of the
command script. I wanted to also send notification if the mock starts or stops
but it wouldn't know which channel to use and I didn't want to complicate things
by trying to remember the most recent channel.

2) our collection is not a 'nice' 0mq socket that knows about request-reply
pairs we need an asyncio Lock object to avoid concurrent requests. NB:
concurrent requests are technically allowed because they can be matched by their
request_id but I'm lazy.

3) the notifications are running in a separate loop. Mainly with the idea that
ntfy.sh requests may take longer or time-out altogether and we don't want to
delay the ui update frequency to much lower than 1Hz.'''

class MockRubinApp(App):
    TITLE = "Mock Rubin"
    BINDINGS = [
        ('q', 'quit', 'Exit')
    ]
    CSS_PATH = 'mock_rubin.css'

    lock = asyncio.Lock()
    request_id = count()
    queue = asyncio.Queue()
    real_channel = '429f2ff8-16fb-4821-8cc7-4fb6793f7210'
    sim_channel = '28c71cd6-89c5-4ce7-86c7-90658238a874'

    def compose(self) -> ComposeResult:
        yield Header(name=self.TITLE)
        yield Vertical(
            Horizontal(
                DataTable(),
                RichLog(id='status', auto_scroll=False),
                id='toprow'
            ),
            Horizontal(
                Vertical(
                    Static('Controls:', classes='header'),
                    Horizontal(
                        Static('OPEN Dome:'),
                        Switch(id='switch_dome'),
                        classes='switch'
                    ),
                    Horizontal(
                        Static('Weather OK:'),
                        Switch(id='switch_weather'),
                        classes='switch'
                    ),
                    classes='leftcolumn'
                ),
                RichLog(id='log'),
                id='bottomrow'
            )
        )
        yield Footer()

    async def on_mount(self):
        # setup logging area to track natice python logging
        richlog = self.query_one('#log', RichLog)
        log.addHandler(RichLogHandler(richlog, self))
        richlog.border_title = 'Log:'

        # build table
        table: DataTable = self.query_one(DataTable)
        table.border_title = 'Data products:'
        table.add_column('kind')
        table.add_column('type')
        table.add_column('seq')
        table.add_column('start')
        table.add_column('end')
        table.add_column('server')
        table.add_column('size')
        table.add_column('filename')
        table.add_column('sha256')

        self.query_one('#status').border_title = 'Station state:'
        self.query_one('.leftcolumn').border_title = 'Controls:'

        # start the worker to manage the connection
        self.run_worker(self.reconnect_loop())

    async def reconnect(self):
        while True:
            try:
                self.reader, self.writer = await asyncio.open_connection('127.0.0.1', 5000)
            except ConnectionRefusedError:
                await asyncio.sleep(1)
            else:
                break

    async def reconnect_loop(self):
        await asyncio.sleep(0.1)
        log.info('Connecting to dream command server...')
        while True:
            # disable ui interaction until connected
            self.query_one('#switch_weather', Switch).disabled = True
            self.query_one('#switch_dome', Switch).disabled = True

            # reset the flags so that if this script remains on we don't
            # accidentially give an ok signal if the control software is started
            # the "with switch.prevent() temporarily disables change events
            # which would trigger network requests which currently can't happen.
            # This is mostly to keep the log clean more than anything else.
            switch = self.query_one('#switch_weather', Switch)
            with switch.prevent(Switch.Changed):
                switch.value = False
            switch = self.query_one('#switch_dome', Switch)
            with switch.prevent(Switch.Changed):
                switch.value = False

            # attempt to connect forever until successful
            await self.reconnect()
            log.info('connected')

            # re-enable the ui
            self.query_one('#switch_weather', Switch).disabled = False
            self.query_one('#switch_dome', Switch).disabled = False

            try:
                # send the state of the switches to the command server
                await self.send_both_switches()

                # run a task group for anything else
                async with asyncio.TaskGroup() as tg:
                    tg.create_task(self.status_polling_loop())
                    tg.create_task(self.weather_loop())
                    tg.create_task(self.notification_loop())
                    tg.create_task(self.dataproduct_loop())
            except ExceptionGroup as eg:
                for e in eg.exceptions:
                    log.error(e.__repr__())

            log.info('Reconnecting...')

    async def notify(self, message, tag='', priority='default', simulated=False):
        async with aiohttp.ClientSession() as session:
            now = datetime.datetime.now(pytz.timezone(cfg.SITE_ZONE)).isoformat(timespec='seconds')
            simulator_message = '(DOME SIMULATED)' if simulated else ''
            channel = self.sim_channel if simulated else self.real_channel
            messagetext = f'{now}: {simulator_message} {message}'
            async with session.post(f'https://ntfy.sh/{channel}', data=messagetext, headers={'Title':'Dream', 'Priority': priority, 'Tags': tag}) as resp:
                await resp.text()
                log.info(f'Notification sent: {messagetext}')

    async def weather_loop(self):
        while True:
            async with self.lock:
                switch = self.query_one('#switch_weather', Switch)
                # send a request
                request = {
                    'request_id': next(self.request_id),
                    'action': 'setWeather',
                    'data': switch.value
                }

                self.writer.write(json.dumps(request).encode() + b'\n')

                # get a reply:
                reply = await self.reader.readline()

                if len(reply) == 0:
                    log.info('Weather loop error')
                    raise RuntimeError('weather loop disconnected')

                status = json.loads(reply)
                if status['result'] != 'ok':
                    log.error('Dream did not ACK our weather update')

            await asyncio.sleep(1)

    async def notification_loop(self):
        try:
            prev_status = await self.queue.get()
            # depending on simulator state we send to the simulated channel or the real channel
            is_simulated = Warnings.SIMULATED_DOME in prev_status['warnings']
            await self.notify(f'Mock Rubin connected to DREAM', simulated=is_simulated)

            while True:
                status = await self.queue.get()
                is_simulated = Warnings.SIMULATED_DOME in prev_status['warnings']

                # check if dome has changed
                if prev_status['actual_dome_state'] != status['actual_dome_state']:
                    log.info('dome changed')
                    await self.notify(f'Dome is {status["actual_dome_state"]}', tag='warning', priority='default', simulated=is_simulated)

                # check for new errors
                for err in status['errors']:
                    if err not in prev_status['errors']:
                        await self.notify(f'Dream error: {err}',tag='rotating_light', priority='urgent', simulated=is_simulated)

                prev_status = status
        except asyncio.CancelledError:
            #log.info('Notify loop cancelled')
            # print a pretty warning about losing the connection
            if prev_status['limit_switches']['dome_closed'] != 'hit':
                await self.notify(f'Mock Rubin lost connection to DREAM with the dome OPEN!!!!!', simulated=is_simulated, priority='urgent')
            else:
                await self.notify(f'Mock Rubin lost connection to DREAM', simulated=is_simulated)

            # caught cancellation should always be allowed to bubble
            raise

    async def dataproduct_loop(self):
        while True:
            await asyncio.sleep(1)
            async with self.lock:
                request = {
                    'request_id': next(self.request_id),
                    'action': 'getNewDataProducts'
                }
                self.writer.write(json.dumps(request).encode() + b'\n')

                # get a reply:
                rawreply = await self.reader.readline()
                if len(rawreply) == 0:
                    raise RuntimeError('data product disconnected')
                reply = json.loads(rawreply)
                if reply['result'] != 'ok':
                    error = reply['reason']
                    log.error(f'Server error when reading data products: {error}')
                    continue

                dataproducts = reply['new_products']
                pretty = json.dumps(dataproducts, indent=2)
                log.info(pretty)

                table: DataTable = self.query_one(DataTable)
                for product in dataproducts:
                    table.add_row(*product.values())


            await asyncio.sleep(1)

    async def status_polling_loop(self):
        while True:
            async with self.lock:
                # send a request
                request = {
                    'request_id': next(self.request_id),
                    'action': 'getStatus'
                }
                self.writer.write(json.dumps(request).encode() + b'\n')

                # get a reply:
                reply = await self.reader.readline()
                if len(reply) == 0:
                    log.info('status loop error')
                    raise RuntimeError('status loop disconnected')
                status = json.loads(reply)
                pretty = json.dumps(status, indent=2)

                # print in log area
                statusarea = self.query_one('#status', RichLog)
                statusarea.clear()
                statusarea.write(pretty)

                # send ntfy message if errors or dome changed
                self.queue.put_nowait(status['status'])

            await asyncio.sleep(1)

    async def on_switch_changed(self, event):
        #log.info(f'{event.switch.id} = {event.value}')
        if event.switch.id == 'switch_dome':
            action = 'setRoof'
        elif event.switch.id == 'switch_weather':
            action = 'setWeather'
        else:
            raise RuntimeError(f'Unknown Switch id {event.switch.id}')

        await self.send_switch(action, event.value)

    async def send_switch(self, action, data):
        # send a request
        request = {
            'request_id': next(self.request_id),
            'action': action,
            'data': data
        }

        async with self.lock:
            log.info(f'Sending switch {action}={data}')
            self.writer.write(json.dumps(request).encode() + b'\n')

            # get a reply:
            reply = await self.reader.readline()
            if len(reply) == 0:
                log.info('failed to send switch info')
                # just ignore the error and rely on the pollers and the re-connect mechanism to fix things
                pass

    async def send_both_switches(self):
        switch: Switch = self.query_one('#switch_weather', Switch)
        await self.send_switch('setWeather', switch.value)

        switch: Switch = self.query_one('#switch_dome', Switch)
        await self.send_switch('setRoof', switch.value)



if __name__ == '__main__':
    setup_logging(log, filename='mock_rubin.log')
    app = MockRubinApp()
    app.run()
