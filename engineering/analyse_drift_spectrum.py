#!/usr/bin/env python3

import pyqtgraph as pg
import numpy as np
app = pg.mkQApp()
pg.setConfigOption('background', 'w')
pg.setConfigOption('foreground', 'k')
pg.setConfigOptions(antialias=True)

plot = pg.plot()#PlotWidget()
plot.addLegend()

color = 0
filename = 'drift_log.csv'
data = np.loadtxt(filename, skiprows=1, delimiter=',')

exposuretimes = np.unique(data[:, 1])
exposuretimes = np.sort(exposuretimes)

for exposuretime in exposuretimes:
    subset = data[data[:, 1] == exposuretime]
    assert len(subset > 0)
    starttime = subset[0, 0]
    drift = subset[:, 3]
    indices = subset[:, 2]
    times = subset[:, 0] - starttime

    # plot
    plot.plot(times, drift-drift[0], pen=pg.intColor(color, len(exposuretimes)), name=f'exp={exposuretime:.4f}')
    color += 1

plot.setLabel('bottom', 'time (s)')
plot.setLabel('left', 'drift (s)')

#plot.show()

# second plot for coefficients
exposuretimes = np.unique(data[:, 1])
exposuretimes = np.sort(exposuretimes)
coefficients = []
for exposuretime in exposuretimes:
    subset = data[data[:, 1] == exposuretime]
    assert len(subset > 0)
    drift = subset[:, 3]
    indices = subset[:, 2]

    # linear regression
    coef = np.polyfit(indices, drift, 1)[0]
    print(f'{exposuretime}: remaining drift coefficient: {coef*1000.:.3f} (ms per exposure)')
    coefficients.append(coef)
plot = pg.plot()#PlotWidget()
plot.setLabel('bottom', 'exposure time (s)')
plot.setLabel('left', '(linear fit of) drift (ms/exposure)')
plot.plot(exposuretimes, coefficients, pen='red', symbol='o', symbolPen=None, symbolBrush='red', symbolSize=5)
#plot.show()


# Third plot
# Comparing requested expsure time to actually achieved exposure rate
exposureperiods = []
group = 0
plot = pg.plot()
scatter = pg.ScatterPlotItem()
spots = []
colors = [
    '#1F77B4', '#FF7F0E', '#2CA02C', '#D62728', '#9467BD'
]
for i, exposuretime in enumerate(exposuretimes):
    subset = data[data[:, 1] == exposuretime]
    assert len(subset > 0)
    times = subset[:, 0]

    # start a new group/color when the coeficient wraps
    if i > 0 and coefficients[i] > coefficients[i-1]:
        group += 1

    exposureperiod = (times[-1] - times[0]) / (len(times) - 1)
    color = colors[group % len(colors)]
    spots.append({'pos': (exposuretime, exposureperiod),
                  'size': 5,
                  'pen': None,
                  'brush': color})
scatter.addPoints(spots)
plot.addItem(scatter)
#import ipdb;ipdb.set_trace()
plot.plot([np.min(exposuretimes),np.max(exposuretimes)], [np.min(exposuretimes),np.max(exposuretimes)])
plot.setLabel('bottom', 'requested  exposure time (s)')
plot.setLabel('left', 'achieved exposure period (s)')
plot.showGrid(x = True, y = True, alpha = 0.3)

#plot = pg.plot()#PlotWidget()
#plot.setLabel('bottom', 'requested  exposure time (s)')
#plot.setLabel('left', 'achieved exposure period (s)')
#plot.plot(exposuretimes, exposureperiods, pen='red', symbol='o', symbolPen=None, symbolBrush='red', symbolSize=5)
#plot.show()


# Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    app.exec_()  # Start QApplication event loop ***
