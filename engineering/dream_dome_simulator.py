#!/usr/bin/env python3

import time
import sys; sys.path.append('..')
import dream_config as cfg
from dream_utils import Singleton
import dream_logging
import logging
import random

log = logging.getLogger(cfg.LOGGER_NAME)

class DomeSimulator(object, metaclass=Singleton):
    # we start closed. STT: it appears control depends on this
    #_position = cfg.DOME_CLOSED_POSITION
    _position = 110.
    _last_change = 0.0
    _max_speed = 10.0 # rotations per second
    _dir = True      # False means closing, True means opening
    _onoff = False
    _ps_voltage = 0.
    _ps_current = 20.
    # POSITION_*_DOME are the absolute maximum safe range The endstops trigger
    # slightly earlier. Control counts on this and takes a +/- 0.2 margin before
    # the POSITION_*_DOME to stop the motor. The control considers the dome
    # clsed at an even higher margin of o.25 rotations, to avoid bouncing
    # behaviour.

    def _update(self):
        now = time.time()
        dt = now - self._last_change
        absspeed = min(self._ps_voltage * 0.1, self._max_speed)
        speed = 0.0 if not self._onoff else  - absspeed if self._dir else absspeed
        self._position = self._position + speed * dt
        if self._position < cfg.DOME_OPEN_POSITION - 5 or self._position > cfg.DOME_CLOSED_POSITION + 5:
            print("Dome: WARNING: motor not stopped at the endstop")
        self._last_change = now

    def get_dome_position(self):
        self._update()
        # log.debug(f'Simulated dome position: {self._position:.1f}')
        return self._position

    def get_dome_closed_switch(self):
        self._update()
        return self._position >= cfg.DOME_CLOSED_POSITION - cfg.DOME_MARGIN

    def get_dome_open_switch(self):
        self._update()
        return self._position <= cfg.DOME_OPEN_POSITION + cfg.DOME_MARGIN

    def set_motor_on(self):
        self._update()
        self._onoff = True

    def set_motor_off(self):
        self._update()
        self._onoff = False

    def set_motor_opening(self):
        self._update()
        self._dir = True

    def set_motor_closing(self):
        self._update()
        self._dir = False

    def set_ps_voltage(self, voltage: float):
        self._update()
        self._ps_voltage = voltage

    def set_ps_current(self, current: float):
        self._ps_current = current

    def get_ps_voltage(self) -> float:
        return self._ps_voltage + 0.1 * random.random()

    def get_ps_current(self) -> float:
        # if not on, then report a random current close to 0
        if not self._onoff:
            return 0.01 * random.random()

        # otherwise, report a current proportional to the voltage
        return 0.4 * self._ps_voltage + 0.1 * random.random()
