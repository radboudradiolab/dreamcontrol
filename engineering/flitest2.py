import time

import numpy as np

import pyfli

import mascara_sites as sites
import datetime
rfsite=sites.Site('Reference')
global flst


def calframe(hcam,exptime):
    wait_lst(hcam,6.4,6.4,initial=True,calframe=True)
    pyfli.setImageArea(hcam,0,0,4080,2721)               
    pyfli.setExposureTime(hcam,exptime*1000)
    mill=pyfli.getExposureStatus(hcam)
    time.sleep((mill+150.)/1000.)    
    tmp=pyfli.grabFrame(hcam)
    print(np.mean(tmp),np.shape(tmp))
    


def wait_lst(hcam,the_exptime,cadence,initial=False,calframe=False):
        '''
        In case of continuous exposures at fixed Local Sidereal Timing,
        if the read-out and download time is shorter than the
        exposure time, the function put the camera in waiting mode

        Args:
            the_exptime (float): the exposure time
        '''
        # Changed by ANL for the LPST reference
        # Replace datetime.utcnow() by datetime.now(tz=pytz.utc)
        (_, flst,
         _) = rfsite.local_sidereal_time(datetime.datetime.utcnow(),
                                              texp=cadence)
        # End Changes
        print(flst)
        if initial==True:        
            time_to_wait = (1.-(flst - int(flst)))*cadence
            #if int(flst%50) != 49:
            #    time_to_wait = time_to_wait+int(flst%50)*(9e-5)*6.382525225  #6.268141e-5*6.382525225
            time.sleep(time_to_wait)
            if calframe==False:
                pyfli.setExposureTime(hcam,the_exptime*1000.)
                pyfli.setImageArea(hcam,0,0,4080,2721)               
                pyfli.startVideoMode(hcam)

            #initial==False
            #exposure_status=pyfli.getExposureStatus(hcam)
        else:
            time_to_wait=0.
        return time_to_wait


devarray = np.zeros(2000,dtype='int')

exparray = np.zeros(2000,dtype='int')

timarray = np.zeros(2000,dtype='float')

intflstarray= np.zeros(2000,dtype='float')
flstarray= np.zeros(2000,dtype='float')

margin = np.zeros(2000,dtype='float')

counter = 0

devs=pyfli.FLIList('usb','camera')

hcam1 = pyfli.FLIOpen(devs[0][0],'usb','camera')

for i in np.arange(10):
    calframe(hcam1,0.5)
    
exptime=6.382525225 #6.4 sidereal seconds
#print "Status Begin: "+str(pyfli.getDeviceStatus(hcam1) & 3)
pyfli.stopVideoMode(hcam1)
pyfli.setExposureTime(hcam1,exptime*1000.)
#stat = pyfli.getExposureStatus(hcam1)
t0 = time.clock()
#print "Stat: "+str(stat)
totframe=0

for j in np.arange(3):    
    wait_lst(hcam1,exptime,6.4,initial=True)
    margin[totframe]=time.clock()-t0

    print("Status Begin: "+str(pyfli.getDeviceStatus(hcam1) & 3))

    exp = pyfli.getExposureStatus(hcam1)

    print("Stat: "+str(exp))
    framenr=0


    counter2=0
    for i in np.arange(700000):
        time.sleep(0.001)
        old_exp=exp*1.
        exp = pyfli.getExposureStatus(hcam1)
        #print exp,old_exp
        if counter2 > 0 and exp-old_exp>0:
            t1 = time.clock()
            exparray[totframe] = exp
            timarray[totframe] = t1-t0        
            
            tmp = pyfli.grabVideoFrame(hcam1)
            #framenr = framenr+1
            (_, flst,_) = rfsite.local_sidereal_time(datetime.datetime.utcnow(),
                                              texp=6.4)
            framenr=int(flst)%50
            intflstarray[totframe]=framenr*1.
            flstarray[totframe]=flst*1.
            
            
            totframe=totframe+1
            print(framenr, flst, np.mean(tmp), np.shape(tmp))
            if framenr == 49:
                counter = counter + 1
                counter2= counter2+1
                break
            
        counter = counter+1
        counter2=counter2+1
    pyfli.stopVideoMode(hcam1)


import pylab

pylab.plot(timarray,exparray,".")

for i in np.arange(100):
    pylab.plot([margin[0]+0.997269566*6.4 *i,margin[0]+6.4*0.997269566*i],[0,7000],"-r")
    #pylab.plot([margin[i]+1.5,margin[i]+1.5],[0,7000],"-r")
pylab.xlabel("elapsed time (s)")
pylab.ylabel("remaining time (ms)")
pylab.show()

np.savetxt("timing_"+str(int(exptime*1000))+"_4.txt",np.array([intflstarray,flstarray]).T)


#pylab.plot(intflstarray[(intflstarray != 0)],flstarray[(intflstarray != 0)]%1-np.mean(flstarray[(intflstarray != 0)]%1),".")
#pylab.xlabel("index within sequence of 50")
#pylab.ylabel("fractional lst index at grabVideoFrame - avg")
#pylab.show()
#pylab.plot([2*6.4,2*6.4],[0,7000],"-")
#pylab.plot([2*6.4+1.5,2*6.4+1.5],[0,7000],"-")

#pylab.plot([3*6.4,3*6.4],[0,7000],"-")
#pylab.plot([3*6.4+1.5,3*6.4+1.5],[0,7000],"-")

#devarraypylab.show()