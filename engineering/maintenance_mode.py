#!/usr/bin/env python3
import pyfli
import numpy as np
import time
import asyncio
import astropy.io.fits as fits
from scipy.ndimage import zoom
import datetime
import sep
import os
import socket
from pylab import cm
import matplotlib
from threading import Thread
from textual.app import App, ComposeResult, RenderResult
from textual.containers import Horizontal, Vertical, Grid
from textual import events
from textual.reactive import reactive
from textual.widgets import Static, Button, Header, Footer, RichLog, Input, Label
from textual.widget import Widget
from textual.binding import Binding
from textual.message import Message
from textual.types import MessageTarget
from rich.traceback import install
from rich.progress import Progress
from rich.live import Live
import traceback
install(show_locals=True)


REFRESH_RATE = 4 # Hz
CCD_MARGIN = (36, 25, 36, 25)

# open camera
devs = pyfli.FLIList('usb','camera')
if len(devs) < 1:
    raise RuntimeError("No FLI Camera found")
cam = pyfli.FLIOpen(devs[0][0], 'usb', 'camera')

def ts() -> str:
    """A nice human readable timestamp for the log pane."""
    return datetime.datetime.now().strftime('%x %X.%f')

def scale_down(img, width, height):
    """Scale an image down. Not by zooming/interpolating but by summing counts."""
    res = np.zeros((height, width))
    for y, band in enumerate(np.array_split(img, height, axis=0)):
        for x, block in enumerate(np.array_split(band, width, axis=1)):
            res[y, x] = np.max(block)
    return res

def weighted_median(data, weights):
    """Compute the median of a weighted list.
    See wikipedia for explanation of weighted median."""
    if len(data) == 0:
        return np.nan
    if len(data) != len(weights):
        raise RuntimeError("Weights and data must have equal length")
    # calculate the sortorder of the data without reordering
    order = np.argsort(data)
    # apply that order to the weights and get the cumulative sum
    cumsum = np.cumsum(np.array(weights)[order])
    # determine the median weight
    midweight = cumsum[-1]/2
    # find the index in the data array that overlaps with that weight
    index = np.searchsorted(cumsum, midweight)
    # return the data item at that location
    return np.array(data)[order][index]


def weighted_mean(data, weights):
    """Compute the mean of a weighted list."""
    return np.sum(np.array(data) * np.array(weights)) / np.sum(weights)


def fwhm_map(img, width, height):
    """Create a map of size width x height bins containing the median fwhm value of stars in each bin."""

    # start by extracting sources using sep
    background = sep.Background(img)
    img = img - background
    objects = sep.extract(img, 5, err=background.globalrms)

    # compute the resolution and points on the new grid
    xbins = np.linspace(0, img.shape[1], width)
    ybins = np.linspace(0, img.shape[0], height)
    xres = xbins[1]
    yres = ybins[1]

    # prepare arrays for bins
    binned = [[[] for _ in range(width)] for _ in range(height)]
    weights = [[[] for _ in range(width)] for _ in range(height)]

    # add objects to b ins
    for i in range(len(objects['x'])):
        x = objects['x'][i]
        y = objects['y'][i]
        a = objects['a'][i]
        b = objects['b'][i]
        # this is a good-enough heuristic
        fwhm = 2 * np.sqrt(np.log(2) * (a**2 + b**2))

        # save the center in new-pixel coordinates
        p = np.array([x / xres,  y / yres])

        # calculate the indices of the 4 surrounding bins
        g00 = np.array([x//xres    , y//yres    ]).astype(int)
        g10 = np.array([x//xres + 1, y//yres    ]).astype(int)
        g01 = np.array([x//xres    , y//yres + 1]).astype(int)
        g11 = np.array([x//xres + 1, y//yres + 1]).astype(int)

        # calculate the weights for each corner
        alpha = np.abs((p - g01).prod())
        beta =  np.abs((p - g11).prod())
        gamma = np.abs((p - g00).prod())
        delta = np.abs((p - g10).prod())

        # add fwhm to bins
        binned[g00[1]][g00[0]].append(fwhm)
        binned[g10[1]][g10[0]].append(fwhm)
        binned[g01[1]][g01[0]].append(fwhm)
        binned[g11[1]][g11[0]].append(fwhm)

        # add weight of opposing area to weights
        weights[g00[1]][g00[0]].append(beta)
        weights[g10[1]][g10[0]].append(alpha)
        weights[g01[1]][g01[0]].append(delta)
        weights[g11[1]][g11[0]].append(gamma)

    # apply the weighted median to each bin
    pixels = np.array([[weighted_mean(binned[y][x], weights[y][x]) for x in range(width)] for y in range(height)])
    return np.ma.masked_invalid(pixels)


def choose(indices, choices):
    """Returns an array in the shape of indices where values are taken from
    choices according to the indices in the input. See also numpy.choose(), but
    without the 32 element limit on the choices, probably at the cost of
    performance.    """
    # flatten
    tmp = indices.flatten()
    # apply lookup
    res = np.ma.zeros(tmp.shape, dtype=np.array(choices).dtype)
    for i in range(len(tmp)):
        index = tmp[i]
        res[i] = choices[index] if index is not np.ma.masked else np.ma.masked
    # reshape
    return res.reshape(indices.shape)


def star_map(img, width, height, cols=5, rows=5):
    # calculate the bounds of the regions
    xbounds = np.linspace(0, img.shape[1], cols + 1).astype(int)
    ybounds = np.linspace(0, img.shape[0], rows + 1).astype(int)

    # result array in which None represents no star found or no image set
    res = [[None for y in range(rows)] for x in range(cols)]

    # for each of the cols*rows sub areas
    for col in range(cols):
        for row in range(rows):
            # the subimg is the whole region in which we look for the star closest to the center
            subimg = np.ascontiguousarray(img[ybounds[row]:ybounds[row+1], xbounds[col]:xbounds[col+1]])

            # extract objects in this subimg
            background = sep.Background(subimg)
            subimg = subimg - background
            objects = sep.extract(subimg, 15, err=background.globalrms)

            # if there are no stars we can't do anything
            if len(objects['x']) == 0:
                continue

            # mask the ones too bright and the ones <10 pixels
            # the latter also implies we don't need dark-frame subtraction to avoid hotpixels
            mask1 = np.ma.masked_greater(objects['peak'], 0.75 * 2**16).mask
            mask2 = np.ma.masked_less(objects['npix'], 10).mask

            if np.sum(~mask1 & ~mask2) == 0:
                res[col][row] = None
                continue

            # calculate the center of the subimg
            cx = xbounds[1] / 2
            cy = ybounds[1] / 2

            # calculate the distance from the center
            d = np.sqrt((objects['x'] - cx)**2 + (objects['y'] - cy)**2)

            # mask the distances according to the two criteria above
            d = np.ma.masked_array(d, mask1 | mask2)

            # find the unmasked star closest to that midpoint
            index = np.argmin(d)

            # find the coords of that star
            xstar = int(round(objects['x'][index]))
            ystar = int(round(objects['y'][index]))

            # save the offset (in the large image)
            res[col][row] = (xbounds[col] + xstar , ybounds[row] + ystar)

    return res


def make_legend(colormap, height, min, max):
    """Make an ascii art color map."""
    # make a list of colors equally long to the window height
    # first we create two helpers to generate the rich syntax to print a color
    # as well as an approprite foreground to match it.
    def background(color):
        r, g, b, _ = color
        r, g, b = int(r * 256), int(g * 256), int(b * 256)
        return f'rgb({r},{g},{b})'
    def foreground(color):
        r, g, b, _ = color
        R = r/12.92 if r <= 0.03928 else ((r+0.055)/1.055) ** 2.4
        G = r/12.92 if g <= 0.03928 else ((g+0.055)/1.055) ** 2.4
        B = r/12.92 if b <= 0.03928 else ((b+0.055)/1.055) ** 2.4

        l = 0.2126 * R + 0.7152 * G + 0.0722 * B
        if l > 0.5:
            return '#000000'
        else:
            return '#ffffff'

    # we create two color maps
    bglevels = [background(colormap(i)) for i in range(colormap.N)]
    fglevels = [foreground(colormap(i)) for i in range(colormap.N)]

    # pick the colors based on levels
    bgcolors = choose(np.linspace(0, len(bglevels)-1, height).round().astype(int), bglevels)
    fgcolors = choose(np.linspace(0, len(fglevels)-1, height).round().astype(int), fglevels)

    # make a list of labels
    text = [f'{val: 5.1f}' for val in np.linspace(min, max, height)]
    text.reverse()

    # merge labels with colors and newlines
    return [f'[{fgcolors[i]} on {bgcolors[i]}]{text[i]}[/]\n' for i in range(height)]


class TempGauge(Widget):
    """Simple widget to display CCD temperatures."""
    DEFAULT_CSS = """
    TempGauge {
        width: 18;
        height: 1fr;
        min-height: 8;
    }
    """
    current = reactive(20.3)
    target = reactive(12.5)
    upper = 40
    lower = -20
    renderinprogress = False

    def render(self):
        #if self.renderinprogress:
        #    raise RuntimeError("Multiple concurrent calls to render")
        self.renderinprogress = True
        height = self.size.height
        bar = height * ['']
        #for i, level in enumerate(np.linspace(self.upper, self.lower, self.size.height)):
        #for i, level in enumerate(np.arange(self.upper, self.lower, (self.lower - self.upper) / height)):
        for i, level in enumerate(np.linspace(self.upper, self.lower-(self.lower-self.upper)/height, height)):
            if level > self.current and level > self.target:
                bar[i] = '    '
            elif level < self.current and level < self.target:
                bar[i] = '  [green on green]**[/]'
            else:
                bar[i] = '  [red on red]**[/]'

        bar [0]  += f' {self.upper:+.2f}°C'
        bar [-1] += f' {self.lower:+.2f}°C'

        current_index = len(bar) * (self.current - self.lower) // (self.upper - self.lower)
        target_index = len(bar) * (self.target - self.lower) // (self.upper - self.lower)

        current_index = min(max(int(current_index), 2), len(bar) -1)
        target_index = min(max(int(target_index), 2), len(bar) -1)

        if target_index == current_index:
            if self.target < self.current and target_index != 1:
                # put it one lower
                target_index = target_index - 1
            else:
                target_index = target_index + 1

        bar[-current_index] += f' Tccd={self.current:+.2f}°C'
        bar[-target_index] += f' Tset={self.target:+.2f}°C'

        #num = 3
        #bar = num * '  [red on red]**[/]\n'
        label = f'{self.current:.1f}°C'
        self.renderinprogress = False
        return '\n'.join(bar)# + label


class ImagePreview(Widget):
    """A widget to display an ascii art version of an image."""

    DEFAULT_CSS = """
    ImagePreview {
        content-align: center middle;
    }
    """

    img = None

    def set_image(self, img):
        right, bottom, left, top = CCD_MARGIN
        self.img = img[left:-right, bottom:-top].astype('<f4')
        self.refresh()


    def render(self) -> RenderResult:
        # if no image is set
        if self.img is None:
            return "Capture an image to view here..."

        # resize image
        #x_scale = self.size.height / self.img.shape[0]
        #y_scale = self.size.width / self.img.shape[1]

        # scale to widget size
        #zoomed = zoom(self.img, (x_scale, y_scale), order=5)
        zoomed = scale_down(self.img, self.size.width, self.size.height)

        # normalize and map
        cmap = cm.get_cmap('Greys', 127) # greys, plasma, or virisis look nice
        levels = [f'[white on {matplotlib.colors.rgb2hex(cmap(i))}] [/]' for i in range(cmap.N)]
        levels.reverse()

        # normalize
        max = np.max(zoomed)
        min = np.min(zoomed)
        if min == max:
            max = min + 1 # to avoid /0
        normalized = (zoomed - min) / (max - min) * (len(levels)-1)

        # map to indexed colors
        indices = normalized.astype(int)
        #characters = np.array([[levels[l] if l >= 0 else nanlevel for l in row] for row in indices])
        pixels = choose(indices, levels)#np.vectorize(levels.__getitem__)(indices)

        # Add newlines and flatten into one long string
        newlines = np.full((self.size.height, 1), '\n')
        pixels = np.concatenate((pixels, newlines), axis=1)
        ascii = ''.join(pixels.flatten())

        return ascii


class FocusPreview(Widget):
    """A widget to display an ascii art version of an image."""

    DEFAULT_CSS = """
    FocusPreview {
        content-align: center middle;
    }
    """

    img = None

    def set_image(self, img):
        right, bottom, left, top = CCD_MARGIN
        self.img = img[left:-right, bottom:-top].astype('<f4')
        self.refresh()


    def render(self) -> RenderResult:
        # if no image is set
        if self.img is None:
            return "Capture an image to view here..."

        # apply fwhm binning
        # to get less missing values we want larger bins and hence the grouping of 4x2 pixels into a bin
        map = fwhm_map(self.img, (self.size.width-5)//4, self.size.height//2)

        # scale up to widget size
        map = map.repeat(2, axis=0).repeat(4, axis=1)

        cmap = cm.get_cmap('plasma', 256) # plasma or virisis look nice
        levels = [f'[white on {matplotlib.colors.rgb2hex(cmap(i))}] [/]' for i in range(cmap.N)]
        nanlevel = ' '

        # normalize
        max = np.max(map)
        min = np.min(map)
        if min == max:
            max = min + 1
        normalized = (map - min) / (max - min) * (len(levels)-1)

        # map to pixels
        indices = normalized.astype(int)
        pixels = choose(indices, levels)

        # replace masked pixels with spaces
        pixels = pixels.filled(fill_value=' ')

        # Add newlines and flatten into one long string
        #newlines = np.full((pixels.shape[0], 1), '\n')
        #pixels = np.concatenate((pixels, newlines), axis=1)
        colorbar = make_legend(cmap, pixels.shape[0], min, max)
        colorbar = np.array([colorbar]).transpose()
        pixels = np.concatenate((pixels, colorbar), axis=1)
        ascii = ''.join(pixels.flatten())

        return ascii


class StarMapPreview(Widget):
    """Widget that displays previews of single stars in cols*rows regions of the image."""

    def __init__(self, cols=5, rows=5, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cols = cols
        self.rows = rows
        self.img = None
        self.stars = None

    def set_image(self, img):
        right, bottom, left, top = CCD_MARGIN
        self.img = img.astype('<f4')[left:-right, bottom:-top]
        self.stars = star_map(self.img, self.cols, self.rows)
        self.refresh()


    def render(self) -> RenderResult:
        # if no image is set
        if self.img is None:
            return "Capture an image to view here..."

        # if the window is too small
        if self.size.width < self.cols * 2 or self.size.height < self.rows * 2:
            return "Too small"

        if self.stars is None:
            return "Working..."

        # allocate some pixels
        # using a masked array is easier than adding nan values because
        # nan values mess with min/max etc functions.
        pixels = np.ma.masked_all((self.size.width, self.size.height))

        # compute a nice color scale
        cmap = cm.get_cmap('viridis', 50) # plasma or virisis look nice
        levels = [f'[white on {matplotlib.colors.rgb2hex(cmap(i))}]' for i in range(cmap.N)]
        maskedlevel = ' '

        # determine the bounds of the ascii art images
        xbounds = np.linspace(0, self.size.width, self.cols + 1).astype(int)
        ybounds = np.linspace(0, self.size.height, self.rows + 1).astype(int)

        # copy subimages into window-sized image
        for col in range(self.cols):
            for row in range(self.rows):
                # determine size of ascii art preview
                width = xbounds[col + 1] - xbounds[col]
                height = ybounds[row + 1] - ybounds[row]

                # skip if missing
                if self.stars[col][row] is None:
                    continue

                # get the star coords in original image
                xstar, ystar = self.stars[col][row]

                imgsize = self.img.shape

                # cut out region around star in the source image
                source = self.img[
                    ystar - height//2:ystar - height//2 + height,
                    xstar - width//2:xstar - width//2 + width,
                ]

                # normalize
                source = (source - np.min(source)) / (np.max(source) - np.min(source))

                # transpose
                source = source.transpose()

                # copy to pixels
                pixels[xbounds[col]:xbounds[col + 1], ybounds[row]:ybounds[row + 1]] = source

        # normalize to levels
        pixels = (pixels - np.min(pixels)) / (np.max(pixels) - np.min(pixels)) * (len(levels)-1)

        # convert pixels to ascii art
        indices = pixels.astype(int)
        #markup = np.vectorize(levels.__getitem__)(indices)
        markup = choose(indices, levels)

        # foreground is used to draw some edges
        foreground = np.array([[' ' for y in range(self.size.height)] for x in range(self.size.width)])
        #for i in range(1, self.cols-1):
        foreground[xbounds[1:-1],:] = '▏' # ▕
        foreground[:,ybounds[1:-1]] = '▔' # ▁

        xbounds = np.linspace(0, self.size.width, self.cols + 1).astype(int)
        ybounds = np.linspace(0, self.size.height, self.rows + 1).astype(int)


        res = ''
        for y in range(self.size.height):
            for x in range(self.size.width):
                if markup[x, y] is np.ma.masked:
                    res = res + foreground[x, y]
                else:
                    res = res + markup[x, y] + foreground[x, y] + '[/]'
            res = res + '\n'

        return res



class RichProgress(Static):
    DEFAULT_CSS = """
    RichProgress {
        content-align: center middle;
    }
    """
    def __init__(self, **kwargs):
        super().__init__("", **kwargs)
        self.progress = Progress()
        self.taskid = self.progress.add_task("DL:", total=None, auto_refresh=False)

    def redraw(self):
        self.progress.refresh()
        self.update(self.progress)

    def on_mount(self) -> None:
        self.set_interval(1/10, self.redraw)

    def set_progress(self, *args, **kwargs):
        self.progress.update(task_id=self.taskid, *args, **kwargs)


class DreamMaintenance(App[str]):
    TITLE = "Dream Maintenance Interface"
    CSS = """
    Screen {
    }
    .panel {
        border: solid green;
    }
    #top-pane {
        width: 100%;
        height: 3fr;
    }
    .left {
        width: 21;
        height: 1fr;
    }
    #preview, #progress, #raw, #focus, #starmap {
        width: 1fr;
        height: 1fr;
        display: none;
    }
    #preview, #raw {
        display: block;
    }
    #console {
        width: 100%;
        height: 1fr;
    }
    #buttonbar {
        height: 3;
    }
    Button {
        border-bottom: tall blue;
    }
    #grid {
        layout: grid;
        grid-size: 5 5;
        grid-rows: 1fr;
        grid-columns: 1fr;
        grid-gutter: 1;
        width: 1fr;
    }

    #grid Static {
        background: green;
        height: 100%;
    }
    """

    BINDINGS = [
        ("i", "temp_up", "Increase Temp"),
        ("k", "temp_down", "Decrease Temp"),
        ("e", "expose_frame", "Expose frame"),
        ("c", "cycle_mode", "Cycle preview mode"),
        ("s", "save_image", "Save"),
        ("Ctrl+c", "quit", "Quit"),
        Binding("ctrl+t", "toggle_dark", "Toggle Light/Dark", False)
    ]

    img = None



    def compose(self) -> ComposeResult:
        yield Header("DREAM maintenance interface")
        yield Vertical(
            Horizontal(
                Vertical(
                    Vertical(
                        TempGauge(),
                        classes="panel"
                    ),
                    Vertical(
                        Label("CCD Temp setpoint:"),
                        Input(id="setpoint"),
                        Label("Exposure time: "),
                        Input(value="6.4", id="exptime"),
                        Label("Exposure started:"),
                        Static(id="expstarted"),
                        Label("FLI serial:"),
                        Static(id="camserial"),
                        Label("Station:"),
                        Static(id="station"),
                        Label("Extra header:"),
                        Input(id="extra"),
                        classes="panel", id="stats"
                    ),
                    classes="left",
                ),
                Horizontal(
                    ImagePreview(id='raw'),
                    FocusPreview(id='focus'),
                    StarMapPreview(id='starmap'),
                    id='preview', classes="panel"
                ),
                RichProgress(id='progress', classes="panel"),
                id="top-pane"
            ),
            RichLog(highlight=True, markup=True, id="console", classes="panel"),
        )
        yield Footer()


    def on_mount(self):
        text_log = self.query_one(RichLog)
        temp = pyfli.getTemperature(cam)
        new_temp = round(temp)
        text_log.write(f"{ts()}: Initial CCD temp: {temp:.5f} °C")
        text_log.write(f"{ts()}: Assuming setpoint was: {new_temp} °C")
        self.query_one(TempGauge).target = new_temp
        editfield = self.query_one('#setpoint')
        editfield.value = f'{new_temp:.1f}'
        pyfli.setTemperature(cam, new_temp)
        self.set_interval(1/REFRESH_RATE, self.read_ccd_temp)


    def on_input_submitted(self, event):
        text_log = self.query_one(RichLog)
        if event.control.id == "setpoint":
            try:
                temp = float(event.control.value)
                text_log.write(f'Setting CCD temp to {temp}°C')
                gauge = self.query_one(TempGauge)
                gauge.target = temp
                pyfli.setTemperature(cam, gauge.target)
            except Exception as e:
                text_log.write(e)



    def read_ccd_temp(self):
        temp = pyfli.getTemperature(cam)
        gauge = self.query_one(TempGauge)
        text_log = self.query_one(RichLog)
        gauge.current = temp

    def action_cycle_mode(self) -> None:

        (
            self.query_one('#raw').styles.display,
            self.query_one('#focus').styles.display,
            self.query_one('#starmap').styles.display
        ) = (
            self.query_one('#starmap').styles.display,
            self.query_one('#raw').styles.display,
            self.query_one('#focus').styles.display
        )



    def action_temp_up(self) -> None:
        text_log = self.query_one(RichLog)
        gauge = self.query_one(TempGauge)
        gauge.target += 1.0
        editfield = self.query_one('#setpoint')
        editfield.value = f'{gauge.target:.1f}'
        pyfli.setTemperature(cam, gauge.target)

    def action_temp_down(self) -> None:
        text_log = self.query_one(RichLog)
        gauge = self.query_one(TempGauge)
        gauge.target -= 1.0
        editfield = self.query_one('#setpoint')
        editfield.value = f'{gauge.target:.1f}'
        pyfli.setTemperature(cam, gauge.target)

    async def action_expose_frame(self) -> None:
        try:
            self.img = None
            text_log = self.query_one(RichLog)
            text_log.write(f"{ts()}: starting exosure")
            exptime = 1000 * float(self.query_one('#exptime').value)
            text_log.write(f"{ts()}: Setting exposure time to {exptime}")

            utctime = datetime.datetime.utcnow()
            hw = pyfli.getHWRevision(cam)
            fw = pyfli.getFWRevision(cam)
            lib = pyfli.getLibVersion()
            serial = pyfli.getSerialString(cam)
            model = pyfli.getModel(cam)
            ccdtemp = pyfli.getTemperature(cam)
            host = socket.getfqdn()

            self.query_one('#expstarted').update(utctime.strftime('%Y-%m-%dT%H:%M:%S'))
            self.query_one('#camserial').update(serial)
            self.query_one('#station').update(host)


            self.hdr = fits.Header()
            self.hdr['EXPTIME'] = (f'{exptime / 1000:.3f}', "Exposuretime in seconds")
            self.hdr['HOSTNAME'] = (host, "Observation site")
            self.hdr['DATE-OBS'] = (utctime.strftime('%Y-%m-%dT%H:%M:%S'), 'YYYY-MM-DDThh:mm:ss observation start, UT')
            self.hdr['UTCTIME'] = (utctime.timestamp(), 'UTC time')
            self.hdr['CCDTEMP'] = (ccdtemp, 'CCD temperature at start of exposure in C')
            self.hdr['APTDIA'] = (24e-3/1.4, 'Aperture diameter of telescope in meter')
            self.hdr['APTAREA'] = ((24e-3/1.4)**2, 'Aperture area of telescope in meter^2')
            self.hdr['FOCALLEN'] = (24e-3, 'Focal Length in meter')
            self.hdr['INSTRUME'] = (model, 'Camera Identifier')
            self.hdr['MANUFACT'] = ('FLI', 'Camera Manufacturer')
            self.hdr['CAMSER'] = (serial, 'Camera Serial Number')
            self.hdr['APIVER'] = (lib, 'Camera API Version')
            self.hdr['CAMVER'] = (fw, 'Camera FW Version')
            self.hdr['IMAGETYP'] = ('maintenance', 'Type of image')
            self.hdr['X0'] = (36, 'start coordinate of active region')
            self.hdr['XSIZE'] = (4008 , 'width of active region')
            self.hdr['Y0'] = (25 , 'start coordinate of active region')
            self.hdr['YSIZE'] = (2672, 'height of active region')
            self.hdr['EXTRA'] = (self.query_one('#extra').value, 'Extra data entered in gui')

            pyfli.setExposureTime(cam, exptime)
            pyfli.exposeFrame(cam)

            preview = self.query_one('#preview')
            progress = self.query_one(RichProgress)
            preview.display = False
            progress.display = True

            while True:
                remaining = pyfli.getExposureStatus(cam)
                progress.set_progress(description="Exposing...", total=exptime, completed=exptime-remaining)
                await asyncio.sleep(0.1)
                if remaining == 0:
                    break
            text_log.write(f'{ts()}: Exposing done')

            img = None
            def download():
                start = time.time()
                self.img = pyfli.grabFrame(cam)
                hdr['DWLDTIME'] = (time.time()-start, 'Time to get the image from buffer')

            t = Thread(target=download)
            t.start()

            starttime = time.time()
            downloadtime = 1.4 # experimentally determined
            while t.is_alive():
                progress.set_progress(description="Downloading...", total=downloadtime, completed=time.time()-starttime)
                await asyncio.sleep(0.1)

            text_log.write(f"{ts()}: Image downloaded")
            progress.display = False
            preview.display = True
            self.refresh()
            text_log.write('Image statistics: ')
            text_log.write({"min": np.min(self.img),
                            "max": np.max(self.img),
                            "mean": np.mean(self.img),
                            "median": np.median(self.img)})

            if False: # toggle to enable debugging
                scriptdir = os.path.dirname(os.path.abspath(__file__))
                mockfile = os.path.join(scriptdir, '34646120LSC.fits')
                self.img = fits.getdata(mockfile)

            # update the three displays
            self.query_one('#raw').set_image(self.img)
            self.query_one('#starmap').set_image(self.img)
            self.query_one('#focus').set_image(self.img)

        except Exception as e:
            text_log = self.query_one(RichLog)
            tb = "".join(traceback.format_exception(etype=None, value=e, tb=e.__traceback__))
            text_log.write(tb)


    def action_save_image(self) -> None:
        text_log = self.query_one(RichLog)
        if self.img is None:
            text_log.write(f"{ts()}: Nothing to save")
            return
        filename = f"maintenance_mode_image_{int(time.time()*1000):d}.fits"
        fits.writeto(filename, self.img, self.hdr)
        text_log.write(f"{ts()}: Image saved as '{filename}'")
        self.img = None


    def action_toggle_dark(self) -> None:
        """An action to toggle dark mode."""
        text_log = self.query_one(RichLog)
        text_log.write(f"Toggling dark mode {'OFF' if self.dark else 'ON'}")
        self.dark = not self.dark

if __name__ == "__main__":
    app = DreamMaintenance()
    app.run()
