#!/usr/bin/env python3

import sys
sys.path.append('.')
from dream_camera import FLICameraSimulator, FLICamera
import dream_exposure_timing
import asyncio
import logging
import dream_logging

log = logging.getLogger(__name__)
dream_logging.setup_logging(log)
log.setLevel(logging.DEBUG)


async def main():
    cam = FLICamera()
    task = asyncio.create_task(dream_exposure_timing.science_mode(cam))
    log.info('task started')
    await asyncio.sleep(60)
    log.info('cancelling...')
    task.cancel()
    log.info('waiting for task to finish')
    await task
    log.info('cancel done')





if __name__ == '__main__':
    asyncio.run(main())
