#!/usr/bin/env python3

import asyncio
from rich import print
import time

timestarttime = time.time()
loopstarttime = 0

class Simulator():
    def __init__(self):
        self._time = time.time()
        self._awaiters = 0

    def time(self):
        return self._time

    async def sleep(self, delay):
        now = time.time()
        if self._time + delay > now:
            # stop the loop to give back control
            # or yield to other tasks
            pass
        else:
            # yield, we are not done yet
            self._awaiters += 1
            await asyncio.sleep(0)



def ts(loop = None):
    if not loop:
        loop = asyncio.get_running_loop()
    return f'{time.time() - timestarttime:.3f}/{loop.time() - loopstarttime:.3f}/{simtime}'

async def sometask(name: str = 'unnamed', num: int = 10, delay: float = 1.0):
    for i in range(num):
        print(f'{ts()}: {name}: {i}')
        await asyncio.sleep(delay)


async def simtimetask():
    global simtime
    loop = asyncio.get_running_loop()
    while True:
        await asyncio.sleep(0.1)
        simtime += 0.1

async def asyncmain():
    await asyncio.gather(
        sometask('task 1', 10, 1.0),
        sometask('task 2', 10, 0.5),
        #simtimetask()
    )


#async def pause(delay: float = 0.0):
#    loop = asyncio.get_running_loop()
#    startpause = simtime
#    while simtime < #

#    await asyncio.sleep(0)
#    loop.stop()

def main():
    global loopstarttime
    loop = asyncio.new_event_loop()
    loop.create_task(asyncmain())
    print('main: loop initialized')
    loopstarttime = loop.time()

    while True:
        before = time.time()
        for i in range(100):
            loop.stop()
            loop.run_forever()
        after = time.time()

        print(f'{ts(loop)}: back to main in {after - before:.3f} (s)')
        #print(f'running tasks: {len(asyncio.all_tasks(loop))}')
        #for task in asyncio.all_tasks(loop):
        #    print(f'    {task._coro}')
        time.sleep(1.5)





if __name__ == '__main__':
    main()
