#!/usr/bin/env python3

import asyncio
import zmq
import zmq.asyncio
import time
from rich import print

start = time.time()
def ts():
    dt = time.time() - start
    return f'{dt:.3f}: '

ctx = zmq.asyncio.Context()

async def client_handler(name: str, socket: zmq.asyncio.Socket):
    for i in range(10):
        await socket.send_multipart([name.encode(), b'', b'foo', str(i).encode()])
        print(f'{ts()}req sent to {name}')

        reply = await socket.recv_multipart()
        print(f'{ts()}rep received from {name}:')
        print(reply)
        await asyncio.sleep(1)


async def dealer():
    socket = ctx.socket(zmq.DEALER)
    socket.bind('tcp://*:9000')

    for client in ['client1', 'client 2', 'client 3']:
        asyncio.create_task(client_handler(client, socket))
    print('dealer creation done')


async def rep(name: str):
    socket = ctx.socket(zmq.REP)
    socket.connect('tcp://localhost:9000')

    while True:
        req, i = await socket.recv_multipart()
        print(f'{ts()}req received by {name}: {req} {i}')
        await socket.send_multipart([b'bar', i])
        print(f'{ts()}reply sent by {name}')




async def main():
    central = dealer()
    client1 = rep('client 1')
    client2 = rep('client 2')

    await asyncio.gather(central, client1, client2)

if __name__ == '__main__':
    asyncio.run(main())
