#!/usr/bin/env python3

#!/usr/bin/env python3
import asyncio
from textual.app import App, ComposeResult
from textual.widgets import Header, Footer, Input, Static, Button, RichLog
from textual.containers import Vertical
import logging
import sys; sys.path.append('..')
from dream_logging import setup_logging
from dream_logging import RichLogHandler
import dream_config as cfg

# we take the work from the async test as an example it creates its own logger
# object, we use the same name to test that we can receive messages both ways.
from textuaL_async_test import work

log = logging.getLogger(cfg.LOGGER_NAME)

class NormalExit(Exception):
    pass


async def gui():
    try:
        app = TestApp()
        log.info('Starting GUI...')
        log.warning('Further dream logs are deferred to the GUI (and log file)')
        log.warning('GUI logs are only shown here and not logged to file')
        await app.run_async()
    except Exception as e:
        log.error('GUI error:')
        log.exception(e)
        raise
    finally:
        log.warning('GUI closed. Dream logging to console resumed')
        log.info('See log file for full log')
        raise NormalExit


class TestApp(App):
    TITLE = "Test App"
    BINDINGS = [
        ('x', 'quit', 'Exit')
    ]

    def compose(self) -> ComposeResult:
        yield Header(name=self.TITLE)
        yield Vertical(
            Input(),
            Button('Test'),
            Static('TestApp'),
            RichLog()
        )
        yield Footer()

    async def action_quit(self):
        log.error('quit')
        await super().action_quit()

    def on_button_pressed(self):
        raise RuntimeError('FOO')


    def on_mount(self):
        richlog = self.query_one(RichLog)
        log.addHandler(RichLogHandler(richlog, self))


async def main():
    try:
        log.info('starting a task group')
        async with asyncio.TaskGroup() as tg:
            tg.create_task(work(1))
            tg.create_task(work(2))
            tg.create_task(gui())
        log.info('taskgroup closed')
    except ExceptionGroup as eg:
        numerrors = sum([not isinstance(e, NormalExit) for e in eg.exceptions])
        if numerrors > 0:
            log.error(f'ExceptionGroup caught with {len(eg.exceptions)} exceptions:')
        for e in eg.exceptions:
            if isinstance(e, NormalExit):
                log.info('GUI exited as expected')
            else:
                log.error(e.__repr__())


if __name__ == '__main__':
    setup_logging(log)
    asyncio.run(main())
