#!/usr/bin/env python3

import zmq
import zmq.asyncio
import asyncio
import sys; sys.path.append('..')
import dream_logging
import dream_config as cfg
from dream_utils import CameraServerMode, json_default
import json
import logging
import time

log = logging.getLogger(cfg.LOGGER_NAME)


async def change_mode(mode: CameraServerMode, socket: zmq.asyncio.Socket):
    request = {
        'type': 'script',
        'name': 'alternating blanks and bias script',
        'mode': mode
    }
    log.debug(request)

    await socket.send_multipart([b'script', json.dumps(request, default=json_default).encode()])


    reply = await socket.recv_json()
    log.debug(reply)

    if 'status' not in reply or reply['status'] != 'ok':
        raise RuntimeError('Unexpected reply from server. Aborting')




async def alternating_blanks_and_biases():
    # connect to central command:
    context = zmq.asyncio.Context()
    socket = context.socket(zmq.REQ)
    socket.connect(f'tcp://{cfg.IP_DREAM_CONTROL}:{cfg.CAMERA_CONTROL_PORT}')

    try:
        await asyncio.sleep(1)

        #await change_mode(CameraServerState.DARKS, socket)
        #await asyncio.sleep(300)

        for _ in range(1000):
            await change_mode(CameraServerMode.DARKS, socket)
            #await asyncio.sleep(5)
            time.sleep(5)
            await change_mode(CameraServerMode.BIAS, socket)
            await asyncio.sleep(5)


    except (asyncio.CancelledError, KeyboardInterrupt):
        print('\r   ')
        log.info('Ctrl-C: gracefully shutting down..')
        await change_mode(CameraServerMode.IDLE, socket)
    else:
        print('Finishing experiment')
        await change_mode(CameraServerMode.IDLE, socket)

if __name__ == '__main__':
    dream_logging.setup_logging(log)
    try:
        asyncio.run(alternating_blanks_and_biases())
    except KeyboardInterrupt:
        log.info('Bye')
