import artemis_fli as art
import socket
import mascara_sites

sock = socket.create_connection(("192.168.0.100", 8904), timeout=10)
cam=art.Camera(0,sock,mascara_sites.Site('Leiden'))
cam.start_exposure_sequence()
cam.beginning_of_night()