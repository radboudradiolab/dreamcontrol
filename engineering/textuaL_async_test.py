#!/usr/bin/env python3
import asyncio
from textual.app import App, ComposeResult
from textual.widgets import Header, Footer, Input, Static, Button
from textual.containers import Vertical

import logging
import sys; sys.path.append('..')
import dream_logging
import dream_config as cfg

log = logging.getLogger(cfg.LOGGER_NAME)
#setup_logging(log)


'''Architecture note:

This test is to figure out how to combine a bunch of asyncio workers in a
taskgroup with a textual gui. The first attempt was to start the task group from
within the on_mount textual event. This results in unpredictable behaviour
because textual sometimes sends multiple cancellations, possibly cancelling the
cleanup.

A better attempt is demonstrated below. The gui is started as a separate task
inside the taskgroup. The only caveat is that if the cancellation happens inside
one of the gui related tasks, textual exits with a CancelledError, which is not
enough to stop the taskgroup, leaving the other tasks running. We stop the dream
tasks by raising a custom exception after the gui returns. For cleanliness we
catch this later and don't let it bubble to the logs or console.'''

class NormalExit(Exception):
    pass

async def work(i):
    try:
        while True:
        #for _ in range(5):
            log.info(f'work {i}')
            await asyncio.sleep(1)
        # this is for a test:
        raise RuntimeError('FOO')
    except asyncio.CancelledError:
        log.info(f'work {i} cancelled. Cleaning up...')
        try:
            await asyncio.sleep(1)
            log.info(f'work {i} cleaned up')
            #raise
        except asyncio.CancelledError:
            log.error(f'Cleanup {i} cancelled :(')
            raise

async def gui():
    try:
        app = TestApp()
        await app.run_async()
    except asyncio.CancelledError:
        log.error('Gui was cancelled')
        raise
    except Exception as e:
        log.error('GUI error:')
        log.exception(e)
        raise
    finally:
        log.info('Gui terminated')
        raise NormalExit


class TestApp(App):
    TITLE = "Test App"
    BINDINGS = [
        ('x', 'quit', 'Exit'),
        ('r', 'trouble', 'Make trouble')
    ]

    def compose(self) -> ComposeResult:
        yield Header(name=self.TITLE)
        yield Vertical(
            Input(),
            Button('Test'),
            Static('TestApp')
        )
        yield Footer()


    async def action_quit(self):
        log.error('quit')
        await super().action_quit()


    def action_trouble(self):
        raise RuntimeError('BAR')



def leaf_generator(exc):
    if isinstance(exc, BaseExceptionGroup):
        for e in exc.exceptions:
            yield from leaf_generator(e)
    else:
        yield exc



async def main_without_pep654():
    try:
        log.info('starting a task group')
        async with asyncio.TaskGroup() as tg:
            tg.create_task(work(1))
            tg.create_task(work(2))
            tg.create_task(gui())
        log.info('taskgroup closed')
    except ExceptionGroup as eg:
        log.error(f'ExceptionGroup caught with {len(eg.exceptions)} exceptions:')
        for e in eg.exceptions:
            log.error(e.__repr__())

    except asyncio.CancelledError:
        # TODO: why is this never reached? Neither an exception in a task, not
        # an active quit, nor ctrl-c actually procuces this message?
        log.info('taskgroup cancelled')
        raise
    except NormalExit:
        # this is normal. no backtrace needed
        log.info('Normal exit caught')
        pass
    except Exception as e:
        log.error(f'Another error occurred: {e.__repr__()}')

async def main():
    try:
        log.info('starting a task group')
        async with asyncio.TaskGroup() as tg:
            tg.create_task(work(1))
            tg.create_task(work(2))
            tg.create_task(gui())
        log.info('taskgroup closed')
    except* asyncio.CancelledError:
        # TODO: why is this never reached? Neither an exception in a task, not
        # an active quit, nor ctrl-c actually procuces this message?
        log.info('taskgroup cancelled')
        raise
    except* NormalExit:
        # this is normal. no backtrace needed
        pass
    except* Exception as eg:
        log.info('Exceptions bubbled to main:')
        for e in leaf_generator(eg):
            if isinstance(e, NormalExit):
                log.info('GUI exited normally')
            else:
                log.exception(e)
                # This should be enough but rich logging does not understand
                # exceptiongroups (yet, as of 2023-08-11). The above will print the
                # traceback of the ExceptionGroup itself. So as a hint we print the
                # following message.
                location = e.__traceback__.tb_frame.f_code.co_filename
                linenumber = e.__traceback__.tb_lineno
                log.warning(f'Original exception: {e.__repr__()} in {location} line {linenumber}. logfile contains a proper traceback.')
                # log.warning('See log file for full trace.')

if __name__ == '__main__':
    dream_logging.setup_logging(log)
    asyncio.run(main_without_pep654())
