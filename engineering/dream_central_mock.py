#!/usr/bin/env python3

# import sys
# import os
import time
# import datetime
import asyncio
import zmq
import zmq.asyncio
# from zmq.utils.monitor import parse_monitor_message, recv_monitor_message
import json
import logging
import sys; sys.path.append('..')
import dream_logging
from dream_utils import CameraServerMode, format_zmq_id, format_float_or_none, Tracked
import dream_config as cfg
from dream_command_main import dream_command
from dream_electronics import Hardware
from dream_state import DreamState, DomeTargetState
from rich.console import Group
from rich.logging import RichHandler
from rich.traceback import Traceback
from rich.table import Table
from rich import box
import textual
import datetime
from textual.app import App, ComposeResult
from textual.binding import Binding
from textual.widgets import (
    Header, Footer, Static, Label, RichLog, RadioSet, RadioButton, Switch)
from textual.containers import Horizontal, Vertical


log = logging.getLogger(cfg.LOGGER_NAME)

# TODO: remove the side log used in debugging the logging window
sidelog = logging.getLogger('sidelog')
handler = logging.FileHandler('side.log')

sidelog.setLevel(logging.DEBUG)
handler.setLevel(logging.DEBUG)
handler.setFormatter(logging.Formatter("%(levelname)s %(asctime)s [%(filename)s:%(funcName)s:%(lineno)d] %(message)s"))
sidelog.addHandler(handler)

ctx = zmq.asyncio.Context()
log.info('ZeroMQ context created')

class CustomHandler(RichHandler):
    def __init__(self, text_log, **kwargs):
        super().__init__(rich_tracebacks=True, **kwargs)
        self.text_log = text_log

    def emit(self, record):
        sidelog.info('Record received')
        sidelog.debug(record)
        try:
            if record.exc_info and record.exc_info != (None, None, None):
                exc_type, exc_value, exc_traceback = record.exc_info
                traceback = Traceback.from_exception(
                    exc_type,
                    exc_value,
                    exc_traceback,
                    width=self.text_log.virtual_size.width,
                    show_locals=True,
                )
                sidelog.info('record had an exception attached:')
                sidelog.info(traceback)
            else:
                traceback = None
                sidelog.info('record without tracelog')
            message = record.getMessage()
            renderable = self.render_message(record, message)
            rendered = self.render(record=record, traceback=traceback, message_renderable=renderable)
            self.text_log.write(rendered)
        except Exception as e:
            sidelog.exception(e)


class CameraWidget(Static):
    heartbeat = 0.0
    _stale = False
    heartbeat_info = {}
    image_info = {}

    @property
    def stale(self):
        return self._stale

    @stale.setter
    def stale(self, val: bool):
        self._stale = val
        log.debug('widget stale ' + str(val))
        if val:
            self.add_class('stale')
        else:
            self.remove_class('stale')

    def update_heartbeat(self, data):
        self.heartbeat_info = data
        self.update()

    def update_image(self, data):
        log.debug('Setting image info in camera widget')
        self.image_info = data
        self.update()

    def update(self):
        table1 = Table(expand=True, box=box.MINIMAL)
        table1.add_column('Camera: ' + self.heartbeat_info['name'])
        table1.add_column('connection id')
        table1.add_column('blanks')
        table1.add_column('biases')
        table1.add_column('flats')
        table1.add_column('darks')
        table1.add_column('science')
        table1.add_column('missed')
        table1.add_column(self.heartbeat_info['time'].strftime('%X'))
        table1.add_row(
            str(self.heartbeat_info.get('state', '-')),
            self.heartbeat_info.get('client_id', '-'),
            str(self.heartbeat_info.get('num_blanks', '-')),
            str(self.heartbeat_info.get('num_bias', '-')),
            str(self.heartbeat_info.get('num_flats', '-')),
            str(self.heartbeat_info.get('num_darks', '-')),
            str(self.heartbeat_info.get('num_science', '-')),
            str(self.heartbeat_info.get('num_missed', '-')),
            format_float_or_none(self.heartbeat_info.get('ccd_temp'), '{:.1f} °C', 'offline')
        )

        table2 = Table(expand=True, box=box.MINIMAL)
        table2.add_column(f'Last image: {self.image_info.get("index", 0)}')
        table2.add_column('latency')
        table2.add_column('dl time')
        table2.add_column('dl margin')
        table2.add_column('mean')
        table2.add_row(
            self.image_info.get('triggertime').strftime('%X') if self.image_info else '-',
            f'{self.image_info.get("timing_latency", 0) * 1000:.3f} ms',
            f'{self.image_info.get("download_time", 0) * 1000:.3f} ms',
            f'{self.image_info.get("download_margin", 0):.3f} s',
            f'{self.image_info.get("mean", 0):.1f}',
        )
        super().update(Group(table1, table2))


class DreamCentralMock(App):
    TITLE = "Mock Dream Central Command"
    CSS_PATH = "dream_central_mock.css"
    BINDINGS = [
        ('s', 'cycle_state', 'Cycle state'),
        ('l', 'cycle_leds', 'Toggle LED\'s'),
        Binding('d', 'toggle_dark', 'Toggle dark mode', False),
        ('Ctrl+c', 'quit', 'Quit')
    ]

    clients = {}
    state = CameraServerMode('IDLE')
    dome = 'CLOSED'
    leds = Tracked('alloff')
    flash_task = None

    def compose(self) -> ComposeResult:
        yield Header(name=self.TITLE)
        yield Horizontal(
            Vertical(
                Static('Observatory state:'),
                RadioSet(
                    RadioButton('Idle', value=True, id='IDLE', classes='statebutton'),
                    RadioButton('Standby', id='STANDBY', classes='statebutton'),
                    RadioButton('Blanks', id='BLANKS', classes='statusbutton'),
                    RadioButton('Bias', id='BIAS', classes='statebutton'),
                    RadioButton('Flats', id='FLATS', classes='statebutton'),
                    RadioButton('Darks', id='DARKS', classes='statebutton'),
                    RadioButton('Science images', id='SCIENCE', classes='statebutton'),
                    id='statelist',
                    name='Observation state',
                    classes='group'
                ),
                Static('Request dome:'),
                RadioSet(
                    RadioButton('Closed', value=True, id='CLOSED'),
                    RadioButton('Open', id='OPEN'),
                    RadioButton('Force open', id='FORCEOPEN'),
                    RadioButton('Stop', id='STOP'),
                    id='request_state',
                    classes='group'
                ),
                Static('Dome state:'),
                RadioSet(
                    RadioButton('Closed', id='CLOSED'),
                    RadioButton('Opening', id='OPENING'),
                    RadioButton('Open', id='OPEN'),
                    RadioButton('Closing', id='CLOSING'),
                    RadioButton('Stopped', id='STOPPED'),
                    RadioButton('Error', id='ERROR'),
                    disabled=True,
                    id='actual_state',
                    classes='group'
                ),
                #Vertical(
                #    Horizontal(
                #        Switch(id='dome-target'),
                #        Static('Open'),
                #        classes='switchgroup'
                #    ),
                #    Horizontal(
                #        Switch(id='dome-enable'),
                #        Static('Enable'),
                #        classes='switchgroup'
                #    ),
                #    classes='group'
                #),
                Static('LED state:'),
                RadioSet(
                    RadioButton('All off', id='alloff', value=True),
                    RadioButton('All on', id='allon'),
                    RadioButton('Flats', id='flats'),
                    # RadioButton('Blink', id='blink'),
                    id='leds',
                    name='LED status',
                    classes='group'
                ),
                Static('Hardware:'),
                Vertical(
                    Static('Temp/hum:'),
                    Static('Switches:'),
                    Static('Encoder:'),
                    Static('Cooling:'),
                    Static('LED\'s:'),
                    Static('UPS:'),
                    Static('PSU:'),
                    Static('Dome:'),
                    classes='group'
                ),
                id='leftcolumn'
            ),
            Vertical(
                Label('Camera List:'),
                id='cameralist'
            )
        )
        yield RichLog(id='log')
        yield Footer()

    async def heartbeat(self):
        while True:
            try:
                for name in list(self.clients.keys()):
                    camera = self.clients[name]
                    if camera.heartbeat < time.time() - 5:
                        log.info(f'client {name} timed out. Stale for more than 15s. Removing.')
                        camera.remove()
                        del self.clients[name]
                    elif camera.heartbeat < time.time() - 3 and not camera.stale:
                        log.info(f'client {name} timed out. Considering stale from now on')
                        camera.stale = True
            except Exception as e:
                log.exception(e)
                textual.log(e)
                raise e
            await asyncio.sleep(1.0)

    async def handle_heartbeat(self, data):
        # first check if this is a new client
        name = data['name']
        if name not in self.clients:
            log.info(f'new client for camera {name} (client id: {data["client_id"]})')
            self.clients[name] = CameraWidget()
            cameralist = self.query_one('#cameralist')
            cameralist.mount(self.clients[name])

        # TODO: display a warning if the client id of a camera changes

        # quick shortcut to the current camera widget
        camera: CameraWidget = self.clients[name]

        # decode json message
        data['state'] = CameraServerMode(data['state'])
        data['time'] = datetime.datetime.fromtimestamp(data['time'])

        # refresh the heartbeat period
        camera.heartbeat = time.time()
        if camera.stale:
            log.info(f'client for {name} no longer stale')
            camera.stale = False

        # update the display
        camera.update_heartbeat(data)

        # send a reply
        return {
            'status': 'ok',
            'current_mode': self.state
        }

    async def handle_image(self, data):
        log.info(f'Image #{data["index"]} received from {data["name"]} with expected type {data["imagetype"]}')

        # decode timestamp as real datetime instead of number
        data['triggertime'] = datetime.datetime.fromtimestamp(data['triggertime'])

        # update the UI
        for name, camera in self.clients.items():
            if name == data['name']:
                camera.update_image(data)

        # tell the camera what to do with the result
        return {
            # TODO: return something more meaningful
            'status': 'ok',
            'type': data['imagetype'] # TODO: decide this based on leds and exp time
        }

    async def handle_script(self, data):
        if 'mode' in data:
            mode = data['mode']
            if mode not in list(CameraServerMode):
                raise ValueError(f'Invalid mode requested: {mode}')

            button: RadioButton = self.query_one('#' + mode, RadioButton)
            button.toggle()
            self.state = mode
            log.info(f'Changing mode to {mode} (requested by scripted input)')

        return {
            'status': 'ok'
        }

    async def server(self):
        socket = ctx.socket(zmq.ROUTER)
        socket.bind(f'tcp://*:{cfg.CAMERA_CONTROL_PORT}')
        log.info('Listening...')

        # create a second task that monitors and removes stale clients
        heartbeat_task = asyncio.create_task(self.heartbeat())

        try:
            while True:
                id, _, rawdata = await socket.recv_multipart()

                try:
                    data = json.loads(rawdata)
                    data['client_id'] = format_zmq_id(id)
                    if 'type' not in data:
                        raise RuntimeError('message without type field received')
                    if data['type'] == 'heartbeat':
                        reply = await self.handle_heartbeat(data)
                    elif data['type'] == 'image':
                        reply = await self.handle_image(data)
                    elif data['type'] == 'script':
                        reply = await self.handle_script(data)
                    else:
                        raise RuntimeError(f'messgae of unknown type {data["type"]}')
                except Exception as e:
                    log.exception(e)
                    reply = {'status': 'error', 'error': e.__repr__()}
                finally:
                    await socket.send_multipart([id, b'', json.dumps(reply).encode()])


        except asyncio.CancelledError:
            # this is just nice cleanup to prevent errors upon exit
            log.info('Closing ZeroMQ socket')
            socket.close()
            # not necessary but for cleanliness
            heartbeat_task.cancel()
        except Exception as e:
            # log.error('Failure in server thread:')
            log.exception(e)
            raise e

    async def dream_state_poller(self):
        while True:
            state = DreamState()
            log.info('Current Dream dome state: '+ state.dome_state)
            button: RadioButton = self.query_one('#actual_state #' + state.dome_state, RadioButton)
            button.toggle()
            await asyncio.sleep(0.2)

    def on_mount(self):
        # install a log handler to display logs inside the text area
        text_log = self.query_one(RichLog)
        handler = CustomHandler(text_log, show_path=False)
        log.addHandler(handler)


        # perpetually run a server. Keep a reference to avoid gc
        self.servertask = asyncio.create_task(self.server())
        self.dream_control_task = asyncio.create_task(dream_command(maintenance=True))
        self.dream_state_poller_task = asyncio.create_task(self.dream_state_poller())

    # these handlers just catch the keypresses
    # they activate the radio which triggers the real handler
    def action_cycle_state(self) -> None:
        if self.state == 'IDLE':
            self.state = 'STANDBY'
        elif self.state == 'STANDBY':
            self.state = 'BLANKS'
        elif self.state == 'BLANKS':
            self.state = 'BIAS'
        elif self.state == 'BIAS':
            self.state = 'FLATS'
        elif self.state == 'FLATS':
            self.state = 'DARKS'
        elif self.state == 'DARKS':
            self.state = 'SCIENCE'
        else:
            self.state = 'IDLE'
        button: RadioButton = self.query_one('#' + self.state, RadioButton)
        button.toggle()


    def action_cycle_leds(self) -> None:
        if self.leds.get() == 'alloff':
            self.leds.set('allon')
        elif self.leds.get() == 'allon':
            self.leds.set('flats')
        else:
            self.leds.set('alloff')
        button: RadioButton = self.query_one('#' + self.leds.get(), RadioButton)
        button.toggle()

    def on_radio_set_changed(self, event: RadioSet.Changed) -> None:
        if event.radio_set.id == 'statelist':
            self.state = CameraServerMode(event.pressed.id)
        if event.radio_set.id == 'request_state':
            self.dome = event.pressed.id
            # also set the dream state
            state = DreamState()
            state.dome_target_state = DomeTargetState(self.dome)
        if event.radio_set.id == 'leds':
            self.leds.set(event.pressed.id)


        if self.leds.get() != 'flats':
            if self.flash_task is not None:
                self.flash_task.cancel()
            hw = Hardware()
            if self.leds.get() == 'allon':
                log.debug('LED\'s on')
                hw.leds_on()
            else:
                log.debug('LED\'s off')
                hw.leds_off()
        else:
            #self.flash_task = asyncio.create_task(led_flashing_for_flats())
            self.flash_task = asyncio.create_task(thread_led_flashing_for_flats())

    def action_toggle_dark(self):
        self.dark = not self.dark



if __name__ == '__main__':
    dream_logging.setup_logging(log)
    app = DreamCentralMock()
    app.run()  # also starts asyncio loop
