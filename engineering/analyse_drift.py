#!/usr/bin/env python3

import pyqtgraph as pg
import numpy as np
import sys
import glob
from pyqtgraph.Qt import QtWidgets
app = QtWidgets.QApplication(sys.argv)
#pg.setConfigOption('background', 'w')
#pg.setConfigOption('foreground', 'k')
pg.setConfigOptions(antialias=True)

plot = pg.PlotWidget()
plot.addLegend()

color = 0
filelist = glob.glob('drift_log*.csv')
for f in filelist:
    data = np .loadtxt(f, skiprows=1, delimiter=',')
    drift = data[:,0]
    # linear regression
    coef = np.polyfit(np.arange(len(drift)), drift, 1)[0]
    print(f'{f}: remaining drift coefficient: {coef*1000.:.3f} (ms per exposure)')
    times = data[:,1]
    plot.plot(times, drift, pen=pg.intColor(color, len(filelist)), name=f)
    color += 1

plot.setLabel('bottom', 'time (s)')
plot.setLabel('left', 'drift (s)')

plot.show()

# Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    app.exec_()  # Start QApplication event loop ***
