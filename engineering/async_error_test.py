#!/usr/bin/env python3

import asyncio
import random
from typing import List, Tuple
from rich.traceback import install
from rich.logging import RichHandler
install()

random.seed(42)

async def async_worker(i: int, stop: asyncio.Event):
    try:
        while not stop.is_set():
            print(f'Async worker {i}...')
            await asyncio.sleep(1 + random.random() / 5)
            if random.random() > 0.95:
                raise RuntimeError(f'Error in Task {i}')
    except asyncio.CancelledError:
        # something went wrong in another tasks and we're doing a clean shutdown
        print(f'Cancelling Task {i}')
        await asyncio.sleep(4*random.random())

        # for debugging purposes let's pretend something goes horribly wrong during the cleanup:
        if i == 2:
            raise RuntimeError(f'Error in shutdown')

        print(f'Task {i} cleanup done')

        # it is considered bad practice to catch the cancel and not let it
        # bubble up.
        raise

async def thread_worker(i: int, stop: asyncio.Event):
    pass

async def work():
    stop = asyncio.Event()
    tasks = [asyncio.create_task(async_worker(i, stop), name=f'Task {i}') for i in range(3)]

    try:
        done, pending = await asyncio.wait(tasks, return_when=asyncio.FIRST_EXCEPTION)

        print(f'The following tasks returned unexpectedly: {[task.get_name() for task in done]}')
        print('Cancelling remaining tasks')
        # this part of the code is never reached if a sigint or Ctrl-C is received
        # This is only here to do a cleanup in case one of the tasks internally failed
        # wait does not cancel other tasks if one task fails (except CancelledError):
        for task in pending:
            task.cancel()

    except (KeyboardInterrupt, asyncio.CancelledError):
        print('\rtasks aborted')

    print('Giving tasks a second chance to cleanup')
    # either way, we want to wait once more for tasks to finish:
    try:
        await asyncio.wait(tasks, return_when=asyncio.ALL_COMPLETED)
    except (KeyboardInterrupt, asyncio.CancelledError):
        print('Please do not attempt to abort clean shutdown!')

    # asyncio.create_task gobbles up exceptions so in order to log the
    # reason for the shutdown we must retrieve and log the exception.
    # Also: asyncio runners complain loudly if exceptions are not fetched.
    for task in tasks:
        print(f'Checking the result of {task.get_name()}')
        if task.done():
            print('task is done')
            if task.cancelled():
                print('task cancelled sucessfully')
            else:
                print(f'exception in task: {task.exception()}')
        else:
            print('task not done. This is bad!!')


        #if not task.done():
        #    print(f'{task.get_name()} never finished. Not even after cancelling and awaiting again!! This is bad...')


        #if task.exception():
        #    print(f'{task.get_name()} finished with exception {task.exception()}')
        #else:
        #    print(f'{task.get_name()} finished sucessfully')

    print('work done')
    # independend of the cause of stopping, we want to await clean shutdown of the remaining tasks

async def main():
    print('Working once')
    await work()
    #print('Working twice')
    #await work()
    print('done')

if __name__ == '__main__':
    try:
        asyncio.run(main())
    except (KeyboardInterrupt, asyncio.CancelledError):
        # A keyboard inteerupt is thrown both inside the asyncio main and
        # outside. Since we handle it in main we don't need to do anything here.
        # In a more real-world scenarion it might be nice to log exceptions
        # other than keyboard interupts.
        print('Bye')
