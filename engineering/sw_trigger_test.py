#!/usr/bin/env python3

# This test is to determine the suiability of the software trigger. The
# advantage would be that the trigger times can be kept in sync with the lst
# time. What needs to be tested is if there is jitter in the exposure time.

import pyfli
import time
import os
import numpy as np
import threading
from threading import Thread
import sys
import astropy
import astropy.time
import datetime
import logging
from scipy.ndimage import zoom
import astropy.io.fits as fits
import math
import uldaq


# pretty logging and tracebacks
from rich.logging import RichHandler
from rich.traceback import install
install(show_locals=True)

#logging.basicConfig(level=logging.DEBUG)
logging.root.setLevel(logging.DEBUG)

log = logging.getLogger('sw_trigger_test')
#handler = logging.StreamHandler()
#handler.setLevel(logging.DEBUG)
#handler.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
#log.addHandler(handler)
log.addHandler(RichHandler(rich_tracebacks=True))

# TODO: do we use this?
np.random.seed(42)

# constants
#CADANCE = 6.4 # sidereal seconds
#EXPOSURETIME = 6.382525225 # s (CADANCE * (365.2422 / 366.2422))
CADANCE = 6.4 # LST seconds of which there are 13500.000 in each lst day
EXPOSURETIME = CADANCE * 365.2422 / 366.2422
#EXPOSURE_MARGIN = 3500 # ms
REFERENCE_LONGITUDE = -17.8780
#LOGFILE = 'sw_trigger_log.csv'
#with open(LOGFILE, 'w') as f:
#    f.write('time,exposuretime,num,drift,zeros,overflows\n')

# open camera
devs = pyfli.FLIList('usb','camera')
cam = pyfli.FLIOpen(devs[0][0],'usb','camera')

log.info('Camera initialized')

def measure_exposure(exptime=EXPOSURETIME*1000):
    hdr = fits.Header()

    pyfli.setExposureTime(cam, exptime)
    hdr['EXPTIME'] = exptime
    hdr['TIME'] = time.time()
    hdr['TYPE'] = "single frame"
    print("Exposing", end='\r', flush=True)
    pyfli.exposeFrame(cam)
    while True:
        remaining = pyfli.getExposureStatus(cam)
        print(f'Exposing: {remaining:2.1f}', end='\r', flush=True)
        time.sleep(0.01)
        if remaining == 0:
            break
    log.debug('Exposing: done           ')
    log.debug('Grabbing image...')
    img = pyfli.grabFrame(cam)
    filename = f"images/measure_exposure_{int(time.time()*1000):d}"
    fits.writeto(filename, img, hdr)


    return np.min(img), np.median(img), np.mean(img), np.max(img), np.std(img)

def single_frames(N=3, results=None):
    result = np.zeros(N)
    for i in range(N):
        _, median, _, _, _ = measure_exposure()
        log.info(f"Image {i} median={median}")
        result[i] = median
        if results is not None:
            results.append((time.time(), median, 'single'))
    return result




def get_lst_idx():
    start = time.time()
    now = astropy.time.Time(datetime.datetime.now())
    lst = now.sidereal_time('apparent', longitude=REFERENCE_LONGITUDE)
    lstidx = lst.value * 3600 / CADANCE
    if time.time() - start > 0.1:
        log.warning(f'Getting the lst index time took longer than expected ({time.time()-start} s)')
    return lstidx

def wait_until_lst(phase=0.0):
    lstidx = get_lst_idx()
    remaining = EXPOSURETIME * ( 1.0 - ((lstidx - phase) % 1.0))
    #log.debug(f'sleeping for {remaining} seconds until next lst')
    start = time.time()
    time.sleep(remaining)
    if np.abs(time.time() - start - remaining) > 0.1:
        log.warning(f'time.sleep() took unexpectedly long ({time.time()-start} s)')
    return remaining

def busy_wait_until_lst(margin = 0.05, phase=0.0):

    now = astropy.time.Time(datetime.datetime.now())
    lst = now.sidereal_time('apparent', longitude=REFERENCE_LONGITUDE)
    lstidx = lst.value * 3600 / CADANCE
    current_timestamp = now.value.timestamp()
    next_lst_timestamp = current_timestamp + EXPOSURETIME * ( 1.0 - ((lstidx - phase) % 1.0))

    # first do a rough sleep
    sleeptime = next_lst_timestamp - current_timestamp
    if sleeptime > margin:
        log.debug(f'sleeping for {sleeptime} seconds until next lst')
        time.sleep(sleeptime - margin)
    else:
        log.debug('Skipping short sleep')

    # then do a fine delay using busy wait
    log.debug('busy waiting...')
    while time.time() < next_lst_timestamp:
        pass


def wait_until_ready_for_trigger():
    #log.debug(f"{get_lst_idx()}: Waiting for 'WAITING_FOR_TRIGGER' state")
    while True:
        status = pyfli.getDeviceStatus(cam) & pyfli.CAMERA_STATUS_MASK
        if status == pyfli.CAMERA_STATUS_WAITING_FOR_TRIGGER:
            #log.debug(f"{get_lst_idx()}: Camera is now waiting for trigger")
            break
        if status == pyfli.CAMERA_STATUS_IDLE:
            raise RuntimeError('Camera unexpectedly returned to IDLE')
        time.sleep(0.001)

def wait_until_data_ready():
    #log.debug(f"{get_lst_idx()}: Waiting for 'DATA_READY' flag")
    while True:
        status = pyfli.getDeviceStatus(cam)
        if status & pyfli.CAMERA_DATA_READY:
            #log.debug("data is ready for readout")
            break



def camera_status_text(status : int) -> str:
    if status == pyfli.CAMERA_STATUS_UNKNOWN:
        return 'UNKNOWN'
    dataready = 'DATA_READY+' if status & pyfli.CAMERA_DATA_READY else ''

    if status & pyfli.CAMERA_STATUS_MASK == pyfli.CAMERA_STATUS_IDLE:
        return dataready+'IDLE'
    if status & pyfli.CAMERA_STATUS_MASK == pyfli.CAMERA_STATUS_WAITING_FOR_TRIGGER:
        return dataready+'WAITING_FOR_TRIGGER'
    if status & pyfli.CAMERA_STATUS_MASK == pyfli.CAMERA_STATUS_READING_CCD:
        return dataready+'READING_CCD'
    if status & pyfli.CAMERA_STATUS_MASK == pyfli.CAMERA_STATUS_EXPOSING:
        return dataready+'EXPOSING'



kill_monitor=False
def monitor_device_status():
    oldstatus = 0
    #kill_monitor = False
    #exp_start = 0
    while not kill_monitor:
        newstatus = pyfli.getDeviceStatus(cam) & (pyfli.CAMERA_STATUS_MASK | pyfli.CAMERA_DATA_READY)
        if oldstatus != newstatus:
            log.debug(f'{get_lst_idx()}: FLI status {camera_status_text(newstatus)}')
            #if newstatus & pyfli.CAMERA_STATUS_EXPOSING:
            #exp_start = time.time()
        oldstatus = newstatus
        time.sleep(0.0001)
    log.debug('Monitoring thread stopped')



def trigger_procedure_with_exposuretime(N=10, results=None):
    pyfli.enableExternalTrigger(cam)
    #pyfli.setExposureTime(cam, np.floor(EXPOSURETIME * 1000) - EXPOSURE_MARGIN)
    pyfli.setExposureTime(cam, 6000)

    pyfli.startVideoMode(cam)

    log.debug(f'{get_lst_idx()}: initial waiting for camera')
    wait_until_ready_for_trigger()

    log.debug(f'{get_lst_idx()}: initial waiting for next lst')
    wait_until_lst()

    log.debug(f'{get_lst_idx()}: initial trigger')
    pyfli.triggerExposure(cam)
    time.sleep(0.5)

    result = np.zeros(N)
    for i in range(N):
        log.debug(f'{get_lst_idx()}: waiting for camera')
        wait_until_ready_for_trigger()
        log.debug(f'{get_lst_idx()}: waiting for next lst')
        wait_until_lst()
        log.debug(f'{get_lst_idx()}: trigger')
        pyfli.triggerExposure(cam)
        log.debug(f'{get_lst_idx()}: waiting for data')
        wait_until_data_ready()
        #log.debug("Grabbing video frame...")
        start = time.time()
        img = pyfli.grabVideoFrame(cam)
        #log.info(f"Video frame downloaded in {time.time()-start} seconds")
        median = np.median(img)
        log.info(f"{get_lst_idx()}: Median image flux: {median}")
        result[i] = median
        if results is not None:
            results.append((time.time(), median, 'internal'))
    return result


def trigger_procedure_with_software_cancel(N=10, results=None):
    pyfli.stopVideoMode(cam)
    pyfli.enableExternalTrigger(cam)
    pyfli.setExposureTime(cam, 2 * EXPOSURETIME * 1000)
    pyfli.startVideoMode(cam)
    log.info(f'{get_lst_idx()}: Video mode started')

    log.debug(f'{get_lst_idx()}: initial waiting for camera')
    wait_until_ready_for_trigger()

    log.debug(f'{get_lst_idx()}: initial waiting for next lst')
    wait_until_lst()

    log.debug(f'{get_lst_idx()}: initial trigger')
    pyfli.triggerExposure(cam)
    time.sleep(0.5) # first trigger needs a delay because ready_for_trigger flag is not cleared fast enough?

    result = np.zeros(N)
    for i in range(N):
        log.debug(f'{get_lst_idx()}: waiting for next lst')
        wait_until_lst()

        log.debug(f'{get_lst_idx()}: stopping exposure')
        pyfli.endExposure(cam)

        log.debug(f'{get_lst_idx()}: waiting for camera')
        #wait_until_ready_for_trigger()

        log.debug(f'{get_lst_idx()}: trigger')
        pyfli.triggerExposure(cam)

        log.debug(f'{get_lst_idx()}: waiting for data')
        wait_until_data_ready()
        time.sleep(1)
        #log.debug("Grabbing video frame...")
        start = time.time()
        img = pyfli.grabVideoFrame(cam)
        #log.info(f"Video frame downloaded in {time.time()-start} seconds")
        median = np.median(img)
        log.info(f"{get_lst_idx()}: Median image flux: {median}")
        result[i] = median
        if results is not None:
            results.append((time.time(), median, 'sw-cancel'))
    return result



def cooldown(target=5, margin=0.5):
    pyfli.setTemperature(cam, target)
    log.info(f"Cooling down to {target}'C")
    while True:
        current_temp = pyfli.getTemperature(cam)
        log.debug(f'CCD temp: {current_temp}\'C')
        if np.abs(current_temp - target) < margin:
            log.info('Cooldown done')
            return
        time.sleep(0.5)

def warmup():
    pyfli.setTemperature(cam, 35)
    log.info(f"Didabling CCD cooling")

def test_exptimelength_in_video_mode(exposuretime=1000*EXPOSURETIME, N=10, filename=None):
    # this alternatingly takes equal length exposures in video and non-video
    # mode to test if self-triggering video mode has the same apparent
    # brightness as single frame exposures.
    if filename is not None:
        f = open(filename, 'w')
        f.write(f'# Exposuretime: {exposuretime} (s)\n')
        f.write(f'bias; photo mode; video mode\n')
    for i in range(N):
        # for the photo mode we already have a function
        _, bias, _, _, _ = measure_exposure(0)
        log.info(f"Bias {i+1}: {bias}")
        _, med1, _, _, _ = measure_exposure(exposuretime)
        log.info(f"Photo exposure {i+1}: {med1}")

        # video mode
        pyfli.startVideoMode(cam)
        wait_until_data_ready()
        img = pyfli.grabVideoFrame(cam)
        pyfli.stopVideoMode(cam)
        med2 = np.median(img)
        log.info(f"Video exposure {i+1}: {med2}")
        if f is not None:
            f.write(f'{bias}; {med1}; {med2}\n')
            f.flush()
    if f is not None:
        f.close()


def auto_download():
    def dl_loop():
        while True:
            status = pyfli.getDeviceStatus(cam)
            if status & pyfli.CAMERA_DATA_READY:
                img = pyfli.grabVideoFrame(cam)
                med = np.median(img)
                log.info(f'Image downloaded: median={med}')
            time.sleep(0.1)
    dl_thread = Thread(target=dl_loop)
    dl_thread.start()

def preview(img, width=100):
    # resize image
    y_scale = width / img.shape[1]
    x_scale = y_scale / 2.5 # just a heuristic based on common terminal fonts and line spacing
    zoomed = zoom(img, (x_scale, y_scale), order=2)

    # normalize and map
    levels = "`^\",:;Il!i~+_-?][}{1)(|\\/tfjrxnuvczXYUJCLQ0OZmwqpdbkhao*#MW&8%B@$"
    max = np.max(zoomed)
    min = np.min(zoomed)
    if min == max:
        characters = np.array([['.' for l in  row] for row in zoomed])
    else:
        normalized = (zoomed - min) / (max - min) * (len(levels)-1)
        indices = normalized.astype(int)
        characters = np.array([[levels[l] for l in  row] for row in indices])

    for row in characters:
        for c in row:
            print(c, end='')
        print()


def cleanup(retries=3):
    # sanity checks

    # stop the camera
    attempts = 0
    while True:
        status = pyfli.getDeviceStatus(cam)
        if (status & pyfli.CAMERA_STATUS_MASK) is pyfli.CAMERA_STATUS_IDLE:
            break
        elif attempts < retries:
            log.warning("Camera not IDLE before starting! Stopping now...")
            pyfli.stopVideoMode(cam)
            pyfli.triggerExposure(cam)
            time.sleep(1)
            attempts += 1
        else:
            raise RuntimeError(f"Camera did return to IDLE even after {retries} retries!")

    # clear data buffer
    attempts = 0
    while True:
        status = pyfli.getDeviceStatus(cam)
        if not (status & pyfli.CAMERA_DATA_READY):
            break
        elif attempts < retries:
            log.warning("Camera reports data is ready before starting! Stopping now...")
            pyfli.stopVideoMode(cam)
            time.sleep(1)
            attempts += 1
        else:
            raise RuntimeError(f"Camera still reports DATA_READY after {retries} retries!")




def free_running_video_mode(N=10, results=None):
    """
    The old (mascara/bring) method of letting the internal timer in the
    camera count the exposure time. This inherently introduces drift, especially
    if the desired exposure period is not a round number of milliseconds.
    """

    cleanup()

    # getting ready
    pyfli.disableExternalTrigger(cam)
    log.debug(f'Setting exposure time for free running video mode to {EXPOSURETIME}')
    pyfli.setExposureTime(cam, EXPOSURETIME*1000)

    # start
    wait_until_lst()
    pyfli.startVideoMode(cam)

    # get N images
    result = np.zeros(N)
    for i in range(N):
        hdr = fits.Header()
        hdr['EXPTIME'] = math.floor(EXPOSURETIME * 1000)
        hdr['TIME'] = time.time()
        hdr['TYPE'] = "free-running video mode"

        wait_until_data_ready()
        img = pyfli.grabVideoFrame(cam)
        filename = f"images/freerunning_{int(time.time()*1000):d}"
        fits.writeto(filename, img, hdr)

        #preview(img)
        median = np.median(img)
        log.info(f"{get_lst_idx()}: Image {i} median: {median}")
        result[i] = median
        if results is not None:
            results.append((time.time(), median, 'internal'))

    pyfli.stopVideoMode(cam)
    return result

def auto_download_with_lst_trigger(N=10, results=None, margin=10):
    done = threading.Event()
    import secrets
    ident = secrets.token_urlsafe(2)
    def trigger_loop(done):
        log.info(f'new Trigger thread {ident}')
        try:
            while True:
                #for i, thread in enumerate(threading.enumerate()):
                #    log.debug(f'Listing running threads: {i} {thread.name}')
                #wait_until_lst()
                busy_wait_until_lst() # more accurate, likely not necessary
                if done.is_set():
                    break
                log.info(f"{get_lst_idx()}: ({ident}) Trigger {int(get_lst_idx())}")
                pyfli.triggerExposure(cam)
            log.info(f'({ident}) Trigger loop finished')
        except Exception as e:
            done.set()
            raise e

    def download_loop(done):
        try:
            numdownloads = 0
            prevlstidx = None
            while numdownloads < N:
                status = pyfli.getDeviceStatus(cam)
                if status & pyfli.CAMERA_DATA_READY:
                    # do not download if trigger is less than 2s in the future
                    lstidx = get_lst_idx()
                    lstidx_int = round(lstidx)
                    if prevlstidx is not None and lstidx_int != prevlstidx + 1:
                        log.error(f'LST index {lstidx_int-1} was missed!')
                    prevlstidx = lstidx_int
                    #log.debug(f'lstidx={lstidx}')
                    if (lstidx % 1.0) > (EXPOSURETIME - 2.0) / EXPOSURETIME:
                        # calculate how long we should sleep until we're safely out of trigger zone
                        sleeptime = (1 - (lstidx % 1.0)) * EXPOSURETIME + 0.2
                        log.info(f'sleeping(1) for {sleeptime:.3f} (s) to avoid colliding with the trigger')
                        time.sleep(sleeptime)
                    # also don't download in the first 0.2 seconds for safety
                    if (lstidx % 1.0) < 0.2 / EXPOSURETIME:
                        sleeptime = 0.2 - (lstidx % 1.0) * EXPOSURETIME
                        log.info(f'sleeping(2) for {sleeptime:.3f} (s) to avoid colliding with the trigger')
                        time.sleep(sleeptime)
                    #log.debug(f'starting image {lstidx} download')
                    start = time.time()
                    img = pyfli.grabVideoFrame(cam)
                    hdr = fits.Header()
                    hdr['EXPTIME'] = EXPOSURETIME*1000 - 100
                    hdr['TIME'] = time.time()
                    hdr['TYPE'] = "single frame"
                    filename = f"images/autodownload_{int(time.time()*1000):d}"
                    fits.writeto(filename, img, hdr)


                    #log.debug(f'{get_lst_idx()}: download finished in {time.time()-start:.3f} (s)')
                    start = time.time()
                    med = np.median(img)
                    #log.debug(f'{get_lst_idx()}: calculating median in {time.time()-start:.3f} (s)')
                    log.info(f'{get_lst_idx()}: Image {lstidx_int} downloaded: median={med}')
                    if results is not None:
                        results.append((time.time(), med, 'sw-trigger'))
                    numdownloads += 1
                time.sleep(0.1)
        except Exception as e:
            done.set()
            raise e
        # when done we're all done
        log.info("Download loop finished")
        done.set()

    def monitor_loop(done):
        try:
            oldstatus = 0
            while not done.is_set():
                newstatus = pyfli.getDeviceStatus(cam) & pyfli.CAMERA_STATUS_MASK
                if oldstatus != newstatus:
                    if newstatus == pyfli.CAMERA_STATUS_IDLE:
                        log.debug(f'{get_lst_idx()}: Camera status: IDLE')
                        done.set()
                    elif  newstatus == pyfli.CAMERA_STATUS_WAITING_FOR_TRIGGER:
                        log.debug(f'{get_lst_idx()}: Camera status: WAITING_FOR_TRIGGER')
                    elif  newstatus == pyfli.CAMERA_STATUS_EXPOSING:
                        log.debug(f'{get_lst_idx()}: Camera status: EXPOSING')
                    elif  newstatus == pyfli.CAMERA_STATUS_READING_CCD:
                        log.debug(f'{get_lst_idx()}: Camera status: READING_CCD')
                oldstatus = newstatus
                time.sleep(0.1)
        except Exception as e:
            done.set()
            raise e

    cleanup()

    dl_thread = Thread(target=download_loop, args=(done,))
    dl_thread.start()
    tr_thread = Thread(target=trigger_loop, args=(done,))
    tr_thread.start()
    #mt_thread = Thread(target=monitor_loop, args=(done,))
    #mt_thread.start()

    log.debug(f'Setting exposure time for triggered download loop to {EXPOSURETIME + margin / 1000}')
    pyfli.setExposureTime(cam, EXPOSURETIME*1000 - margin) # some margin
    pyfli.enableExternalTrigger(cam)
    pyfli.startVideoMode(cam)

    #done.wait()
    dl_thread.join()
    tr_thread.join()
    log.info("Cleaning up...")
    pyfli.stopVideoMode(cam)
    pyfli.disableExternalTrigger(cam)
    log.info("Experiment done.")


def cascading_download(N=10, results=None):
    """Using the untriggered video mode but purposefully letting the readout
    buffer fill up all the time so the readout triggers the next exposure. This
    works because the camera has no shutter and no other option than to continue
    integrating after the exposure time is reached if the readout buffer is not
    available.
    """

    cleanup()

    # getting ready
    pyfli.disableExternalTrigger(cam)
    pyfli.setExposureTime(cam, 0)
    # NB: it is important to set it to 0 and not any positive interger because
    # that somehow slips in a buffer clear after readout which would counteract
    # the whole idea of this procedure.

    # start
    wait_until_lst()
    pyfli.startVideoMode(cam)

    # almost immediatly the first image will be done and transferred to the
    # readout buffer the second image is now exposing and the first image is
    # ready but we don't read it out yet.

    # Again almost immediately the exposure timer triggers again but the first
    # image still occupies the readout buffer, so the exposure continues. Note
    # that the second exposure might have started at a slight offset because of
    # the shifting of the first exposure and the startup of the video mode for
    # which we don't know the exact timing.

    wait_until_lst()
    # at the next lst time we read out, which starts the third exposure at
    # exactly the right time (TODO: or at a fixed and known offset that we need
    # to determine?) Since this is the first exposure we discard it.
    log.debug(f'{get_lst_idx()}: Reading and discarding first faux image.')
    pyfli.grabVideoFrame(cam)

    wait_until_lst()
    # one lst period later we readout again, this is the second exposure so we
    # also discard it. Now, however, the third exposure moves into the readout
    # buffer. This third exposure started and ended by our lst-coupled readout
    # command.
    log.debug(f'{get_lst_idx()}: Reading and discarding second faux image.')
    pyfli.grabVideoFrame(cam)

    # so now we are ready to start reading real images because the next image
    # will be the 'good' third image. Do note that we are reading the second to
    # last image every time. I.e., the one staring ~2 lst periods ago.

    # get N images
    result = np.zeros(N)
    for i in range(N):
        hdr = fits.Header()
        hdr['EXPTIME'] = 0
        hdr['TIME'] = time.time()
        hdr['TYPE'] = "cascaded download"

        wait_until_lst()
        img = pyfli.grabVideoFrame(cam)
        filename = f"images/cascading_{int(time.time()*1000):d}"
        fits.writeto(filename, img, hdr)

        #preview(img)
        median = np.median(img)
        result[i] = median
        log.info(f"{get_lst_idx()}: Image {i} median: {median}")
        if results is not None:
            results.append((time.time(), median, 'cascade'))

    pyfli.stopVideoMode(cam)
    return result

def alternate_methods(functions, N=4, M=20):
    results = []
    for m in range(M):
        log.info(f'Proceeding with Iteration {m+1}')
        for method in functions:
            log.info(f'Testing method \'{method.__name__}\' with exposure time {EXPOSURETIME}')
            method(N, results)
    return results


daqdev = None
def led_setup():
    global daqdev
    devices = uldaq.get_daq_device_inventory(uldaq.InterfaceType.USB)
    if len(devices) >= 1:
        daqdev = uldaq.DaqDevice(devices[0])
        daqdev.connect()
    else:
        raise ValueError("No DAQ devices available")
    dio = daqdev.get_dio_device()
    # convert string representation of dir to uldaq const
    direction = uldaq.DigitalDirection.OUTPUT
    # set all all ports on the device to this type
    for port in dio.get_info().get_port_types():
        dio.d_config_port(port, direction)


def led_on():
    global daqdev
    log.info(f"{get_lst_idx()}: Turning Led On")
    dio = daqdev.get_dio_device()
    porttype = uldaq.DigitalPortType.FIRSTPORTB
    dio.d_bit_out(porttype, 7, 1)


def led_off():
    global daqdev
    log.info(f"{get_lst_idx()}: Turning Led Off")
    dio = daqdev.get_dio_device()
    porttype = uldaq.DigitalPortType.FIRSTPORTB
    dio.d_bit_out(porttype, 7, 0)


def run_while_blinking(method, phase):
    done = threading.Event()
    finished = threading.Event()
    def blinker_loop(done):
        try:
            while True:
                #wait_until_lst()
                busy_wait_until_lst(phase=phase) # more accurate, likely not necessary
                led_on()
                if done.is_set():
                    break
                busy_wait_until_lst(phase=phase) # more accurate, likely not necessary
                led_off()
                if done.is_set():
                    break
            log.info('Blinker thread done')
        except Exception as e:
            done.set()
            raise e

    blinker_thread = Thread(target=blinker_loop, args=(done,))
    blinker_thread.start()

    results = []
    method(results=results, N=10)

    done.set()
    blinker_thread.join()
    return results



if __name__ == "__main__":
    # the first call to get_lst_idx delays stuff a lot because it does a network
    # lookup so we force one before doing anything else.
    get_lst_idx()

    # cooldown the camera
    cooldown(-5)

    led_setup()
    led_on()

    # 50 blanks
    #for i in range(50): # 50
    #    log.info(f"Blank {i+1}")
    #    measure_exposure(EXPOSURETIME*1000)
    log.info('Starting Blanks')
    auto_download_with_lst_trigger(N=50)

    #led_on()

    #min_flux, med_flux, avg_flux, max_flux, std_flux = measure_exposure(0)
    #log.info(f'non-video image exposure (exp=0): min: {min_flux}, median: {med_flux}, mean:{avg_flux}, max: {max_flux}')
    #min_flux, med_flux, avg_flux, max_flux, std_flux = measure_exposure(1)
    #log.info(f'non-video image exposure (exp=1): min: {min_flux}, median: {med_flux}, mean:{avg_flux}, max: {max_flux}')

    # 10 'darks' for calibration
    log.info('Taking Darks')
    #led_off()
    #for _ in range(10): # 10
    #    min_flux, med_flux, avg_flux, max_flux, std_flux = measure_exposure(EXPOSURETIME*1000)
    #    log.info(f'non-video image exposure (exp={EXPOSURETIME} s): min: {min_flux}, median: {med_flux}, mean:{avg_flux}, max: {max_flux}')
    #auto_download_with_lst_trigger(N=10)
    #led_on()


    # results = alternate_methods([free_running_video_mode, single_frames,
    #                              free_running_video_mode, cascading_download,
    #                              free_running_video_mode, auto_download_with_lst_trigger,
    #                              free_running_video_mode
    #                              ])
    # np.save('alternating_results.npy', results)

    #results = alternate_methods([free_running_video_mode, single_frames])
    #np.save('freerunning_vs_singles.npy', results)

    #results = alternate_methods([free_running_video_mode, cascading_download])
    #np.save('freerunning_vs_cascading.npy', results)

    #EXPOSURETIME = 6.382
    #results = alternate_methods([free_running_video_mode, auto_download_with_lst_trigger])
    #np.save('freerunning_vs_trigger.npy', results)

    results = {
        5.0: [],
        10.0: [],
        15.0: [],
        20.0: []
    }
    for _ in range(30):
        for CADANCE in [5.0, 10.0, 15.0, 20.0]:
            EXPOSURETIME = CADANCE * 365.2422 / 366.2422
            auto_download_with_lst_trigger(N=5, results=results[CADANCE])

    for CADANCE in [5.0, 10.0, 15.0, 20.0]:
        np.save(f'exposure_{CADANCE}.npy', results[CADANCE])

    # CADANCE = 5.0
    # EXPOSURETIME = CADANCE * 365.2422 / 366.2422
    # results = alternate_methods([free_running_video_mode, auto_download_with_lst_trigger])
    # np.save('trigger_5.0.npy', results)

    # CADANCE = 10.0
    # EXPOSURETIME = CADANCE * 365.2422 / 366.2422
    # results = alternate_methods([free_running_video_mode, auto_download_with_lst_trigger])
    # np.save('trigger_10.0.npy', results)

    # CADANCE = 15.0
    # EXPOSURETIME = CADANCE * 365.2422 / 366.2422
    # results = alternate_methods([free_running_video_mode, auto_download_with_lst_trigger])
    # np.save('trigger_15.0.npy', results)

    # CADANCE = 20.0
    # EXPOSURETIME = CADANCE * 365.2422 / 366.2422
    # results = alternate_methods([free_running_video_mode, auto_download_with_lst_trigger])
    # np.save('trigger_20.0.npy', results)


    warmup()

    # this spews a message every time the device status changes by monitoring it in a background thread
    #mon = Thread(target=monitor_device_status)
    #mon.start()



    #trigger_procedure_with_software_cancel()

    # cleanup
    #pyfli.stopVideoMode(cam)
    #pyfli.disableExternalTrigger(cam)
    #pyfli.FLIClose(cam)
    #kill_monitor = True
