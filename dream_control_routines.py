#!/usr/bin/env python3

import asyncio
import datetime
import logging
import time

import dream_config as cfg
from dream_command_gui import NormalExit
from dream_dataclasses import (CameraServerMode, DomeState, DomeTargetState,
                               HeaterState, LedState, PeltierState, Warnings)
from dream_electronics import Hardware
from dream_monitoring import DreamMonitoring
from dream_nightly_plots import make_nightly_plots_async
from dream_state import DreamState
from dream_utils import dewpoint, map_range, next_seq, shutdown, sun_alt, send_slack_message

log = logging.getLogger(cfg.LOGGER_NAME)


async def power_control_loop():
    '''Turn off power to computers if ups battery gets dangerously low.

    The remaining battery charge does not at all fall linearly when the battery
    is activated. (In fact it looks like what you'd expect from the battery
    voltage). Hence we make decision based on remaining time and charge
    together as well as the time spent on battery.

    Since the battery is only supposed to bridge small power interruptions we go
    to powersave quite quickly. I.e. after 1 min of working on battery power.
    This includes:

    * closing the dome
    * turn off cameras
    * turn off camera computers
    * turn off power supplies for electronics

    When the ups is charging and again above 90% charge we restart the camera
    computers and allow the dome to open.

    If the ups reports < 5 minutes of remaining charge and discharging we do a
    clean shutdown on the control computer itself. If power is restored at this
    time, the station requires manual intervention to boot up again. If the ups
    really reaches 0% the computers will boot up automatically as soon as power
    is supplied.

    The shutdown is achieved by sending a message to the camera computers in the
    heartbeat reply. The camera computer shuts down its own camera and pdu port
    and does a powerdown. The only thing we need to do is set the station mode
    to SHUTDOWN and arrange the power up after the recovery.

    The total station shutdown is also handled here.

    Note: the power to the cameras is controlled by the corresponding control
    computer itself when it starts/stops image sequences.

    '''
    monitoring = DreamMonitoring()
    hw = Hardware()
    state = DreamState()

    while True:
        await asyncio.sleep(1)

        if not monitoring.ups_status:
            # no need to deal with this here
            continue

        # check if we need to go to powersave
        if (time.time() - monitoring.ups_status['last_online'] > cfg.UPS_MAX_POWERGAP and
                monitoring.ups_status['ups_status'] != 'ONLINE' and
                state.mode != CameraServerMode.SHUTDOWN):
            log.info('Going to power save mode')
            state.mode = CameraServerMode.SHUTDOWN
            state.set_problem(Warnings.POWERSAVING_ACTIVATED, True)

        # check if we need to shutdown
        if (monitoring.ups_status['battery_remaining'] < cfg.UPS_SHUTDOWN_REMAINING_MINUTES and
                monitoring.ups_status['ups_status'] != 'ONLINE'):
            if state.dome_state == DomeState.CLOSED:
                log.info('Going to shutdown mode')
                for name in ['PSU 1', 'PSU 2']:
                    pdunum, outlet = cfg.PDU_OUTLETS[name]
                    pdu = hw.pdu1 if pdunum == 1 else hw.pdu2
                    await pdu.turn_off_delayed(outlet)
                await asyncio.sleep(1)
                shutdown()
                raise NormalExit
            else:
                log.error('Battery dangerously low while the dome is not closed!')

        # check if we are operational
        if (monitoring.ups_status['battery_charge'] > cfg.UPS_RESUME_LEVEL and
            monitoring.ups_status['ups_status'] == 'ONLINE'):

            # we go to IDLE, the camera_mode loop will soon override this, just
            # like after a fresh start:
            if state.mode == CameraServerMode.SHUTDOWN and state.target_mode != CameraServerMode.SHUTDOWN:
                # this is the only way to get out of the shutdown state
                state.mode = CameraServerMode.IDLE
                state.set_problem(Warnings.POWERSAVING_ACTIVATED, False)

                # turn on any pdu outlet that was turned off:
                for name, (pdunum, outlet) in cfg.PDU_OUTLETS.items():
                    if not name.endswith('Camera'):
                        if not monitoring.pdu_status[name]:
                            log.info(f'Found PDU{pdunum} outlet {outlet} to be off. Powering on {name} now...')
                            pdu = hw.pdu1 if pdunum == 1 else hw.pdu2
                            await pdu.turn_on(outlet)



async def end_of_night_calibrations():
    '''Does the end of night calibrations. This does not rely on counters but
    instead uses a simply heuristic to give each calibration type a limited
    amount of time. Returns False if it was not possible to do the calibrations
    because the observatory mode was not AUTO. Does not check that images are
    actually coming in or if the dome was closed.

    '''
    log.info('Time for of night calibrations')
    state = DreamState()

    async def wait(timeout: float, dt: float = 1):
        for _ in range(int(timeout // dt)):
            if state.target_mode is not CameraServerMode.AUTO:
                log.info('Skipped')
                break
            await asyncio.sleep(1)


    log.info('Switching to DARKS for end-of-night')
    state.mode = CameraServerMode.DARKS
    await wait(cfg.N_DARKS * cfg.EXPOSURETIME + 10)

    log.info('Switching to FLATS for end-of-night')
    state.mode = CameraServerMode.FLATS
    await wait(cfg.N_FLATS * cfg.EXPOSURETIME + 10)

    log.info('Switching to BIAS for end-of-night')
    state.mode = CameraServerMode.BIAS
    await wait(cfg.N_BIAS * cfg.EXPOSURETIME + 10)

    log.info('End of night calibrations done')
    if state.target_mode == CameraServerMode.AUTO:
        log.info('Switching to IDLE after end-of-night')
        state.mode = CameraServerMode.IDLE
    else:
        log.info('End of night calibrations skipped or truncated because mode was not AUTO')




async def end_of_night_loop():
    '''Separate loop that waits for sunrise, then waits for the dome to close
    and then does the post-night calibrarions and the end of night report.    '''
    state = DreamState()
    while True:
        #wait for sunrise
        prev_alt, _ = sun_alt()
        while True:
            await asyncio.sleep(1)
            sun, _ = sun_alt()
            if sun > cfg.SUN_ALT_SUNRISE_CLOSEDOME and prev_alt < cfg.SUN_ALT_SUNRISE_CLOSEDOME:
                break
            prev_alt = sun
        log.info(f'Sun crossed the sunrise threshold ({cfg.SUN_ALT_SUNRISE_CLOSEDOME}°)')

        # give the dome 2 minutes to close
        timeout = 120
        while timeout > 0 and state.dome_state is not DomeState.CLOSED:
            await asyncio.sleep(1)
            timeout -= 1

        # if dome not closed now that's a huge problem. This is checked elsewhere (see monitoring)
        # however, in that case we do log an error about being forced to skip end-of-night calibrations
        if state.dome_state is not DomeState.CLOSED:
            log.error('Skipping end-of-night calibrations because dome was not closed')
        else:
            await end_of_night_calibrations()

        log.info('Making end of night plots')
        await make_nightly_plots_async()
        log.info('End of night routine done')




async def station_mode_loop():
    '''Control the observing mode based on time of day, and reported calibration
    state of the cameras. The general heuristic follows mascara: start cooling
    (standby) at a defined sun angle, then start. Just like everything else,
    this is polling the target state, which could change in the gui. Hence we
    must not sleep for too long in this loop.

    '''
    state = DreamState()

    # This loops needs no special cleanup so we do not catch asyncio.CancelledError
    while True:
        # we await at the start of the loop so we can use continue statements
        await asyncio.sleep(1)

        # generate some warnings if not every camera is online
        state.set_problem(Warnings.CAMERA_N_NOT_CONNECTED, 'N' not in state.camerainfo.keys())
        state.set_problem(Warnings.CAMERA_E_NOT_CONNECTED, 'E' not in state.camerainfo.keys())
        state.set_problem(Warnings.CAMERA_S_NOT_CONNECTED, 'S' not in state.camerainfo.keys())
        state.set_problem(Warnings.CAMERA_W_NOT_CONNECTED, 'W' not in state.camerainfo.keys())
        state.set_problem(Warnings.CAMERA_C_NOT_CONNECTED, 'C' not in state.camerainfo.keys())

        script_clients = [client for client in state.camerainfo.values() if client.client_type == 'script']
        state.set_problem(Warnings.SCRIPT_CLIENT_ACTIVE, len(script_clients) > 0)

        # if we are in shutdown mode, don't even check the auto mode
        if state.mode == CameraServerMode.SHUTDOWN:
            continue

        # first, get the sun position and direction
        sun, rising = sun_alt()

        # if the state is not AUTO (i.e. override by gui or script) follow the
        # manual override blindly:
        if state.target_mode != CameraServerMode.AUTO:
            #log.info(f'Following target mode: {state.target_mode}')
            if state.mode != state.target_mode:
                log.info(f'Switching {state.target_mode} (override)')
            state.mode = state.target_mode
            continue

        if sun > cfg.SUN_ALT_SUNRISE_CLOSEDOME and rising:
            # it's morning, do nothing
            # not even idle: the end_of_night loop takes control from here
            #log.debug('it is morning, doing nothing')
            continue
        elif sun > cfg.SUN_ALT_SUNSET_CALIBRATIONS and not rising:
            # it's afternoon. Idle until sun below calibration threshold
            #state.mode = CameraServerMode.IDLE
            #log.debug('it is afterdoon, doing nothing')
            continue
        else:
            # it is time for observations

            # General heuristic: We wait for each phase as long as at least one
            # camera is still working on that phase. Making progress (or being
            # alive) is defined as having taken an image within the last 30
            # seconds.
            max_age = datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(seconds=30)
            alive_clients = [client for client in state.camerainfo.values()
                             if client.client_type == 'camera'
                             and client.camera_data is not None # also consider non-alive if counters are missing because we need them
                             and client.image_data is not None
                             and client.image_data.triggertime > max_age
                             ]
            alive_client_names = [client.camera_data.name for client in alive_clients]

            # skip if no cameras
            if len(alive_clients) == 0:
                # we must ask for blanks and not standby or idle, otherwise we never get any alive clients
                if state.mode != CameraServerMode.BLANKS:
                    log.info('Requesting blanks until at least one camera online')
                state.mode = CameraServerMode.BLANKS
                continue

            if state.mode == CameraServerMode.SCIENCE:
                # do not return to calibrating if we are already in science mode
                continue

            if any([client.camera_data.num_blanks < cfg.N_BLANKS for client in alive_clients]):
                if state.mode != CameraServerMode.BLANKS:
                    log.info('Calibrations: BLANKS')
                state.mode = CameraServerMode.BLANKS
                continue
            if any([client.camera_data.num_bias < cfg.N_BIAS for client in alive_clients]):
                if state.mode != CameraServerMode.BIAS:
                    log.info('Calibrations: BIAS')
                state.mode = CameraServerMode.BIAS
                continue
            if any([client.camera_data.num_flats < cfg.N_FLATS for client in alive_clients]):
                if state.mode != CameraServerMode.FLATS:
                    log.info('Calibrations: FLATS')
                state.mode = CameraServerMode.FLATS
                continue
            if any([client.camera_data.num_darks < cfg.N_DARKS for client in alive_clients]):
                if state.mode != CameraServerMode.DARKS:
                    log.info('Calibrations: DARKS')
                state.mode = CameraServerMode.DARKS
                continue

            if state.mode != CameraServerMode.SCIENCE:
                log.info('Calibrations done: SCIENCE mode active')
            state.mode = CameraServerMode.SCIENCE







async def led_control_loop():
    '''Worker that operates the leds. Since it polls the target state, it can
    take upto a second to respond. We use an asyncio routine. A thread might
    seem more accurate but it would requite a locking mechnism to avoid
    concurrent hardware access, the timing accuracy would still be limited by
    other coroutines running the dome anyway (which also make frequent daq
    calls). The async implementation offers better readability and has
    cancellation built-in. Note how the led_target_state gets superceded by the
    moving dome.
    '''
    state = DreamState()
    hw = Hardware()

    try:
        # Helper function that computed the target state if AUTO is requested,
        # Also overrides the requested target state when the dome is moving :
        def target_state():
            if ( state.dome_state == DomeState.OPENING or
                 state.dome_state == DomeState.CLOSING or
                 state.dome_state == DomeState.STOP and not hw.get_dome_closed_switch()):
                return LedState.FLASH

            if state.led_target_state == LedState.AUTO:
                #log.info('leds are auto')
                if state.mode == CameraServerMode.FLATS:
                    #log.info('flats')
                    return LedState.BLINK
                else:
                    #log.info('something else')
                    return LedState.OFF
            # else/default case:
            #log.info('leds default')
            return state.led_target_state

        while True:
            #log.info('led loop')
            # beware: we call target_state() multiple times below and rely on the
            # fact that until we first await anything, the result will not
            # change.

            # if we go from a 'not safe' to a 'safe' mode we have to update the
            # ready_since flag in the state:
            if ( state.led_state in [LedState.ON, LedState.FLASH] and
                 target_state() in [LedState.OFF, LedState.BLINK] ):
                state.leds_ready_since = datetime.datetime.now(datetime.timezone.utc)
                log.info('LEDs switched to a non-interfering mode')

            # save it:
            state.led_state = target_state()
            log.info(f'Switching LED\'s to {str(state.led_state)}')

            # And do the actual work:

            # each of the sections below runs until the target_state() helper
            # return another value. This helps avoid spamming the logs with
            # on/off messages when the leds are already in that state.

            if state.led_state is LedState.ON:
                hw.leds_on()
                while target_state() == LedState.ON:
                    await asyncio.sleep(1)

            elif state.led_state is LedState.OFF:
                hw.leds_off()
                while target_state() == LedState.OFF:
                    await asyncio.sleep(1)

            elif state.led_state is LedState.BLINK:
                # Blinking starts with off, then we calculate how long until the
                # next blink starts. For responsiveness we never sleep more than
                # 1 second. Since blinking takes 0.25 seconds and we don't
                # cancel between the start and end of the blink, we subtract
                # that 0.25 from the 1 second margin.
                hw.leds_off()
                while target_state() == LedState.BLINK:
                    _, index, remaining = next_seq(margin=0.1)
                    offset = cfg.EXPOSURETIME / 2 - cfg.EXPTIME_FLATS / 2
                    sleep = (remaining + offset) % cfg.EXPOSURETIME
                    if sleep > 1.0 - cfg.EXPTIME_FLATS:
                        #log.info(f'Blinking in {sleep:.1f}...')
                        await asyncio.sleep(1 - cfg.EXPTIME_FLATS)
                    else:
                        await asyncio.sleep(sleep)
                        #log.info('Blinking LED\'s... on')
                        start = time.time()
                        hw.leds_on()
                        await asyncio.sleep(cfg.EXPTIME_FLATS)
                        hw.leds_off()
                        log.info(f'Blinked for #{index}\'s... (accuracy: {1000*(time.time()-start-cfg.EXPTIME_FLATS):.3f} ms)')
                        state.recent_led_blinks = state.recent_led_blinks[-4:] + [index]


            elif state.led_state is LedState.FLASH:
                while target_state() == LedState.FLASH:
                    hw.leds_cycle()
                    await asyncio.sleep(0.25)



    except asyncio.CancelledError:
        log.info('LED control loop cancelled')
        raise
    finally:
        log.info('Turning LEDs off for shutdown')
        hw.leds_off()


''' Architecture notes: Dome control is separated into several loops that
communicate through a number of flags. Most importantly we distinguish target
state and state. The target state is set by the camera loop or by human operator
in manual operations mode. Under normal operations it has only two possible
values: open or close. The other two options (stop and force-open) are only for
maintenance mode during operationalisarion. The slow control loop looks at this
target state, combines it with a number of overrides and warning conditions, and
if all is clear it sets the actual state to OPENING or CLOSING if a move is
needed. The fast/lowlevel loop picks up on this and moves the dome. Once it is
done it sets the state to OPEN/CLOSED. If cancelled, the fast loop autonomously
closes. There is no other way to stop the fast loop so it is also the fast loop
that does sanity checks (both limit switches activated, or timeout).

Whenever the dome is moving the leds should blink and the command server should
beep to warn any human operators.

The motor and the peltiers share the same power supply. We must temporarily
inhibit the peltier control while moving the dome. After the dome reaches its
new position the power supplies can be used by the peltiers again. The current
limit of 20A applies to both usages and can be set once at startup. Two relays
control if the power is supplied to the motors or to the peltiers. They should
never be active at the same time.

Motor voltage must ramp up linearly for the first 3 revolutions on the encoder
and last 10 revolutions before approaching the known end location. The voltage
while moving will vary between 10 and 24 volts.
'''

async def temperature_loop():
    hw = Hardware()
    state = DreamState()
    monitoring = DreamMonitoring()

    try:
        while True:
            await asyncio.sleep(1)

            # window heaters are completely independent of motor/peltier
            # First, we remember the old state
            prev_heater_state: HeaterState = state.heater_state

            if ('dream_inlet' in monitoring.rh_status and
                'camera_bay' in monitoring.rh_status):

                temperature = monitoring.rh_status['dream_inlet']['temperature']
                humidity = monitoring.rh_status['dream_inlet']['humidity']
                sunalt, rising = sun_alt(temperature=temperature, humidity=humidity)

                # get environment
                T_outside = monitoring.rh_status['dream_inlet']['temperature']
                H_outside = monitoring.rh_status['dream_inlet']['humidity'] / 100
                T_inside = monitoring.rh_status['camera_bay']['temperature']
                H_inside = monitoring.rh_status['camera_bay']['humidity'] / 100

                dewpoint_outside = dewpoint(T_outside, H_outside)
                dewpoint_inside = dewpoint(T_inside, H_inside)

                monitoring.get_logfile('dewpoint_outside').write(f'{time.time()},{dewpoint_outside}\n')
                monitoring.get_logfile('dewpoint_inside').write(f'{time.time()},{dewpoint_inside}\n')
            else:
                dewpoint_inside = dewpoint_outside = None
                sunalt, rising = sun_alt()

            # setting low and high set points depending whether the sun is up:
            SUNSET_THRESHOLD = 2 # Sun altitude at which start night temperature control
            SUNRISE_THRESHOLD = -2 # Sun altitude at which start day temperature control

            # pre-compute the sun threshold which is used for both heaters and peltiers
            threshold = SUNRISE_THRESHOLD if rising else SUNSET_THRESHOLD

            # Decide what to do with the heaters
            if state.heater_target_state is not HeaterState.AUTO:
                # just follow
                state.heater_state = state.heater_target_state
            elif dewpoint_inside is not None: # and mode is AUTO
                # In AUTO mode, the heaters are enabled when there is a risk of
                # condensation on the windows. We compare the temperature and
                # humidity at the inlet vents with those in the camera bay. For
                # each we compute the dewpoint. If either the outside air would
                # condensate at the inside temperature or the inside air would
                # condensate at the outside temperature we enable the heaters.

                if sunalt > threshold:
                    # never use heaters during the day
                    state.heater_state = HeaterState.OFF
                elif T_inside < dewpoint_outside - cfg.DEWPOINT_MARGIN:
                    # water would condensate on outside of window
                    state.heater_state = HeaterState.ON
                elif T_outside < dewpoint_inside - cfg.DEWPOINT_MARGIN:
                    # water would condensate on inside of window
                    state.heater_state = HeaterState.ON
                else:
                    state.heater_state = HeaterState.OFF
            else:
                # this is a pretty big problem, but we don't deal with it here
                # a warning will be issues in the readout routine already
                if state.heater_state == HeaterState.AUTO:
                    # AUTO means OFF in this case
                    state.heater_state = HeaterState.OFF

            # Apply result to heater relay
            if prev_heater_state is not state.heater_state:
                if state.heater_state is HeaterState.ON:
                    hw.set_heater_on()
                else:
                    hw.set_heater_off()

            # Peltiers
            # Remember the old state:
            prev_peltier_state: PeltierState = state.peltier_state

            # Follow the target state:
            state.peltier_state = state.peltier_target_state

            # in case of AUTO: apply some logic:
            if state.peltier_target_state is PeltierState.AUTO:
                # Get the temp inside Dream
                if 'camera_bay' in monitoring.rh_status:
                    temp = monitoring.rh_status['camera_bay']['temperature']
                    min_temp = cfg.DAY_MIN_TEMP if sunalt > threshold else cfg.NIGHT_MIN_TEMP
                    max_temp = cfg.DAY_MAX_TEMP if sunalt > threshold else cfg.NIGHT_MAX_TEMP

                    if temp < min_temp:
                        state.peltier_state = PeltierState.HEAT
                    elif temp >= max_temp:
                        state.peltier_state = PeltierState.COOL
                    else:
                        state.peltier_state = PeltierState.OFF
                else:
                    # This is a pretty serious issue. We don't deal with it here
                    # Just turn off the pelties.
                    state.peltier_state = PeltierState.OFF

            # When the dome is moving: override to OFF
            # if the dome is moving, do not enable the peltiers
            if state.dome_state not in [DomeState.CLOSED, DomeState.OPEN, DomeState.STOP]:
                state.peltier_state = PeltierState.OFF

            # Apply result to relay if it has changed:
            if state.peltier_state is not prev_peltier_state:
                if state.peltier_state is PeltierState.HEAT:
                    hw.set_ps_voltage(cfg.PELTIER_HEATING_SETPOINT)
                    hw.set_peltiers_on()
                    hw.set_peltiers_to_heat()
                elif state.peltier_state is PeltierState.COOL:
                    hw.set_ps_voltage(cfg.PELTIER_COOLING_SETPOINT)
                    hw.set_peltiers_on()
                    hw.set_peltiers_to_cool()
                else:
                    hw.set_peltiers_off()


    except asyncio.CancelledError:
        log.info('Stopping temperature control..')
        hw.set_peltiers_off()
        hw.set_heater_off()


async def move_dome(direction: DomeState):
    '''This function attempts one move in the specified direction. If an error
    occurs before it can finish, it simply gives up. It also checks the the move
    is still requested and that the backdoor remains closed.

    '''
    hw = Hardware()
    state = DreamState()
    monitoring = DreamMonitoring()

    try:
        send_slack_message(f'Dream is {direction.value}')
        # delay a little bit, to inform users that the dome is about to open
        # inside the try-catch because a cancel here should trigger the emergency close
        for i in range(cfg.DOME_MOTION_DELAY, 0, -1):
            log.info(f'{direction.value} the dome in {i}...')
            await asyncio.sleep(1)

        # get ready
        if direction == DomeState.CLOSING:
            hw.set_motor_closing()
        else:
            hw.set_motor_opening()

        # Since we have no threads we can safely switch off the peltiers
        hw.set_peltiers_off()
        # it could take upto 1 second for the temperature control loop
        # to pick up on the opening/closing state. However, since we use
        # asyncio there is no possibility of race conditions and no need
        # for locking/events to synchonize access to the psu.

        # enable the power supplies, start voltage at 0.0
        hw.set_ps_voltage(0.)
        hw.set_motor_on()

        log.info('Motor ready')

        # record start position and time of move
        move_start_position = hw.get_dome_position()
        move_start_time = time.time()

        # This is only used to throttle the motor log bit
        prev_log_time = None

        # do the move, as long as it is requested, or until done
        while state.dome_state == direction:
            # get current position
            position = hw.get_dome_position()
            dome_open_switch = hw.get_dome_open_switch()
            dome_closed_switch = hw.get_dome_closed_switch()

            monitoring.get_logfile('dome_open_switch').write(f'{time.time()},{dome_open_switch}\n')
            monitoring.get_logfile('dome_closed_switch').write(f'{time.time()},{dome_closed_switch}\n')
            monitoring.get_logfile('dome_position').write(f'{time.time()},{position}\n')

            # test if we are done
            if dome_closed_switch and direction == DomeState.CLOSING:
                log.info('Dome closed')
                state.dome_state = DomeState.CLOSED
                state.dome_last_move = datetime.datetime.now(datetime.timezone.utc)
                send_slack_message(f'Dream is CLOSED')
                break
            if dome_open_switch and direction == DomeState.OPENING:
                log.info('Dome open')
                state.dome_state = DomeState.OPEN
                state.dome_last_move = datetime.datetime.now(datetime.timezone.utc)
                send_slack_message(f'Dream is OPEN')
                break

            # test if the backdoor is closed:
            if not hw.get_backdoor_switch():
                log.info('Aborting because back door was opened')
                state.dome_state = DomeState.STOP
                state.dome_last_move = datetime.datetime.now(datetime.timezone.utc)
                break

            # test if we are running out of margin:
            if (not dome_open_switch
                and direction == DomeState.OPENING
                and position < cfg.DOME_OPEN_POSITION - cfg.DOME_MARGIN):
                raise RuntimeError('Dome should be open but endstop not hit')
            if (not dome_closed_switch
                and direction == DomeState.CLOSING
                and position > cfg.DOME_CLOSED_POSITION + cfg.DOME_MARGIN):
                raise RuntimeError('Dome should be closed but endstop not hit')

            # detect a timeout
            if time.time() - move_start_time > 120:
                log.error('Dome move took more than 120(s)')
                raise TimeoutError

            # sanity check:
            if dome_open_switch and dome_closed_switch:
                raise RuntimeError('Inconsistent dome limit switches')

            # set an appropriate motor voltage
            dome_range = cfg.DOME_CLOSED_POSITION - cfg.DOME_OPEN_POSITION
            if direction == DomeState.OPENING:
                # calculate the transition points P1 and P2
                P1 = (cfg.DOME_CLOSED_POSITION -
                      dome_range * cfg.DOME_OPENING_STARTRAMP_FRACTION)
                P2 = (cfg.DOME_OPEN_POSITION +
                      dome_range * cfg.DOME_OPENING_FINALRAMP_FRACTION)

                # determine the ramp phase and calculate voltage
                if position > P1:
                    voltage = map_range(position,
                                        cfg.DOME_CLOSED_POSITION, P1,
                                        cfg.DOME_OPENING_START_VOLTAGE,
                                        cfg.DOME_OPENING_NOMINAL_VOLTAGE)
                elif position > P2:
                    voltage = cfg.DOME_OPENING_NOMINAL_VOLTAGE
                else:
                    voltage = map_range(position,
                                        P2, cfg.DOME_OPEN_POSITION,
                                        cfg.DOME_OPENING_NOMINAL_VOLTAGE,
                                        cfg.DOME_OPENING_FINAL_VOLTAGE)
            else:
                # calculate the transition points P1 and P2
                P1 = (cfg.DOME_OPEN_POSITION +
                      dome_range * cfg.DOME_CLOSING_STARTRAMP_FRACTION)
                P2 = (cfg.DOME_CLOSED_POSITION -
                      dome_range * cfg.DOME_CLOSING_FINALRAMP_FRACTION)

                # determine the ramp phase and calculate voltage
                if position < P1:
                    voltage = map_range(position,
                                        cfg.DOME_OPEN_POSITION, P1,
                                        cfg.DOME_CLOSING_START_VOLTAGE,
                                        cfg.DOME_CLOSING_NOMINAL_VOLTAGE)
                elif position < P2:
                    voltage = cfg.DOME_CLOSING_NOMINAL_VOLTAGE
                else:
                    voltage = map_range(position,
                                        P2, cfg.DOME_CLOSED_POSITION,
                                        cfg.DOME_CLOSING_NOMINAL_VOLTAGE,
                                        cfg.DOME_CLOSING_FINAL_VOLTAGE)

            # apply
            hw.set_ps_voltage(voltage)

            # Throttle this log because it really floods the output
            if not prev_log_time or time.time() > prev_log_time + 0.1:
                log.info(f'Motor voltage to {voltage:.1f} (V) dome position {position:.1f}')
                prev_log_time = time.time()

            # log a trace
            monitoring.get_logfile('voltage_setpoint').write(f'{time.time()},{hw.setpoint_voltage}\n')
            monitoring.get_logfile('current_setpoint').write(f'{time.time()},{hw.setpoint_current}\n')
            monitoring.get_logfile('voltage_feedback').write(f'{time.time()},{hw.get_ps_voltages()}\n')
            monitoring.get_logfile('current_feedback').write(f'{time.time()},{hw.get_ps_currents()}\n')

            # yield to other loops, but sleep as short as posible
            # TODO: how many are there in a full open/close move? if more than 50% of total, then filter some(?)
            await asyncio.sleep(0.)

        log.info('Move done')
        hw.set_motor_off()
        hw.set_ps_voltage(0.0)
        log.info('Motors off')

    except asyncio.CancelledError:
        log.error('move_dome cancelled')
        # Cleanup will happen in the wrapper
        raise
    except TimeoutError:
        # TODO: should errors persist to the next restart of the software to
        # enforce a manual clear in case of really unpexpected errors?
        # TODO: yes, after a crash block opening.
        send_slack_message('Dome move timed out!')
        if direction == DomeState.OPENING:
            log.error('Dome move timed out while opening')
        else:
            log.error('Dome move timed out while closing')
        raise
    except Exception as e:
        # asyncio cancel and any other exception are treated equally here
        # stop the motor, then let it bubble
        log.error('Exception while moving the dome!')
        log.exception(e)
        send_slack_message(f'Exception in dome move: {e}')
        raise


async def dome_fast_control_loop():
    '''Loop that takes the target state and does the motor control. It does fast
       polling of the switches and encoder. If the master state changes mid-open
       or mid-close this is detected and the direction of the motor is reversed
       or the motor is stopped if necessary.

       A note on cancellation. It is really important that we don't leave the
       dome open in case of errors. Therefore we catch any cancellations and
       errors inside the inner loop rather than the outer. If something goes
       wrong, an attempt is made to still close the dome.

    '''
    # The main routine continuously checks if moves are needed. If so, one full
    # move is attempted before checking again
    state = DreamState()
    hw = Hardware()

    try:
        while True:
            if state.dome_state == DomeState.OPENING:
                log.info('Initiating a dome OPENING move')
                await move_dome(DomeState.OPENING)
            if state.dome_state == DomeState.CLOSING:
                log.info('Initiaying a dome CLOSING move')
                await move_dome(DomeState.CLOSING)

            await asyncio.sleep(1)
    finally:
        log.info('Motor control finished. Setting power supplies to 0(V) 0(A)')
        hw.set_motor_off()
        hw.set_ps_voltage(0)
        hw.set_ps_current(0)


async def emergency_close_if_needed():
    hw = Hardware()

    # skip if nog needed
    if hw.get_dome_closed_switch():
        return

    # skip in maintenance mode
    if cfg.SKIP_EMERGENCY_CLOSE:
        log.warning('Emergency Dome close skipped in maintenance mode!')
        send_slack_message('Emergency Dome close skipped in maintenance mode')
        return

    # The actual move
    log.warning('Doing an emergency close...')
    try:
        await emergency_close()
    except asyncio.CancelledError:
        log.error('Emergency close cancelled!')
        send_slack_message('Emergency dome close cancelled!', wait=True)


async def emergency_close():
    try:
        state = DreamState()
        # try it once:
        state.dome_state = DomeState.CLOSING
        await move_dome(DomeState.CLOSING)

    except Exception as e:
        log.error('An error occured during the emergency dome close.')
        log.exception(e)
        raise

    # check if it worked:
    if state.dome_state == DomeState.CLOSED:
        log.warning('Emergency close successful')
    else:
        raise RuntimeError('Dome failed to do the emergency close!')


async def dome_slow_control_loop():
    '''This routine combines the different reasons for the dome to open and/or
    close at any given time. During normal operations the dome_request_state is
    AUTO and the dome is controlled by the weather flag from rubin and the
    calibration state of the station. There are several error conditions/events
    that should cause the dome to stop/close. The TUI can set the requested
    target state to open, closed or stopped. This overrides the position with a
    low priority, keeping the dome closed in unsafe observing conditions. During
    testing the FORCEOPEN can be requested which overrides the dome position
    with high priority. Only an open backdoor or critical hardware failure will
    cause the dome to stop in this case. The following reasons determine the
    dome position (in descending order of importance):

    * open backdoor -> stop
    * force open selected in tui -> open
    * UPS powersaving mode -> close
    * humidity gets too high -> close
    * open/close/stop selected from tui
    * last close event less than 5 minutes ago -> close
    * bad weather signal from Rubin -> close
    * calibrations in progress -> close
    * nighttime -> open
    '''
    hw = Hardware()
    state = DreamState()

    # set the PSU's to limit to 20A
    # This is never changed until station shutdown
    hw.set_ps_current(20.)

    try:
        while True:
            # compute if we need to move according to science mode
            # only open the dome for science images
            must_open = state.mode == CameraServerMode.SCIENCE
            must_close = not must_open

            # override from requested state
            if state.dome_target_state == DomeTargetState.OPEN:
                must_open = True
                must_close = False
            if state.dome_target_state == DomeTargetState.CLOSED:
                must_open = False
                must_close = True

            # override if we didn't get a go/nogo or if it's a nogo from rubin
            go_nogo = state.get_go_nogo()
            state.set_problem(Warnings.DOME_CLOSED_BY_NOGO, not go_nogo)
            if not go_nogo:
                must_open = False
                must_close = True

            # test if last close was too recent
            now = datetime.datetime.now(datetime.timezone.utc)
            when = state.dome_last_closed + datetime.timedelta(seconds = cfg.DOME_MINIMUM_CLOSE_TIME)
            state.set_problem(Warnings.DOME_CLOSED_BY_TIMER, must_open and now < when)
            if must_open and now < when:
                #log.info(f'Postponing Dome Open until {when.strftime("%X")} to enforce minimum close time ({cfg.DOME_MINIMUM_CLOSE_TIME/60:.0f} m)')
                must_open = False
                must_close = True

            # check that the sun is down
            sun, rising = sun_alt()
            if rising:
                should_close = sun > cfg.SUN_ALT_SUNRISE_CLOSEDOME
            else:
                should_close = sun > cfg.SUN_ALT_SUNSET_OPENDOME
            state.set_problem(Warnings.DOME_CLOSED_BY_SUN, should_close)
            if should_close:
                must_open = False
                must_close = True

            # override if there are humidity issues
            state.set_problem(Warnings.DOME_CLOSED_BY_ENV, not state.env_ok)
            if not state.env_ok:
                # log.info('Humidty and/or temparature at Dream too high')
                must_close = True
                must_open = False

            # act on UPS level
            state.set_problem(Warnings.DOME_CLOSED_BY_UPS, not state.ups_ok)
            if not state.ups_ok:
                # log.info('Dome set to close because the UPS reports <50%')
                must_close = True
                must_open = False

            # if stop or force open are selected they get extra high prio
            state.set_problem(Warnings.DOME_STOPPED, state.dome_target_state == DomeTargetState.STOP)
            if state.dome_target_state == DomeTargetState.STOP:
                must_open = False
                must_close = False

            state.set_problem(Warnings.DOME_FORCED_OPED, state.dome_target_state == DomeTargetState.FORCEOPEN)
            if state.dome_target_state == DomeTargetState.FORCEOPEN:
                must_open = True
                must_close = False

            # override from backdoor:
            state.set_problem(Warnings.DOME_CLOSED_BY_DOOR, not hw.get_backdoor_switch())
            if not hw.get_backdoor_switch():
                must_close = False
                must_open = False

            # closing on error/exceptions happens in the fast loop and therefore
            # automatically takes higher precedence than anything we set here.
            # Since we do want to stop the dome on exit in maintenance mode
            # rather than close it, we have to handle that there. See fast loop
            # for more comments.

            # apply
            if must_open and state.dome_state != DomeState.OPEN:
                if state.dome_state != DomeState.OPENING:
                    log.info('Dome should be OPENING')
                state.dome_state = DomeState.OPENING
            if must_close and state.dome_state != DomeState.CLOSED:
                state.dome_last_closed = datetime.datetime.now(datetime.timezone.utc)
                if state.dome_state != DomeState.CLOSING:
                    log.info('Dome should be CLOSING')
                    nextopen = state.dome_last_closed + datetime.timedelta(seconds=cfg.DOME_MINIMUM_CLOSE_TIME)
                    log.info(f'Postponing the next opening move until no earlier than {nextopen.isoformat(timespec="seconds")}')
                state.dome_state = DomeState.CLOSING


            if not must_close and not must_open and state.dome_state != DomeState.STOP:
                #log.info('Stopping the dome')
                state.dome_state = DomeState.STOP

            # sleep
            await asyncio.sleep(1.0)

    except asyncio.CancelledError as e:
        log.info('Dome slow control loop cancelled')
        raise
