#!/usr/bin/env python3

from dataclasses import dataclass
import datetime
import enum
import numpy as np
from typing import Optional, Union, Literal, List

class CameraServerMode(str, enum.Enum):
    """Enum representing the current mode of operation. Making the enum values
    equal to the names makes it easier to recognise and (de-)serialize values.
    Subclassing str makes the serialisation even transparent. Deserialize still
    needs an explicit call to the CameraServerState constructor.
    """
    AUTO = 'AUTO'       # Let the control software decide based on camera input
    IDLE = 'IDLE'       # Disconnect from camera
    STANDBY = 'STANDBY' # connect to camera and start cooldown
    BLANKS = 'BLANKS'   # 6.4s exposures that are discarded during thermalisation
    BIAS = 'BIAS'       # 0.0s exposures at 6.4s intervals
    FLATS = 'FLATS'     # 6.4s exposures with leds on
    DARKS = 'DARKS'     # 6.4s exposures with the leds off and dome closed
    SCIENCE = 'SCIENCE' # 6.4s exposures with dome open
    SHUTDOWN = 'SHUTDOWN' # stop and shutdown the computer

@dataclass
class TriggerInfo():
    seq: int
    triggertime: datetime.datetime
    timing_latency: float
    usb_latency: float
    artificial_latency: float
    imagetype: str
    ccdtemp: float

@dataclass
class ImageInfo(TriggerInfo):
    download_time: float
    download_start: datetime.datetime
    download_margin: float
    data: np.ndarray
    min: int
    max: int
    med: int
    avg: int

class DomeTargetState(str, enum.Enum):
    AUTO = 'AUTO'
    OPEN = 'OPEN'
    CLOSED = 'CLOSED'
    STOP = 'STOP' # stop the motor, even if the dome is half open
    FORCEOPEN = 'FORCEOPEN' # only ignored when backdoor is open or in case of critical hardware failure

class DomeState(str, enum.Enum):
    OPEN = 'OPEN'
    CLOSED = 'CLOSED'
    OPENING = 'OPENING'
    CLOSING = 'CLOSING'
    STOP = 'STOP'

# For the Leds, the window heaters and the peltiers the difference between the
# target and the actual state is only the addition of AUTO which can be the
# target but never the actual state.
class LedState(str, enum.Enum):
    AUTO = 'AUTO'
    ON = 'ON'
    OFF = 'OFF'
    BLINK = 'BLINK' # for flats
    FLASH = 'FLASH' # for dome alarm

class HeaterState(str, enum.Enum):
    AUTO = 'AUTO'
    ON = 'ON'
    OFF = 'OFF'
    UNKNOWN = 'UNKNOWN'

class PeltierState(str, enum.Enum):
    AUTO = 'AUTO'
    COOL = 'COOL'
    HEAT = 'HEAT'
    OFF  = 'OFF'
    UNKNOWN = 'UNKNOWN'


@dataclass
class CameraControlInfo():
    '''This represents exactly one heartbeat package from a camera computer.'''
    name: str
    state: CameraServerMode
    ccd_temp: Optional[float] # optional because in IDLE we cannot read ccd temp
    num_blanks: int
    num_darks: int
    num_bias: int
    num_flats: int
    num_science: int
    num_missed: int
    time: datetime.datetime

@dataclass
class CameraClientInfo():
    '''This reprents everything we display and log. Including the info from the heartbeat.
    But also the last image and client identifier.'''
    client_id: str                                   # from 0mq message
    heartbeat: datetime.datetime                     # timestamp of last received heartbeat
    client_type: Literal['script', 'camera', 'gui']  # which type of client (only used for rendering in the gui)
    camera_data: Optional[CameraControlInfo] = None  # last heartbeat, None if not received or if script client
    image_data: Optional[ImageInfo] = None           # last image, None if not received or if script client

@dataclass
class RubinClientInfo():
    '''Representation of the data we get from rubin'''
    ip_address: str
    last_request_time: datetime.datetime
    last_request_id: Union[str, int]
    weather_ok: bool
    weather_updated: datetime.datetime
    dome_ok: bool

@dataclass
class DataProduct():
    kind: Literal['cloud', 'image']  # possibly in the future 'calibration', 'lightcurve'
    type: Optional[Literal['dark', 'flat', 'bias', 'science']]
    seq: List[int]
    start: datetime.datetime
    end: datetime.datetime
    server: Literal['N', 'E', 'S', 'W', 'C', 'B']
    size: int  # in bytes
    filename: str
    sha256: str

class Errors(str, enum.Enum):
    # unexpected events
    LIMIT_SWITCH_ERROR         = 'Both open and closed are pushed'
    DOME_ERROR                 = 'Dome should be closed but it is not'
    # things that could happen but need to be fixed if not intentional
    BACKDOOR_OPEN              = 'Backdoor is open'
    DOOR_OPEN                  = 'Door is open'
    # missing/unreachable hardware
    UPS_ERROR                  = 'UPS not reachable or not responding'
    PDU1_ERROR                 = 'PDU 1 not reachable'
    PDU2_ERROR                 = 'PDU 2 not reachable'
    TEMPHUM_ERROR              = 'Temp hum sensor not reachable'
    PSU_ERROR                  = 'PSU reports a problem'
    UPS_BATTERY_NEEDS_REPLACE  = 'UPS battery needs replacing'
    # environment errors
    CAMERA_BAY_TEMP_ERROR      = 'Camera bay temperature too high'
    CAMERA_BAY_HUM_ERROR       = 'Humidity in camera bay too high'
    ELECTRONICS_HUMIDITY_ERROR = 'Humidity in electronics box > limit'
    DOME_HUMIDITY_ERROR        = 'Humidty under dome > limit'


class Warnings(str, enum.Enum):
    # highlevel station related flags:
    POWERSAVING_ACTIVATED      = 'Power saving mode activated'
    UPS_ON_BATTERY             = 'UPS is on battery'
    # dome decision insights
    DOME_CLOSED_BY_NOGO        = 'Dome opening blocked by NO-GO'
    DOME_CLOSED_BY_SUN         = 'Dome opening blocked by sun alt'
    DOME_CLOSED_BY_ENV         = 'Dome opening blocked by temp/hum'
    DOME_CLOSED_BY_UPS         = 'Dome opening blocked by UPS error'
    DOME_CLOSED_BY_TIMER       = 'Dome opening blocked by timer'
    DOME_STOPPED               = 'User stopped dome movement'
    DOME_FORCED_OPED           = 'User forced dome open'
    DOME_CLOSED_BY_DOOR        = 'Dome opening blocked by backdoor'
    # rubin client related flags:
    RUBIN_NO_CLIENT            = 'Rubin client not connected'
    RUBIN_MULTIPLE_CLIENTS     = 'More than 1 client on rubin socket'
    RUBIN_CLIENT_STALE         = 'Rubin client update too old'
    RUBIN_WEATHER_TOO_OLD      = 'Rubin-provided weather flag too old'
    RUBIN_BAD_WEATHER          = 'Bad weather flag set by Rubin'
    RUBIN_NOGO                 = 'NO-GO for dome open set by Rubin'
    # script client
    SCRIPT_CLIENT_ACTIVE       = 'There is an active script client'
    # environment warning
    INLET_HUM_WARNING          = 'Dream inlet humidity high'
    INLET_TEMP_WARNING         = 'Dream inlet temperature low'
    # warnings about missing cameras:
    CAMERA_N_NOT_CONNECTED     = 'North camera not connected'
    CAMERA_S_NOT_CONNECTED     = 'South camera not connected'
    CAMERA_E_NOT_CONNECTED     = 'East camera not connected'
    CAMERA_W_NOT_CONNECTED     = 'West camera not connected'
    CAMERA_C_NOT_CONNECTED     = 'Center camera not connected'
    # simulator warnings
    SIMULATED_DOME             = 'Dome movements are simulated'
    SIMULATED_LEDS             = 'LEDs are simulated'
    SIMULATED_UPS              = 'UPS is simulated'
    SIMULATED_PDU              = 'PDU is simulated'
    SIMULATED_ENV              = 'Temp/humidity sensors are simulated'
    SIMULATED_SUN              = 'Sun angle is simulated'
