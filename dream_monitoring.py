#!/usr/bin/env python3

import asyncio
import dream_config as cfg
from dream_state import DreamState, Warnings, Errors
from dream_electronics import Hardware, PDUInterface
from dream_utils import Singleton, get_current_night, sun_alt
import logging
import datetime
import time
import os
import pytz
import psutil

log = logging.getLogger(cfg.LOGGER_NAME)


class DreamMonitoring(object, metaclass=Singleton):
    '''A class wrapper everything to do with monitoring.

    * The monitoring loop

    * a cache of the snmp values because snmp is a bit slow to query on the fly

    * a simple callback mechanism used by the gui for quick refreshes.

    '''

    def __init__(self):
        self.sensor_callbacks = []
        self.rh_status = {}
        self.ups_status = {}
        self.pdu_status = {}
        self.filehandles = {}

    def get_logfile(self, field: str):
        if field not in self.filehandles:
            # determine current night for file base
            thisnight = get_current_night(format='%Y_%m_%d')

            # determine the filename for this field
            filename = os.path.join(
                cfg.LOG_PATH, thisnight, f'{field}.csv'
            )

            # open and add to handles
            os.makedirs(os.path.dirname(filename), exist_ok=True)
            self.filehandles[field] = open(filename, 'a')

        # return the existing or newly created file handle
        return self.filehandles[field]

    async def logrotate(self):
        '''rotate logs once per day at local noon.'''
        lastnight = get_current_night()
        while True:
            if lastnight != get_current_night():
                # close all log streams, forcing them to be reopened on next write
                for f in self.filehandles.values():
                    f.close()
                self.filehandles = {}

                # rotate python logging file destination
                for handler in log.handlers:
                    if isinstance(handler, logging.handlers.WatchedFileHandler):
                        runningname = handler.baseFilename
                        archivename = os.path.splitext(runningname)[0] + '_' + lastnight + '.log'
                        os.rename(runningname, archivename)

                # save new current night
                lastnight = get_current_night()

            # flush all log files at least once per minute
            for f in self.filehandles.values():
                f.flush()

            # sleep for a while
            await asyncio.sleep(60)

    async def temp_hum_loop(self, update_period: float = 1.0):
        hw = Hardware()
        state = DreamState()

        while True:
            # update
            rh_status = {}
            errors = False
            for tmp in hw.tmp:
                try:
                    rh_status = rh_status | await tmp.get_all()
                except asyncio.CancelledError:
                    raise
                except:
                    errors = True

            state.set_problem(Errors.TEMPHUM_ERROR, errors)
            self.rh_status = rh_status

            # log results
            for location, data in self.rh_status.items():
                for field, value in data.items():
                    f = self.get_logfile(f'{location}_{field}')
                    f.write(f'{time.time()},{value}\n')

            # update the station ok flag
            # missing sensor data triggers bigger warnings elsewhere
            camera_bay_temp_ok = self.rh_status['camera_bay']['temperature'] < cfg.TEMP_MAX_CAMERA_BAY if 'camera_bay' in self.rh_status else True
            camera_bay_hum_ok = self.rh_status['camera_bay']['humidity'] < cfg.HUM_MAX_CAMERA_BAY if 'camera_bay' in self.rh_status else True
            box_hum_ok = self.rh_status['electronics_box']['humidity'] < cfg.HUM_MAX_ELECTRONICS if 'electronics_box' in self.rh_status else True
            top_hum_ok = self.rh_status['electronics_top']['humidity'] < cfg.HUM_MAX_DOME if 'electronics_top' in self.rh_status else True
            inlet_temp_ok = self.rh_status['dream_inlet']['temperature'] > cfg.TEMP_MIN_INLET if 'dream_inlet' in self.rh_status else True
            inlet_hum_ok = self.rh_status['dream_inlet']['humidity'] < cfg.HUM_MAX_INLET if 'dream_inlet' in self.rh_status else True


            state.env_ok = (
                'camera_bay' in self.rh_status and
                'electronics_box' in self.rh_status and
                'electronics_top' in self.rh_status and
                camera_bay_hum_ok and
                camera_bay_temp_ok and
                box_hum_ok and
                top_hum_ok)

            state.set_problem(Errors.CAMERA_BAY_TEMP_ERROR, not camera_bay_temp_ok)
            state.set_problem(Errors.CAMERA_BAY_HUM_ERROR, not camera_bay_hum_ok)
            state.set_problem(Errors.ELECTRONICS_HUMIDITY_ERROR, not box_hum_ok)
            state.set_problem(Errors.DOME_HUMIDITY_ERROR, not top_hum_ok)

            state.set_problem(Warnings.INLET_HUM_WARNING, not inlet_hum_ok)
            state.set_problem(Warnings.INLET_TEMP_WARNING, not inlet_temp_ok)

            # to do one update per second we wait until the next full period.
            # note the negative sign and the fact that modulo in python always
            # returns the same sign as the divisor, i.e. positive here. This
            # approach targets 1 update per second, does not drift, but also
            # does not queue up old updates if lag does occur.
            remaining = (- time.time()) % update_period
            await asyncio.sleep(remaining)

    async def pdu_loop(self, pdunum: int, update_period: float = 1.0):
        state = DreamState()
        hw = Hardware()

        pdu = hw.pdu1 if pdunum == 1 else hw.pdu2
        errflag = Errors.PDU1_ERROR if pdunum == 1 else Errors.PDU2_ERROR

        while True:
            # Do the update
            try:
                status = await pdu.get_status()
            except asyncio.CancelledError:
                raise
            except:
                # if anything at all goes
                state.set_problem(errflag, True)
            else:
                state.set_problem(errflag, False)

                # translate list of outlets states to to a named dict
                outlets = {name: status[port-1] for name, (num, port) in cfg.PDU_OUTLETS.items() if pdunum==num}

                # log the update:
                for key, value in outlets.items():
                    key = key.replace(' ', '_')
                    f = self.get_logfile(f'pdu_{key}')
                    f.write(f'{time.time()},{value}\n')

                # merge with self.pdu_status
                self.pdu_status |= outlets

            # to do one update per second we wait until the next full period.
            # note the negative sign and the fact that modulo in python always
            # returns the same sign as the divisor, i.e. positive here. This
            # approach targets 1 update per second, does not drift, but also
            # does not queue up old updates if lag does occur.
            remaining = (- time.time()) % update_period
            await asyncio.sleep(remaining)

    async def ups_loop(self, update_period: float = 1.0):
        hw = Hardware()
        state = DreamState()
        last_online = time.time()

        while True:
            # Do the update
            try:
                self.ups_status = await hw.ups.get_status()
            except asyncio.CancelledError:
                raise
            except Exception as e:
                # if anything at all goes wrong
                log.error(e)
                self.ups_status = {}
                state.set_problem(Errors.UPS_ERROR, True)
            else:
                state.set_problem(Errors.UPS_ERROR, False)

            # log the update:
            for key, value in self.ups_status.items():
                key = key.replace(' ', '_')
                f = self.get_logfile(f'ups_{key}')
                f.write(f'{time.time()},{value}\n')

            # record last online
            if 'ups_status' in self.ups_status and self.ups_status['ups_status'] == 'ONLINE':
                last_online = time.time()
            self.ups_status['last_online'] = last_online

            # check battery health
            battery_needs_replacing = 'battery_needs_replacing' in self.ups_status and self.ups_status['battery_needs_replacing']

            # update the station ok flag
            state.ups_ok = (
                time.time() - self.ups_status['last_online'] < cfg.UPS_MAX_POWERGAP and
                state.mode != 'SHUTDOWN' and
                not battery_needs_replacing)


            state.set_problem(Warnings.UPS_ON_BATTERY, 'ups_status' in self.ups_status and self.ups_status['ups_status'] != 'ONLINE')
            state.set_problem(Errors.UPS_BATTERY_NEEDS_REPLACE, battery_needs_replacing)


            # to do one update per second we wait until the next full period.
            # note the negative sign and the fact that modulo in python always
            # returns the same sign as the divisor, i.e. positive here. This
            # approach targets 1 update per second, does not drift, but also
            # does not queue up old updates if lag does occur.
            remaining = (- time.time()) % update_period
            await asyncio.sleep(remaining)

    async def other_loop(self, update_period: float = 1.0):
        hw = Hardware()
        state = DreamState()

        while True:
            # Log the encoder position
            f = self.get_logfile(f'encoder')
            f.write(f'{time.time()},{hw.get_dome_position()}\n')

            # Log the DAQ hardware status
            fields = {
                'psu_voltage_setpoint': hw.setpoint_voltage,
                'psu_voltage_feedback': hw.get_ps_voltages(),
                'psu_current_setpoint': hw.setpoint_current,
                'psu_current_feedback': hw.get_ps_currents(),
                'psu_status_temperr': hw.get_ps_status()[0],
                'psu_status_inputerr': hw.get_ps_status()[1],
                'endstop_dome_open': hw.get_dome_open_switch(),
                'endstop_dome_closed': hw.get_dome_closed_switch(),
                'endstop_frontdoor': hw.get_door_switch(),
                'endstop_backdoor': hw.get_backdoor_switch(),
                'relay_motor_onoff': hw.motor_relay,
                'relay_motor_dir': hw.motor_dir_relay,
                'relay_peltiers_onoff': hw.peltier_relay,
                'relay_peltiers_dir': hw.peltier_dir_relay,
                'relay_heater': hw.heater_relay,
                # leds are not monitored here
            }
            for key, value in fields.items():
                f = self.get_logfile(key)
                f.write(f'{time.time()},{value}\n')

            # set some warnings
            state.set_problem(Errors.BACKDOOR_OPEN, not hw.get_backdoor_switch())
            state.set_problem(Errors.DOOR_OPEN, not hw.get_door_switch())
            state.set_problem(Errors.PSU_ERROR, not all(hw.get_ps_status()))
            state.set_problem(Errors.LIMIT_SWITCH_ERROR, hw.get_dome_closed_switch() and hw.get_dome_open_switch())

            # independent check that the dome is closed
            # since this is so important, we implement a redundant warning independent of the state, the config, etc.
            sun, _ = sun_alt()
            state.set_problem(Errors.DOME_ERROR, sun > -4 and not hw.get_dome_closed_switch())

            # rubin client info:
            # logged when it changes. See dream rubin server

            # Dream station state/target state: Not logged because it's probably
            # hard to plot and can be retrieved from the python loggging output
            # if needed for debugging

            # Camera status: Not logged because it's probably hard to plot and
            # can be retrieved from the log if needed for debugging

            # to do one update per second we wait until the next full period.
            # note the negative sign and the fact that modulo in python always
            # returns the same sign as the divisor, i.e. positive here. This
            # approach targets 1 update per update priod, does not drift, but
            # also does not queue up old updates if lag does occur.

            mem = psutil.virtual_memory()
            memfree = mem.available / mem.total
            f = self.get_logfile('memfree')
            f.write(f'{time.time()},{memfree}\n')

            load1, load5, load15 = os.getloadavg()
            f = self.get_logfile('load1')
            f.write(f'{time.time()},{load1}\n')

            for disk in ['', 'tmpdata', 'data', 'home']:
                path = '/'+disk
                if not os.path.exists(path):
                    continue
                stats = os.statvfs(path)
                available = stats.f_bavail * stats.f_bsize / 1024 # 1K block, just like df
                f = self.get_logfile(f'disk_{disk}')
                f.write(f'{time.time()},{available}\n')

            remaining = (- time.time()) % update_period
            await asyncio.sleep(remaining)

    async def monitoring_loop(self, update_period: float = 1.0):
        '''Run the loops for each device separately. One loop does the usb
        devices, which are generally fast.

        '''
        async with asyncio.TaskGroup() as tg:
            tg.create_task(self.pdu_loop(1, update_period))
            tg.create_task(self.pdu_loop(2, update_period))
            tg.create_task(self.ups_loop(update_period))
            tg.create_task(self.temp_hum_loop(update_period))
            tg.create_task(self.other_loop(update_period))
