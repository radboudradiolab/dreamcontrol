import asyncio
import datetime
import logging

import matplotlib
import numpy as np
import pytz
from pylab import cm
from rich import box
from rich.console import group
from rich.rule import Rule
from rich.table import Table
from rich.text import Text
from rich.styled import Styled
from textual import on
from textual.app import App, ComposeResult, NoMatches
from textual.binding import Binding
from textual.containers import Grid, Horizontal, Vertical
from textual.screen import ModalScreen
from textual.widgets import (Button, Digits, Footer, Header, Label,
                             ProgressBar, RadioButton, RadioSet, RichLog,
                             Static, Switch)

import dream_config as cfg
from dream_command_server import DreamCommandServer
from dream_dataclasses import (CameraServerMode, DomeState, DomeTargetState,
                               Errors, HeaterState, LedState, PeltierState,
                               Warnings)
from dream_electronics import Hardware
from dream_logging import RichLogHandler
from dream_monitoring import DreamMonitoring
from dream_state import DreamState
from dream_utils import (ago, choose, column_grid, image_preview,
                         install_textual_degree_symbol, sun_alt)

# initialize some logging
log = logging.getLogger(cfg.LOGGER_NAME)

# Exception thrown by gui to indicate a clean shutdown This is necessary because
# the TUI library doesn't have a way to cancel the other tasks otherwise
class NormalExit(Exception):
    pass


class DreamCentralGui(App):
    TITLE = "Dream Command"
    CSS_PATH = "dream_command_gui.css"
    BINDINGS = [
        Binding('d', 'toggle_dark', 'Toggle dark mode', False),
        Binding('a', 'toggle_autoscroll', 'Toggle auto-scroll', False),
        ('Ctrl+c', 'quit', 'Quit')
    ]

    def compose(self) -> ComposeResult:
        yield Header(name=self.TITLE)
        yield Horizontal(
            Vertical(
                # State requested to cameras
                Static('Observatory mode:', classes='header'),
                Horizontal(
                    RadioSet(
                        RadioButton('Auto', id='AUTO', classes='statebutton'),
                        RadioButton('Idle', value=True, id='IDLE', classes='statebutton'),
                        RadioButton('Standby', id='STANDBY', classes='statebutton'),
                        RadioButton('Blanks', id='BLANKS', classes='statusbutton'),
                        RadioButton('Bias', id='BIAS', classes='statebutton'),
                        RadioButton('Flats', id='FLATS', classes='statebutton'),
                        RadioButton('Darks', id='DARKS', classes='statebutton'),
                        RadioButton('Science images', id='SCIENCE', classes='statebutton'),
                        RadioButton('Shutdown', id='SHUTDOWN', classes='statebutton'),
                        id='mode_target',
                        classes='target'
                    ),
                    RadioSet(
                        RadioButton('', value=True, id='AUTO', classes='statebutton'),
                        RadioButton('', id='IDLE', classes='statebutton'),
                        RadioButton('', id='STANDBY', classes='statebutton'),
                        RadioButton('', id='BLANKS', classes='statusbutton'),
                        RadioButton('', id='BIAS', classes='statebutton'),
                        RadioButton('', id='FLATS', classes='statebutton'),
                        RadioButton('', id='DARKS', classes='statebutton'),
                        RadioButton('', id='SCIENCE', classes='statebutton'),
                        RadioButton('', id='SHUTDOWN', classes='statebutton'),
                        id='mode_actual',
                        classes='actual',
                        disabled=True
                    ),
                    classes='group'
                ),
                # LEDs
                Static('Leds:', classes='header'),
                Horizontal(
                    RadioSet(
                        RadioButton('Auto', id='AUTO'),
                        RadioButton('All off', id='OFF', value=True),
                        RadioButton('All on', id='ON'),
                        RadioButton('Blink (for flats)', id='BLINK'),
                        RadioButton('Flash (alarm)', id='FLASH'),
                        id='leds_target',
                        name='LED status',
                        classes='target'
                    ),
                    RadioSet(
                        RadioButton('', id='AUTO'),
                        RadioButton('', id='OFF'),
                        RadioButton('', id='ON'),
                        RadioButton('', id='BLINK'),
                        RadioButton('', id='FLASH'),
                        id='leds_actual',
                        name='LED status',
                        classes='actual',
                        disabled=True
                    ),
                    classes='group'
                ),
                # Peltiers:
                Static('Peltier state:', classes='header'),
                Horizontal(
                    RadioSet(
                        RadioButton('Auto', id='AUTO'),
                        RadioButton('Cool', id='COOL'),
                        RadioButton('Heat', id='HEAT'),
                        RadioButton('Off', id='OFF', value=True),
                        id='peltiers_target',
                        classes='target'
                    ),
                    RadioSet(
                        RadioButton('', id='AUTO'),
                        RadioButton('', id='COOL'),
                        RadioButton('', id='HEAT'),
                        RadioButton('', id='OFF'),
                        id='peltiers_actual',
                        classes='actual',
                        disabled=True
                    ),
                    classes='group'
                ),
                # Window Heater:
                Static('Window heater state:', classes='header'),
                Horizontal(
                    RadioSet(
                        RadioButton('Auto', id='AUTO'),
                        RadioButton('On', id='ON'),
                        RadioButton('Off', id='OFF', value=True),
                        id='heater_target',
                        classes='target'
                    ),
                    RadioSet(
                        RadioButton('', id='AUTO'),
                        RadioButton('', id='ON'),
                        RadioButton('', id='OFF'),
                        id='heater_actual',
                        classes='actual',
                        disabled=True
                    ),
                    classes='group'
                ),
                # Request dome state
                Static('Request dome:', classes='header'),
                Horizontal(
                    RadioSet(
                        RadioButton('AUTO', id='AUTO'),
                        RadioButton('Stop', id='STOP', value=True),
                        RadioButton('Closed',  id='CLOSED'),
                        RadioButton('Open', id='OPEN'),
                        RadioButton('Force open', id='FORCEOPEN'),
                        RadioButton('Opening', id='OPENING', disabled=True),
                        RadioButton('Closing', id='CLOSING', disabled=True),
                        id='dome_target',
                        classes='target'
                    ),
                    RadioSet(
                        RadioButton('AUTO', id='AUTO'),
                        RadioButton('Stop', id='STOP', value=True),
                        RadioButton('Closed',  id='CLOSED'),
                        RadioButton('Open', id='OPEN'),
                        RadioButton('Force open', id='FORCEOPEN'),
                        RadioButton('Opening', id='OPENING'),
                        RadioButton('Closing', id='CLOSING'),
                        id='dome_actual',
                        classes='actual',
                        disabled=True
                    ),
                    classes='group'
                ),
                # Actual dome state
                Static('Dome encoder position:', classes='header'),
                Vertical(
                    Digits('', id='dome_pos'),
                    ProgressBar(total=1.0, show_percentage=False, show_eta=False, id='dome_progress'),
                    classes='group'
                ),
                # Actual dome state
                Static('Sun altitude:', classes='header'),
                Vertical(
                    Digits('', id='sun_alt'),
                    classes='group'
                ),

                # Place for persistent warning signs:
                Static('Errors and Warnings:', classes='header'),
                Vertical(
                    *[
                        Static(f' X {error.value}', name=error.value, classes='problem error hidden')
                        for error in Errors],
                    *[
                        Static(f' ! {warning.value}', name=warning.value, classes='problem warn hidden')
                        for warning in Warnings],
                    id='warningarea'
                ),
                classes='leftcolumn'
            ),
            Vertical(
                # Time
                Static('Time:', classes='header'),
                Vertical(
                    Horizontal(
                        Digits('  :  :  ', id='localtime'),
                        Static('(local)'),
                    ),
                    Horizontal(
                        Digits('  :  :  ', id='utctime'),
                        Static('(UTC)'),
                    ),
                    classes='group',
                    id='clocks'
                ),
                # Switches
                Static('Switches:', classes='header'),
                Vertical(
                    Horizontal(
                        Static('  Dome OPEN:'),
                        Switch(disabled = True, id='switch_open'),
                        classes='switch'
                    ),
                    Horizontal(
                        Static('  Dome CLOSED:'),
                        Switch(disabled = True, id='switch_closed'),
                        classes='switch'
                    ),
                    Horizontal(
                        Static('  Door closed:'),
                        Switch(disabled = True, id='switch_door'),
                        classes='switch'
                    ),
                    Horizontal(
                        Static('  Backdoor closed:'),
                        Switch(disabled = True, id='switch_backdoor'),
                        classes='switch'
                    ),
                    classes='group'
                ),

                # Relays
                Static('Relays:', classes='header'),
                Vertical(
                    Horizontal(
                        Static('  Motor:'),
                        Switch(disabled = True, id='relay_motor'),
                        classes='switch'
                    ),
                    Horizontal(
                        Static('  Motor dir: (on=close)'),
                        Switch(disabled = True, id='relay_motor_dir'),
                        classes='switch'
                    ),
                    Horizontal(
                        Static('  Peltier:'),
                        Switch(disabled = True, id='relay_peltier'),
                        classes='switch'
                    ),
                    Horizontal(
                        Static('  Peltier dir: (on=heating)'),
                        Switch(disabled = True, id='relay_peltier_dir'),
                        classes='switch'
                    ),
                    Horizontal(
                        Static('  Heater:'),
                        Switch(disabled = True, id='relay_heater'),
                        classes='switch'
                    ),
                    classes='group'
                ),

                # Temp/hum
                Static('Temp/hum:', classes='header'),
                Static('', id='temphum', classes='group'),

                # PSU
                Static('PSU:', classes='header'),
                Static('', id='psu', classes='group'),

                # UPS
                Static('UPS:', classes='header'),
                Static(' ', id='ups', classes='group'),

                # PDU
                Static('PDU:', classes='header'),
                Vertical(*[ # list comprehension immediate expanded to a list of arg
                    Horizontal(
                        Static(f'  {name}:'),
                        Switch(disabled=True, id=name.replace(' ', '_')),
                        classes = 'switch'
                    ) for name in cfg.PDU_OUTLETS],
                    classes='group'
                ),

                # Rubin
                Static('Rubin:', classes='header'),
                Static(' ', id='rubin', classes='group'),

                # class of the column itself
                classes='leftcolumn'
            ),
            Vertical(
                Static('', id='clientlist'),
                RichLog(id='log')
                #Static('', id='rubinclient'),
                #Static('', id='sunangle')
            )
        )
        #yield
        yield Footer()

    async def dream_state_poller(self):
        hw = Hardware()
        state = DreamState()
        monitoring = DreamMonitoring()
        while self.is_running:
            try:
                # the actual dome state:
                #label: Static = self.query_one('#actual_state', Static)
                #label.update(f'  Dome: {str(state.dome_state)}')

                # Update the actual state displays:
                self.query_one(f'#mode_actual #{state.mode.value}', RadioButton).toggle()
                self.query_one(f'#leds_actual #{state.led_state.value}', RadioButton).toggle()
                self.query_one(f'#peltiers_actual #{state.peltier_state.value}', RadioButton).toggle()
                self.query_one(f'#heater_actual #{state.heater_state.value}', RadioButton).toggle()
                self.query_one(f'#dome_actual #{state.dome_state.value}', RadioButton).toggle()


                # Encoder position:
                pos = hw.get_dome_position()
                relative_pos = (cfg.DOME_CLOSED_POSITION - pos) / (cfg.DOME_CLOSED_POSITION - cfg.DOME_OPEN_POSITION)
                bar: ProgressBar = self.query_one('#dome_progress', ProgressBar)
                bar.update(progress = relative_pos)

                digits: Digits = self.query_one('#dome_pos', Digits)
                digits.update(f'{pos:.2f}')
                #bartext: Static = self.query_one('#dome_text', Static)
                #bartext.update(f'  Encoder: {pos:.2f}')

                # Sun altitude
                # sun alt
                sunalt: Digits = self.query_one('#sun_alt', Digits)
                alt, rising = sun_alt()
                sunalt.update(f'{alt:.1f}°')
                sunalt.set_class(alt > cfg.SUN_ALT_SUNRISE_CLOSEDOME if rising else alt > cfg.SUN_ALT_SUNSET_OPENDOME, 'danger')

                # clocks
                clock: Digits = self.query_one('#utctime', Digits)
                clock.update(datetime.datetime.now(datetime.timezone.utc).strftime('%X'))

                clock: Digits = self.query_one('#localtime', Digits)
                clock.update(datetime.datetime.now(pytz.timezone(cfg.SITE_ZONE)).strftime('%X'))


                # Limit and door switches
                self.query_one('#switch_open', Switch).value = hw.get_dome_open_switch()
                self.query_one('#switch_closed', Switch).value = hw.get_dome_closed_switch()
                self.query_one('#switch_door', Switch).value = hw.get_door_switch()
                self.query_one('#switch_backdoor', Switch).value = hw.get_backdoor_switch()

                # temp hum
                table = Table(expand=True, box=box.MINIMAL, show_header=False, show_edge=False)
                table.add_column('')
                table.add_column('')
                table.add_column('')

                label: Static = self.query_one('#temphum', Static)
                if monitoring.rh_status:
                    for loc, data in monitoring.rh_status.items():
                        temp = data['temperature']
                        hum = data['humidity']
                        table.add_row(
                            f' {loc}',
                            f'{temp:.1f}°C',
                            f' {hum:.1f}%'
                        )
                    label.update(table)
                else:
                    label.update('connecting...')

                # PSU measurements:
                table = Table(expand=True, box=box.MINIMAL, show_header=False, show_edge=False)
                table.add_column('')
                table.add_column('feeback')
                table.add_column('setpoint')
                table.add_column('units')

                v_in = hw.get_ps_voltages()
                i_in = hw.get_ps_currents()
                v_out = hw.setpoint_voltage
                i_out = hw.setpoint_current
                table.add_row(' Voltage', f'{v_in:4.1f}', f'{v_out:4.1f}', '(V)')
                table.add_row(' Current', f'{i_in:4.1f}', f'{i_out:4.1f}', '(A)')

                label: Static = self.query_one('#psu', Static)
                label.update(table)

                # relays:
                self.query_one('#relay_motor', Switch).value = hw.motor_relay
                self.query_one('#relay_motor_dir', Switch).value = hw.motor_dir_relay
                self.query_one('#relay_peltier', Switch).value = hw.peltier_relay
                self.query_one('#relay_peltier_dir', Switch).value = hw.peltier_dir_relay
                self.query_one('#relay_heater', Switch).value = hw.heater_relay

                # UPS:
                label: Static = self.query_one('#ups', Static)
                if monitoring.ups_status:
                    table = Table(expand=True, box=box.MINIMAL, show_header=False, show_edge=False)
                    table.add_column('#')
                    table.add_column('value')
                    table.add_column('units')

                    table.add_row('status',         monitoring.ups_status['ups_status']             , '')
                    table.add_row('charge',         f"{monitoring.ups_status['battery_charge']:.1f}"         , '(%)')
                    table.add_row('temperature',    f"{monitoring.ups_status['battery_temperature']:.1f}"    , '(°C)')
                    table.add_row('voltage',        f"{monitoring.ups_status['battery_voltage']:.1f}"        , '(V)')
                    table.add_row('remaining',      f"{monitoring.ups_status['battery_remaining']:.0f}"      , '(m)')
                    table.add_row('needs replacing',str(monitoring.ups_status['battery_needs_replacing']), '')
                    table.add_row('last reason',    monitoring.ups_status['input_last_error']       , '')
                    table.add_row('output load',    f"{monitoring.ups_status['output_load']:.1f}"            , '(%)')
                    table.add_row('output current', f"{monitoring.ups_status['output_current']:.1f}"         , '(A)')

                    label.update(table)
                else:
                    label.update('connecting...')

                # PDU:
                if monitoring.pdu_status:
                    for name in cfg.PDU_OUTLETS:
                        id = name.replace(' ', '_')
                        self.query_one(f'#{id}', Switch).value = monitoring.pdu_status[name]

                # rubin client
                dreamstate = DreamState()
                rubinlabel = self.query_one('#rubin', Static)
                table = Table(expand=True, box=None, show_header=False, show_edge=False)
                table.add_column('key')
                table.add_column('val')
                table.add_row('IP addr:', dreamstate.rubin.ip_address)
                table.add_row('Last request:', f'{dreamstate.rubin.last_request_id}')
                table.add_row('Received:', ago(dreamstate.rubin.last_request_time))
                table.add_row('Weather:', 'Good' if dreamstate.rubin.weather_ok else 'Bad')
                table.add_row('  since: ', ago(dreamstate.rubin.weather_updated))
                table.add_row('Dome:', 'Open' if dreamstate.rubin.dome_ok else 'Close')
                table.add_row('Decision:', 'GO' if dreamstate.get_go_nogo() else 'NO-GO'),
                rubinlabel.update(table)

            except NoMatches:
                pass
            except asyncio.CancelledError:
                # cancellation should cleanly stop
                break
            except Exception as e:
                # Exceptions in the gui should not stop the operations
                log.exception(e)

            # wait a bit
            if state.dome_state in [DomeState.OPENING, DomeState.CLOSING]:
                await asyncio.sleep(0.1)
            else:
                await asyncio.sleep(1)


    def on_mount(self):
        # install a log handler to display logs inside the text area
        text_log = self.query_one(RichLog)
        handler = RichLogHandler(text_log, self)
        log.addHandler(handler)

        # perpetually run a server. Keep a reference to avoid gc
        #self.servertask = asyncio.create_task(self.server())
        self.dream_state_poller_task = asyncio.create_task(self.dream_state_poller())

        # register callbacks from the 0mq server
        server = DreamCommandServer()
        server.callback = self.on_camera_update


    def on_camera_update(self):
        '''Redraw all camera details. Probably not the most efficient but it's only a few lines of text.'''
        state = DreamState()
        try:
            clientlist: Static = self.query_one('#clientlist', Static)
            clientlist.update(self._render_clients())
        except NoMatches:
            pass

        # make flashy warnings
        dom_node: Static
        for dom_node in self.query('.problem'):
            flag = dom_node.name # could be either error or warning
            dom_node.set_class(flag not in state.problems,'hidden')


    @group()
    def _render_clients(self):
        state = DreamState()
        for id, client in state.camerainfo.items():
            # script clients are handled elsewhere
            if client.client_type == 'script':
                continue
            if not client.camera_data:
                # this should not happen: image received before heartbeat
                # to avoid excessive if/then/else logic below we skip the update
                continue

            # make a header
            outdated = client.heartbeat < datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(seconds=5)
            yield Styled(Rule(f'Camera: {id}', characters=' ', align='left'), style='grey85 on #aa3333' if outdated else 'grey85 on #333333')

            # build a table

            height = 7
            if client.image_data:
                width = height * 3 * 2 // 2 # the image is ~ 2:3 but characters are ~2:1 height:width
                thumb = image_preview(client.image_data.data, width, height)
                # normalize and map
                cmap = cm.get_cmap('plasma', 127) # greys, plasma, or virisis look nice
                levels = [f'[white on {matplotlib.colors.rgb2hex(cmap(i))}] [/]' for i in range(cmap.N)]
                #levels.reverse()

                # normalize
                maxval = np.max(thumb)
                minval = np.min(thumb)
                if minval == maxval:
                    maxval = minval + 1 # to avoid /0
                thumb = (thumb - minval) / (maxval - minval) * (len(levels)-1)

                # discretize colors
                thumb = thumb.astype(int)
                thumb = choose(thumb, levels)

                # join into rows of ascii
                thumb = [''.join(thumb[i]) for i in range(height)]

            else:
                thumb = [''] * height

            table = column_grid([
                [
                    f'  last update:     {client.heartbeat.strftime("%X")}',
                    f'  client_id:       {client.client_id}',
                    f'  mode:            {client.camera_data.state}',
                    f'  ccd_temp:        ' + (f'{client.camera_data.ccd_temp:.1f}' if client.camera_data.ccd_temp is not None else 'N/A'),
                ],
                [
                    '  trigger latency:  ' + (f'{1000 * client.image_data.timing_latency:.1f} (ms)'     if client.image_data else '--'),
                    '  usb latency:      ' + (f'{1000 * client.image_data.usb_latency:.1f} (ms)'        if client.image_data else '--'),
                    '  artificial delay: ' + (f'{1000 * client.image_data.artificial_latency:.1f} (ms)' if client.image_data else '--'),
                    '  download time:    ' + (f'{client.image_data.download_time:.3f} (s)'              if client.image_data else '--'),
                    '  download margin:  ' + (f'{client.image_data.download_margin:.3f} (s)'            if client.image_data else '--'),
                ],
                [
                    f'  last image:      {client.image_data.seq       if client.image_data else "--"}',
                    f'  type:            {client.image_data.imagetype if client.image_data else "--"}',
                    f'  trigger at:      {client.image_data.triggertime.strftime("%X") if client.image_data else "--"}',
                    f'  min:             {client.image_data.min if client.image_data else "--"}',
                    f'  max:             {client.image_data.max if client.image_data else "--"}',
                    f'  median:          {client.image_data.med if client.image_data else "--"}',
                    f'  average:         {client.image_data.avg if client.image_data else "--"}',
                ],
                [
                    f'  blanks:  {client.camera_data.num_blanks  if client.camera_data else "--"}',
                    f'  darks:   {client.camera_data.num_darks   if client.camera_data else "--"}',
                    f'  biases:  {client.camera_data.num_bias    if client.camera_data else "--"}',
                    f'  flats:   {client.camera_data.num_flats   if client.camera_data else "--"}',
                    f'  science: {client.camera_data.num_science if client.camera_data else "--"}',
                    f'  missed:  {client.camera_data.num_missed  if client.camera_data else "--"}',
                ],
                thumb
            ], expand=True, box=box.MINIMAL, show_header=False, show_edge=False, padding=0)

            yield table
            yield Text('')

    @on(RadioSet.Changed, '#mode_target')
    def handle_mode_target_change(self, event: RadioSet.Changed) -> None:
        state = DreamState()
        state.target_mode = CameraServerMode(event.pressed.id)
        log.debug(f'target mode set to {state.target_mode}')

    @on(RadioSet.Changed, '#dome_target')
    def handle_dome_target_changed(self, event: RadioSet.Changed) -> None:
        state = DreamState()
        if event.pressed.id == DomeTargetState.FORCEOPEN:
            # this is such a dangerous move that we ask for extra confirmation
            def cb(result):
                if result == 'open':
                    # apply the new state anyway
                    state.dome_target_state = DomeTargetState.FORCEOPEN
                else:
                    # revert the ui in cacse of cancellation
                    prev_button = self.query_one(f'#dome_target #{state.dome_target_state.value}', RadioButton)
                    log.info('Previous button:')
                    log.info(prev_button)
                    prev_button.action_toggle()
            self.push_screen(ConfirmationRequestScreen(), cb)
        else:
            state.dome_target_state = DomeTargetState(event.pressed.id)

    @on(RadioSet.Changed, '#leds_target')
    def handle_leds_target_changed(self, event: RadioSet.Changed) -> None:
        state = DreamState()
        state.led_target_state = LedState(event.pressed.id)

    @on(RadioSet.Changed, '#peltiers_target')
    def handle_peltiers_target_changed(self, event: RadioSet.Changed) -> None:
        state = DreamState()
        state.peltier_target_state = PeltierState(event.pressed.id)

    @on(RadioSet.Changed, '#heater_target')
    def handle_heater_target_changed(self, event: RadioSet.Changed) -> None:
        state = DreamState()
        state.heater_target_state = HeaterState(event.pressed.id)

    def action_toggle_dark(self):
        self.dark = not self.dark


    def action_toggle_autoscroll(self):
        log: RichLog = self.query_one(RichLog)
        log.auto_scroll = not log.auto_scroll


class ConfirmationRequestScreen(ModalScreen):
    def compose(self) -> ComposeResult:
        yield Grid(
            Label('Are you really sure?', id='question'),
            Label('This overrules all safety checks and has the potential to damage the cameras.', id='text'),
            Button('Cancel', variant='success', id='cancel'),
            Button('Open the dome', variant='error', id='open'),
            id='dialog'
        )

    def on_button_pressed(self, event: Button.Pressed) -> None:
        self.dismiss(event.button.id)


# Loop that starts the gui
async def maintenance_gui_loop():
    install_textual_degree_symbol()
    try:
        app = DreamCentralGui()
        log.info('Starting GUI...')
        log.warning('Further console log messages are deferred to the GUI')
        await app.run_async()
    except Exception as e:
        log.error('GUI error:')
        log.exception(e)
        raise
    finally:
        log.warning('GUI closed. Logging to console resumed')
        log.warning('See log file for full log')
        raise NormalExit
