import datetime
import socket
import os
from typing import Optional

STATION_NAME = 'DREAM'
STATION_ABBRV = 'CP'  # Cerro Pachon

# filename for logging
LOGGER_NAME = 'dream'
LOG_PATH = '/Software/logs/'
LOG_CAMERA_FILENAME = 'dreamcamera.log'
LOG_CONTROL_FILENAME = 'dreamcontrol.log'
TRACE_FILENAME = 'dream.csv'
STATE_FILE = '/Software/dream.state'

# start all control loops in auto mode. If False, temperature control, camera
# control, etc will start in Idle. This is used in TUI mode:
AUTOSTART = True

# this gets enabled by TUI mode too. The rationale is that during
# testing/debugging we don't want to start moving things in case of a crash or
# problem.
SKIP_EMERGENCY_CLOSE = False

# Enable these to simulate components/events
SIMULATE = [
     #'CAMERA',
     #'DOME',
     #'LEDS',
     #'UPS',
     #'PDU',
     #'GUDE',
     #'SHUTDOWN',
     #'SUNSET',
    ]

# TCP port for camera control
CAMERA_CONTROL_PORT = 5001
REDUCTION_CONTROL_PORT = 5002
IP_PDU_1 = '139.229.170.91'
IP_PDU_2 = '139.229.170.92'
IP_UPS = '139.229.170.90'            # IP of UPS
#
IP_DREAM_CONTROL = '139.229.170.95'  # IP of control computer
IP_DREAM_CENTRAL = '139.229.170.96'  # IP of camera computer (Central)
IP_DREAM_EAST    = '139.229.170.97'  # IP of camera computer (East)
IP_DREAM_NORTH   = '139.229.170.98'  # IP of camera computer (North)
IP_DREAM_WEST    = '139.229.170.99'  # IP of camera computer (West)
IP_DREAM_SOUTH   = '139.229.170.100' # IP of camera computer (South)
# for debug:
#IP_DREAM_CONTROL = '127.0.0.1'

# LSST
SITE_LATITUDE = -30.244769
SITE_LONGITUDE = -70.749433
SITE_ALTITUDE = 2663.
SITE_ZONE = 'America/Santiago'  # Used to get local time

# PDU outlets
PDU_OUTLETS = {
    # map from name to (#pdu, #port) # both numbers start at 1, like in docs
    'Switch': (1, 2),
    'USB hub': (1, 3),
    'PSU 1': (1, 4),
    'PSU 2': (1, 5),
    'Command Server': (2, 1),
    'Central Server': (2, 2),
    'North Server': (2, 3),
    'East Server': (2, 4),
    'South Server': (2, 5),
    'West Server': (2, 6),
    'Central Camera': (1, 6),
    'North Camera': (1, 7),
    'East Camera': (1, 8),
    'South Camera': (2, 7),
    'West Camera': (2, 8)
}

PDU_POWEROFF_DELAY = 60  # seconds

# UPS shutdownlevel
UPS_MAX_POWERGAP = 60  # 1 minute
UPS_SHUTDOWN_REMAINING_MINUTES = 5  # minutes
UPS_RESUME_LEVEL = 90  # %


def get_camera_orientation_from_hostname() -> Optional[str]:
    hostname = os.uname().nodename.upper()
    try:
        camera = hostname[5]
        if camera not in ['N', 'E', 'S', 'W', 'C']:
            return None
        else:
            return camera
    except IndexError:
        return None


# We really only need to run this once:
CAMERA = get_camera_orientation_from_hostname()

# expected DAQ serial
# DAQ_SERIAL = '01FA42C2' # spare daq
DAQ_SERIAL = '01FA42C3'
SN_ENCODER = 'FT4YE7O2'

RAWDATA_DIR = '/tmpdata/raw'
THUMBNAIL_DIR = '/tmpdata/thumbs'
CLOUDMAP_DIR = '/data/products/'

CADANCE = 6.4  # Image cadance in LST seconds. 13500.000 exposures per lst day
EXPOSURETIME = CADANCE * 23.9344696 / 24.  #365.2422 / 366.2422  # length of CADANCE in wall time
EXPOSURE_MARGIN = 0.010  # margin left for readout and trigger
if 'CAMERA' in SIMULATE:
    EXPOSURE_MARGIN = 0.100  # because my laptop is slow
#
# Longitude of the virtual reference station which is used to sync up the lst
# sequence intervals with mascara stations:
REFERENCE_LONGITUDE = -17.8780

# Defined start of the LST sequence numbering
INITIAL_DAY = datetime.datetime(2012, 12, 31, 18, 29, 10, 680000, datetime.timezone.utc)


CCD_OPERATIONAL_TEMP = -10.0    # target temperature of CCD's
CCD_IDLE_TEMP = 50              # large temp to set ccd to when not cooling
CCD_MAX_DELTA_T = 50            # maximum offset from ambient for the peltiers
CCD_TEMP_MARGIN = 0.1           # consider the CCD to be on temp if deviation less than margin
CCD_MARGIN = (36, 25, 36, 25)   # right, bottom, left, top

DEWPOINT_MARGIN = 5 # degrees C (above dewpoint we start heating windows)

N_DARKS = 20  # Number of darks to take
N_FLATS = 20  # Number of flats to take
N_BIAS = 20  # Number of bias to take
N_BLANKS = 100

# EXPTIME_SCIENCE = 6.382525225  # Exposure time for science observations
# EXPTIME_CADENCE = 6.4
EXPTIME_FLATS = 0.25    # illumination time for flat (exposure stays long)
EXPTIME_BIAS = 0.001  # Exposure time for bias

# heartbeat interval used between camera and central control
HEARTBEAT_INTERVAL = 1.0
HEARTBEAT_TIMEOUT = 2.0

RUBIN_PORT = 5000
RUBIN_TIMEOUT = 30 # seconds


DOME_OPEN_POSITION = 90.50
DOME_CLOSED_POSITION = 116.3

# opening and closing ramps
DOME_OPENING_STARTRAMP_FRACTION = 0.2
DOME_OPENING_FINALRAMP_FRACTION = 0.4
DOME_OPENING_START_VOLTAGE = 3.75
DOME_OPENING_FINAL_VOLTAGE = 2.25
DOME_OPENING_NOMINAL_VOLTAGE = 7.5

DOME_CLOSING_STARTRAMP_FRACTION = 0.1
DOME_CLOSING_FINALRAMP_FRACTION = 0.4
DOME_CLOSING_START_VOLTAGE = 3.75
DOME_CLOSING_FINAL_VOLTAGE = 3.75
DOME_CLOSING_NOMINAL_VOLTAGE = 15


# The motors stop turning 0.2 turns before the end
DOME_MARGIN = 0.20

# Sun angles
SUN_ALT_SUNSET_CALIBRATIONS = 0  # Sun altitude at which we start calibrations
SUN_ALT_SUNSET_OPENDOME = -10  # Sun altitude at which we do science observations
SUN_ALT_SUNRISE_CLOSEDOME = -10  # Sun altitude at which we close the dome
# there is no separate calibrations minimum sunalt. we do them as soon as the dome is closed

DAY_MIN_TEMP = 5     # Temperature below which we start heating during day time
DAY_MAX_TEMP = 40    # Temperature above which we start cooling during day time
NIGHT_MIN_TEMP = 5   # Temperature below which we start heating during night
NIGHT_MAX_TEMP = 16  # Temperature above which we start cooling during night

PELTIER_COOLING_SETPOINT = 24  # voltage send to power supplies while cooling.
PELTIER_HEATING_SETPOINT = 17  # voltage send to power supplies while heating.

# Do not open the dome for 5 minutes if it was just closed
DOME_MINIMUM_CLOSE_TIME = 5 * 60  # seconds
# how long to blink led's and print warnings until the actual move
DOME_MOTION_DELAY = 10  # seconds, integer

GUDE_SENSOR_PORTS = {
    '139.229.170.93': [
        (1, 'electronics_top'),
        (2, 'camera_bay'),
        (3, 'electronics_box')
    ],
    '139.229.170.94': [
        (1, 'rack_top'),
        (2, 'rack_bottom'),
        (3, 'dream_inlet')
    ]
}

TEMP_MAX_CAMERA_BAY = 50  # Max. temperature inside camera enclosure (in C)
HUM_MAX_CAMERA_BAY = 90  # Humidity inside camera encosure above which dome closes (in %)
HUM_MAX_ELECTRONICS = 90  # Humidity inside electronics box above which dome closes (in %)
HUM_MAX_DOME = 90  # Humidity above electronics below camera bay

# for warnings only:
HUM_MAX_INLET = 80  # Outlet humidy
TEMP_MIN_INLET = 0  # Outside temperature

CLOUD_COMBINE_FRAMES = 5
CLOUD_COMBINE_FAST = False # skip timeout for cameras that are offline
                           # set to False when testing with cloud replay

CLOUD_COMBINE_NEST_HEALPIX = True
CLOUD_COMBINE_CLEAN = False
