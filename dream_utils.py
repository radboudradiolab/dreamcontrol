#!/usr/bin/env python3

import asyncio
import datetime
import fcntl
import glob
import hashlib
import logging
import os
import pathlib
import shelve
import threading
import time
from contextlib import contextmanager
from typing import Generic, Optional, Sequence, Tuple, TypeVar, Union

import aiofiles
import aiohttp
import astropy.coordinates
import astropy.time
import astropy.units
import numpy as np
import pytz
import requests
import textual.renderables
from astropy.utils.iers import IERS_A, conf
from imageio import imwrite
from rich.console import RenderableType
from rich.table import Table

import dream_config as cfg
import dream_credentials as creds
from dream_dataclasses import CameraServerMode, DataProduct

log = logging.getLogger(cfg.LOGGER_NAME)


async def shutdown_signal_handler(loop):
    print('\r', end='')
    log.info('Shutdown requested by INT or TERM signal.')
    tasks = [t for t in asyncio.all_tasks(loop) if t is not asyncio.current_task(loop)]
    for task in tasks:
        task.cancel()
    # Wait for tasks to finish
    await asyncio.gather(*tasks, return_exceptions=True)
    log.info('All routines finished')


def handle_sigterm(loop):
    """Ensure shutdown is scheduled inside the running event loop."""
    asyncio.get_running_loop().call_soon_threadsafe(
        lambda: asyncio.create_task(shutdown_signal_handler(loop))
    )


def iers_init():
    conf.auto_download = True
    conf.auto_max_age = None

    # do one update, retry until sucess
    while True:
        try:
            # start a mini self-containted asyncio loop
            asyncio.run(update_iers_tables())
        except Exception as e:
            raise RuntimeError('IERS Initialization failed') from e
            #log.warning(f'IERS table update errror: {e}')
            #time.sleep(5)
        else:
            break


async def iers_update_loop():
    # exponential back-off for failing download
    exponential_increase = 2
    min_interval = 2
    max_interval = 3600
    interval = min_interval

    # start with a long sleep
    await asyncio.sleep(24 * 60 * 60)

    # try infinitely to update tables
    while True:
        try:
            await update_iers_tables()
        except Exception as e:
            log.warning(f'IERS table update error: {e}')
            await asyncio.sleep(interval)
            interval = min(max_interval, interval * exponential_increase)
        else:
            # after a successful update, wait one day. Most days we will
            # re-download the same file, but that's fine.
            await asyncio.sleep(24 * 60 * 60)
            # reset the error interval
            interval = min_interval


async def update_iers_tables():
    """
    Asynchronously downloads the IERS-A table, caches it locally, and loads it into Astropy.
    """
    iers_url = 'https://datacenter.iers.org/data/9/finals2000A.all'
    leap_url = 'https://hpiers.obspm.fr/iers/bul/bulc/Leap_Second.dat'

    for url in [leap_url, iers_url]:
        url_hash = hashlib.md5(url.encode('utf-8')).hexdigest()

        # Directory where Astropy caches downloaded files
        cache_path = astropy.config.paths.get_cache_dir()
        file_path = os.path.join(cache_path, 'download', 'url', url_hash, 'contents')

        # Download the IERS file asynchronously
        async with aiohttp.ClientSession() as session:
            log.info(f'Downloading {url}...')
            os.makedirs(os.path.dirname(file_path), exist_ok=True)
            async with session.get(url) as response:
                if not response.ok:
                    log.error(f'Error while downloading: {response.reason}')
                file_contents = await response.read()
                async with aiofiles.open(file_path, 'wb') as f:
                    await f.write(file_contents)
                log.info(f'Table saved to {file_path}')

    # Load the table for use in Astropy
    IERS_A.open(file_path)
    log.info('IERS-A table loaded into Astropy.')


def get_lst_idx(when: Optional[datetime.datetime] = None) -> float:
    """Get the lst index (float) within the current day. Note that a date can be
    provided. If none is set the current time will be used. This is always in
    relation to the reference station location (see config) to maintain
    synchonization with other sites.

    """
    # Note: with respect to mascara the lst time computation was updated. the
    # old code has a 24h offset sometime in the future. astropy is presumably
    # more correct. There are differences between the two upto several seconds.
    # so registration compared to other stations will be lost!
    t = astropy.time.Time(datetime.datetime.now(datetime.timezone.utc) if when is None else when)
    lst = t.sidereal_time('apparent', longitude=cfg.REFERENCE_LONGITUDE)
    lstidx = lst.value * 3600 / cfg.CADANCE
    return lstidx


def get_lst_seq(when: Optional[datetime.datetime] = None) -> int:
    """ Get the lst sequence number as offset from Dec 31st 2012.
    """
    # There was an undicovered problem with the lst time calculation in previous
    # control software. e.g. [get_lst_seq(datetime.datetime(2033, 1, 1, 18, 24,
    # 38, 1000*x)) for x in range(0, 1000, 10)] shows that for about 80 ms
    # around lst midnight the calculated seq id is wrong because the LST day
    # flips over before the multiple of LST days actually passed. (This happens
    # with either the astropy.time and the old implementation. It's a
    # consequence of delta t)
    #
    # We deal with it in the following manner: The lst idx is used to compare
    # between stations and nights and must be 'locked' to the aparant LST and
    # therefore to the star positions. The sequence number, on the other hand,
    # is used to uniquely identify images. Typically the current index should be
    # obtained from sequence number modulo 13500. To maintain this relation, it
    # must overflow at the exact same time as the index. Since the
    # seq_at_start_of_today can overflow a little early or a little later than
    # the lst index overflows, we calculate the seq at start of day twice, once
    # before and once after the current time. Almost always these two numbers
    # will be identical. Just around the LST overflow time there can be a
    # discrepancy. We pick the higher or lower of the two depending on wether or
    # not the LST index has overflowed yet.

    when = when or datetime.datetime.now(datetime.timezone.utc)

    # calculate the time since the start of the sequence numbering in 2012
    seconds_since_epoch = (when - cfg.INITIAL_DAY).total_seconds()
    # calculate the length of an lst day
    seconds_per_lst_day = 13500 * cfg.EXPOSURETIME

    # calculate twice what the sequence number would have been 1 minute ago and 1 minute in the future
    seq_at_start_of_today_a = int((seconds_since_epoch - 60) / seconds_per_lst_day) * 13500
    seq_at_start_of_today_b = int((seconds_since_epoch + 60) / seconds_per_lst_day) * 13500

    # get the lst index
    lst_idx = get_lst_idx(when)

    # decide if the seq number should be rounded up or down
    if lst_idx > 13500 / 2:
        seq_at_start_of_today = min(seq_at_start_of_today_a, seq_at_start_of_today_b)
    else:
        seq_at_start_of_today = max(seq_at_start_of_today_a, seq_at_start_of_today_b)

    # calculate the compound sequence number
    return seq_at_start_of_today + int(lst_idx)


def next_seq(when: Optional[datetime.datetime] = None, margin: float = 0.1) -> Tuple[datetime.datetime, int, float]:
    """Get the number and the start time of the next sequence image. Taking a
    margin into consideration to avoid starting an exposure too late because it
    is imminent."""

    # let the requested input default to current time
    when = when or datetime.datetime.now(datetime.timezone.utc)

    # calculate idx and seq number around that time
    idx = get_lst_idx(when)
    seq = get_lst_seq(when)

    # calculate remaining time in seconds
    remaining = cfg.EXPOSURETIME - (idx % 1.0) * cfg.EXPOSURETIME

    # we are interested in the next
    next_seq = seq + 1
    start = when + datetime.timedelta(seconds=remaining)

    # add a whole extra cycle if margin is too tight
    if remaining < margin:
        next_seq += 1
        start += datetime.timedelta(seconds=cfg.EXPOSURETIME)
        remaining += cfg.EXPOSURETIME

    # return the number and the start time
    return (start, next_seq, remaining)


def seq_to_datetime(seq: int) -> datetime.datetime:
    '''Offsetting an arbitrary seq and its starttime by an interger multiple of
    EXPOSURETIME should yield the start time of any seq number. However, leap
    seconds, drift and precision erors make this not true. In particular, the
    result can be off by several ms per day that the initial guess is off.

    We fix this by computing a rough estimate first, and then using that as the
    initial_guess for a second call to next_seq.
    '''
    # let the initial guess default to now
    guess1 = datetime.datetime.now(tz=datetime.timezone.utc)
    # compute a datetime/seq combo
    dt1, seq1, _ = next_seq(guess1, 0)
    # offset to the requested seq
    guess2 = dt1 - datetime.timedelta(seconds=cfg.EXPOSURETIME * (seq1-seq))
    # compute a more precise datetime/seq close to the requested seq
    dt2, seq2, _ = next_seq(guess2, 0)
    # offset once more, in case seq2 is one more or less than seq
    result = dt2 - datetime.timedelta(seconds=cfg.EXPOSURETIME * (seq2-seq))

    return result


class Singleton(type):
    """Use this as a metaclass to make something a singleton transparantly."""
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

    def clear(cls):
        try:
            del Singleton._instances[cls]
        except KeyError:
            pass


def get_current_night(when: Optional[datetime.datetime] = None, format='%Y%m%d') -> str:
    '''Returns the current night as YYYYMMdd'''

    if when is None:
        when = datetime.datetime.now(pytz.timezone(cfg.SITE_ZONE))
    if when.hour < 12:
        when = when - datetime.timedelta(days=1)
    return when.strftime(format)


class FrameCounters():
    """A utility class that counts the number of each frame types that have been
    recorded this night. It automatically clears the counters as the night rolls
    over. You can also request the current night, represented as the %Y-%m-%d at
    the start of that night. Storage is file-backed so counters persist over
    readboots/restart.

    This is implemented by keeping for each type: how many there are, and what
    the current night is.

    Note: get/inc are not thread-safe and are intended for use with asyncio.
    """

    def __init__(self):
        # initialize file backed storage
        self.db = shelve.open(f'/tmp/counters_{cfg.CAMERA}.db')

        # ensure all keys exists
        tonight = get_current_night()
        if 'blanks' not in self.db:
            self.db['blanks'] = (0, tonight)
        if 'bias' not in self.db:
            self.db['bias'] = (0, tonight)
        if 'flats' not in self.db:
            self.db['flats'] = (0, tonight)
        if 'darks' not in self.db:
            self.db['darks'] = (0, tonight)
        if 'science' not in self.db:
            self.db['science'] = (0, tonight)
        if 'missed' not in self.db:
            self.db['missed'] = (0, tonight)
        self.db.sync()

    def _cleanup(self, key: str):
        "Roll-over the current night indication"
        last_night = self.db[key][1]
        current_night = get_current_night()

        if current_night != last_night:
            self.db[key] = (0, current_night)
            self.db.sync()

    def get(self, key: str):
        # cleanup
        self._cleanup(key)
        # return the current count
        return self.db[key][0]

    def inc(self, key: str):
        self._cleanup(key)
        count, night = self.db[key]
        self.db[key] = (count + 1, night)
        self.db.sync()

    def rst(self, key: str):
        self._cleanup(key)
        self.db[key] = (0, get_current_night())
        self.db.sync()


async def wrap_with_exception_loggging(task, logger=log):
    """Log exceptions occuring in a task. Does not suppress the exception but if
    the task is not awaited when it happens exceptions can get lost."""
    try:
        await task
    except Exception as e:
        logger.error('An exception occurred in a background task!')
        logger.exception(e)
        raise


def format_zmq_id(id: bytes):
    return ':'.join(id.hex().upper()[2 * i : 2 * i + 2] for i in range(len(id)))


# The following two functions are used to serialize and deserialize datetime
# objects with the json module:
def json_default(obj):
    if isinstance(obj, datetime.datetime):
        return {'_isoformat': obj.isoformat()}
    if isinstance(obj, CameraServerMode):
        return {'_mode': obj.value}
    if isinstance(obj, np.ndarray):
        return {'_nparray': obj.tolist()}
    if isinstance(obj, DataProduct):
        return {'_dataproduct': obj.__dict__}
    raise TypeError(f'Cannot serialize custom type {type(obj)}')

def json_object_hook(obj):
    _isoformat = obj.get('_isoformat')
    if _isoformat is not None:
        return datetime.datetime.fromisoformat(_isoformat)

    _mode = obj.get('_mode')
    if _mode is not None:
        return CameraServerMode(_mode)

    _nparray = obj.get('_nparray')
    if _nparray is not None:
        return np.array(_nparray)

    _dataproduct = obj.get('_dataproduct')
    if _dataproduct is not None:
        return DataProduct(**_dataproduct)

    return obj


def image_preview(img, width, height):
    """Scale an image down. Not by zooming/interpolating but by summing counts."""
    res = np.zeros((height, width))
    for y, band in enumerate(np.array_split(img, height, axis=0)):
        for x, block in enumerate(np.array_split(band, width, axis=1)):
            res[y, x] = np.max(block)
    return res


def column_grid(columns: Sequence[Sequence[RenderableType]], **kwargs) -> RenderableType:
    '''Render a grid of text as a table without having to bother about the order of adding things'''
    rows = max([len(col) for col in columns])
    table = Table(**kwargs)

    for col in columns:
        table.add_column()

    for row in range(rows):
        table.add_row(*[col[row] if len(col) > row else '' for col in columns])
    return table


def choose(indices, choices):
    """Returns an array in the shape of indices where values are taken from
    choices according to the indices in the input. See also numpy.choose(), but
    without the 32 element limit on the choices, probably at the cost of
    performance.    """
    # flatten
    tmp = indices.flatten()
    # apply lookup
    res = np.ma.zeros(tmp.shape, dtype=np.array(choices).dtype)
    for i in range(len(tmp)):
        index = tmp[i]
        res[i] = choices[index] if index is not np.ma.masked else np.ma.masked
    # reshape
    return res.reshape(indices.shape)


def ago(when: datetime.datetime) -> str:
    if when.timestamp() == 0:
        return 'never'
    seconds = (datetime.datetime.now(datetime.timezone.utc) - when).total_seconds()
    if seconds < 10:
        return f'{seconds:.1f}s ago'
    if seconds < 60:
        return f'{seconds:.0f}s ago'
    if seconds < 10 * 60:
        return f'{seconds//60:.0f}m {seconds%60:.0f}s ago'
    if seconds < 60 * 60:
        return f'{seconds/60:.0f}m ago'
    if seconds < 24 * 60 * 60:
        return f'{seconds//3600:.0f}h {seconds%3600/60:.0f}m ago'
    return when.strftime('%Y-%m-%d %X')


@contextmanager
def open_and_lock(filename: str):
    '''Open and lock a file exclusively. Useful for atomic file operations. I.e.
    when preserving state through a file.
    '''

    # without making any assumptions the file could exist or not exists opening
    # with r+ requires the file to exists, opening with x+ requires the file to
    # not exists and opening with w+ creates the file if it does not exist but
    # truncates it if it does. Trying to open the file first with rb+ and then
    # with wb+ might fail if another process does the same at the same time.
    # Hence we 'touch' the file before opening. If two processes do this it
    # doesn't matter and it guarantees that the file can be opened with rb+.
    pathlib.Path(filename).touch()

    # we don't catch any errors from the open() call since we know
    # FileNotFoundError can't occur and any other error is unexpected and should
    # bubble up.
    fd = open(filename, 'rb+')

    # the context manager part just locks and unlocks the file
    try:
        fcntl.flock(fd, fcntl.LOCK_EX)
        yield fd
    finally:
        fcntl.flock(fd, fcntl.LOCK_UN)
        fd.close()


def install_textual_degree_symbol():
    # small visual addition for rendering a nice and large degrees symbol
    textual.renderables.digits.DIGITS += '°'
    #log.info(textual.renderables.digits.DIGITS3X3)
    textual.renderables.digits.DIGITS3X3 += [' ┏┓']
    textual.renderables.digits.DIGITS3X3 += [' ┗┛']
    textual.renderables.digits.DIGITS3X3 += ['   ']
    #log.info(textual.renderables.digits.DIGITS3X3)


T = TypeVar('T')
class Tracked(Generic[T]):
    def __init__(self, val : T):
        self.value : T = val
        self.when = time.time()

    def get(self) -> T:
        return self.value

    def changed(self) -> float:
       return self.when

    def set(self, newval: T):
       self.value = newval
       self.when = time.time()

U = TypeVar('U')
class Box(Generic[U]):
    """The simplest possible generic container. It contains just one item of the
    generic type."""
    def __init__(self, val: U):
        self.val = val

    def get(self) -> U:
        return self.val

    def set(self, val: U) -> None:
        self.val = val



def get_data_dirs():
    rawdir = os.path.join(cfg.RAWDATA_DIR,
                          get_current_night() + cfg.STATION_ABBRV + cfg.CAMERA)
    thumbnaildir = os.path.join(cfg.THUMBNAIL_DIR,
                          get_current_night() + cfg.STATION_ABBRV + cfg.CAMERA)

    if not os.path.exists(rawdir):
        log.info(f'Creating raw dir \'{rawdir}\'')
        os.makedirs(rawdir)

    if not os.path.exists(thumbnaildir):
        log.info(f'Creating thumbnail dir \'{thumbnaildir}\'')
        os.makedirs(thumbnaildir)

    return rawdir, thumbnaildir



def format_float_or_none(val: Union[None, float], fmt: str = '{:.1f}', default='N/A'):
    if val is None:
        return default
    else:
        return fmt.format(val)




def save_thumbnail(image, filename: str, binfactor: int = 8) -> None:
    # before rebinning, crop to multiple of binfactor
    width, height = image.shape
    image = image[:height // binfactor * binfactor][:width // binfactor * binfactor]

    # rebin
    # https://scipython.com/blog/binning-a-2d-array-in-numpy/
    image = image.reshape((width // binfactor, binfactor, height // binfactor, binfactor))
    image = image.mean(-1).mean(1)

    # rescale:
    image = (np.abs(image)**0.1)*2**6.4
    image = np.uint8(image)

    imwrite(filename, image)


def flatten_exception_group(eg: Exception) -> [Exception]:
    if isinstance(eg, ExceptionGroup):
        return [flatten_exception_group(e) for e in eg.exceptions]
    else:
        return eg


_start_time = datetime.datetime.now(datetime.timezone.utc)
def simulate_sun_alt_sine(when: datetime.datetime, period=15*60, start=200, rise=True):
    '''Simulate a sinusoidal sun motion. For simulation purposes it is nice to
    have a fast period than 24 hours. The start parameter determines how many
    seconds after start-up the simulated sun will rise.'''
    global _start_time
    dt = (when - _start_time).total_seconds()
    freq = 1 / period
    # phase offset to get the desired start offset in seconds
    phase_start_offset = start / period * np.pi * 2
    # phase offset to get the desired start sun altitude
    if rise:
        phase_sun_offset = np.arcsin(cfg.SUN_ALT_SUNRISE_CLOSEDOME / 90)
    else:
        phase_sun_offset = np.pi + np.arcsin(cfg.SUN_ALT_SUNSET_CALIBRATIONS / 90)
    # compute angle
    angle = 90 * np.sin(2 * np.pi * freq * dt + phase_sun_offset - phase_start_offset)
    # compute direction
    rising = when.astimezone(pytz.timezone(cfg.SITE_ZONE)).hour < 12

    return angle, rising


def simulate_sun_fast(when: datetime.datetime, start=60, rise=True, verbose=False):
    '''Simulate a linearly rising and setting sun. Timed such that we see all
    phases of the process without much delay.

    We define the profile by 2 linear regions, the slove of which is defined by
    the time it takes to do calibrations. To avoid having to do intersection
    math we use 3 linear regions to deal with the periodicty of days. Let time 0
    be the time of sunrise, Tcal be the time needed to do the calibrations, and
    Tday and Tobs resp. the time spend idle and observing. The first region is
    then defined by:

    sun_1 = (Asunrise-Adomeclose) / Tcal * x

    The setting sun is defined by:

    sun_2 = (Ascience-Adomeopen) / Tcal * (x-Tday)

    So, during the day, the sun is the minimum of sun_1 and sun_2. This way we
    do not need to calculate the intersection point between these two linear
    segments. After sunset the angle continues to be defined by sun_2 until
    sun_3 becomes larger:

    sun_3 = (Asunrise-Adomeclose) / Tcal * (x-Ttotal+Tcal)

    So, after Tday, the sun is defined by the maximum of sun_2 and sun_3.

    '''
    global _start_time

    # estimate how much time things cost: 35 for the dome opening, 5 extra
    # exposures for margin and because during switching some lsd seq are missed:
    Tcal = 35 + (cfg.N_BIAS + cfg.N_BLANKS + cfg.N_DARKS + cfg.N_FLATS + 5) * cfg.EXPOSURETIME

    # time for nigh and day tasks
    Tday = 2 * 60
    Tobs = 10 * 60

    Ttotal = Tday + Tobs + 2 * Tcal

    dt = (when - _start_time).total_seconds() - start
    if not rise:
        dt += Tday
    t = dt % Ttotal  # always positive

    sun_1 = (-cfg.SUN_ALT_SUNRISE_CLOSEDOME) / Tcal * t
    sun_2 = (cfg.SUN_ALT_SUNSET_OPENDOME - cfg.SUN_ALT_SUNSET_CALIBRATIONS) / Tcal * (t - Tday)
    sun_3 = (-cfg.SUN_ALT_SUNRISE_CLOSEDOME) / Tcal * (t - Ttotal)

    if verbose:
        log.info(f'start_time={_start_time}')
        log.info(f'when={when}')
        log.info(f'Tcal={Tcal}')
        log.info(f'Ttotal={Ttotal}')
        log.info(f'dt={dt}')
        log.info(f't={t}')
        log.info(f'sun_1={sun_1}')
        log.info(f'sun_2={sun_2}')
        log.info(f'sun_3={sun_3}')

    if sun_2 > sun_1:
        return sun_1, True
    elif sun_3 > sun_2:
        return sun_3, True
    else:
        return sun_2, False





def sun_alt(when: Optional[datetime.datetime] = None, refraction: bool = False, temperature: float = 15., humidity: float = 50.) -> Tuple[float, bool]:
    '''Get the sun altitude, and a flag indicating if the sun is rising. We do
    not use refraction in practise but it is here to confirm that the difference
    is small enough to ignore.

    Parameters:
        when (datetime.datetime): the time used to calculate sun position. Defaults to now.
        refraction (bool): whether or not to correct for atmospheric refraction.
        temparture (float): temperature at site in degrees centigrade
        humidity (float): humidity at site in percent (0-100)
    Returns:
        altitude (int) : sun altitude at site at specified time
        rising (bool) : True if the sun is rising

    '''
    when = when or datetime.datetime.now(datetime.timezone.utc)
    # I think we are implicity relying here on the fact that system time is utc
    # otherwise a timezone-unaware datetime could be coerced here. Hence this
    # check:
    if when.tzinfo is None:
        raise RuntimeError('sun_alt() needs a timezone aware datetime')
    when = when.astimezone(pytz.timezone(cfg.SITE_ZONE))

    if 'SUNSET' in cfg.SIMULATE:
        return simulate_sun_fast(when, rise=False, verbose=False)
    if 'SUNRISE' in cfg.SIMULATE:
        return simulate_sun_fast(when, rise=True, verbose=False)

    # prepare an astropy.time.Time object
    t = astropy.time.Time(when)

    # calculate sun position
    sun = astropy.coordinates.get_sun(t)
    location = astropy.coordinates.EarthLocation(
        lat=cfg.SITE_LATITUDE * astropy.units.deg,
        lon=cfg.SITE_LONGITUDE * astropy.units.deg,
        height=cfg.SITE_ALTITUDE * astropy.units.m
    )

    # get atmospheric parameters
    if refraction:
        # we do not have a pressure sensor. So we make a calculated estimate:
        EARTH_GRAVITY = 9.80665          # Earth surface acceleration (m/s2)
        AIR_MOLAR_MASS = 0.0289644       # Molar mass of dry air (kg/mol)
        UNIVERSAL_GAS_CONSTANT = 8.31447 # Universal gas constant J/(mol.K)
        ATM_PRESSURE = 101325            # Standard atmosphere (Pa)
        CELSIUS2KELVIN = 273.15          # Conversion for Celsius to Kelvin

        num = -EARTH_GRAVITY * AIR_MOLAR_MASS * cfg.SITE_ALTITUDE
        den = UNIVERSAL_GAS_CONSTANT*(CELSIUS2KELVIN+temperature)
        pressure = ATM_PRESSURE * np.exp(num/den)
    else:
        pressure = 0. # disables atmospheric corrections

    # transform
    alt = sun.transform_to(
        astropy.coordinates.AltAz(
            obstime = t,
            location = location,
            pressure = pressure * astropy.units.Pa,
            temperature = temperature * astropy.units.Celsius,
            relative_humidity = humidity / 100.
        )).alt

    # calculate if we're rising or falling
    rising =  when.hour < 12

    return alt.deg, rising



def map_range(x, old_lower, old_upper, new_lower, new_upper):
    '''Remap x from an old range to a new range. Linear transform. It is
    guaranteed that the return value is clipped to the output range'''
    remapped = (x-old_lower)/(old_upper-old_lower)*(new_upper-new_lower)+new_lower
    if remapped > max(new_lower, new_upper):
        return max(new_lower, new_upper)
    if remapped < min(new_lower, new_upper):
        return min(new_lower, new_upper)
    return remapped


def dewpoint(temperature, relative_humidity):
    '''Calculate the ambient dewpoint given air temperature and relative
    humidity. The dewpoint formulas are based on the implementation in metpy.
    See https://github.com/Unidata/MetPy/blob/main/src/metpy/calc/thermo.py
    However, I stripped all units, decorators, etc. to avoid pulling in metpy in
    its entirty as a dependency.

    Parameters
    ----------
    temperature : Air temperature in degrees C

    relative_humidity : Relative humidity expressed as a ratio in the range 0 < relative_humidity <= 1

    Returns
    -------
    Dewpoint temperature in degrees C

    '''
    zero_degc = 273.15
    tmp = np.log(relative_humidity) + 17.67 * temperature / (zero_degc + temperature - 29.65)
    return 243.5 * tmp / (17.67 - tmp)


def test_dewpoint_equivalence(deviation=0.001):
    from metpy.calc import dewpoint_from_relative_humidity
    from metpy.units import units
    for rh in np.arange(0.1, 1, 0.1):
        for T in np.arange(-30, 50, 1):
            true = dewpoint_from_relative_humidity(T * units.degC, 100 * rh * units.percent)
            calc = dewpoint(T, rh)
            print(f'T={T:.1f} rh={rh:.1f}: dewpoint = {calc:.1f} =? {true.magnitude:.1f}')
            if np.abs(true.magnitude - calc) > deviation:
                raise RuntimeError(f'Dewpoint calculation failed for T={T} and rh={rh}')


def send_slack_message(message: str, wait=False):
    '''Send a slack message. We do this in a thread because we do not want to
    await it. Failing is preferred over stalling.'''
    def task():
        post_url = 'https://slack.com/api/chat.postMessage'
        post_payload = {
            'channel': creds.SLACK_CHANNEL,
            'text': message,
        }
        headers = {
            'Authorization': f'Bearer {creds.SLACK_TOKEN}',
            'Content-Type': 'application/json'
        }

        post_response = requests.post(post_url, headers=headers, json=post_payload)
        post_result = post_response.json()
        if not post_result.get('ok'):
            err = post_result.get('error')
            raise Exception(f'Message post failed: {err}')

    # Create a thread to run the task
    thread = threading.Thread(target=task)
    thread.start()
    if wait:
        thread.join()

def clean_old_images(target: int):
    '''Remove old image files until we have freed 'target' bytes.'''
    log.warning(f'Low disk space. Cleaning up {target/(2**20):.3f} MiB to make space')

    image_patt = f'{cfg.RAWDATA_DIR}/*/*{cfg.STATION_ABBRV}{cfg.CAMERA}.fits'
    thumb_patt = f'{cfg.THUMBNAIL_DIR}/*/*{cfg.STATION_ABBRV}{cfg.CAMERA}.png'
    log.debug(f'Looking for files in {image_patt} and {thumb_patt}')

    all_files = glob.glob(image_patt) + glob.glob(thumb_patt)

    all_files.sort(key=os.path.basename)
    freed = 0
    for filename in all_files:
        freed += os.path.getsize(filename)
        log.info(f'Deleting oldest file {filename}')
        os.unlink(filename)
        if freed > target:
            log.info('Enough space freed')
            break


def send_slack_file(filename: str, wait=False):
    def task():
        with open(filename, 'rb') as file_data:
            upload_url = 'https://slack.com/api/files.upload'
            headers = {
                'Authorization': f'Bearer {creds.SLACK_TOKEN}'
            }
            upload_payload = {
                'channels': creds.SLACK_CHANNEL
            }
            files = {
                'file': file_data
            }

            upload_response = requests.post(upload_url, headers=headers, data=upload_payload, files=files)
            upload_result = upload_response.json()

            if not upload_result.get('ok'):
                err = upload_result.get('error')
                raise Exception(f'File upload failed: {err}')

    # Create a thread to run the task
    thread = threading.Thread(target=task)
    thread.start()
    if wait:
        thread.join()


def shutdown():
    '''Shutdown the OS.
    '''
    log.info('Shutting down for power saving!')
    if 'SHUTDOWN' not in cfg.SIMULATE:
        os.system('ssh -o NoHostAuthenticationForLocalhost=yes -i /Software/shutdown root@localhost')

    # this was really the only way to do this from inside a Singularity container
    # If we ever migrate away from that we should reconsider.
    # os.system('dbus-send --system --print-reply --dest=org.freedesktop.login1 /org/freedesktop/login1 "org.freedesktop.login1.Manager.Reboot" boolean:true')

    # the following more pythonic implemenation does not work because dbus-send
    # apparently has special permissions that python does not have and I didn't
    # want to mess with the polkit settings that live outside singularity too
    # much:
    #
    # bus = dbus.SystemBus()
    # login1 = bus.get_object('org.freedesktop.login1', '/org/freedesktop/login1')
    # props = dbus.Interface(login1, 'org.freedesktop.DBus.Properties')
    # props.set('org.freedesktop.login1.Manager.Reboot', True)
