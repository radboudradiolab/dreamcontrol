#!/usr/bin/env python3
import asyncio
import datetime
import glob
import hashlib
import itertools
import json
import logging
import os
import queue
import shutil
# from queue import Queue
import threading
import time

import astropy
import astropy.io.fits as fits
import numpy as np
import zmq
import zmq.asyncio

import dream_config as cfg
from dream_camera import CameraInterface, CameraStatus
from dream_dataclasses import CameraServerMode, ImageInfo, TriggerInfo
from dream_state import DataProduct
from dream_utils import (Box, FrameCounters, clean_old_images,
                         get_current_night, get_data_dirs, get_lst_idx,
                         get_lst_seq, image_preview, json_default, next_seq,
                         save_thumbnail)

log = logging.getLogger(cfg.LOGGER_NAME)

# TODO: future: deal with KeyBoardInterrupt better
# Look at dream command for example, which was implemented later



# """This file contains standalone routines that manage the timing of the image
# sequences. This can be a bit convoluted since images are aligned to integer
# multiples of 6.4 lst seconds for backward compatibility with mascara stations.
# These routines are async and are effectively the glue between the camera api
# and the higher level camera server state machine."""
async def cleanup(camera: CameraInterface):
    """Expunge all and any data from camera buffers, cancel ongoing
    exposures and wait for camera to be idle. If all goes according to plan
    this should not be necessary but we do it anyway for good measure."""

    # Note: A small sleep between stop_video_mode and trigger as well as between
    # trigger and cancel_exposure is necessary. The reason is unclear but the
    # first frame can get partial data from the last frame before the stop if we
    # don't. This seems to happen only with bias frames so it may be a race
    # condition in the camera itself that only shows when the exposure time is
    # very low.

    # tell the camera to stop
    camera.stop_video_mode()
    log.debug('video mode stopped')
    await asyncio.sleep(1)

    # if the camera currently waiting for a trigger it will block until it
    # gets one. If not waiting this trigger is ignored by the camera.
    camera.trigger_exposure()
    log.debug('faux trigger sent')
    await asyncio.sleep(1)

    # if we are exposing (either because we just triggered that or because
    # that was already the case)
    camera.cancel_exposure()
    log.debug('exposure cancelled')

    # readout is uncancellable so we may still need to wait
    while camera.get_device_state() is not CameraStatus.IDLE:
        log.info('Waiting for camera to accept final trigger')
        await asyncio.sleep(0.5)

    log.debug('cleanup done')

    # No need to read the data that is left, it will be cleared when a
    # single shot or video mode is started

async def camera_debugging(camera: CameraInterface):
    """Coroutine that continuously polls the camera for it's state and data
    flags. It should be clear: don't use in production, it can skew the trigger
    timing.
    """
    try:
        while True:
            while not camera.is_connected():
                await asyncio.sleep(1.0)
            state = camera.get_device_state()
            remaining = camera.get_exposure_status()
            data = camera.get_data_ready()
            log.debug(f'camera: {state}: {remaining} (ms) remaining. Data ready: {data}')
            await asyncio.sleep(0.001)
    except asyncio.CancelledError:
        log.debug('Camera debugging stopped')



async def image_sequence(camera: CameraInterface, exposuretime: float, counters: FrameCounters, imagetype: Box[str]):
    """Async procedure to capture images. Designed to be cancellable if the mode
    changes again. In that case the last image is finished and the routine
    finishes cleanly. Note how the image type is a container such that the value
    can be changed later. This is used when switching between science, flats,
    and darks without interrupting.
    """

    def trigger_loop(stop: threading.Event, done: threading.Event, trigger_queue: queue.Queue[TriggerInfo]):
        """Internal main loop which gets run in a background thread for timing
        accuracy. When the science_mode is cancelled, the stop event is set and
        the background thread is cleanly exited."""
        log.info('science mode initiated')
        prev_latency = 0
        while not stop.is_set():
            # start by waiting for an lst trigger time
            when, index, sleep = next_seq(margin=0.1)

            # Event.wait is more precice than time.sleep :o
            # with the added benefit that we can stop early if requested
            if stop.wait(timeout=sleep):
                break

            # NOTE: busy waiting is not faster than Event.wait() but both are more accurate than time.sleep()

            # log.debug(f'camera state before trigger: {camera.get_device_state()}')

            # One caveat remains: when the previous image was triggered a bit
            # late (high latency), then it's possible that the current trigger
            # will not register if we are more than the margin earlier. We could
            # avoid issues by setting a relatively large margin, but this is
            # undesireable for other reasons. What we do instead is to linger a
            # little bit if the current latency is much less than the previous
            # latency. We do not wish to delay all further triggers so we leave
            # room for exactly the exposure margin (and 1 ms to compensate for
            # the time spent between calculating the latency and doing the
            # actual trigger) which will artificially force the latency to
            # gradually build down in case of suddenly reduced system load.
            before = time.time()
            loops = 0
            if prev_latency < 0.1:
                while time.time() < when.timestamp() + prev_latency - cfg.EXPOSURE_MARGIN + 0.001:
                    loops += 1
            artificial_latency = time.time() - before

            t1 = datetime.datetime.now(datetime.timezone.utc)

            # do the trigger
            camera.trigger_exposure()
            t2 = datetime.datetime.now(datetime.timezone.utc)
            # log.debug(f'camera state after trigger: {camera.get_device_state()}')

            # calculate how accurate we were
            timing_latency = (t1 - when).total_seconds()
            usb_latency = (t2 - t1).total_seconds()
            prev_latency = timing_latency

            # we get the temp now because later the camera may disconnect. if
            # anything this is the most representative time for this image
            # anyway
            ccd_temp = camera.get_ccd_temp()

            # push an item onto the trigger queue
            # we don't catch the queue.Full exception since we set no size limit
            trigger_queue.put_nowait(TriggerInfo(
                seq = index,
                triggertime = t1,
                timing_latency = timing_latency,
                usb_latency = usb_latency,
                artificial_latency = artificial_latency,
                imagetype = imagetype.get(),
                ccdtemp = ccd_temp
            ))

            log.debug(f'Artifial latency added {loops} busy wait cycles: {artificial_latency*1000:.3f} ms')
            log.info(f'Trigger #{index} (latency={timing_latency * 1000:.3f} ms, delay={usb_latency * 1000:.3f} ms)')
        log.info('Trigger loop finished')
        done.set()


    async def download_loop(trigger_queue: queue.Queue[TriggerInfo], image_queue: asyncio.Queue[ImageInfo]):
        """A secondary loop that monitors for available data. Even though the
        FLI api is threadsafe we need to avoid accessing the bus around the
        trigger time because this thread safety is implemented using a lock
        which could inadvertantly delay the trigger. This loop is run as a
        coroutine and needs no threading events but relies on the asyncio cancel
        to terminate cleanly. To keep this readout loop from being late for the
        next readout we defer the saving and processing of the image to yet
        another loop. The trigger timing info is retrieved from the
        trigger_queue and download timing info is sent to the image_queue such
        that the process loop below knows all the metadata of this image.

        """
        prev_index = None
        while True:
            # test for data availability
            seq = get_lst_seq()

            # since bias frames are shorter we expect to read them out
            # immediately during the same interval for all other frame types we
            # expect to read them during the interval after the one they cover
            if imagetype.get() == 'BIAS':
                expected_image_index = seq
            else:
                expected_image_index = seq - 1

            # log.debug(f'LST: {lst_idx}')
            # log.debug(f'lst offset in seconds: {(lst_idx % 1.0) * cfg.EXPOSURETIME}')

            # avoid readout around trigger time
            if not (0.2 < (get_lst_idx() % 1.0) * cfg.EXPOSURETIME < cfg.EXPOSURETIME - 2):
                # log.debug('Avoiding data ready check during trigger period')
                await asyncio.sleep(1.0)
                continue

            if not camera.get_data_ready():
                # log.debug('No data ready')
                await asyncio.sleep(1.0)
                continue

            # get the info from the trigger loop
            # we purposely don't catch the queue.Empty because it should crash if the queue is somehow empty
            trigger_info = trigger_queue.get_nowait()
            index = trigger_info.seq

            # we do have other sanity checkes. Better fail fast than to let things run awry later
            if index != expected_image_index:
                log.error(f'Expected to download image {expected_image_index} but the trigger timing suggests that data for {index} was not retrieved yet.')
                # we trust the expected sequence number and don't continue with
                # the download. The next loop through here will fetch the next
                # trigger and then the expected seq will (hopefully) match.
                # TODO: fail hard if this happens 3 times in a row or some
                # heuristic like that
                counters.inc('missed')
                continue

            log.info(f'Downloading image #{index}')

            # check if we missed anything
            if prev_index is not None and index - prev_index != 1:
                log.warning(f'LST indices not consecutive! Missing {prev_index + 1} until {index - 1}')
            prev_index = index

            # get the raw data
            start = time.time()
            data = camera.grab_video_frame()
            dltime = time.time() - start

            # save some stats
            remaining = (1.0 - get_lst_idx() % 1.0) * cfg.EXPOSURETIME

            # collect all metadata about the image in an ImageInfo struct
            dl_start_time = datetime.datetime.fromtimestamp(start, datetime.timezone.utc)
            info = ImageInfo(
                download_time=dltime,
                download_start=dl_start_time,
                download_margin=remaining,
                data=data,
                min=int(np.min(data)),
                max=int(np.max(data)),
                med=int(np.median(data)),
                avg=int(np.average(data)),
                # add everything from the trigger_info object:
                **trigger_info.__dict__
            )

            log.info(f'download time: {info.download_time * 1000:.3f} ms')
            log.info(f'download margin: {info.download_margin * 1000:.3f} ms')
            log.info(f'mean pixel value: {np.mean(info.data):.0f}')

            # the queue has no size limit so there is no possibility to raise Queue.Full
            image_queue.put_nowait(info)

    async def process_loop(image_queue: asyncio.Queue[ImageInfo]):
        """Internal secondary loop which is scheduled as an asyncio task. It
        processes the collected images and notifies central command. If the
        science mode is cancelled, this is automatically cancelled too. We still
        need to catch the cancellation and properly cleanup to avoid leaving
        things in a bad state.
        """
        checksum_prev = 0
        prev_savetime = None
        for exposure_index in itertools.count(start=1):
            with zmq.asyncio.Context() as context:
                # connect a second control socket. We can't reuse the other one
                # because each socket needs to follow the request-reply-request
                # pattern and that would not be guaranteed if we async the image
                # and the heartbeat loops
                image_socket: zmq.asyncio.Socket = context.socket(zmq.REQ)
                image_socket.connect(f'tcp://{cfg.IP_DREAM_CONTROL}:{cfg.CAMERA_CONTROL_PORT}')

                dataproduct_socket: zmq.asyncio.Socket = context.socket(zmq.PUB)
                dataproduct_socket.connect(f'tcp://{cfg.IP_DREAM_CONTROL}:{cfg.REDUCTION_CONTROL_PORT}')

                while True:
                    # wait for an image
                    image: ImageInfo = await image_queue.get()

                    # prepare a data package, starting with a copy of image data
                    data = dict(image.__dict__)

                    # add some more parameters
                    data['name'] = cfg.CAMERA
                    data['preview'] = image_preview(image.data, 60, 40)

                    # remove the raw data
                    del data['data']

                    log.debug(f'Sending image update: {data}')
                    # send a message to central
                    await image_socket.send_multipart([
                        b'image',
                        json.dumps(data, default=json_default).encode()])

                    # wait for a reply but not forever
                    if (await image_socket.poll(1000) & zmq.POLLIN) == 0:
                        log.error('Image update did not get a timely response!')
                        image_socket.setsockopt(zmq.LINGER, 0)
                        image_socket.close()
                        # we break the inner while True loop, forcing a reconnect
                        break

                    reply = await image_socket.recv_json()
                    log.info(f'Image reply: {reply}')

                    if reply['status'] != 'ok':
                        log.error(f'Command server had an error while processing image ({reply["error"]}). Discarding #{image.seq}...')
                        continue

                    if reply['type'] == 'discard':
                        log.warning(f'Discarding image {image.seq} by server directive')
                        continue

                    if image.imagetype != reply['type']:
                        log.warning(f'Image {image.seq} taken as a {image.imagetype} is actually a {reply["type"]}. Saving as such.')
                        image.imagetype = reply['type']

                    # add to counters
                    if reply['type'] == 'BLANKS':
                        counters.inc('blanks')
                    if reply['type'] == 'BIAS':
                        counters.inc('bias')
                    if reply['type'] == 'FLATS':
                        counters.inc('flats')
                    if reply['type'] == 'DARKS':
                        counters.inc('darks')
                    if reply['type'] == 'SCIENCE':
                        counters.inc('science')

                    if image.imagetype == 'BLANKS':
                        log.info('Not saving blank')
                        continue

                    header = fits.Header()
                    trigger_time_str = image.triggertime.strftime('%Y-%m-%dT%H:%M:%S.%f')
                    version = camera.get_version()
                    obsendtime = (image.download_start+datetime.timedelta(seconds=exposuretime+image.download_time)).strftime('%Y-%m-%dT%H:%M:%S.%f')
                    night = get_current_night()
                    mid = image.triggertime + datetime.timedelta(seconds=exposuretime / 2)
                    lst = astropy.time.Time(mid).sidereal_time('apparent', longitude=cfg.SITE_LONGITUDE).value
                    lpst = astropy.time.Time(mid).sidereal_time('apparent', longitude=cfg.REFERENCE_LONGITUDE).value
                    tod = (mid.hour * 3600.0 + mid.minute * 60.0 +
                           mid.second + mid.microsecond / 1e6)
                    jd = astropy.time.Time(mid, scale='utc').jd
                    doy = mid.timetuple().tm_yday
                    nzeros = np.sum(image.data == 0)


                    # this image and it's statistics
                    header['IMAGETYP'] = (image.imagetype, 'Type of image')
                    header['CCDTEMP']  = (image.ccdtemp, 'CCD temperature in C')
                    header['EXPTIME']  = (exposuretime, 'Exposure time in seconds')
                    header['TRIGLAT']  = (image.timing_latency, 'Latency of trigger timing routine')
                    header['ARTLAT']   = (image.artificial_latency, 'Artificial latency included in trigger latency to smoothen timing')
                    header['USBLAT']   = (image.usb_latency, 'Time spent sending the trigger command over usb')
                    header['SAVETIME'] = (prev_savetime or 'N/A', 'Time to save image in seconds')
                    header['DWLDTIME'] = (image.download_time, 'Time to get image from buffer')
                    header['WAITTIME'] = (0, 'Time spent in seconds waiting')
                    header['MARGTIME'] = (image.download_margin, 'margin until beginning of next exposure')
                    header['EXPTIMES'] = (exposuretime, 'Set exposure time in seconds')
                    header['SUMPIX']   = (np.sum(image.data, dtype=np.int64), 'Sum of all pixel values on the CCD')
                    header['NSAT']     = (len((image.data == 2**16 - 1).nonzero()[0]), 'Number of saturated pixels')
                    header['CEXP']     = (exposure_index, 'Current exposure in sequence')
                    header['ERROR']    = (0, 'Error during image acquisition')

                    # time of the image
                    header['TIMESYS']  = ('UTC', 'All timestamps, unless otherwise noted are in UTC')
                    header['JD']       = (jd, 'Julian Date at midpoint of exposure')
                    header['DATE-OBS'] = (trigger_time_str, 'YYYY-MM-DDThh:mm:ss.sss observation start, UTC')
                    header['IDX']      = (image.seq % 13500, 'Image index (within day, at reference)')
                    header['SEQ']      = (image.seq, 'Image index (counting from arbitrary start date)')
                    header['DATE-BEG'] = (trigger_time_str, 'YYYY-MM-DDThh:mm:ss.sss observation start, UTC')
                    header['DATE-END'] = (obsendtime, 'YYYY-MM-DDThh:mm:ss.sss observation end, UTC')
                    header['OBSID']    = (f'DREAM_{night}_{image.seq % 13500}', 'unique id within the night. DREAM_<YYYYMMDD>_<LSTIDX>')
                    header['UTC-OBS']  = (mid.strftime('%Y-%m-%d %H:%M:%S.%f'), 'YYYY-MM-DD hh:mm:ss.ssss at midpoint, UTC')
                    header['LST']      = (lst, 'Local Sidereal Time in hrs at image midpoint')
                    header['LPST']     = (lpst, 'Reference Sidereal Time in hrs at image midpoint')
                    header['LSTIDX']   = (image.seq % 13500, 'Exposure Nr in Reference Sidereal Time')
                    header['LPSTIDX']  = (image.seq % 13500, 'Exposure Nr in Reference Sidereal Time (old)')
                    header['LSTSEQ']   = (image.seq, 'Reference LST sequence number')
                    header['LPSTSEQ']  = (image.seq, 'Reference LST sequence number (old)')
                    header['LSTREF']   = (cfg.INITIAL_DAY.strftime('%Y-%m-%d %H:%M:%S.%f'), 'ISO UTC at time reference')
                    header['REFSITE']  = ('Reference', 'Reference')
                    header['TOD']      = (tod, 'Time of day in seconds')
                    header['DOY']      = (doy, 'Day of year in days')
                    header['DATE-AVG'] = (mid.strftime('%Y-%m-%d %H:%M:%S.%f'), 'YYYY-MM-DD hh:mm:ss.ssss at midpoint, UTC')
                    header['MJD-AVG']  = (jd-2400000.5, 'MJD at midpoint of exposure')

                    # site
                    header['SITE']     = ('Cerro Pachon', 'Observation site')
                    header['SITE-OBS'] = ('Cerro Pachon', 'Observation site')
                    header['OBSGEO-X'] = (cfg.SITE_LATITUDE, 'Site latitude')
                    header['OBSGEO-Y'] = (cfg.SITE_LONGITUDE, 'Site longitude')
                    header['OBSGEO-Z'] = (cfg.SITE_ALTITUDE/1000, 'Site altitude in km')
                    header['ORIGIN']   = ('Vera C Rubin Observatory', 'Institute of origin')

                    # camera characterics
                    header['INSTRUME'] = ('DREAM', 'Instrument name')
                    header['CAMMODEL'] = (camera.get_model(), 'Camera Identifier')
                    header['CAMERA']   = (cfg.CAMERA, 'Camera orientation (N/E/S/W/C)')
                    header['CAMSER']   = (camera.get_serial(), 'Camera Serial Number')
                    header['SERIAL']   = (camera.get_serial(), 'Camera Serial Number')
                    header['APIVER']   = (version.lib, 'Camera API Version')
                    header['AVERSION'] = ('1.0', 'Acquisition software version')
                    header['CAMVER']   = (f'HW:{version.hw} FW:{version.fw}', 'Camera FW/HW Version')
                    header['XPIXSZ']   = (9.0, 'Pixel Width in microns after binning')
                    header['YPIXSZ']   = (9.0, 'Pixel Height in microns after binning')
                    header['APTDIA']   = (24e-3/1.4, 'Aperture diameter of telescope in meter')
                    header['APTAREA']  = ((24e-3/1.4)**2, 'Aperture area of telescope in meter^2')
                    header['FOCALLEN'] = (24e-3, 'Focal Length in meter')
                    header['MANUFACT'] = ('FLI', 'Camera Manufacturer')
                    header['X0']       = (36, 'start coordinate of active region')
                    header['XSIZE']    = (4008 , 'width of active region')
                    header['Y0']       = (25 , 'start coordinate of active region')
                    header['YSIZE']    = (2672, 'height of active region')

                    if nzeros > 66800:
                        log.error(f'Image #{image.seq} contains too many zeros ({nzeros}). Skipping.')
                        continue

                    # this blocks the event loop. Certified not a problem
                    # right now, but would be neater in a thread
                    checksum = hash(image.data.tobytes())
                    if checksum == checksum_prev:
                        log.error(f'Image #{image.seq} is identical to the previous exposure. Skipping.')
                        continue

                    header['NZEROS'] = (nzeros, 'Number of zero valued pixels')

                    medy = np.median(image.data, axis=1)
                    medx = np.median(image.data, axis=0)
                    maxmedx = max(np.abs(medx[4:-5]-medx[5:-4]))/np.median(medx)
                    maxmedy = max(np.abs(medy[4:-5]-medy[5:-4]))/np.median(medy)
                    header['MAXDX'] = (maxmedx, 'Maximum interline variation')
                    header['MAXDY'] = (maxmedy, 'Maximum interrow variation')

                    tagmap = {
                        'SCIENCE': '',
                        'DARKS': 'dark',
                        'FLATS': 'flat',
                        'BIAS': 'bias',
                        # any of these should never be saved, but for completeness:
                        'BLANKS': 'blank',
                        'IDLE': 'idle',
                        'STANDBY': 'standby',
                        'UNKNOWN': 'unknown'
                    }
                    datadir, thumbnaildir = get_data_dirs()
                    filenamebase = f'{image.seq}{cfg.STATION_ABBRV}{cfg.CAMERA}{tagmap[image.imagetype]}'
                    rawfilename = os.path.join(datadir, filenamebase + '.fits')
                    thumbfilename = os.path.join(thumbnaildir, filenamebase + '.png')
                    header['FILENAME'] = (filenamebase+".fits")

                    start = time.time()
                    log.info(f'Saving thumbnail to \'{thumbfilename}\'')
                    save_thumbnail(image.data, thumbfilename)

                    def save_to_file(filename, data, header):
                        start = time.time()
                        fits.writeto(filename+".tmp", data, header)
                        os.rename(filename+'.tmp', filename)
                        delay = time.time() - start
                        log.info(f'Saving raw file \'{filename}\' took {delay*1000:.1f} ms')
                    await asyncio.to_thread(save_to_file, rawfilename, image.data, header)

                    prev_savetime = time.time() - start

                    dataproduct = DataProduct(
                        kind='image',
                        type=image.imagetype,
                        seq=[image.seq],
                        start=image.triggertime,
                        end=image.triggertime + datetime.timedelta(seconds=cfg.EXPOSURETIME),
                        server=cfg.CAMERA,
                        size=os.stat(rawfilename).st_size,
                        filename=rawfilename,
                        sha256=hashlib.sha256(open(rawfilename, 'rb').read()).hexdigest()
                    )

                    # send a message to central
                    await dataproduct_socket.send_multipart([
                        b'image',
                        json.dumps(dataproduct, default=json_default).encode()])

                    # sure we always have some disk space:
                    _, _, free = shutil.disk_usage(os.path.dirname(rawfilename))
                    # we want at least 1 GiB
                    if free < 2**30:
                        clean_old_images(2**30 - free)


    # run the function in a thread, stop and done are used to request and wait
    # for clean shutdown
    stop = threading.Event()
    done = threading.Event()
    done.set()  # set for now to avoid waiting for trigger loop if cancelled before it starts
    trigger_queue: queue.Queue = queue.Queue()
    image_queue: asyncio.Queue = asyncio.Queue()
    try:
        log.debug('Doing cleanup in case of unclean shutdown')
        await cleanup(camera)

        # start the observation
        log.debug('configuring camera for next exposure sequence')
        camera.set_exposure_time(exposuretime)
        camera.set_image_area(0, 0, 4080, 2721)
        camera.enable_external_trigger()
        log.debug('Starting exposure sequence')
        camera.start_video_mode()

        done.clear()
        trigger_task = asyncio.to_thread(trigger_loop, stop=stop, done=done, trigger_queue=trigger_queue)
        download_task = asyncio.create_task(download_loop(trigger_queue, image_queue))
        process_task = asyncio.create_task(process_loop(image_queue=image_queue))

        # if gather gets cancelled, all tasks are cancelled. This works
        # seamlessly except for the thread (trigger_task), which can't be
        # cancelled. Therefore we also catch CancelledError to stop the thread
        await asyncio.gather(trigger_task, download_task, process_task)

    except (asyncio.CancelledError, KeyboardInterrupt):
        log.info('science mode stop requested')
        stop.set()

        log.info('waiting for science mode to terminate')
        while not done.is_set():
            await asyncio.sleep(0.1)
        log.info('science mode terminated')

        # we gobble the CancelledError here because we don't want it to bubble
        # up to the camera server code. That should just wait for the task to
        # finish, which will happen soon.
        # raise

    except Exception as e:
        log.exception(e)
        raise
