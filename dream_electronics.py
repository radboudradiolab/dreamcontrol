#!/usr/bin/env python3

import asyncio
import dream_config as cfg
from dream_utils import Singleton
#from easysnmp import Session as SNMPSession
from functools import partial
import logging
import dream_credentials as creds
import threading
import time
from typing import Union, List, Callable, Any, Sequence, Literal, Tuple, Optional
import numpy as np
from engineering.dream_dome_simulator import DomeSimulator
from hardware.encoder import Encoder
from hardware.temp_hum import TempSensorSim, TempSensor
from hardware.daq import DAQ
from hardware.pdu import PDUInterface, PDUSim, PDU
from hardware.ups import UPSSim, UPS


log = logging.getLogger(cfg.LOGGER_NAME)

'''A note on PowerSupply current and voltage. When dream control software is not
running, it attempts to shutdown with a setpoint of 0 for both current and
voltage. When the dome is not moving the power to the motor is cut using the
relay and the voltage and current are both set to 20 during standby.
'''

class Hardware(object, metaclass=Singleton):
    """An abstraction layer to get/set all Dream hardware. It allows access to
    fields without knowing wich pdu/port/daq/etc it is attached to. In addition
    the class automatically is a singleton such that it can be accessed from
    different location in the code without having to pass around device handles
    all the time.
    """

    # ANALOG OUTPUT
    # channels for output
    CHANNEL_PS_VOLTAGE = 0  # Analog output 0, pin 13
    CHANNEL_PS_CURRENT = 1  # Analog output 1, pin 14

    # ANALOG INPUT
    # channels for switches (are analog because we are lazy)
    CHANNEL_DOME_OPEN = 0    # Analog input 0, pin 1
    CHANNEL_DOME_CLOSED = 1  # Analog input 1, pin 2
    CHANNEL_DOOR = 2         # Analog input 2, pin 4
    CHANNEL_BACKDOOR = 3     # Analog input 3, pin 5

    # channels for reading feedback
    CHANNEL_PS_VOLTAGE_FEEDBACK = 4  # Analog input 4, pin 7
    CHANNEL_PS_CURRENT_FEEDBACK = 5  # Analog input 5, pin 8
    CHANNEL_PS_OVER_TEMPERATURE = 6  # Analog input 6, pin 10
    CHANNEL_PS_INPUT_FAILURE    = 7  # Analog input 7, pin 11

    # DIGITAL OUTPUT
    # channels for motors
    CHANNEL_MOTOR_ON = 15       # Port B7, Pin 39
    CHANNEL_MOTOR_DIR = 14      # Port B6, Pin 38

    # channels for heating/cooling

    CHANNEL_PELTIERS_DIR = 13 # WARN: reversed from mascara! # Port B5, Pin 37
    CHANNEL_PELTIERS_ON = 2   # WARN: reversed from mascara! # Port A2, Pin 23
    CHANNEL_HEATER = 12  # Port B4, Pin 36

    # channels for leds
    CHANNEL_LED_N = 7                 # Port A3, Pin 24
    CHANNEL_LED_E = 6                 # Port A4, Pin 25
    CHANNEL_LED_S = 5                 # Port A5, Pin 26
    CHANNEL_LED_W = 4                 # Port A6, Pin 27
    CHANNEL_LED_C = 3                 # Port A7, Pin 28
    # all together now
    CHANNELS_LEDS = [3, 4, 5, 6, 7]

    # high sides of limit switches. Should be hard-wired in the future but for
    # now we need to pull these high because they are wired to io pins.
    CHANNELS_PULLUPS = [8, 9, 10, 11] # Ports B0 - B3, Pins 32 - 35

    def __init__(self):
        log.info('Initializing Dream hardware')
        # connect to PDU's
        if 'PDU' in cfg.SIMULATE:
            self.pdu1: PDUInterface = PDUSim(cfg.IP_PDU_1)
            self.pdu2: PDUInterface = PDUSim(cfg.IP_PDU_2)
        else:
            self.pdu1: PDUInterface = PDU(cfg.IP_PDU_1, (creds.PDU_USERNAME, creds.PDU_PASSWORD))
            self.pdu2: PDUInterface = PDU(cfg.IP_PDU_2, (creds.PDU_USERNAME, creds.PDU_PASSWORD))

        # connect to UPS
        if 'UPS' in cfg.SIMULATE:
            self.ups = UPSSim()
        else:
            self.ups = UPS(cfg.IP_UPS, (creds.UPS_USERNAME, creds.UPS_PASSWORD))

        # connect to encoder and DAQ
        if 'DOME' in cfg.SIMULATE:
            self.dome = DomeSimulator()
            self.enc = None
        else:
            self.enc = Encoder()
            self.daq = DAQ(cfg.DAQ_SERIAL)

        # connect to RH sensors
        if 'GUDE' in cfg.SIMULATE:
            self.tmp = [TempSensorSim(ip) for ip in cfg.GUDE_SENSOR_PORTS]
        else:
            self.tmp = [TempSensor(ip) for ip in cfg.GUDE_SENSOR_PORTS]

        # fields to remember last set values
        self.setpoint_voltage = None
        self.setpoint_current = None
        self.motor_relay = None
        self.motor_dir_relay = None
        self.peltier_relay = None
        self.peltier_dir_relay = None
        self.heater_relay = None

        # for safety, start with everything off
        self.set_motor_off()
        self.set_peltiers_off()
        self.set_heater_off()
        self.leds_off()

        # just to have a fully defined state
        self.set_peltiers_to_cool()
        self.set_motor_closing()
        self.set_ps_current(0.)
        self.set_ps_voltage(0.)

        # enable software pullups for switches
        self.set_pullups()



    #################################
    #   High-level dome interface   #

    # def is_dome_open(self):
    #     return ( self.get_dome_open_switch() or
    #              self.get_dome_position() - cfg.DOME_OPEN_POSITION < cfg.DOME_MARGIN )

    # def is_dome_closed(self):
    #     return ( self.get_dome_closed_switch() or
    #              cfg.DOME_CLOSED_POSITION - self.get_dome_position() < cfg.DOME_MARGIN )

    ################
    #   Temp/hum   #

    async def get_temp_hum(self):
        res = {}
        for sensor in self.tmp:
            res = res | await sensor.get_all()
        return res

    ###############
    #   Encoder   #

    def get_dome_position(self):
        if 'DOME' in cfg.SIMULATE:
            return self.dome.get_dome_position()
        else:
            return self.enc.get_position()

    ######################
    #   Power supplies   #

    def set_ps_voltage(self, voltage = 20.0):
        # clip to 0-24V range
        self.setpoint_voltage = np.clip(voltage, 0., 24.)
        if 'DOME' in cfg.SIMULATE:
            self.dome.set_ps_voltage(self.setpoint_voltage)
        else:
            # scale the voltage to 0-5. Full range of the psu is 0-30V.
            # However, it has been rescaled to 0-24 by a hardware potentiometer.
            scaled_voltage = self.setpoint_voltage / 24. * 5.
            self.daq.set_analog_output(self.CHANNEL_PS_VOLTAGE, scaled_voltage)

    def set_ps_current(self, current = 50.):
        # clip to 0-20A range
        self.setpoint_current = np.clip(current, 0., 20.)
        if 'DOME' in cfg.SIMULATE:
            self.dome.set_ps_current(self.setpoint_current)
        else:
            # scale the current to a 0-5V range. Full range on the psu is 0-20A
            scaled_current = self.setpoint_current / 50. * 5.
            self.daq.set_analog_output(self.CHANNEL_PS_CURRENT, scaled_current)

    def get_ps_voltages(self) -> float:
        if 'DOME' in cfg.SIMULATE:
            return self.dome.get_ps_voltage()
        else:
            # factor 6 follows from range conversion: 0-5V from the psu idicates 0-30V actual output
            return 6. * self.daq.read_analog_input(self.CHANNEL_PS_VOLTAGE_FEEDBACK)

    def get_ps_currents(self) -> float:
        if 'DOME' in cfg.SIMULATE:
            return self.dome.get_ps_current()
        else:
            return 10 * self.daq.read_analog_input(self.CHANNEL_PS_CURRENT_FEEDBACK)

    def get_ps_status(self) -> Tuple[bool, bool]:
        '''Get the status of the power supply. There are 2 feedback channels. If
        either becomes logic high (5V) it indicates a problem with the
        temperature or the input voltage respectively.'''
        if 'DOME' in cfg.SIMULATE:
            return True, True
        else:
            return (self.daq.read_analog_input(self.CHANNEL_PS_OVER_TEMPERATURE) < 2.5,
                    self.daq.read_analog_input(self.CHANNEL_PS_INPUT_FAILURE) < 2.5 )


    ###################
    #   LED control   #

    def leds_on(self):
        # leds are wired in current sourcing/open-drain configuration, so we
        # must write a low value to turn them on
        log.info('LEDs On')
        if 'LEDS' not in cfg.SIMULATE:
            for led in self.CHANNELS_LEDS:
                self.daq.set_digital_output(led, False)

    def leds_off(self):
        log.info('LEDs Off')
        if 'LEDS' not in cfg.SIMULATE:
            for led in self.CHANNELS_LEDS:
                self.daq.set_digital_output(led, True)

    def leds_cycle(self):
        period = 1. # 1 Hz. If changed: Call this function at least 4x per period
        phase = time.time() % period / period
        A = 0.00 < phase < 0.50
        B = 0.25 < phase < 0.75
        # log.info(f"LEDs: {'*' if A else '-'}{'*' if B else '-'}{'*' if not A else '-'}{'*' if not B else '-'}")
        if 'LEDS' not in cfg.SIMULATE:
            self.daq.set_digital_output(self.CHANNEL_LED_N, A)
            self.daq.set_digital_output(self.CHANNEL_LED_E, B )
            self.daq.set_digital_output(self.CHANNEL_LED_S, not A )
            self.daq.set_digital_output(self.CHANNEL_LED_W, not B )

            self.daq.set_digital_output(self.CHANNEL_LED_C, A )


    ################
    #   Switches   #

    def set_pullups(self):
        log.info(f'LED pull-ups activated')

        if 'DOME' in cfg.SIMULATE:
            return
        for channel in self.CHANNELS_PULLUPS:
            self.daq.set_digital_output(channel, True)

    def get_door_switch(self):
        if 'DOME' in cfg.SIMULATE:
            return True
        else:
            return self.daq.read_analog_input(self.CHANNEL_DOOR) < 2.5

    def get_backdoor_switch(self):
        if 'DOME' in cfg.SIMULATE:
            return True
        else:
            return self.daq.read_analog_input(self.CHANNEL_BACKDOOR) > 2.5

    def get_dome_open_switch(self):
        if 'DOME' in cfg.SIMULATE:
            return self.dome.get_dome_open_switch()
        else:
            return self.daq.read_analog_input(self.CHANNEL_DOME_OPEN) < 2.5

    def get_dome_closed_switch(self):
        if 'DOME' in cfg.SIMULATE:
            return self.dome.get_dome_closed_switch()
        else:
            return self.daq.read_analog_input(self.CHANNEL_DOME_CLOSED) < 2.5

    ##############
    #    Motor   #

    def set_motor_on(self):
        log.info('Motor ON')
        self.motor_relay = True
        if 'DOME' in cfg.SIMULATE:
            self.dome.set_motor_on()
        else:
            self.daq.set_digital_output(self.CHANNEL_MOTOR_ON, True)

    def set_motor_off(self):
        log.info('Motor OFF')

        self.motor_relay = False
        if 'DOME' in cfg.SIMULATE:
            self.dome.set_motor_off()
        else:
            self.daq.set_digital_output(self.CHANNEL_MOTOR_ON, False)

    def set_motor_closing(self):
        log.info('Motor dir CLOSING')
        self.motor_dir_relay = True
        if 'DOME' in cfg.SIMULATE:
            self.dome.set_motor_closing()
        else:
            self.daq.set_digital_output(self.CHANNEL_MOTOR_DIR, self.motor_dir_relay)

    def set_motor_opening(self):
        log.info('Motor dir OPENING')
        self.motor_dir_relay = False
        if 'DOME' in cfg.SIMULATE:
            self.dome.set_motor_opening()
        else:
            self.daq.set_digital_output(self.CHANNEL_MOTOR_DIR, self.motor_dir_relay)

    #######################
    #   heating/cooling   #

    def set_peltiers_on(self):
        log.info('Peltiers ON')
        self.peltier_relay = True
        if 'DOME' not in cfg.SIMULATE:
            self.daq.set_digital_output(self.CHANNEL_PELTIERS_ON, not self.peltier_relay)

    def set_peltiers_off(self):
        log.info('Peltiers OFF')
        self.peltier_relay = False
        if 'DOME' not in cfg.SIMULATE:
            self.daq.set_digital_output(self.CHANNEL_PELTIERS_ON, not self.peltier_relay)

    def set_peltiers_to_cool(self):
        log.info(f'Peltiers COOLING')
        self.peltier_dir_relay = False
        if 'DOME' not in cfg.SIMULATE:
            self.daq.set_digital_output(self.CHANNEL_PELTIERS_DIR, self.peltier_dir_relay)

    def set_peltiers_to_heat(self):
        log.info(f'Peltiers HEATING')
        self.peltier_dir_relay = True
        if 'DOME' not in cfg.SIMULATE:
            self.daq.set_digital_output(self.CHANNEL_PELTIERS_DIR, self.peltier_dir_relay)

    def set_heater_off(self):
        log.info('Window heaters OFF')
        self.heater_relay = False
        if 'DOME' not in cfg.SIMULATE:
            self.daq.set_digital_output(self.CHANNEL_HEATER, self.heater_relay)

    def set_heater_on(self):
        log.info('Window heaters ON')
        self.heater_relay = True
        if 'DOME' not in cfg.SIMULATE:
            self.daq.set_digital_output(self.CHANNEL_HEATER, self.heater_relay)

    async def get_pdu_status(self):
        async with asyncio.TaskGroup() as tg:
            tg.create_task(self.pdu1_get_status())
            tg.create_task(self.pdu2_get_status())

        status = {1: status_pdu1, 2: status_pdu2}

        return {
            name: status[pdu][port-1] for name, (pdu, port) in cfg.PDU_OUTLETS.items()
        }
