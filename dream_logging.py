#!/usr/bin/env python3
import asyncio
import datetime
import logging
import logging.handlers
import os
from pathlib import Path

import pytz
import rich
import textual
from astropy.utils.iers import IERSRangeError, IERS_A
from rich.console import Console
from rich.containers import Renderables
from rich.highlighter import ReprHighlighter
from rich.logging import RichHandler
from rich.style import Style
from rich.table import Table
from rich.text import Text
from rich.traceback import Traceback

import dream_config as cfg
from dream_utils import get_current_night, get_lst_idx


def lst_idx_formatter(d: datetime.datetime):
    """A custom formatter that includes lst index time"""
    dateandtime = d.strftime('%Y-%m-%dT%H:%M:%S.%f')
    if IERS_A.iers_table is not None:
        lst = f'{get_lst_idx():.4f}'
    else:
        lst = '----------'
    return f'{dateandtime} {lst}'


def setup_logging(logger: logging.Logger, filename: str = 'dream.log'):
    # create logging dir if not exists
    if not os.path.exists(cfg.LOG_PATH):
        os.makedirs(cfg.LOG_PATH)

    # detect if we are inside a detached (systemd) session
    try:
        os.get_terminal_size()
        console = None
    except OSError:
        # when inside systemd, we force a slightly wider terminal than the default 80
        console = Console(width=160)

    # setup logging to stdout
    richhandler = RichHandler(console=console,
                              rich_tracebacks = True,
                              tracebacks_suppress = [rich, textual, asyncio])

    # setup logging to file
    filehandler = logging.handlers.WatchedFileHandler(os.path.join(cfg.LOG_PATH, filename))

    # set all log levels to DEBUG
    logger.setLevel(logging.DEBUG)
    richhandler.setLevel(logging.DEBUG)
    filehandler.setLevel(logging.DEBUG)

    # setup two different formatters for file and screen
    richhandler.setFormatter(logging.Formatter("%(message)s"))
    filehandler.setFormatter(logging.Formatter("%(levelname)s %(asctime)s [%(filename)s:%(funcName)s:%(lineno)d] %(message)s"))

    # add the handlers to the logger
    logger.addHandler(richhandler)
    logger.addHandler(filehandler)

    # print the first message
    logger.info('Logger initialized')


async def logrotate(log: logging.Logger):
    '''rotate logs once per day at local noon.'''
    while True:
        # determine time of noon (could be previous or next)
        now = datetime.datetime.now(pytz.timezone(cfg.SITE_ZONE))
        noon = now.replace(hour=12, minute=0, second=0, microsecond=0)

        # sleep until next noon
        seconds_till_noon = (noon-now).total_seconds() % 86400
        log.info(f'Scheduling next log rotation at {now+datetime.timedelta(seconds=seconds_till_noon)}')
        await asyncio.sleep(seconds_till_noon)
        log.info('Rotating log file...')

        # rotate python logging file destination
        for handler in log.handlers:
            # we can only rotate WatchedFileHandlers because the move needs to be detected to work
            if isinstance(handler, logging.handlers.WatchedFileHandler):
                # get the tag from an hour ago, 11:00:00.000 and a bit
                # before 12 noon get_current_night returns the date tag of the day before
                now = datetime.datetime.now(pytz.timezone(cfg.SITE_ZONE))
                lastnight = get_current_night(now - datetime.timedelta(hours=-1))
                runningname = handler.baseFilename
                archivename = os.path.splitext(runningname)[0] + '_' + lastnight + '.log'

               # do the file move. logging.WatchedFileHandler detects this and reopens a new file
                os.rename(runningname, archivename)

        log.info('Log file rotated')



class RichLogHandler(logging.Handler):
    '''logging.LogHandler that formats logs messages nicely and then prints them
    inside a textual.RichLog text area. Heavily inspired by rich.RichLogger'''
    def __init__(self, textlog, app, **kwargs):
        super().__init__(**kwargs)
        self.textlog = textlog
        self.app = app
        self._last_time = None
        self._last_idx = 0
        self.highlighter = ReprHighlighter()

    def render_traceback(self, record: logging.LogRecord):
        if record.exc_info and record.exc_info != (None, None, None):
            exc_type, exc_value, exc_traceback = record.exc_info
            return Traceback.from_exception(
                exc_type, exc_value,
                exc_traceback,
                width=self.textlog.virtual_size.width,
                show_locals=True,
                suppress = (asyncio, rich, textual)
            )
        else:
            return None


    def render_time(self, record: logging.LogRecord):
        log_time = datetime.datetime.fromtimestamp(record.created)
        real = log_time.strftime('%X')
        frac = log_time.strftime('.%f')[:4]
        if real == self._last_time:
            return Text(' ' * len(real) + frac)
        else:
            self._last_time = real
            return Text(real + frac)

    def render_lst_idx(self, record: logging.LogRecord):
        when = datetime.datetime.fromtimestamp(record.created, tz=datetime.timezone.utc)
        if IERS_A.iers_table is None:
            return Text('')
        seq = get_lst_idx(when)
        if int(seq) == self._last_idx:
            return Text(' ' * len(str(self._last_idx)) + f'.{int((seq % 1) * 1000):03d}')
        else:
            self._last_idx = int(seq)
            return Text(f'{seq:.3f}')

    def render_level(self, record: logging.LogRecord):
        level_name = record.levelname
        level_text = Text.styled(level_name.ljust(8), f"logging.level.{level_name.lower()}")
        return level_text

    def render_file(self, record: logging.LogRecord):
        path_name = Path(record.pathname).name
        line_no = record.lineno
        return Text.styled(f'{path_name}:{line_no}', Style(color='grey50'))

    def render_message(self, record: logging.LogRecord):
        message = Text(record.getMessage())
        message = self.highlighter(message)
        return message



    def emit(self, record: logging.LogRecord):
        table = Table.grid(padding=(0, 1), expand=True)

        table.add_column(style="log.time")
        table.add_column(style='hot_pink')
        table.add_column(style="log.level", width=8)
        table.add_column(ratio=1, style="log.message", overflow="fold")
        table.add_column(style="log.path")

        message = self.render_message(record)
        traceback = self.render_traceback(record)
        renderables = [message, traceback] if traceback else [message]

        table.add_row(
            self.render_time(record),
            self.render_lst_idx(record),
            self.render_level(record),
            Renderables(renderables),
            self.render_file(record),
        )
        if self.app.is_running:
            self.textlog.write(table, expand=True)
