#!/usr/bin/env python3
import dream_config as cfg
import logging
import yaml

log = logging.getLogger(cfg.LOGGER_NAME)

try:
    with open('dream_credentials.yaml', 'r') as f:
        credentials = yaml.safe_load(f)

    PDU_USERNAME = credentials['pdu']['username']
    PDU_PASSWORD = credentials['pdu']['password']
    UPS_USERNAME = credentials['ups']['username']
    UPS_PASSWORD = credentials['ups']['password']

    SLACK_CHANNEL = credentials['slack']['channel']
    SLACK_TOKEN = credentials['slack']['token']

except KeyError as e:
    raise RuntimeError(f'Missing a mandatory key in dream_credentials.yaml: {e}') from e
except FileNotFoundError:
    if cfg.CAMERA == '':
        log.warning(f'Could not find dream_credentials.yaml. Please copy the example file and edit the passwords.')
    else:
        log.info(f'Could not find dream_credentials.yaml. If this is a camera computer that is fine.')
except yaml.YAMLError as e:
    raise RuntimeError('Could not parse credentials') from e
