#!/usr/bin/env python3

from abc import ABC, abstractmethod
import astropy.io.fits as fits
import asyncio
from collections import namedtuple
import dream_logging
import dream_config as cfg
import enum
import logging
import numpy as np
if 'CAMERA' not in cfg.SIMULATE:
    import pyfli
import socket
import time

log = logging.getLogger(cfg.LOGGER_NAME)

VersionInfo = namedtuple('VersionInfo', ['hw', 'fw', 'lib'])

class CameraStatus(enum.Enum):
    IDLE = enum.auto()
    WAITING = enum.auto()
    EXPOSING = enum.auto()
    READING = enum.auto()
    UNKNOWN = enum.auto()


class CameraInterface(ABC):
    """Abstract base class for FLI Camera access. This only wraps the pyfli
    interface methods that we need in Dream. For the logic of timing the frames
    to LST sequences see dream_triggering
    """

    @abstractmethod
    def get_num_cameras(self) -> int:
        pass

    @abstractmethod
    def connect(self) -> None:
        pass

    @abstractmethod
    def disconnect(self) -> None:
        pass

    @abstractmethod
    def is_connected(self) -> bool:
        pass

    @abstractmethod
    def get_serial(self) -> str:
        pass

    @abstractmethod
    def get_version(self) -> VersionInfo:
        pass

    @abstractmethod
    def get_model(self) -> str:
        pass

    @abstractmethod
    def get_ccd_temp(self) -> float:
        pass

    @abstractmethod
    def set_ccd_temp(self, setpoint: float) -> None:
        pass

    @abstractmethod
    def expose_frame(self) -> None:
        pass

    @abstractmethod
    def cancel_exposure(self) -> None:
        pass

    @abstractmethod
    def set_exposure_time(self, exptime: float) -> None:
        pass

    @abstractmethod
    def get_exposure_status(self) -> float:
        pass

    @abstractmethod
    def grab_frame(self) -> np.ndarray:
        pass

    @abstractmethod
    def get_device_state(self) -> CameraStatus:
        pass

    @abstractmethod
    def get_data_ready(self) -> bool:
        pass

    @abstractmethod
    def enable_external_trigger(self) -> None:
        pass

    @abstractmethod
    def disable_external_trigger(self) -> None:
        pass

    @abstractmethod
    def start_video_mode(self) -> None:
        pass

    @abstractmethod
    def grab_video_frame(self) -> np.ndarray:
        pass

    @abstractmethod
    def trigger_exposure(self) -> None:
        pass

    @abstractmethod
    def stop_video_mode(self) -> None:
        pass

    @abstractmethod
    def set_image_area(self, ul_x: int, ul_y: int, lr_x: int, lr_y: int) -> None:
        pass

class AsyncSimulator(CameraInterface):
    def __init__(self):
        # initialize some variables for the ccd temp control
        self.ccd_temp_func = lambda: 42.0
        self.loop = asyncio.get_running_loop()
        self._ccd_reached_temp = self.loop.call_later(0, lambda: 42)

        # initialize camera function parameters
        self.exposing = False
        self.reading = False
        self.waiting = False
        self.data = False
        self.external = False
        self.trigger = asyncio.Event()
        self.videomode = False
        self.exptime = 1.0
        self.exp_task = None
        self.connected = False

    def get_num_cameras(self) -> int:
        return 1

    def get_serial(self) -> str:
        # for uniqueness purposes this uses the last byte of the local ip
        # address to generate a serial number.
        hostname = socket.getfqdn()
        ipaddress = socket.gethostbyname(hostname)
        lastbyte = ipaddress.split('.')[-1]
        return f'ML{lastbyte:0>7}'

    def get_version(self) -> VersionInfo:
        return VersionInfo(0, 0, 'Simulated FLI camera')

    def get_model(self) -> str:
        return 'Simulated MicroLine ML11002'

    def get_ccd_temp(self) -> float:
        return self.ccd_temp_func()

    def set_ccd_temp(self, setpoint: float) -> None:
        log.debug(f'setting CCD temp to {setpoint}')
        ccd_temp = self.ccd_temp_func()
        delta = setpoint - ccd_temp
        speed = 2.3 * np.sign(delta) # degrees per second

        # y = a * x + b
        # through (current time, current temp)
        # a being speed or -speed
        # ccd_temp = speed * time.time() + b
        # b = ccd_temp - speed * time.time()
        offset = ccd_temp - time.time() * speed

        def make_temp_linear(speed, offset):
            def func():
                return speed * time.time() + offset
            self.ccd_temp_func = func

        make_temp_linear(speed, offset)

        # cancel any timer and start a new one
        self._ccd_reached_temp.cancel()
        self._ccd_reached_temp = self.loop.call_later(delta / speed, make_temp_linear, 0, setpoint)


    async def _exposure(self):
        self.exposing = True
        try:
            self.exp_finished = time.time() + self.exptime
            self.exp_task = asyncio.create_task(asyncio.sleep(self.exptime))
            await self.exp_task
            self.exp_task = None
        except asyncio.CancelledError:
            self.exposing = False
        else:
            self.exposing = False
            self.reading = True
            await asyncio.sleep(1.5)
            self.data = True
            self.reading = False

    async def _readout(self):
        log.debug('Starting readout')
        self.reading = True
        if self.data:
            log.warning('Image overwritten before it was retrieved')
        await asyncio.sleep(1.5)
        self.reading = False
        self.data = True
        log.debug('Readout done')

    async def _video_mode(self):
        exptime = self.exptime
        external = self.external
        stop = False
        self.data = False
        self.videomode = True
        while self.videomode and not stop:
            self.waiting = True
            if external:
                # trigger wait is not cancelable
                log.debug('Clearing trigger flag for next image')
                self.trigger.clear()
                log.debug('Waiting for trigger')
                await self.trigger.wait()
                log.debug('Trigger received')
            self.waiting = False
            self.exposing = True
            try:
                log.debug('Starting exposure')
                self.exp_finished = time.time() + exptime
                start = time.time()
                self.exp_task = asyncio.create_task(asyncio.sleep(exptime))
                await self.exp_task
                log.debug(f'Simulator error: {time.time() - start - exptime:.3f}')
                log.debug('exposure done')
            except asyncio.CancelledError:
                log.debug('exposure cancelled')
                stop = True
            finally:
                # log.debug('cleaning up background task')
                self.exp_task = None
            self.exposing = False
            # start reading in the background
            if not stop:
                asyncio.create_task(self._readout())
        self.data = False
        log.debug('Video mode terminated')




    def expose_frame(self) -> None:
        asyncio.create_task(self._exposure())

    def cancel_exposure(self) -> None:
        if self.exp_task:
            self.exp_task.cancel()

    def set_exposure_time(self, exptime: float) -> None:
        self.exptime = exptime

    def get_exposure_status(self) -> float:
        if self.exposing:
            return int(1000 * max(0, self.exp_finished - time.time()))
        else:
            return 0

    def grab_frame(self) -> np.ndarray:
        return self.grab_video_frame()

    def get_device_state(self) -> CameraStatus:
        if self.reading:
            return CameraStatus.READING
        if self.exposing:
            return CameraStatus.EXPOSING
        if self.waiting:
            return CameraStatus.WAITING
        return CameraStatus.IDLE

    def get_data_ready(self) -> bool:
        return self.data

    def enable_external_trigger(self) -> None:
        self.external = True

    def disable_external_trigger(self) -> None:
        self.external = False

    def start_video_mode(self) -> None:
        self._video_mode_task = asyncio.create_task(self._video_mode())

    def grab_video_frame(self) -> np.ndarray:
        if self.data is None:
            log.warning('An image read was attempted when the ready flag was not advertised.')
            time.sleep(1.0)
            return np.zeros((2721, 4080))
        else:
            log.debug('Reading image data')
            time.sleep(1.5)
            log.debug('Clear data ready flag')
            self.data = False
            start = time.time()
            data = fits.getdata('simulator_image.fits')
            log.info(f'Fits image loaded in {time.time()-start:.3f} s')
            return data

    def trigger_exposure(self) -> None:
        '''The simulator, unlike the real camera,uses asyncio to simulate
        operations. This means that we cannot call methods from other threads.
        Fortunately, exposure timing routines are mostly asyncio based too and
        play nice toether. The trigger loop is the exception, which, for
        accuracy reasons, is run from a dedicated thread. That means we have to
        take appropriate measures to expect this method to be invoked from a
        thread different from the asyncio/main thread.
        '''
        def internal_trigger():
            if self.trigger.is_set():
                log.warning('Trigger already set!')
            log.debug('Setting the trigger flag')
            self.trigger.set()
        self.loop.call_soon_threadsafe(internal_trigger)

    def stop_video_mode(self) -> None:
        self.videomode = False

    def connect(self) -> None:
        self.connected = True

    def disconnect(self) -> None:
        self.connected = False

    def is_connected(self) -> bool:
        return self.connected

    def set_image_area(self, ul_x: int, ul_y: int, lr_x: int, lr_y: int) -> None:
        pass


class FLICamera(CameraInterface):
    """Wrapper class to interface to FLI camera.

    Note that it is not needed to lock the device since pyfli does that already."""
    cam = None

    def get_num_cameras(self) -> int:
        return len(pyfli.FLIList('usb', 'camera'))

    def connect(self) -> None:
        # open camera
        devs = pyfli.FLIList('usb', 'camera')
        if len(devs) < 1:
            raise RuntimeError('No FLI Camera found')
        self.cam = pyfli.FLIOpen(devs[0][0], 'usb', 'camera')

    def disconnect(self) -> None:
        pyfli.FLIClose(self.cam)
        self.cam = None

    def is_connected(self) -> bool:
        return self.cam is not None

    def get_serial(self) -> str:
        return pyfli.getSerialString(self.cam)

    def get_version(self) -> VersionInfo:
        hw = pyfli.getHWRevision(self.cam)
        fw = pyfli.getFWRevision(self.cam)
        lib = pyfli.getLibVersion()
        # there is a decoding bug in the python wrapper
        if lib.startswith('b\''):
            lib = lib[1].strip('\'')

        return VersionInfo(hw, fw, lib)

    def get_model(self) -> str:
        return pyfli.getModel(self.cam)

    def get_ccd_temp(self) -> float:
        ccd_temp = pyfli.getTemperature(self.cam)
        return ccd_temp


    def set_ccd_temp(self, setpoint: float) -> None:
        pyfli.setTemperature(self.cam, setpoint)

    def expose_frame(self) -> None:
        pyfli.exposeFrame(self.cam)

    def cancel_exposure(self) -> None:
        pyfli.cancelExposure(self.cam)

    def set_exposure_time(self, exptime: float) -> None:
        pyfli.setExposureTime(self.cam, int(exptime * 1000))

    def get_exposure_status(self) -> float:
        return 0.001 * pyfli.getExposureStatus(self.cam)

    def grab_frame(self) -> np.ndarray:
        img = pyfli.grabFrame(self.cam)
        return np.array(img)

    def get_device_state(self) -> CameraStatus:
        status = pyfli.getDeviceStatus(self.cam)
        status = status & pyfli.CAMERA_STATUS_MASK

        if status == pyfli.CAMERA_STATUS_IDLE:
            return CameraStatus.IDLE
        if status == pyfli.CAMERA_STATUS_WAITING_FOR_TRIGGER:
            return CameraStatus.WAITING
        if status == pyfli.CAMERA_STATUS_EXPOSING:
            return CameraStatus.EXPOSING
        if status == pyfli.CAMERA_STATUS_READING_CCD:
            return CameraStatus.READING
        return CameraStatus.UNKNOWN

    def get_data_ready(self) -> bool:
        status = pyfli.getDeviceStatus(self.cam)
        return bool(status & pyfli.CAMERA_DATA_READY)

    def enable_external_trigger(self) -> None:
        pyfli.enableExternalTrigger(self.cam)

    def disable_external_trigger(self) -> None:
        pyfli.disableExternalTrigger(self.cam)

    def start_video_mode(self) -> None:
        pyfli.startVideoMode(self.cam)

    def grab_video_frame(self) -> np.ndarray:
        img = pyfli.grabVideoFrame(self.cam)
        return np.array(img)

    def trigger_exposure(self) -> None:
        pyfli.triggerExposure(self.cam)

    def stop_video_mode(self) -> None:
        pyfli.stopVideoMode(self.cam)

    def set_image_area(self, ul_x: int, ul_y: int, lr_x: int, lr_y: int) -> None:
        pyfli.setImageArea(self.cam, ul_x, ul_y, lr_x, lr_y)


if __name__ == '__main__':
    dream_logging.setup_logging(log)
    log.setLevel(logging.DEBUG)

    async def ccd_test():
        cam = AsyncSimulator()
        log.info('Camera instance created')
        log.info(f'CCD temp: {cam.get_ccd_temp():.2f} \'C')
        cam.set_ccd_temp(-20)
        log.info(f'CCD temp: {cam.get_ccd_temp():.2f} \'C')
        for i in range(10):
            await asyncio.sleep(0.5)
            log.info(f'CCD temp: {cam.get_ccd_temp():.2f} \'C')
        log.info('changing setpoint to 40')
        cam.set_ccd_temp(40)
        for i in range(10):
            await asyncio.sleep(0.5)
            log.info(f'CCD temp: {cam.get_ccd_temp():.2f} \'C')

    async def single_exp_test():
        cam = AsyncSimulator()
        log.info('Camera instance created')
        async def monitor():
            for _ in range(140):
                log.info(f'camera: {cam.get_device_state()} data: {cam.get_data_ready()} remaining: {cam.get_exposure_status()}')
                await asyncio.sleep(0.1)
        async def procedure():
            await asyncio.sleep(1.0)
            cam.set_exposure_time(6.382525)
            cam.expose_frame()
            await asyncio.sleep(10)
            cam.expose_frame()
            await asyncio.sleep(2)
            cam.cancel_exposure()

        await asyncio.gather(monitor(), procedure())

    async def video_mode_test():
        cam = AsyncSimulator()
        log.info('Camera instance created')
        async def monitor():
            for _ in range(160):
                log.info(f'camera: {cam.get_device_state()} data: {cam.get_data_ready()} remaining: {cam.get_exposure_status()}')
                await asyncio.sleep(0.1)
        async def procedure():
            await asyncio.sleep(1.0)
            cam.set_exposure_time(6.382525)
            cam.enable_external_trigger()
            log.info('Starting video mode')
            cam.start_video_mode()
            await asyncio.sleep(1.0)
            log.info('Sending trigger')
            cam.trigger_exposure()
            await asyncio.sleep(6.5)
            log.info('Sending second trigger')
            cam.trigger_exposure()
            await asyncio.sleep(2.0)
            log.info('Reading frame')
            cam.grab_video_frame() # takes ~ 1.5 s
            await asyncio.sleep(5)
            log.info('Stopping')
            cam.stop_video_mode()
            await asyncio.sleep(0.2)
            # cleanup
            log.info('Trigger for cleanup')
            cam.trigger_exposure()
            await asyncio.sleep(0.2)
            log.info('Cancel for cleanup')
            cam.cancel_exposure()


        await asyncio.gather(monitor(), procedure())

    # asyncio.run(ccd_test())
    # asyncio.run(single_exp_test())
    asyncio.run(video_mode_test())
