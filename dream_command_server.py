#!/usr/bin/env python3

import asyncio
import datetime
import json
import logging
import time
from typing import Callable, Optional

import zmq
import zmq.asyncio

import dream_config as cfg
from dream_dataclasses import (CameraClientInfo, CameraControlInfo,
                               CameraServerMode, DomeState)
from dream_electronics import Hardware
from dream_exposure_timing import ImageInfo
from dream_monitoring import DreamMonitoring
from dream_state import DreamState
from dream_utils import Singleton, format_zmq_id, json_object_hook

log = logging.getLogger(cfg.LOGGER_NAME)

class DreamCommandServer(object, metaclass=Singleton):
    '''Class that handles the clients from each camera.

    For simplicity (see design notes) we rely on a very simple polling mechanism
    almost everywhere. But here we make an exception, because it is really nice
    if the gui updates a bit responsively to camera events. Especially since the
    heartbeat polling already introduces a short delay.
    '''

    def __init__(self):
        # initially empty dict of clients by id
        #self.clients: dict[str, CameraClientInfo] = {}

        # callback, used by gui for more responsive redrawing
        self.callback: Optional[Callable] = None

        # add outself to the state
        #state = DreamState()
        #state.server = self

    async def heartbeat_loop(self):
        '''A separate loop that periodically checks if any of the heartbeats has
        timed out and removes that camera handle from the list.'''
        state = DreamState()
        while True:
            for name in list(state.camerainfo.keys()):
                camera = state.camerainfo[name]
                if camera.heartbeat < datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(seconds=60):
                    log.info(f'client {name} timed out. Stale for more than 60s. Removing.')
                    del state.camerainfo[name]
            # always call the callback at least once to update out-of-date clients
            if self.callback:
                self.callback()

            # sleep
            await asyncio.sleep(1.0)


    async def serve_cameras_loop(self):
        ctx = zmq.asyncio.Context()
        log.info('ZeroMQ context created')

        socket = ctx.socket(zmq.ROUTER)
        socket.bind(f'tcp://*:{cfg.CAMERA_CONTROL_PORT}')
        log.info('Listening...')

        state = DreamState()

        try:
            while True:
                # receive a message
                id, _, msg_type, rawdata = await socket.recv_multipart()
                # convert meta data to str
                client_id = format_zmq_id(id)
                msg_type = msg_type.decode()

                # fallback reply
                reply  = {'status': 'error', 'error': 'unknown error'}

                try:
                    # extract the info
                    data = json.loads(rawdata, object_hook=json_object_hook)

                    # extract name for easy access
                    name = data['name']

                    # check if it is a new client so we can create the data structure
                    if name not in state.camerainfo:
                        log.info(f'new client for camera {name} (client id: {client_id})')
                        client_type = 'script' if msg_type == 'script' else 'camera'
                        now = datetime.datetime.now(datetime.timezone.utc)
                        state.camerainfo[name] = CameraClientInfo(client_id, now, client_type)

                    # process the message
                    if msg_type == 'heartbeat':
                        reply = self.handle_heartbeat(data)
                    elif msg_type == 'image':
                        reply = self.handle_image(data)
                    elif msg_type == 'script':
                        reply = self.handle_script(data)
                    else:
                        reply  = {'status': 'error', 'error': 'missing or unknown message type'}

                    # call the callback
                    if self.callback:
                        self.callback()

                except Exception as e:
                    log.exception(e)
                    reply = {'status': 'error', 'error': e.__repr__()}
                finally:
                    await socket.send_multipart([id, b'', json.dumps(reply).encode()])

        except asyncio.CancelledError:
            # this is just nice cleanup to prevent errors upon exit
            log.info('Closing ZeroMQ server loop')
            raise
        finally:
            socket.close()
            ctx.destroy()


    def handle_heartbeat(self, data):
        # convert datetimes and enums: automatic now due to object_hook
        state = DreamState()
        # coerce into a named tuple,  discarding data that does not fit
        keys = CameraControlInfo.__dataclass_fields__ & data.keys()
        data = {key: data[key] for key in keys}
        info = CameraControlInfo(**data)
        state.camerainfo[data['name']].camera_data = info

        # log ccd temp
        monitoring = DreamMonitoring()
        if info.ccd_temp is not None:
            f = monitoring.get_logfile(f'ccd_{info.name}')
            f.write(f'{time.time()},{info.ccd_temp}\n')

        # record the heartbeat
        state.camerainfo[data['name']].heartbeat = datetime.datetime.now(datetime.timezone.utc)

        # make an appropriate reply
        state = DreamState()
        return {
            'status': 'ok',
            'current_mode': state.mode
        }

    def handle_image(self, data):
        # get the 'special' data
        name = data['name']
        data['data'] = data['preview']

        state = DreamState()
        hw = Hardware()

        # coerce into a named tuple,  discarding data that does not fit
        keys = ImageInfo.__dataclass_fields__ & data.keys()
        data = {key: data[key] for key in keys}
        info = ImageInfo(**data)

        state.camerainfo[name].image_data = info

        # record a log
        log.info(f'Image #{info.seq} received from {name} with expected type {info.imagetype}')

        # get a handle to the state
        state = DreamState()

        # test if leds were on or flashing due to override
        if info.imagetype != 'BLANKS' and state.leds_ready_since > info.triggertime:
            log.info(f'Discarding image #{info.seq} of type {info.imagetype} because leds were on or flashing')
            return {
                'status': 'ok',
                'type': 'discard'
            }

        # test if the dome is not moving
        if info.imagetype !='BLANKS' and state.dome_last_move > info.triggertime:
            log.info(f'Discarding image #{info.seq} of type {info.imagetype} because dome was moving')
            return {
                'status': 'ok',
                'type': 'discard'
            }

        # test if the dome is open/closed and not stopped
        if info.imagetype == 'SCIENCE' and state.dome_state is not DomeState.OPEN:
            log.info(f'Discarding image #{info.seq} of type {info.imagetype}: dome was not open')
            return {
                'status': 'ok',
                'type': 'discard'
            }
        # for calibrations, the stop state is acceptable as long as the dome is closed
        if info.imagetype in ['DARKS', 'FLATS', 'BIAS'] and not hw.get_dome_closed_switch():
            log.info(f'Discarding image #{info.seq} of type {info.imagetype}: dome was not closed')
            return {
                'status': 'ok',
                'type': 'discard'
            }

        # test that led flashed if this was a flat and not otherwise
        if info.imagetype == 'FLATS' and info.seq not in state.recent_led_blinks:
            log.info(f'Discarding image #{info.seq} of type {info.imagetype}: leds were not blinked')
            return {
                'status': 'ok',
                'type': 'discard'
            }
        if info.imagetype in ['SCIENCE', 'DARKS', 'BIAS'] and info.seq in state.recent_led_blinks:
            log.info(f'Discarding image #{info.seq} of type {info.imagetype}: leds were blinked')
            return {
                'status': 'ok',
                'type': 'discard'
            }

        # if the dome was not moving and in the right position for the image type
        # and the leds were not overruled and blinking according to the image type,
        # then all is ok and the image is indeed of the type that was expected.
        return {
            'status': 'ok',
            'type': info.imagetype
        }

    def handle_script(self, data):
        log.info(f'Handling script input: {data}')
        if 'mode' in data:
            mode = data['mode']
            if mode not in list(CameraServerMode):
                raise ValueError(f'Invalid mode requested: {mode}')

            log.info(f'Scripted change to {mode} mode')
            state = DreamState()
            state.target_mode = CameraServerMode(mode)

            state.camerainfo[data['name']].heartbeat = datetime.datetime.now(datetime.timezone.utc)

        return {
            'status': 'ok'
        }
