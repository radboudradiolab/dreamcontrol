#!/usr/bin/env python3

import asyncio
import datetime
from dream_state import DreamState, Warnings, Errors, CameraClientInfo
import dream_config as cfg
from dream_electronics import Hardware
from dream_monitoring import DreamMonitoring
from functools import partial
import logging
from typing import Any
import time
import json

log = logging.getLogger(cfg.LOGGER_NAME)


class RubinHandler():
    num_connected_clients = 0
    # for a little bit of extra robustness we copy the new data products to
    # here. And then we only remove them from the global cache once we sent out
    # the ack.
    new_data_products = []

    def camera_summary(self, cam: CameraClientInfo):
        return {
            'last_heartbeat': cam.heartbeat.isoformat(timespec='milliseconds'),
            'camera_mode': cam.camera_data.state if cam.camera_data else None,
            'ccd_temp': cam.camera_data.ccd_temp if cam.camera_data else None,
            'num_blanks': cam.camera_data.num_blanks if cam.camera_data else None,
            'num_darks': cam.camera_data.num_darks if cam.camera_data else None,
            'num_bias': cam.camera_data.num_bias if cam.camera_data else None,
            'num_flats': cam.camera_data.num_flats if cam.camera_data else None,
            'num_science': cam.camera_data.num_science if cam.camera_data else None,
            'num_missed': cam.camera_data.num_missed if cam.camera_data else None,
            'last_image_seq': cam.image_data.seq if cam.image_data else None,
            'last_image_triggertime': cam.image_data.triggertime.isoformat(timespec='milliseconds') if cam.image_data else None,
            'last_image_timing_latency': cam.image_data.timing_latency if cam.image_data else None,
            'last_image_usb_latency': cam.image_data.usb_latency if cam.image_data else None,
            'last_image_artificial_latency': cam.image_data.artificial_latency if cam.image_data else None,
            'last_image_type': cam.image_data.imagetype if cam.image_data else None,
            'last_image_pixel_median': cam.image_data.med if cam.image_data else None,
            }


    def dream_summary(self):
        '''Generate a json formatted summary of the DREAM station status'''
        hw = Hardware()
        state = DreamState()
        monitoring = DreamMonitoring()
        return {
            # high level state and target states
            'target_observing_mode': state.target_mode,
            'actual_observing_mode': state.mode,
            'target_dome_state': state.dome_target_state,
            'actual_dome_state': state.dome_state,
            'target_heater_state': state.heater_target_state,
            'actual_heater_state': state.heater_state,
            'target_peltier_state': state.peltier_target_state,
            'actual_peltier_state': state.peltier_state,
            # monitoring and hardware state
            'temp_hum': monitoring.rh_status,
            'pdu_status': monitoring.pdu_status,
            'ups_status': monitoring.ups_status,
            'psu_status': {
                'temp_error': not hw.get_ps_status()[0],
                'input_error': not hw.get_ps_status()[1],
                'voltage_feedback': hw.get_ps_voltages(),
                'current_feedback': hw.get_ps_currents(),
                'voltage_setpoint': hw.setpoint_voltage,
                'current_setpoint': hw.setpoint_current
            },
            'limit_switches': {
                'front_door': 'hit' if hw.get_door_switch() else 'open',
                'back_door': 'hit' if hw.get_backdoor_switch() else 'open',
                'dome_open': 'hit' if hw.get_dome_open_switch() else 'not hit',
                'dome_closed': 'hit' if hw.get_dome_closed_switch() else 'not hit',
            },
            'electronics': {
                'motor_relay': 'on' if hw.motor_relay else 'off',
                'motor_dir': 'closing' if hw.motor_dir_relay else 'opening',
                'peltier_relay': 'on' if hw.peltier_relay else 'off',
                'peltier_dir': 'cooling' if hw.peltier_dir_relay else 'heating',
                'window_heaters': 'on' if hw.heater_relay else 'off'
            },
            'dome_position': hw.get_dome_position(),
            'errors': [error for error in state.problems if isinstance(error, Errors)],
            'warnings': [warning for warning in state.problems if isinstance(warning, Warnings)],
            'cameras': {name: self.camera_summary(cam) for name, cam in state.camerainfo.items() if cam.client_type=='camera'}
        }


    def handle_incomming(self, payload: Any, source: str):
        try:
            # decode some payload properties:
            request_id = payload['request_id']
            action = payload['action']

            # whatever happens next, we consider this a sign of life
            state = DreamState()
            state.rubin.last_request_time = datetime.datetime.now(datetime.timezone.utc)
            state.rubin.last_request_id = request_id

            monitoring = DreamMonitoring()

            # handle the request:
            if action == 'getStatus':
                return {
                    'result': 'ok',
                    'request_id': request_id,
                    'msg_type': 'status',
                    'status': self.dream_summary()
                }
            elif action == 'getNewDataProducts':
                self.new_data_products = state.new_data_products
                return {
                    'result': 'ok',
                    'request_id': request_id,
                    'msg_type': 'list',
                    # we cannot rely on json_default here because we have to stick to rubin specs
                    'new_products': [{'kind': p.kind,
                                      'type': p.type,
                                      'seq': p.seq,
                                      'start': p.start.isoformat(),
                                      'end': p.end.isoformat(),
                                      'server': p.server,
                                      'size': p.size,
                                      'filename': p.filename,
                                      'sha256': p.sha256} for p in self.new_data_products]
                }
            elif action == 'setWeather':
                state.rubin.weather_ok = bool(payload['data'])
                state.rubin.weather_updated = datetime.datetime.now(datetime.timezone.utc)
                #log.info(f'Weather {state.rubin.weather_ok}')

                monitoring.get_logfile('rubin_weather_ok').write(
                    f'{time.time()}, {state.rubin.weather_ok}, {source}\n')

                return {
                    'request_id': request_id,
                    'result': 'ok'
                }

            elif action == 'setRoof':
                state = DreamState()
                state.rubin.dome_ok = bool(payload['data'])
                state.sync()
                #log.info(f'Dome {state.rubin.dome_ok}')

                monitoring.get_logfile('rubin_dome_ok').write(
                    f'{time.time()}, {state.rubin.dome_ok}, {source}\n')

                return {
                    'request_id': request_id,
                    'result': 'ok'
                }
            elif action == 'heartbeat':
                return {
                    'request_id': request_id,
                    'result': 'ok'
                }
            else:
                return {
                    'result': 'error',
                    'reason': f'Unknown action {action}'
                }
        except KeyError as e:
            return {
                'result': 'error',
                'reason': f'Invalid request: a mandatory key is missing: {e.args[0]}'
            }

    def update_warning_flags(self):
        state = DreamState()
        state.set_problem(Warnings.RUBIN_MULTIPLE_CLIENTS, self.num_connected_clients > 1)
        state.set_problem(Warnings.RUBIN_NO_CLIENT, self.num_connected_clients == 0)

    async def client_connect_cb(self, reader: asyncio.StreamReader, writer: asyncio.StreamWriter):
        addr = writer.get_extra_info('peername')[0]
        log.info(f'Rubin client connected from {addr}')

        state = DreamState()
        state.rubin.ip_address = addr

        # this need not be threadsafe because asyncio is inherently safe
        self.num_connected_clients += 1
        self.update_warning_flags()

        while True:
            # get one line
            data = await reader.readline()

            try:
                # detect when client hangs up
                if len(data) == 0:
                    log.info(f'Rubin client {addr} disconnected')
                    self.num_connected_clients -= 1
                    self.update_warning_flags()

                    return

                # utf-8 decode
                message = data.decode()

                payload = json.loads(message)
                reply = self.handle_incomming(payload, addr)
            except json.JSONDecodeError:
                log.error('Malformed JSON received')
                reply = {
                    'result': 'error',
                    'reason': 'malformed input'
                }
            except Exception as e:
                # we eat up any other error after logging
                log.error(f'Error in communication with Rubin client from {addr}:')
                log.exception(e)
                # clear the new data products copy, just in case
                self.new_data_products = []
                reply = {
                    'result': 'error',
                    'reason': 'see server logs'
                }
            finally:
                # whatever happened, we send the reply
                rawreply = json.dumps(reply).encode()
                writer.write(rawreply)
                writer.write(b'\n')
                await writer.drain()
                # now that we are certain they are out on the network, we can
                # remove them from our cache. Since network may still fail but
                # that is outside of our control:
                state.clear_data_products(self.new_data_products)
                self.new_data_products = []

    async def server_loop(self):
        self.update_warning_flags()
        server = await asyncio.start_server(
            self.client_connect_cb,
            port=cfg.RUBIN_PORT,
            start_serving=True)

        for sock in server.sockets:
            addr, port = sock.getsockname()[:2]
            log.info(f'Serving on {addr}:{port}')

        # there is some magic in asyncio start_server that cancels the client
        # loops when this routine gets cancelled, even tought we just return
        # here
