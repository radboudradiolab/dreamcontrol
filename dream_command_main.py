#!/usr/bin/env python3

import argparse
import asyncio
import logging
import os
import signal
from tempfile import TemporaryDirectory

import filelock
from astropy.config import set_temp_cache
from astropy.utils.iers import conf

import dream_config as cfg
import dream_logging
from dream_cloud_aggregator import CloudmapAggregator
from dream_command_gui import NormalExit, maintenance_gui_loop
from dream_command_server import DreamCommandServer
from dream_control_routines import (dome_fast_control_loop,
                                    dome_slow_control_loop,
                                    emergency_close_if_needed,
                                    end_of_night_loop, led_control_loop,
                                    power_control_loop, station_mode_loop,
                                    temperature_loop)
from dream_electronics import Hardware
from dream_monitoring import DreamMonitoring
from dream_rubin_server import RubinHandler
from dream_state import DreamState, Warnings
from dream_utils import (flatten_exception_group, handle_sigterm, iers_init,
                         iers_update_loop, send_slack_message)

log = logging.getLogger(cfg.LOGGER_NAME)
log.setLevel(logging.DEBUG)

'''Architecture note:

The central command server has multiple parallel tasks. We employ an asyncio
TaskGroup to manage them. If any fails with an uncaught exception the other ones
are cancelled. They can catch the cancellation and do cleanup. The TaskGroup
waits for this cleanup before restarting. The reason to divide all works into
separate loops is that the implementation stays cleaner if each loop awaits one
event at a time. When you find yourself implementing posix-select like logic
(although perfectly possible using asyncio.wait() or lower level
asyncio.TaskGroup instances) it may be time to split things into finer grained
task loops. Conversely, if a loop has nothing to wait for, it may candidate for
merging with another loop. I.e., this was already done for the monitoring loop,
which is embedded into the sensor readout loop.

The tasks are as follows:

* camera control loop: determines the mode of operation. It needs input from the
  dome, and from the go/nogo condition. It decides the mode and when to
  shutdown computers and cameras (during daytime and/or power outages). Since
  the mode is only computed when a camera computer requests it, this is also the
  loop that awaits heartbeats and images from the camera computers. The mock
  dream gui application lets a human operator take the role of this loop.

* optionally, the above can be replaced with the maintenance gui, which runs in
  its own loop.

* rubin command loop: receive go/nogo updates and report back station status

* reduction result loop: receive and merge clouds for passing onto rubin

* sensor readout and monitoring loop: collect all data about the station
  sensors. This could potentially also be the logging loop since the information
  mostly overlaps.

* dome control loop: based on calibration phase and weather conditions
  open/close the dome. It also needs access to the limit switches. This loop is
  in charge of the control logic: the dome is not allowed to open within X
  minutes after closing (to prevent bouncing behaviour).

These loops all use and produce certain variables, some of which should trigger
other loops to take action or are queried by other loops to make decisions. From
past experience we know that we should refrain from implementing a key-value
store and/or advanced value based trigger system. Instead we rely on
old-fashioned polling. It results in much cleaner, more readable, and robuster
code and a lower system load. Only in select cases the timing may not allow
this. In particular: cloud map updates should be propagated as they come in. And
during dome open/close the limit switches need to be polled much faster than is
necessary for monitoring/housekeeping.

The loops communicate through a number of flags. Each flag is set by only one of
the loops. In the implementation we store the flags in a singleton class that
also persists the values over restarts such that the station can transparently
survive reboots/restarts of the control software. We list the flags here ordered
by the controlling loop:

* Dome target state. This is set by the camera control loop. It is used by the
  dome loop to determine if the dome should open/close (but it does not
  blindly follow that because there could also be overriding circumstances). Its
  main purpose is to be sent back to the cameras.

* Go/nogo. Set by the rubin command loop. It is used by the dome control loop
  to open/closed as requested. Informing the camera computers to take blanks is
  not necesary as the camera loop will poll the history of opens/closes that is
  kept by the dome control loop.

* UPS level. Set by the sensor loop. Used by the control loop to initiate
  standby/shutdown if needed.

* Humidity ok. Set by the sensor loop. Used by the dome loop to close the dome
  to prevent condensation.

* hardware warning/error. This is a flag set by the sensor loop. In case of
  communication errors with any of the hardware it is set. In case of warning
  this is logged and warning messages are sent. In case of error the control
  loop will perform a full station reboot to attempt recovery.

* dome state. The actual (not the target) state of the dome. Set by the dome
  loop. It must also contain the last change move time since the camera loop
  needs to know this to assess the status of newly made images. Also used to
  override the peltiers and heaters to temporarily turn off while moving the
  dome.

'''


async def dream_command(maintenance=False):
    try:
        # notify the world that we restarted
        send_slack_message('Dream Command starting up...')

        # these are unused but inialized to trigger errors early on if there are any
        hw = Hardware()
        state = DreamState()
        rubin = RubinHandler()
        aggregator = CloudmapAggregator()

        log.info('Starting monitoring...')
        monitoring = DreamMonitoring()

        state.set_problem(Warnings.SIMULATED_DOME, 'DOME' in cfg.SIMULATE)
        state.set_problem(Warnings.SIMULATED_LEDS, 'LEDS' in cfg.SIMULATE)
        state.set_problem(Warnings.SIMULATED_UPS, 'UPS' in cfg.SIMULATE)
        state.set_problem(Warnings.SIMULATED_PDU, 'PDU' in cfg.SIMULATE)
        state.set_problem(Warnings.SIMULATED_ENV, 'GUDE' in cfg.SIMULATE)
        state.set_problem(Warnings.SIMULATED_SUN, 'SUNRISE' in cfg.SIMULATE or 'SUNSET' in cfg.SIMULATE)

        log.info('Starting command server...')
        server = DreamCommandServer()

        async with asyncio.TaskGroup() as tg:
            tg.create_task(iers_update_loop())
            tg.create_task(station_mode_loop())
            tg.create_task(power_control_loop())
            tg.create_task(dome_fast_control_loop())
            tg.create_task(monitoring.logrotate())
            tg.create_task(temperature_loop())
            tg.create_task(monitoring.monitoring_loop())
            tg.create_task(led_control_loop())
            tg.create_task(server.heartbeat_loop())
            tg.create_task(server.serve_cameras_loop())
            tg.create_task(dome_slow_control_loop())
            tg.create_task(rubin.server_loop())
            tg.create_task(end_of_night_loop())
            tg.create_task(aggregator.server_loop())
            if maintenance:
                tg.create_task(maintenance_gui_loop())

    except* NormalExit:
        log.info('TUI exited as expected')
    except* Exception as e:
        log.error('Dream control crashed')
        send_slack_message('Dream Command ran into an unexpected error. Closing down and cleaning up.')

        for subexception in flatten_exception_group(e):
            log.error(subexception)
            # This should be log.exception() but rich logging does not
            # understand exceptiongroups (yet (feb 2024)). log.except would
            # print the traceback of the ExceptionGroup itself.
            log.exception(subexception)
            location = subexception.__traceback__.tb_frame.f_code.co_filename
            linenumber = subexception.__traceback__.tb_lineno

            log.warning(f' in {location} line {linenumber}. logfile contains a proper traceback.')
    except* BaseException:
        # Cancellation, SystemExit etc. are handled and we do not want to pollute the console with errors
        pass
    finally:
        await emergency_close_if_needed()
        log.info('Closing hardware connections')
        # for hardware handles this is absolutely necessary.
        # without it python segfaults :o
        Hardware.clear()


def main():
    # update tables before doing anything else
    iers_init()

    # for extra safety
    if ('UPS' in cfg.SIMULATE or
        'PDU' in cfg.SIMULATE or
        'GUDE' in cfg.SIMULATE or
        'SUNSET' in cfg.SIMULATE or
        'SUNRISE' in cfg.SIMULATE):
        log.warning('Forcing DOME simulation because other hardware is simulated.')
        cfg.SIMULATE.append('DOME')

    # parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--gui', help='Start a TUI instead of autonomous control', action='store_true')
    args = parser.parse_args()

    # fake terminal color support
    os.environ['TERM'] = 'xterm-256color'

    if args.gui:
        log.info('Disabling auto-start in TUI mode')
        cfg.AUTOSTART = False
        cfg.SKIP_EMERGENCY_CLOSE = True
        cfg.DOME_MINIMUM_CLOSE_TIME = 30

    # start the task group
    loop = asyncio.new_event_loop()  # Explicitly create a new loop
    asyncio.set_event_loop(loop)    # Set it as the current loop

    # Attach SIGTERM and SIGINT handler. SIGINT is already handled by
    # asyncio by default but the handler is too agressive and also cancels
    # the emergency close.
    loop.add_signal_handler(signal.SIGTERM, lambda: handle_sigterm(loop))
    loop.add_signal_handler(signal.SIGINT, lambda: handle_sigterm(loop))

    loop.run_until_complete(dream_command(maintenance=args.gui))

    log.info("Waiting for running async tasks to finish...")
    tasks = [t for t in asyncio.all_tasks(loop) if t is not asyncio.current_task(loop)]

    # TODO: this seems to have become obsolete in python 3.12.7, consider removing
    for task in tasks:
        task.cancel()

    if tasks:
        loop.run_until_complete(asyncio.gather(*tasks, return_exceptions=True))

    loop.close()
    log.info('Bye')
    send_slack_message('Dream Command shutting down.')


if __name__ == '__main__':
    # prevent running two instances
    with filelock.FileLock('/tmp/dreamcontrol.lock', timeout=0):
        # start logging to file
        dream_logging.setup_logging(log, cfg.LOG_CONTROL_FILENAME)

        # create a temporary cache
        with TemporaryDirectory() as tmpdir:
            log.info(f'Using IERS cache dir: {tmpdir}')
            with set_temp_cache(tmpdir):
                main()
