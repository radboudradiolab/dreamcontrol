#!/usr/bin/env python3
import argparse
import asyncio
import datetime
import json
import logging
import signal
from tempfile import TemporaryDirectory
from typing import Literal

import filelock
import zmq
import zmq.asyncio
from astropy.config import set_temp_cache

import dream_config as cfg
import dream_credentials as creds
import dream_exposure_timing
import dream_logging
from dream_camera import AsyncSimulator, FLICamera
from dream_dataclasses import CameraControlInfo, CameraServerMode
from dream_utils import (Box, FrameCounters, get_lst_idx, handle_sigterm,
                         iers_init, iers_update_loop, json_default,
                         json_object_hook, shutdown,
                         wrap_with_exception_loggging)
from hardware.pdu import PDU, PDUInterface, PDUSim

log = logging.getLogger(cfg.LOGGER_NAME)


class DreamCameraServer:
    ccd_temp = None

    async def run(self):
        # initialize camera
        if 'CAMERA' in cfg.SIMULATE:
            self.camera = AsyncSimulator()
            cfg.EXPOSURE_MARGIN += 0.050  # this simulator is somewhat inaccurate
        else:
            self.camera = FLICamera()

        # initialize other camera variables
        self.state = CameraServerMode.IDLE
        self.state_changed = asyncio.Queue()
        self.counters = FrameCounters()

        # create a zmq context
        self.context: zmq.asyncio.Context = zmq.asyncio.Context()
        log.info("ZMQ context created")
        await asyncio.gather(
            wrap_with_exception_loggging(self.heartbeat_loop()),
            wrap_with_exception_loggging(self.camera_loop()),
            wrap_with_exception_loggging(dream_logging.logrotate(log)),
            wrap_with_exception_loggging(iers_update_loop()),
        )
        log.debug('Camera server stopped gracefully')
        if self.state == CameraServerMode.SHUTDOWN:
            # schedule a pdu poweroff:
            pdu, outlet = self.get_pdu('server')
            if pdu and 'SHUTDOWN' not in cfg.SIMULATE:
                await pdu.turn_off_delayed(outlet)
            # exit python and os shutdown:
            shutdown()

    def get_pdu(self, what: Literal['camera', 'server']) -> (PDUInterface, int):
        server = ('Central' if cfg.CAMERA == 'C' else
                  'North' if cfg.CAMERA == 'N' else
                  'East' if cfg.CAMERA == 'E' else
                  'South' if cfg.CAMERA == 'S' else
                  'West' if cfg.CAMERA == 'W' else
                  'Unknown')
        outletname = f'{server} {what.capitalize()}'
        if outletname not in cfg.PDU_OUTLETS:
            return None, None

        pdunum, outlet = cfg.PDU_OUTLETS[outletname]
        ip = cfg.IP_PDU_1 if pdunum == 1 else cfg.IP_PDU_2
        pdu: PDUInterface = (
            PDUSim(ip) if 'PDU' in cfg.SIMULATE else
            PDU(ip, (creds.PDU_USERNAME, creds.PDU_PASSWORD)))
        return pdu, outlet

    def get_ccd_temp(self):
        """Get the latest CCD temp from camera. Performs a readout if it is safe
        to do so without interfering with the trigger timing. Returns the cached
        last value otherwise. If the camera is disconnected (during
        daytime/idling) None is returned because the CCD temperature cannot be
        retrieved at such times.

        """
        # check if we are connected
        if not self.camera.is_connected():
            return None

        # check if we can update
        current_offset = cfg.EXPOSURETIME * (get_lst_idx() % 1.0)
        if 0.5 < current_offset < cfg.EXPOSURETIME - 0.5:
            self.ccd_temp = self.camera.get_ccd_temp()

        # return the latest available measurement
        return self.ccd_temp

    async def camera_loop(self):
        """Control loop taking the images."""
        current_task = None
        imagetype: Box[str] = Box('UNKNOWN')

        while True:
            # wait for a state change request
            requested_state = await self.state_changed.get()

            # check if it actually changed
            if requested_state == self.state:
                continue
            else:
                log.info(f'Camera control loop changing mode {self.state} -> {requested_state}')

            # optimisation: when switching between science/flats/darks we can
            # just continue because nothing changed
            long_exposure_states = [ CameraServerMode.BLANKS,
                                     CameraServerMode.SCIENCE,
                                     CameraServerMode.FLATS,
                                     CameraServerMode.DARKS ]
            if ( self.state in long_exposure_states and
                 requested_state in long_exposure_states):
                log.info('Camera can just continue in current mode.')
                self.state = requested_state
                imagetype.set(self.state)
                continue

            # if something was happening, cancel
            if current_task is not None: # in other words: not currently_idle
                current_task.cancel()
                log.info(f'Stopping the worker for {self.state} mode...')
                try:
                    await current_task
                except asyncio.CancelledError:
                    log.info('camera mode cancelled as expected')
                except Exception as e:
                    log.error('An unexpected exception occurred while stopping the science mode:')
                    log.exception(e)
                    # We gobble this exception since the task should be finished now anyways
                    # raise
                log.info(f'The worker for {self.state} has stopped')
                current_task = None

            # warmup if we are going to idle
            # we need to do this before disconnecting
            if requested_state == 'IDLE':
                log.info('Stop CCD cooling')
                self.camera.set_ccd_temp(cfg.CCD_IDLE_TEMP)

            # connect the camera if necessary
            if (requested_state not in [CameraServerMode.IDLE, CameraServerMode.SHUTDOWN]
                    and not self.camera.is_connected()):
                # power up via snmp
                pdu, outlet = self.get_pdu('camera')
                if pdu:
                    await pdu.turn_on(outlet)

                # test if camera is connected
                for i in range(9):
                    if self.camera.get_num_cameras() > 0:
                        break
                    log.info(f'Waiting for USB camera to appear... {i+1}')
                    await asyncio.sleep(1)
                else:
                    raise RuntimeError('USB camera not found')

                # connect via usb
                self.camera.connect()
                # blanks dont count anymore if the camera was offline
                self.counters.rst('blanks')
                self.counters.rst('missed')

            # disconnect the camera if necessary
            if requested_state == CameraServerMode.IDLE and self.camera.is_connected():
                log.info('disconnecting camera')
                self.camera.disconnect()

                # not sure if the disconnect takes any time
                # just for good measure
                await asyncio.sleep(1)

                # power down via snmp
                pdu, outlet = self.get_pdu('camera')
                await pdu.turn_off_immediate(outlet)

            # cooldown for any other mode
            # we need to do this after connecting
            if requested_state not in ['IDLE', 'SHUTDOWN']:
                log.info('Cooling CCD')
                self.camera.set_ccd_temp(cfg.CCD_OPERATIONAL_TEMP)

            # determine exposure time for the new mode
            if requested_state == CameraServerMode.BIAS:
                exposuretime = cfg.EXPTIME_BIAS
            else:
                exposuretime = cfg.EXPOSURETIME - cfg.EXPOSURE_MARGIN

            # update the imagetype used to tag the expected exposure type
            imagetype.set(requested_state)

            # start the new task
            if requested_state not in ['IDLE', 'STANDBY', 'SHUTDOWN']:
                log.info(f'Starting a worker for {requested_state} mode...')
                current_task = asyncio.create_task(
                    dream_exposure_timing.image_sequence(
                        self.camera, exposuretime, self.counters, imagetype))

            # update the state
            self.state = requested_state

            # shutdown if in powersaving mode
            if requested_state == 'SHUTDOWN':
                break




    async def heartbeat_loop(self):
        # forever reconnect if anything goes wrong with communication
        while True:
            # create a socket and connect to command computer:
            self.socket: zmq.asyncio.Socket = self.context.socket(zmq.REQ)
            ip = cfg.IP_DREAM_CONTROL
            port = cfg.CAMERA_CONTROL_PORT
            self.socket.connect(f"tcp://{ip}:{port}")
            log.info(f'ZMQ sockets connected to tcp://{ip}:{port}')

            # forever send heartbeats and read response
            while True:
                # prep an update message
                status = CameraControlInfo(
                    name = cfg.CAMERA,
                    state = self.state,
                    ccd_temp = self.get_ccd_temp(),
                    num_blanks=  self.counters.get('blanks'),
                    num_darks = self.counters.get('darks'),
                    num_bias = self.counters.get('bias'),
                    num_flats = self.counters.get('flats'),
                    num_science = self.counters.get('science'),
                    num_missed = self.counters.get('missed'),
                    time = datetime.datetime.now()
                )
                # send it as a heartbeat
                await self.socket.send_multipart([b'heartbeat',
                    json.dumps(status.__dict__, default=json_default).encode()])

                # fetch a reply:
                if (await self.socket.poll(1000 * cfg.HEARTBEAT_TIMEOUT) & zmq.POLLIN) == 0:
                    log.error('Heartbeat did not get a timely response!')
                    self.socket.setsockopt(zmq.LINGER, 0)
                    self.socket.close()
                    break

                rawreply: bytes = await self.socket.recv()
                reply = json.loads(rawreply, object_hook=json_object_hook)

                # log.debug(f'heartbeat ping-pong {reply["status"]}')
                #log.debug(reply)
                log.info(f'Heartbeat ok. Current station mode: {reply["current_mode"]}')

                # parse the requested state as a CameraServerState
                try:
                    self.state_changed.put_nowait(CameraServerMode(reply['current_mode']))
                except (ValueError, KeyError) as e:
                    log.exception(e)

                # if shutdown, then break the loop
                if reply['current_mode'] == 'SHUTDOWN':
                    log.debug('Bye bye')
                    return

                # sleep
                await asyncio.sleep(cfg.HEARTBEAT_INTERVAL)


def main():
    # update tables before doing anything else
    iers_init()

    parser = argparse.ArgumentParser()
    parser.add_argument('--simulate', help='Simulate the camera instead of accessing usb device', action='store_true')
    parser.add_argument('--blanks', help="Number of blanks", type=int, default=cfg.N_BLANKS)
    parser.add_argument('--darks', help="Number of darks", type=int, default=cfg.N_DARKS)
    parser.add_argument('--flats', help="Number of flats", type=int, default=cfg.N_FLATS)
    parser.add_argument('--bias', help="Number of biases", type=int, default=cfg.N_BIAS)
    parser.add_argument('--camera', help="Override which camera this is", type=str)

    # Do the actual parsing
    args = parser.parse_args()

    # Do something with the parse result
    cfg.N_BLANKS = args.blanks
    cfg.N_DARKS = args.darks
    cfg.N_FLATS = args.flats
    cfg.N_BIAS = args.bias

    # override the camera
    if args.camera:
        cfg.CAMERA = args.camera

    # abort if neither specified nor detectable
    if cfg.CAMERA is None:
        raise RuntimeError('Could not determine which camera we are.')

    if args.simulate:
        if 'CAMERA' not in cfg.SIMULATE:
            cfg.SIMULATE.append('CAMERA')
    else:
        if 'CAMERA' in cfg.SIMULATE:
            cfg.SIMULATE.remove('CAMERA')

    loop = asyncio.new_event_loop()  # Explicitly create a new loop
    asyncio.set_event_loop(loop)    # Set it as the current loop

    # Attach SIGTERM and SIGINT handler. SIGINT is already handled by
    # asyncio by default but the handler is too agressive and also cancels
    # the emergency close.
    loop.add_signal_handler(signal.SIGTERM, lambda: handle_sigterm(loop))
    loop.add_signal_handler(signal.SIGINT, lambda: handle_sigterm(loop))

    # Create an instance of the server class
    server = DreamCameraServer()

    loop.run_until_complete(server.run())

    log.info("Waiting for running async tasks to finish...")
    tasks = [t for t in asyncio.all_tasks(loop) if t is not asyncio.current_task(loop)]

    # TODO: this seems to have become obsolete in python 3.12.7, consider removing
    for task in tasks:
        task.cancel()

    if tasks:
        loop.run_until_complete(asyncio.gather(*tasks, return_exceptions=True))

    loop.close()
    log.info('Bye')


if __name__ == "__main__":
    # prevent running two instances
    with filelock.FileLock(f'/tmp/dream-{cfg.CAMERA}.lock', timeout=0):
        # start logging to file
        dream_logging.setup_logging(log, cfg.LOG_CAMERA_FILENAME)

        # create a temporary cache
        with TemporaryDirectory() as tmpdir:
            log.info(f'Using IERS cache dir: {tmpdir}')
            with set_temp_cache(tmpdir):
                main()
