import argparse
import asyncio
import datetime
import glob
import hashlib
import io
import json
import logging
import os
import pickle
import time
from functools import partial

import astropy.units as units
import h5py
import healpy as hp
import numpy as np
import zmq
import zmq.asyncio
from astropy.io import fits
from astropy.table import Table

import dream_config as cfg
from dream_electronics import Hardware
from dream_monitoring import DreamMonitoring
from dream_state import (CameraClientInfo, DataProduct, DreamState, Errors,
                         Warnings)
from dream_utils import get_current_night, json_object_hook, seq_to_datetime

log = logging.getLogger(cfg.LOGGER_NAME)

'''Note:

The only slightly complicated part about this is the decision process about when
to start combining clouds if part of the data is missing. During normimal
operations all five cameras produce images very regularly. However, the data
reduction pipeline treats every 5th image which introduces variability in the
latency. It is not even guaranteed that clouds maps arrive in-order. To make
matters worse one camera can miss a beat in rare situation and the software
needs to continue in a degraded mode if one or more cameras are offline.

To simplify the aggregation procedure we adopt the following heuristic: We do
not consider a sliding window but only fixed 5-exposure intervals containting
cloud maps for (seq%5==0) until (seq%5==4). This will eliminate most of the
latency variability for our output products.

We trigger an aggregation step in one of 2 cases. Once all images are there, or
after a timeout. Since we wait for the seq%5==4 image anyway we may as well
timeout all images at that time. We define the timeout for the whole block of 5
images to be 40 seconds after the first image of that block was triggered.

From the dream state we can check if cameras are online or offline, this allows
us to skip the timeout for cameras that are not recording. This is just an
optimisation.

The actual combination is done in a background thread because it is a bit of
actual work and we do not want to block the event loop.

The timing of imcoming images can be unexpected and even our of order. The
following depicts the images incoming at + and the corresponding cloud map
coming out at !. Images 0, 1, and 2 may arrive out of order because they are
started more or less at the same time.

Image n-1: uction->!
Image n+0: +<-calibration-><-reduction->!
Image n+1:      +          <-reduction->!
Image n+2:            +    <-reduction->!
Image n+3:                   +<-reduction->!
Image n+4:                         +<-recuction->!
Image n+5:                               +<-calibration-><-reduction->!

'''

class CloudmapAggregator():

    # We store the most recently combined so we do not try to combine again
    most_recently_combined_seq = 0
    # and a map from camera, to a map from seq number to the data
    #cloud_queue: Map[str, Map[int, Any]] = {
    cloud_queue = {
        'N': {},
        'E': {},
        'S': {},
        'W': {},
        'C': {}
    }

    async def server_loop(self):
        ctx = zmq.asyncio.Context()

        socket = ctx.socket(zmq.SUB)
        socket.bind(f'tcp://*:{cfg.REDUCTION_CONTROL_PORT}')
        socket.subscribe('')
        poller = zmq.asyncio.Poller()
        poller.register(socket, zmq.POLLIN)

        log.info('Cloudmap Aggregaror listening...')

        monitoring = DreamMonitoring()
        state = DreamState()

        while True:
            # receive a message

            timeout = await self.assess()
            log.info(f'next timeout: {timeout:.3f} s')
            if not await poller.poll(timeout * 1e3): # poll takes ms
                # poller.poll() returns the list of sockets with data. In this
                # case there is only one socket. If it has no data, the empty
                # list evaluates as False and we recompute a new timeout, which
                # also forces a cloud combination with missing data if
                # appropriate.
                continue
            rawdata = await socket.recv_multipart()
            topic = rawdata[0].decode()

            if topic == 'cloud':
                _, camera, seq, jd, nobs, clouds, sigma = rawdata
                # # convert bytes to objects
                camera = pickle.loads(camera)
                camera = camera[-1].upper()
                seq = pickle.loads(seq)
                jd = pickle.loads(jd)
                nobs = pickle.loads(nobs)
                clouds = pickle.loads(clouds)
                sigma = pickle.loads(sigma)
                log.info(f'Cloud map {seq} received for camera {camera}: nobs shape: {nobs.shape}')
                monitoring.get_logfile(f'cloudmap_timing_{camera}').write(f'{time.time()},{seq}\n')
                self.register_new_map(camera, seq, jd, nobs, clouds, sigma)
                # add a dataproduct:
                starttime = seq_to_datetime(seq)
                dataproduct: DataProduct = DataProduct(
                    kind='cloud',
                    type=None,
                    seq=list(range(seq, seq+4)),
                    start=starttime,
                    end=starttime + datetime.timedelta(seconds=cfg.EXPOSURETIME*5),
                    server=camera,
                    size=-1,  # TODO: how do we get this?
                    filename='<unknown>', # how do we get this?
                    sha256='<missing>'
                )
            elif topic == 'image':
                dataproduct = json.loads(rawdata[1], object_hook=json_object_hook)
                state.add_data_product(dataproduct)
            else:
                log.error(f'Received unexpected message for topic \'{topic}\'')
                continue



    def register_new_map(self, camera: str, seq: int, jd: float, nobs, clouds, sigma) -> None:
        # store it in the queue
        self.cloud_queue[camera][seq] = (jd, nobs, clouds, sigma)

    def cloudmap_(self, camera: str, seq: int) -> bool:

        return seq in self.cloud_queue[camera]


    async def assess(self) -> float:
        '''
        assesses the situation. Combines clouds if there is a complete set.
        Returns the timeout after which it should run again if no image is received.
        '''

        # determine which cameras are alive. Cameras that are not alive will not
        # be waited for:
        state = DreamState()
        max_age = datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(seconds=30)
        alive_clients = [client for client in state.camerainfo.values()
                         if client.client_type == 'camera'
                         and client.camera_data is not None # also consider non-alive if counters are missing because we need them
                         and client.image_data is not None
                         and client.image_data.triggertime > max_age
                         ]
        alive_client_names = [client.camera_data.name for client in alive_clients]

        #log.debug(f'alive clients: {alive_client_names}')

        # early back-out if there are no images at all, needed to avoid min([]) below
        nonempty_cameras = [k for k,v in self.cloud_queue.items() if len(v) > 0]
        if len(nonempty_cameras) == 0:
            #log.debug('No clouds at all')
            return 999.0  # practically float('inf')

        # always consider the oldest seq, because of timeout it is not possible
        # that we are working on more than one block at a time.
        oldest_seq = min([min(self.cloud_queue[cam]) for cam in nonempty_cameras])

        # round to lower multiple of 5 to get the block we are working on
        block = oldest_seq // 5 * 5
        #log.debug(f'Oldest seq={oldest_seq}, Working on block {block}')

        # the deadline is 40 seconds after the last image of the block becomes available
        now = datetime.datetime.now(tz=datetime.timezone.utc)
        #log.info(f'it is now {now}')
        #log.info(f'block {block} starts at {seq_to_datetime(block)}')
        deadline = seq_to_datetime(block) + datetime.timedelta(seconds=cfg.CLOUD_COMBINE_FRAMES * cfg.EXPOSURETIME + 40.)
        #log.info(f'We should wait until {deadline}')
        now = datetime.datetime.now(tz=datetime.timezone.utc)
        timeout = (deadline - now).total_seconds()
        #log.info(f'block should finish in {timeout:.3f} s')

        should_wait = False
        for cam, clouds in self.cloud_queue.items():
        # never wait for a stalling/uninitialized camera
            if cfg.CLOUD_COMBINE_FAST and cam not in alive_client_names:
                continue
            # wait if <5 images are there
            if not all([seq in clouds for seq in range(block, block + cfg.CLOUD_COMBINE_FRAMES)]):
                #log.debug(f'Camera {cam} could still yield more maps')
                should_wait = True
                # break

        # if we should wait, compute how long
        if should_wait and not timeout <= 0.0:
            # just for safety, never return negative. 0.1 margin keeps other tasks responsive
            return timeout

        # if we get to here, we have at least one cloud and whatever we have should be combined
        #log.info(f'Combining cloud maps {block}-{block+4}')
        start = time.time()
        filename = await asyncio.to_thread(self.combine_cloudmaps, block)
        log.info(f'Combining clouds {block}-{block+4} took {time.time()-start:.3f} s')

        # let rubin know
        starttime = seq_to_datetime(block)
        dataproduct = DataProduct(
            kind='cloud',
            type=None,
            seq=list(range(block, block+4)),
            start=starttime,
            end=starttime + datetime.timedelta(seconds=cfg.EXPOSURETIME*5),
            server='B',
            size=os.stat(filename).st_size,
            filename=filename,
            sha256=hashlib.sha256(open(filename, 'rb').read()).hexdigest()
        )

        state.add_data_product(dataproduct)

        # in this case we return 0, which will trigger an immediate re-evaluation
        return 0.

    def combine_cloudmaps(self, lstseq_start, combine='average',
                           file_type='fits', sigma_factor=0):

        clouds_list = []
        sigma_list = []
        nobs_list = []
        jds_list = []
        used = {cam: [] for cam in self.cloud_queue}

        for cam in self.cloud_queue:
            for seq in range(lstseq_start, lstseq_start + cfg.CLOUD_COMBINE_FRAMES):
                if seq in self.cloud_queue[cam]:
                    jd, nobs, clouds, sigma = self.cloud_queue[cam][seq]
                    # remember that we used it for the header later
                    used[cam].append(seq)
                    # remove from queue
                    del(self.cloud_queue[cam][seq])

                    # clouds, sigma and nobs are arrays with the number of
                    # healpix tiles at a chosen level: 12*4**(cfg.hp_level)
                    clouds_list.append(clouds)
                    sigma_list.append(sigma)
                    nobs_list.append(nobs)
                    # convert jd to an array with the same size and mask as clouds
                    # to be able to determine the effective JD as a function of
                    # healpixel (e.g. if an expected clouds value is missing in a
                    # particular healpixel)
                    jds_list.append(np.ma.zeros_like(clouds, dtype=float) + jd)
                    #log.info(f'Cloud {cam}:{seq} jd={jd}')

        # stack the arrays
        clouds_stack = np.ma.stack(clouds_list)
        sigma_stack = np.ma.stack(sigma_list)
        nobs_stack = np.ma.stack(nobs_list)
        jds_stack = np.ma.stack(jds_list)

        log.info(f'jds_stack: {jds_stack}')

        # combine cloud/sigma/nobs
        if combine == 'average':
            clouds_comb = np.ma.average(clouds_stack, axis=0)
            sigma_comb = self.calc_sigma(sigma_stack, axis=0)

        elif combine == 'median':
            clouds_comb = np.ma.median(clouds_stack, axis=0)
            sigma_comb = np.sqrt(np.pi/2) * self.calc_sigma(sigma_stack, axis=0)

        elif combine == 'weighted':
            clouds_comb, weights_sum = np.ma.average(clouds_stack, axis=0,
                                                    weights=1/sigma_stack**2,
                                                    returned=True)
            sigma_comb = 1/np.sqrt(weights_sum)
        else:
            log.error(f'combination method {combine} not known')
            return

        # record nobs as sum
        nobs_comb = np.ma.sum(nobs_stack, axis=0)

        mask = ~clouds_comb.mask & ~sigma_comb.mask

        # record jds as average
        jds_comb =  np.ma.average(jds_stack, axis=0)
        log.info(f'jds_comb: {jds_comb}')

        # effective jd
        jd_eff = np.ma.median(jds_comb)
        log.info(f'jds_eff: {jd_eff}')

        # convert dtype to float32/int32
        clouds_comb = clouds_comb.astype('float32')
        sigma_comb = sigma_comb.astype('float32')
        nobs_comb = nobs_comb.astype('int32')

        # clean arrays in place
        if cfg.CLOUD_COMBINE_CLEAN:
            self.clean_map(clouds_comb, sigma_comb, nobs_comb, cfg.CLOUD_COMBINE_FRAMES, sigma_factor)

        # default name for output_map
        lstmin = lstseq_start
        lstmax = lstseq_start + cfg.CLOUD_COMBINE_FRAMES - 1

        night = get_current_night()
        filename = os.path.join(cfg.CLOUDMAP_DIR, night, f'clouds{lstmin}_{lstmax}_{combine}.{file_type}')
        dirname = os.path.dirname(filename)
        os.makedirs(dirname, exist_ok=True)

        # infer HEALPix parameters from length of clouds
        npix = len(clouds_comb)
        nside = int(np.sqrt(npix//12))
        level = int(np.log2(nside))

        if npix != 2**(2*level)*12:
            log.error(f'Unexpected number of pixels in healpix: {npix} (assuming level {level})')

        # write hdf5 file or fits table depending on filename extension
        #log.info(f'Writing combined output map: {filename}')
        if file_type == 'hdf5':
            self.write_hdf5(filename, nobs_comb, clouds_comb, sigma_comb,
                            jd=jd_eff, lstmin=lstmin, lstmax=lstmax,
                            npix=npix, nside=nside, level=level)

        elif file_type == 'fits':

            # write info to header, starting with an empty one
            header = fits.Header()
            header['JD'] = (jd_eff, '[d] effective mid-exposure Julian Date')
            for cam in self.cloud_queue:
                header[f'INCL_{cam}'] = (','.join([str(x) for x in used[cam]]), f'SEQ used from {cam} camera')

            header['NEPOCHS'] = (cfg.CLOUD_COMBINE_FRAMES, 'number of LSTSEQ epochs combined')
            header['NFILES'] = (sum([len(x) for x in used.values()]), 'total number of cloud files combined')
            header['COMBINE'] = (combine, 'combination method applied')

            # write fits table
            self.write_fits(filename, nobs_comb, clouds_comb, sigma_comb,
                            header=header, lstmin=lstmin, lstmax=lstmax,
                            npix=npix, nside=nside, level=level)

        else:
            log.error(f'Unknown file_type for combined cloud map export: {file_type}')

        return filename

    def write_hdf5(self, filename, nobs, clouds, sigma, npix, nside, level,
                   lstmin=None, lstmax=None, lstlen=None, jd=None):
        # remove if already exists
        if os.path.exists(filename):
            os.remove(filename)

        # write .hdf5 file
        with h5py.File(filename, 'w') as f:

            f.create_dataset('nobs', data=nobs)
            f.create_dataset('clouds', data=clouds)
            f.create_dataset('sigma', data=sigma)
            f.attrs['grid'] = 'healpix'
            f.attrs['level'] = level
            f.attrs['nside'] = nside
            f.attrs['npix'] = npix
            f.attrs['lstmin'] = lstmin
            f.attrs['lstmax'] = lstmax
            f.attrs['lstlen'] = lstlen
            f.attrs['jd'] = jd

    def write_fits(self, filename, nobs, clouds, sigma, npix, nside, level, header,
                   lstmin=None, lstmax=None, lstlen=None, jd=None):
        # create fits table
        names = ['nobs', 'clouds', 'sigma']
        dtypes = ['int32', 'float32', 'float32']
        table = Table([nobs, clouds, sigma], names=names, dtype=dtypes)
        table['clouds'].unit = units.mag
        table['sigma'].unit = units.mag
        table.write(filename, overwrite=True)

        # strip header from image- or table-dependent keywords
        header.strip()

        # add some info
        header['LEVEL'] = (level, 'HEALPix level')
        header['NSIDE'] = (nside, 'HEALPix nside = 2**level')
        header['NPIX'] = (npix, 'number of HEALPix tiles')
        header['SCHEME'] = ('NEST' if cfg.CLOUD_COMBINE_NEST_HEALPIX else 'RING', 'HEALPix scheme')
        header['LSTMIN'] = lstmin
        header['LSTMAX'] = lstmax
        header['LSTLEN'] = lstlen
        header['TCOMM1'] = 'number of stars used in HEALPix tile'
        header['TCOMM2'] = '[mag] weighted mean extinction in HEALPix tile'
        header['TCOMM3'] = '[mag] error in weighted mean extinction in HEALPix tile'

        # update header of fits table
        with fits.open(filename, 'update', memmap=True) as hdulist:
            # update header
            hdulist[-1].header.update(header)
            # remove unnecessary keywords
            for key in ['CTYPE1', 'CTYPE2', 'CRVAL1', 'CRVAL2', 'CRPIX1',
                        'CRPIX2', 'CUNIT1', 'CUNIT2', 'CD1_1', 'CD1_2', 'CD2_1',
                        'CD2_2', 'HISTORY', 'COMMENT',
                        *['PV{}_{}'.format(i, j) for i in range(1, 3) for j in range(1, 41)]]:
                hdulist[-1].header.pop(key, None)

            # flush hdulist with fix+warn verification
            hdulist.flush(output_verify='fix+warn')


    def clean_map(self, clouds, sigma, nobs, nobs_clean, sigma_factor, combine='median'):

        # determine healpix parameters from length of clouds
        npix = len(clouds)
        nside = int(np.sqrt(npix//12))
        nest = cfg.CLOUD_COMBINE_NEST_HEALPIX
        lonlat = True

        # identify healpix indices with low Nobs
        mask_valid = ((~clouds.mask) & (~sigma.mask))
        indices_replace = list(np.nonzero((nobs < nobs_clean) & mask_valid)[0])

        # print ('np.sum(sigma[mask_valid]==0): {}'.format(np.sum(sigma[mask_valid]==0)))

        # identify healpix indices with high sigma
        if sigma_factor > 0:
            # get indices of neighbours of unmasked healpixels
            indices_valid = np.nonzero(mask_valid)[0]
            #print ('mask_valid.shape: {}'.format(mask_valid.shape))
            ras, decs = hp.pixelfunc.pix2ang(nside, indices_valid, nest=nest,
                                             lonlat=lonlat)
            indices_near = hp.pixelfunc.get_all_neighbours(nside, ras, decs, nest=nest,
                                                           lonlat=lonlat)
            #print ('indices_near: {}'.format(indices_near))
            #print ('indices_near.shape: {}'.format(indices_near.shape))
            #print ('sigma[indices_near]: {}'.format(sigma[indices_near]))

            # some healpixels that are not actual neighbours will result
            # in indices of -1; mask out that entry in sigma so that it is
            # not used
            mask_invalid = (indices_near==-1)
            #print ('np.sum(mask_invalid): {}'.format(np.sum(mask_invalid)))
            if np.sum(mask_invalid) > 0:
                sigma[-1] = np.ma.masked

            sigma_median = np.ma.median(sigma[indices_near], axis=0)
            #print ('sigma_median.shape: {}'.format(sigma_median.shape))
            #print ('sigma_median: {}'.format(sigma_median))
            mask_sigma = (sigma[mask_valid] > (sigma_factor * sigma_median))
            #print ('mask_sigma.shape: {}'.format(mask_sigma.shape))
            #print ('np.sum(mask_sigma): {}'.format(np.sum(mask_sigma)))
            indices_replace += list(np.nonzero(mask_sigma)[0])

        # loop indices to replace
        for index in indices_replace:

            # identify neighboring indices
            ra, dec = hp.pixelfunc.pix2ang(nside, index, nest=nest, lonlat=lonlat)
            indices_near = hp.pixelfunc.get_all_neighbours(nside, ra, dec, nest=nest,
                                                           lonlat=lonlat)

            # only use valid indices
            indices_use = [i for i in indices_near if
                           # discard pixels that are not actual neighbors
                           i != -1 and
                           # discard indices that are being replaced
                           i not in indices_replace]

            n_use = len(indices_use)
            if n_use >= 3:

                if combine == 'average':
                    cloud_val = np.ma.average(clouds[indices_use])
                    sigma_val = np.sqrt(np.ma.sum(sigma[indices_use]**2)) / n_use

                elif combine == 'median':
                    print (clouds[indices_use])
                    cloud_val = np.ma.median(clouds[indices_use])
                    sigma_val = (np.sqrt(np.pi/2) *
                                   np.sqrt(np.ma.sum(sigma[indices_use]**2)) / n_use)

                elif combine == 'weighted':
                    cloud_val, weights_sum = np.ma.average(
                        clouds[indices_use], weights=1/sigma[indices_use]**2,
                        returned=True)
                    sigma_val = 1/np.sqrt(weights_sum)

                else:
                    log.error(f'Cloud map cleaning method not recognised: {combine}')
                    return

                # sum of Nobs in neighboring pixels
                nobs_val = np.sum(nobs[indices_use])

                #print ('cloud_val: {}'.format(cloud_val))
                #print ('sigma_val: {}'.format(sigma_val))
                #print ('nobs_val: {}'.format(nobs_val))

                clouds[index] = cloud_val
                sigma[index] = sigma_val
                nobs[index] = nobs_val

    def calc_sigma (self, sigma_array, axis=0):

        #log.info ('sigma_array.shape: {}'.format(sigma_array.shape))
        #log.info ('sigma_array**2: {}'.format(sigma_array[:,:,0:10]**2))

        # calculate quadratic average of [sigma_array] along [axis]; input
        # array needs to be a masked array
        sigma = np.ma.sum(sigma_array**2, axis=axis)

        #log.info ('sigma.shape: {}'.format(sigma.shape))
        #log.info ('sigma 1: {}'.format(sigma[:,0:10]))

        # sum of non-masked elements along axis (N.B. masked array mask
        # indicates masked elements with True)
        nsum = np.ma.sum(~(sigma_array.mask), axis=axis)

        #log.info ('sigma_array.mask.shape: {}'.format(sigma_array.mask.shape))
        #log.info ('nsum.shape: {}'.format(nsum.shape))
        #log.info ('nsum: {}'.format(nsum[:,0:10]))
        #log.info ('sigma_array.mask: {}'.format(sigma_array.mask[:,:,0:10]))

        # only take square root and divide by nsum if latter is not zero
        mask_nonzero = (nsum != 0)
        sigma[mask_nonzero] = np.sqrt(sigma[mask_nonzero]) / nsum[mask_nonzero]

        #log.info ('sigma 2: {}'.format(sigma[:,0:10]))
        #log.info ('sigma[mask_nonzero]: {}'.format(sigma[mask_nonzero]))

        return sigma
