#!/usr/bin/env python3
import argparse
import asyncio
import concurrent.futures
import datetime
import logging
import multiprocessing
import os
from tempfile import TemporaryDirectory
import warnings

import numpy as np
import pyqtgraph as pg
import pyqtgraph.exporters
import pytz
from astropy.config import set_temp_cache
from pyqtgraph.Qt import QtCore

import dream_config as cfg
import dream_logging
from dream_utils import (get_current_night, iers_init, send_slack_file,
                         send_slack_message)

pg.setConfigOption('background', 'w')
pg.setConfigOption('foreground', 'k')
pg.setConfigOptions(antialias=True)

# Note: Logging in a multi-processing environment is a bit complicated/subtle.
# See: https://docs.python.org/3/howto/logging-cookbook.html#logging-to-a-single-file-from-multiple-processes
# And: https://gist.github.com/gwerbin/e9ab7a88fef03771ab0bf3a11cf921bc

# If the logging implementation here seems complicated, this is because:
# * The plotting functions in pyqtgraph require a running qt event loop.
# * A qt event loop refuses to start unless it is in the main thread of a process.
# * Therefore the plotting needs to happen in a background process.
# * it also takes several seconds so we do not want to block the asyncio event loop.
#   For the reason above this must be a background process and not a background thread.

#  The background process must have its own logger, otherwise the filehandler
#  would still write to the same file. _logger_name is the name of this logger.
#  The process is started by an ProcessPoolExecutor and immediately used by
#  run_in_executor so that asyncio can await the process to finish. The main
#  logger (cfg.LOGGER_NAME) and all its handlers are given to a QueueListener
#  which in a background thread listens to the messages from the worker and
#  posts them in the main logger. The worker initializes its own logger and
#  install the QueueHandler as its only destination. The final detail is that
#  the multiprocessing.Queue is not allowed to be passed as an argument to the
#  worker. Instead it must be saved in a global that is created before the fork
#  and accessed by both processes. This happens by setting it as the destination
#  for the logger in the worker_init function. This works because the worker
#  init is still executed from the parent process.
_logger_name = 'nightly_plots'

directions = ['N', 'E', 'S', 'W', 'C']


def make_timeseries(night, fields, filenamepattern, title, ylabel, labelpattern, outputfile, limits=[]):
    log = logging.getLogger(_logger_name)
    log.setLevel(logging.DEBUG)
    log.info(f'Making {title} plot')
    filenames = [os.path.join(cfg.LOG_PATH, night, filenamepattern.format(field)) for field in fields]
    tzoffset = datetime.datetime.now(pytz.timezone(cfg.SITE_ZONE)).utcoffset().seconds
    data = [None for _ in fields]
    color = [pg.intColor(i, len(fields)) for i in range(len(fields))]

    # first try to load all:
    for i, filename in enumerate(filenames):
        if not os.path.exists(filename):
            log.info(f'{filename} does not exists')
            continue
        with warnings.catch_warnings():
            warnings.simplefilter('ignore')
            d = np.genfromtxt(filename, delimiter=',', invalid_raise=False)
            if len(d.shape) != 2 or d.shape[1] != 2:
                log.info(f'{filename} is not a (N,2)-d array')
                continue
            data[i] = (d[:, 0], d[:, 1])
            log.info(f'{filename} loaded')

    # only create a plot if more than 0 produced some data
    if any(data):
        plot = pg.PlotWidget(
            axisItems={'bottom': pg.DateAxisItem(utcOffset=-tzoffset)},
            labels={'left': ylabel, 'bottom': 'time (local)'},
            title=f'{title} - {night}')
        plot.addLegend()
        for i, trace in enumerate(data):
            if trace:
                plot.plot(trace[0], trace[1], name=labelpattern.format(fields[i]), pen=pg.mkPen(color[i]))

        # limits
        limitPen = pg.mkPen(color=pg.mkColor(255,0, 0, 127), width=1, style=QtCore.Qt.PenStyle.DashLine)
        for limit in limits:
            plot.addLine(y=limit, angle=0, pen=limitPen)

        # export to png
        log.info(f'Exporting to {outputfile}')
        exporter = pg.exporters.ImageExporter(plot.plotItem)
        exporter.parameters()['width'] = 800
        exporter.export(os.path.join(cfg.LOG_PATH, night, outputfile))

        send_slack_file(os.path.join(cfg.LOG_PATH, night, outputfile))
    else:
        log.info(f'Skipping {title} because there are no files')
        send_slack_message(f'No data for {title} plot this night.')


def find_events(points, gap=1.0):
    '''Split a list of points into separate traces assuming a gap of defined
    size starts a new event. For motor moves a gap of 1 second is long since
    logging happens much faster during a move and much slower between moves.'''

    # only based on times, not on values
    times = points[:, 0]

    # get the deltas
    deltas = times[1:] - times[:-1]

    # split input times into events
    breaks = 1 + np.nonzero(deltas > gap)[0]
    events = np.split(times, breaks)

    # return start and stop for each event
    return [(e[0], e[-1]) for e in events]




def make_dome_profiles(night):
    log = logging.getLogger(_logger_name)
    log.setLevel(logging.DEBUG)

    positions_filename = os.path.join(cfg.LOG_PATH, night, 'dome_position.csv')
    if not os.path.exists(positions_filename):
        log.info('Dome never moved. Skipping dome plots')
        return
    # the other files must exist too. If they don't we let this bubble up as an unexpected exception.

    # load data, genfromtxt is like loadtxt but with the option to skip invalid lines
    positions = np.genfromtxt(positions_filename, delimiter=',', invalid_raise=False)
    vsetpoint = np.genfromtxt(os.path.join(cfg.LOG_PATH, night, 'voltage_setpoint.csv'), delimiter=',', invalid_raise=False)
    vfeedback = np.genfromtxt(os.path.join(cfg.LOG_PATH, night, 'voltage_feedback.csv'), delimiter=',', invalid_raise=False)
    ifeedback = np.genfromtxt(os.path.join(cfg.LOG_PATH, night, 'current_feedback.csv'), delimiter=',', invalid_raise=False)

    if len(positions) == 0:
        log.info('No dome position data')
        return

    plot = pg.PlotWidget(
        labels={'left': 'Encoder counts', 'bottom': 'time (s)'},
        title=f'Dome position - {night}')
    plot.addLegend(offset=(-0.1, 30))

    # plot each event separately
    events = find_events(positions)
    for i, (start, end) in enumerate(events):
        # since events are based on positions it is guaranteed that each event
        # has at least one measurement and can therefore be plotted

        # slice the samples from this event
        indices = np.logical_and(start <= positions[:, 0], positions[:, 0] <= end)
        eventdata = positions[indices]

        # plot
        when = datetime.datetime.fromtimestamp(start, pytz.timezone(cfg.SITE_ZONE))
        plot.plot(eventdata[:, 0]-start, eventdata[:, 1], name=when.isoformat(timespec='seconds'), pen=pg.intColor(i, len(events)))

    # limits
    limitPen = pg.mkPen(color=pg.mkColor(255,0, 0, 127), width=1, style=QtCore.Qt.PenStyle.DashLine)
    plot.addLine(y=cfg.DOME_CLOSED_POSITION, angle=0, pen=limitPen)
    plot.addLine(y=cfg.DOME_OPEN_POSITION, angle=0, pen=limitPen)
    # export to png
    outputfile = 'dome_profile.png'
    log.info(f'Exporting to {outputfile}')
    exporter = pg.exporters.ImageExporter(plot.plotItem)
    exporter.parameters()['width'] = 800
    exporter.export(os.path.join(cfg.LOG_PATH, night, outputfile))
    send_slack_file(os.path.join(cfg.LOG_PATH, night, outputfile))


    plot = pg.PlotWidget(
        labels={'left': 'Voltage (V)', 'bottom': 'time (s)'},
        title=f'Motor voltage - {night}')
    plot.addLegend(offset=(-0.1, 30))

    # plot motor feedback
    for i, (start, end) in enumerate(events):
        # slice the samples from this event
        indices = np.logical_and(start <= vsetpoint[:, 0], vsetpoint[:, 0] <= end)
        v_sp_data = vsetpoint[indices]
        indices = np.logical_and(start <= vfeedback[:, 0], vfeedback[:, 0] <= end)
        v_fb_data = vfeedback[indices]

        # for all labels we use a local timestamp later
        when = datetime.datetime.fromtimestamp(start, pytz.timezone(cfg.SITE_ZONE)).isoformat(timespec='seconds')
        color = pg.intColor(i, len(events))

        # can't plot line segments with less than 2 datapoints
        if len(v_sp_data) > 1:
            color.setAlpha(50)
            pen = pg.mkPen(color=color, width=7)
            plot.plot(v_sp_data[:, 0]-start, v_sp_data[:, 1], name=f'{when} Sp', pen=pen)
        if len(v_fb_data) > 1:
            color.setAlpha(255)
            pen = pg.mkPen(color=color, width=1)
            plot.plot(v_fb_data[:, 0]-start, v_fb_data[:, 1], name=f'{when} Fb', pen=pg.intColor(i, len(events)))

    # export to png
    outputfile = 'dome_motor_voltage.png'
    log.info(f'Exporting to {outputfile}')
    exporter = pg.exporters.ImageExporter(plot.plotItem)
    exporter.parameters()['width'] = 800
    exporter.export(os.path.join(cfg.LOG_PATH, night, outputfile))
    send_slack_file(os.path.join(cfg.LOG_PATH, night, outputfile))


    plot = pg.PlotWidget(
        labels={'left': 'Current (A)', 'bottom': 'time (s)'},
        title=f'Motor current - {night}')
    plot.addLegend(offset=(-0.1, 30))

    # plot motor feedback
    for i, (start, end) in enumerate(events):
        # slice the samples from this event
        indices = np.logical_and(start <= ifeedback[:, 0], ifeedback[:, 0] <= end)
        i_fb_data = ifeedback[indices]

        # for all labels we use a local timestamp later
        when = datetime.datetime.fromtimestamp(start, pytz.timezone(cfg.SITE_ZONE)).isoformat(timespec='seconds')
        color = pg.intColor(i, len(events))

        # can't plot line segments with less than 2 datapoints
        if len(i_fb_data) > 1:
            color.setAlpha(255)
            pen = pg.mkPen(color=color, width=1)
            plot.plot(i_fb_data[:, 0]-start, i_fb_data[:, 1], name=f'{when} Fb', pen=pg.intColor(i, len(events)))

    # export to png
    outputfile = 'dome_motor_current.png'
    log.info(f'Exporting to {outputfile}')
    exporter = pg.exporters.ImageExporter(plot.plotItem)
    exporter.parameters()['width'] = 800
    exporter.export(os.path.join(cfg.LOG_PATH, night, outputfile))
    send_slack_file(os.path.join(cfg.LOG_PATH, night, outputfile))


def make_nightly_plots(night=None):
    logger = logging.getLogger(_logger_name)
    logger.setLevel(logging.DEBUG)

    try:
    # make sure a qt event loop has been started
        os.environ['QT_QPA_PLATFORM'] = 'offscreen'
        app = pg.mkQApp()

        night = night or get_current_night(format='%Y_%m_%d')

        # Plot ccd temps:
        make_timeseries(night, directions, 'ccd_{}.csv', 'CCD temp', '(°C)', 'ccd {}', 'ccd.png')
        #return

        # plot environment measurements
        locations = [location[1] for sensor in cfg.GUDE_SENSOR_PORTS.values() for location in sensor]
        make_timeseries(night, locations, '{}_temperature.csv', 'Station temperatures', '(°C)', '{}', 'temperatures.png')
        make_timeseries(night, locations, '{}_humidity.csv', 'Station humidities', '(%)', '{}', 'humidities.png')
        make_timeseries(night, ['memfree', 'load1'], '{}.csv', 'Base system', '', '{}', 'system.png')

        # plot dome state over entire night
        make_timeseries(night, ['dome_position'], '{}.csv', 'Dome position', 'enc count', 'dome', 'dome_position.png', [cfg.DOME_CLOSED_POSITION, cfg.DOME_OPEN_POSITION])

        make_dome_profiles(night)

    except Exception as e:
        logger.exception(e)
        # and gobble it up.
        # because this is not critical
        # and raising exceptions in background processes interferes with asyncio clean shutdown
        # so we need this function to return normally, even if there was an error.


def worker_init(log_queue):
    logger = logging.getLogger(_logger_name)
    logger.setLevel(logging.DEBUG)
    handler = logging.handlers.QueueHandler(log_queue)
    logger.addHandler(handler)


async def make_nightly_plots_async(night=None):
    log = logging.getLogger(cfg.LOGGER_NAME)
    log.setLevel(logging.DEBUG)
    log_queue = multiprocessing.Queue()
    log_listener = logging.handlers.QueueListener(log_queue, *log.handlers)
    # this starts in a thread in the main process
    # using threads in an asyncio context is not forbidden
    log_listener.start()

    # now we do the work:
    loop = asyncio.get_running_loop()
    with concurrent.futures.ProcessPoolExecutor(max_workers=1, initializer=worker_init, initargs=(log_queue,)) as pool:
        await loop.run_in_executor(pool, make_nightly_plots, night)

    log_listener.enqueue_sentinel()


async def standalone_main():
    iers_init()

    # parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--night', help='Night to plot, defaults to current night. Specify as Y/m/d from before midnight.')
    args = parser.parse_args()
    if args.night:
        args.night = args.night.replace('/', '_')

    await make_nightly_plots_async(args.night)


if __name__ == '__main__':
    # start logging to file and console
    log = logging.getLogger(cfg.LOGGER_NAME)
    log.setLevel(logging.DEBUG)
    dream_logging.setup_logging(log, 'dream_nightly_plots.log')


    # fetch IERS tables:
    with TemporaryDirectory() as tmpdir:
        log.info(f'Using IERS cache dir: {tmpdir}')
        with set_temp_cache(tmpdir):
            asyncio.run(standalone_main())
