#!/usr/bin/env python3
from abc import ABC, abstractmethod
import dream_config as cfg
from hardware.pysnmp_device import PySNMPDevice
import logging
import random
import time

log = logging.getLogger(cfg.LOGGER_NAME)

class UPSInterface(ABC):
    '''UPS Interface Class'''

    # reasons for switching to battery:
    ERRORS = {
        1: 'no error',
        2: 'high line voltage',
        3: 'brownout',
        4: 'blackout',
        5: 'small momentary sag',
        6: 'deep momentary sag',
        7: 'small momentary spike',
        8: 'large momentary spike',
        9: 'self test',
        10: 'rate of voltage change',
    }

    @abstractmethod
    async def get_status(self):
        # get the raw string values
        pass


class UPSSim(UPSInterface):
    def __init__(self):
        self.starttime = time.time()

    async def get_status(self):
        # simulate full battery
        charge = 100.

        if False:
            # simulate an online battery for the first 60 seconds
            # then a draining battery at 1% per second until empty
            initial = 5
            speed = 0.1
            if time.time() - self.starttime < initial:
                charge = 100.
            else:
                elapsed = time.time() - self.starttime - initial
                charge = max(0., 100 - elapsed * speed)

        if False:
            # simulate a discharging and then charging
            if time.time() - self.starttime < 60:
                elapsed = time.time() - self.starttime
                charge = 100 - 1 * elapsed
            else:
                elapsed = time.time() - self.starttime - 60
                charge = min(100, 100 - 60 + 1 * elapsed)


        # simulate half a minute of remaining time per % of charge
        remaining = charge / 2

        values = {}
        values['ups_status']              = 'ONLINE' if charge == 100 else 'BATTERY'
        values['battery_charge']          = charge
        values['battery_temperature']     = 30.0 + random.random()
        values['battery_voltage']         = 48 + random.random()
        values['battery_remaining']       = remaining
        values['battery_needs_replacing'] = False
        values['input_last_error']        = 'no error'
        values['output_load']             = 25.0 + random.random()
        values['output_current']          = 1.5 + random.random()

        # return the result
        return values


class UPS(PySNMPDevice, UPSInterface):
    '''UPS Class'''
    # see powernet428.mib
    # The 2 field equivalent to the mascara system
    OID_STATUS = '1.3.6.1.4.1.318.1.1.1.4.1.1.0'
    OID_CHARGE = '1.3.6.1.4.1.318.1.1.1.2.3.1.0'

    # some extra diagnostics for fun and logging
    OIDS_EXTRA = {
        'ups_status':              OID_STATUS,
        'battery_charge':          OID_CHARGE,
        'battery_temperature':     '1.3.6.1.4.1.318.1.1.1.2.3.2.0',
        'battery_voltage':         '1.3.6.1.4.1.318.1.1.1.2.3.4.0',
        'battery_remaining':       '1.3.6.1.4.1.318.1.1.1.2.2.3.0',
        'battery_needs_replacing': '1.3.6.1.4.1.318.1.1.1.2.2.4.0',
        'input_last_error' :       '1.3.6.1.4.1.318.1.1.1.3.2.5.0',
        'output_load':             '1.3.6.1.4.1.318.1.1.1.4.3.3.0',
        'output_current':          '1.3.6.1.4.1.318.1.1.1.4.3.4.0',
    }



    async def get_status(self):
        # get the raw string values
        oids = list(self.OIDS_EXTRA.values())
        values = await self.get(*oids)

        # turn back into dict with keys from OIDS_EXTRA
        values = {k:v for k,v in zip(self.OIDS_EXTRA.keys(), values)}
        # log.debug(values)

        # do some sane value conversions
        values['ups_status']              = 'ONLINE' if values['ups_status'] == 2 else 'BATTERY'
        values['battery_charge']          = 0.1 * values['battery_charge']
        values['battery_temperature']     = 0.1 * values['battery_temperature']
        values['battery_voltage']         = 0.1 * values['battery_voltage']
        values['battery_remaining']       = values['battery_remaining'] / 60 / 100 # units are 100th of seconds
        values['battery_needs_replacing'] = values['battery_needs_replacing'] != 1
        values['input_last_error']        = self.ERRORS.get(values['input_last_error'], 'bad error code')
        values['output_load']             = 0.1 * values['output_load']
        values['output_current']          = 0.1 * values['output_current']

        # return the result
        return values
