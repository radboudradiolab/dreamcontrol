#!/usr/bin/env python3

import dream_config as cfg
import logging
if 'DOME' not in cfg.SIMULATE:
    import uldaq

log = logging.getLogger(cfg.LOGGER_NAME)

class DAQ():
    """DAQ wrapper for Dream. This is a simplified version of the mascara/bring
    DAQ class with all unused features stripped. Note that it is not thread-safe
    because dream uses asyncio almost everywhere (except some low-level camera
    commands).

    """

    def __init__(self, serial: str):
        # probe for available DAQ devices
        self.dev = None
        devices = uldaq.get_daq_device_inventory(uldaq.InterfaceType.USB)
        # go through the list looking for one matching the taget serial
        for candidate_dev in devices:
            self.dev = uldaq.DaqDevice(candidate_dev)
            self.dev.connect()
            unique_id = self.dev.get_descriptor().unique_id

            # if we find it, break and don't disconnect
            if unique_id == serial:
                break
            else:
                self.dev.disconnect()

        # fail hard if not found. The station cannot start without it.
        if self.dev is None:
            log.error(f'DAQ with serial {cfg.DAQ_SERIAL} not found')
            raise RuntimeError(f'DAQ with serial {cfg.DAQ_SERIAL} not found')

        # configure the ports:

        # The DAQ has 2 ports of digital IO which are all used in output mode to
        # control leds, motors, heaters and peltiers. The 2 analog output
        # channels are used to control the voltage and current setting of the
        # power supplies. The 8 analog input channels are used to monitor the
        # power supply outputs (4 channels in total for current/volage in 2
        # PSU's). The remaining 4 analog inputs are abused to measure the
        # voltage on the door and dome switches, which is actually a digital
        # signal. The arrangement is like this because the 16 digital io's can
        # only be configured per port of 8 channels and we need more than 8
        # digital outputs.

        # Things could be cleaned up if we were to merge the 5 led outputs
        # (since we only switch them at the same time ever anyway) and don't
        # abuse digital outputs for the constant high signals to the switches.

        # configure digital io ports as all outputs
        dio = self.dev.get_dio_device()
        for port in dio.get_info().get_port_types():
            dio.d_config_port(port, uldaq.DigitalDirection.OUTPUT)

    def close(self):
        self.dev.disconnect()

    def set_digital_output(self, channel: int, val: bool):
        ''' Set a digital output pin.
        Args:
            channel (int): channel in the range 0-15
            val (bool): True to turn the bit on, False for off
        '''
        dio = self.dev.get_dio_device()

        if 0 <= channel < 8:
            channel = channel
            port = uldaq.DigitalPortType.FIRSTPORTA
        elif channel < 16:
            channel = channel - 8
            port = uldaq.DigitalPortType.FIRSTPORTB
        else:
            raise ValueError(f'DAQ channel out of range: {channel} (valid range 0-15)')

        dio.d_bit_out(port, channel, val)



    def read_analog_input(self, channel):
        ''' Reads an analog input channel and returns a voltage.

        Args:
            channel (int): the channel to read

        Returns:
            float: the voltage
        '''
        ai = self.dev.get_ai_device()
        return ai.a_in(channel, uldaq.AiInputMode.SINGLE_ENDED, uldaq.Range.BIP10VOLTS, uldaq.AInFlag.DEFAULT)

    def set_analog_output(self, channel, voltage):
        '''Sets the value of an analog output.

        Args:
            channel (int): the channel
            voltage (float): the desired voltage (0.0-5.0)
        '''
        if voltage < 0 or voltage > 5.0:
            raise ValueError('The voltage must be between 0 and 5V')

        ao = self.dev.get_ao_device()
        ao.a_out(channel, uldaq.Range.UNI5VOLTS, uldaq.AOutFlag.DEFAULT, voltage)
