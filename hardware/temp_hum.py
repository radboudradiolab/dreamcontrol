#!/usr/bin/env python3
from abc import ABC, abstractmethod
import dream_config as cfg
from hardware.pysnmp_device import PySNMPDevice
from random import random
import time
import logging
import asyncio
#import sys
#sys.path.append('..')

log = logging.getLogger(cfg.LOGGER_NAME)

class TempSensorInterface(ABC):

    @abstractmethod
    async def get_all(self):
        pass

class TempSensorSim(TempSensorInterface):
    def __init__(self, ip_address: str):
        self.ip_address = ip_address

    async def get_all(self):
        return {name: { 'temperature': 25 + random(),
                        'humidity':    50 + random()}
                for _, name in cfg.GUDE_SENSOR_PORTS[self.ip_address]}

class TempSensor(PySNMPDevice, TempSensorInterface):
    OID_TMP = '1.3.6.1.4.1.28507.66.1.6.1.1.2.{}'
    OID_HUM = '1.3.6.1.4.1.28507.66.1.6.1.1.3.{}'

    async def get_all(self):
        res = {}
        ports = cfg.GUDE_SENSOR_PORTS[self.ip_address]

        for offset, name in ports:
            tmp, hum = await self.get(
                self.OID_TMP.format(offset),
                self.OID_HUM.format(offset)
            )
            tmp = 0.1 * float(tmp)
            hum = 0.1 * float(hum)

            # add to result dict
            res[name] = { 'temperature': tmp,
                          'humidity':    hum }
        return res
