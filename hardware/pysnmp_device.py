#!/usr/bin/env python3

import asyncio
import dream_config as cfg
import dream_credentials as creds
import dream_logging
import logging
from typing import Tuple, Optional, Sequence, Any
from pysnmp.entity import config
from pyasn1.type.univ import Integer, Boolean
import pysnmp.hlapi.asyncio as snmp
import time

log = logging.getLogger(cfg.LOGGER_NAME)


class PySNMPDevice(object):
    '''Another re-implementation of a generic SNMP device. Using PySNMP so it
    has no external dependencies and it natively asyncio compatible.

    Note: the result is of an internal type Integer or OctetString. While they
    have some interoperability with native int and str, .format nor __equal__
    works well on these, so best to cast to str or int before formatting or
    comparing.

    Note: install pysnmplib from pip not pysnmp. The latter is unmaintained but
    the former is a community driven fork that has continued the development.
    '''

    def __init__(self, ip_address: str, auth: Optional[Tuple[str, str]] = None):
        # remember ip address:
        self.ip_address = ip_address
        # prepare userdata for authentication
        if auth:
            self.userdata = snmp.UsmUserData(
                auth[0], auth[1], auth[1], # username, auth pw, priv pw
                authProtocol=config.usmHMACSHAAuthProtocol,
                privProtocol=config.usmAesCfb128Protocol)
        else:
            self.userdata = snmp.CommunityData('public')

        # prepare engine and transport
        self.engine = snmp.SnmpEngine()
        # this defaults to 5 retries with a timeout of 1s:
        self.transport = snmp.UdpTransportTarget((ip_address, 161), timeout=6, retries=2)
        self.context = snmp.ContextData()
        log.info(f'SNMP device ({ip_address}) initialized')


    async def get(self, *oids: str) -> Sequence[Any]:
            # prepare a list of Object ID's
            vars = [snmp.ObjectType(snmp.ObjectIdentity(oid)) for oid in oids]

            # do the async lookup
            error, _, _, result = await snmp.getCmd(
                self.engine,
                self.userdata,
                self.transport,
                self.context,
                *vars,
                lookupMib=False
            )

            # check for errors
            if error:
                raise error
            if len(result) != len(oids):
                raise RuntimeError('Bad SNMP response')

            # format the result
            res = [objecttype[1] for objecttype in result]

            # convert to native python types
            res = [self.convert(x) for x in res]

            return res

    def convert(self, x):
        if isinstance(x, Integer):
            return int(x)
        if isinstance(x, Boolean):
            return bool(x)
        # anything else as str:
        return str(x)

    async def set(self, oid: str, value):
        # prepare a list of Object ID's
        var = snmp.ObjectType(snmp.ObjectIdentity(oid), value)

        # do the async lookup
        error, _, _, _ = await snmp.setCmd(
            self.engine,
            self.userdata,
            self.transport,
            self.context,
            var,
            lookupMib=False
        )

        # check for errors
        if error:
            raise error


async def test():

    # test a TempHum lookup
    dev = PySNMPDevice(
        ip_address = list(cfg.GUDE_SENSOR_PORTS.keys())[0],
        auth = None)
    tmp, hum = await dev.get('1.3.6.1.4.1.28507.66.1.6.1.1.2.1', '1.3.6.1.4.1.28507.66.1.6.1.1.3.1')
    log.info(f'Connected to TempHum: {float(tmp)/10:.1f}°C / {float(hum)/10:.1f} %')

    # test a ups lookup
    dev = PySNMPDevice(
        ip_address = cfg.IP_UPS,
        auth = (creds.UPS_USERNAME,creds.UPS_PASSWORD))
    ups = await dev.get('1.3.6.1.4.1.318.1.1.1.1.1.1.0')
    log.info(f'Connected to UPS: {str(ups[0])}')

    # test a pdu lookup
    dev = PySNMPDevice(
        ip_address = cfg.IP_PDU_1,
        auth = (creds.PDU_USERNAME,creds.PDU_PASSWORD))
    pdu = await dev.get('1.3.6.1.4.1.318.1.1.12.1.5.0')
    log.info(f'Connected to PDU: {str(pdu[0])}')

    # test pdu set
    log.info('Turning PDU 1 port 7 off...')
    await dev.set('1.3.6.1.4.1.318.1.1.12.3.3.1.1.4.7', Integer(2))
    await asyncio.sleep(1)

    pdu = await dev.get('1.3.6.1.4.1.318.1.1.12.3.3.1.1.4.7')
    log.info(f'PDU: {int(pdu[0])}')

    log.info('Turning PDU 1 port 7 on...')
    await dev.set('1.3.6.1.4.1.318.1.1.12.3.3.1.1.4.7', Integer(1))
    await asyncio.sleep(1)

    pdu = await dev.get('1.3.6.1.4.1.318.1.1.12.3.3.1.1.4.7')
    log.info(f'PDU: {int(pdu[0])}')




async def stresstest():
    dev = PySNMPDevice(ip_address = list(cfg.GUDE_SENSOR_PORTS.keys())[0])
    start = time.time()
    for i in range(50000000):
        before = time.time()
        tmp, hum = await dev.get('1.3.6.1.4.1.28507.66.1.6.1.1.2.1', '1.3.6.1.4.1.28507.66.1.6.1.1.3.1')

        dt = time.time() - before
        log.info(f'{time.time()-start:6.3f} {i: 5d} {dt:2.3f}')



if __name__ == '__main__':
    dream_logging.setup_logging(log)
    asyncio.run(stresstest())
