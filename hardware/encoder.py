#!/usr/bin/env python3

import bitstring
from pyftdi.gpio import GpioSyncController
import dream_config as cfg


class Encoder(object):
    '''Class for communicating with the encoder using SSI protocol. SSI stands
       for Synchronous Serial Interface. It means that in order to get any
       signal from the encoder, the computer has to send a continuous pulse of
       N-bits. The response from the encoder is 20 bits long: the first 8 are
       the multiturn information, the last 12 the singleturn information. The
       communication is in BitBang mode and hex.

       This code requires the ftd2xx.dll to be in the working directory.
    '''
    def __init__(self):
        '''
        Initialize the encoder instance. Requires the ftd2xx.dll to be in the
        working directory. No libusb is required.
        '''
        self.gpio = GpioSyncController()
        # IO direction is 0x01 (clk is DBUS0 on the FTDI and the only output(1))
        # initial value is 1 according to SSI
        self.gpio.configure('ftdi://::{}/1'.format(cfg.SN_ENCODER), initial=0x01, direction=0x01)

        # a sane default for the bitbanging speed
        # STT: The FTDI chip multiplies the actual freq by 5
        # The Mascara code used 460800 which was actually 2304000
        # But the new lib compensates for this.
        self.set_baudrate(460800 * 5)


    def set_baudrate(self, baudrate=460800):
        '''Sets the baud rate for the communication. The encoder needs a
        high baud rate.

        Keyword args:
            baudrate (int): the baud rate.
        '''
        self.gpio.set_frequency(baudrate)

    def get_position(self):
        '''Wrapper that writes to the encoder and reads from it.

        Returns:
            float: encoder position (integer number of revolutions, fraction of
                a revolution)
        '''
        if not self.gpio.is_connected:
            return -999.0
        # 20 rising clock edged, plus some extra
        # should always end with 1 to make the clock high between readous
        # a few nonsense bits are received at the start so we need some extra
        data = b'\x00\x01' * 25
        data = self.gpio.exchange(data)
        # get the second bit from each byte because the data is on the second line
        # get the even bits starting at offset 10
        # the first 5 bits are nonsense added by the encoder
        bits = bitstring.Bits([bool(x & 0x02) for x in data[10::2]])
        intvalue = bits.int # note: this does 2's complement encoding
        # and return the fractional rotation
        return intvalue / (2 ** 12)

    def close(self):
        '''Closes the conection to the encoder.
        '''
        self.gpio.close()
