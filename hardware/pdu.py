#!/usr/bin/env python3
from abc import ABC, abstractmethod
import asyncio
import dream_credentials as creds
import dream_config as cfg
from dream_utils import open_and_lock
import logging
import pickle
import hashlib
import os
import fcntl
from hardware.pysnmp_device import PySNMPDevice
import pysnmp
from typing import Sequence

log = logging.getLogger(cfg.LOGGER_NAME)

class PDUInterface(ABC):
    '''PDU Interface'''

    # enum values for set commands
    IMMEDIATE_ON = pysnmp.proto.api.v1.Integer(1)
    IMMEDIATE_OFF = pysnmp.proto.api.v1.Integer(2)
    IMMEDIATE_REBOOT = pysnmp.proto.api.v1.Integer(3)
    DELAYED_OFF = pysnmp.proto.api.v1.Integer(5)

    @abstractmethod
    async def turn_on(self, outlet):
        '''Turns on a given outlet.

        Args:
            outlet (int): outlet to turn on.
        '''
        pass

    @abstractmethod
    async def turn_off_immediate(self, outlet):
        '''Turns off a given outlet.

        Args:
            outlet (int): outlet to turn off.
        '''
        pass

    @abstractmethod
    async def turn_off_delayed(self, outlet):
        '''Turns off a given outlet.

        Args:
            outlet (int): outlet to turn off.
        '''
        pass

    @abstractmethod
    async def get_status(self) -> Sequence[bool]:
        '''Get status of all outlets.

        Returns:
            list: list of outlet status. True means on
        '''
        pass

    @abstractmethod
    async def get_current(self) -> float:
        '''Gets total current draw in Amps.
        '''
        pass

    #################################
    # The following methods are wrappers and identical for Simulator and Real
    # devices

    async def get_power(self):
        '''Gets total power load.'''

        current = await self.get_current()
        power = 230.0 * current
        return power


class PDUSim(PDUInterface):
    '''PDU Simulator
    '''

    # simulated state
    def __init__(self, ip_address: str):
        self._id = hashlib.md5(ip_address.encode()).hexdigest()[:6]
        # we must do the write attomically
        filename = f'/tmp/pdusim_{self._id}.slv'
        # if anything at all is wrong with the file, we repair it by writing a
        # new state
        with open_and_lock(filename) as f:
            try:
                pickle.load(f)
            except EOFError:
                log.info(f'No PDU state file found for {ip_address}')
                f.seek(0)
                pickle.dump([True] * 8, f)

    async def turn_on(self, outlet):
        '''Turns on a given outlet.

        Args:
            outlet (int): outlet to turn on.
        '''
        # make sure the outlet is truncated to 1-8
        outlet = ((outlet - 1) % 8) + 1
        assert 1 <= outlet <= 8

        # we must do the write attomically
        filename = f'/tmp/pdusim_{self._id}.slv'
        with open_and_lock(filename) as f:
            state = pickle.load(f)
            f.seek(0)
            state[outlet - 1] = True
            pickle.dump(state, f)

    async def turn_off_immediate(self, outlet):
        '''Turns off a given outlet.

        Args:
            outlet (int): outlet to turn off
        '''
        # make sure the outlet is truncated to 1-8
        outlet = ((outlet - 1) % 8) + 1
        assert 1 <= outlet <= 8

        # we must do the write attomically
        filename = f'/tmp/pdusim_{self._id}.slv'
        with open_and_lock(filename) as f:
            state = pickle.load(f)
            f.seek(0)
            state[outlet - 1] = False
            pickle.dump(state, f)

    async def turn_off_delayed(self, outlet):
        '''Turns off a given outlet.

        Args:
            outlet (int): outlet to turn off.
        '''
        # make sure the outlet is truncated to 1-8
        outlet = ((outlet - 1) % 8) + 1
        assert 1 <= outlet <= 8

        async def task(filename, outlet):
            await asyncio.sleep(cfg.PDU_POWEROFF_DELAY)
            # we must do the write attomically
            with open_and_lock(filename) as f:
                state = pickle.load(f)
                f.seek(0)
                state[outlet - 1] = False
                pickle.dump(state, f)

        filename = f'/tmp/pdusim_{self._id}.slv'
        asyncio.create_task(task(filename, outlet))

    async def get_status(self) -> Sequence[bool]:
        '''Get status of all outlets.

        Returns:
            list: list of outlet status. True means on
        '''
        filename = f'/tmp/pdusim_{self._id}.slv'
        with open_and_lock(filename) as f:
            return pickle.load(f)
        #return [True] * 8

    async def get_current(self) -> float:
        '''Gets total current draw in Amps.
        '''
        # for the simulation we assume 0.5A per on port
        self._read()
        return sum([0.5 if on else 0 for on in self._state])


class PDU(PySNMPDevice, PDUInterface):
    '''PDU Class

    Note: although asyncio aware. This only awaits the networked interaction.
    There is a short delay inside the PDU such that writing and immediately
    reading may yield the old value.
    '''

    OID_CONTROL = '1.3.6.1.4.1.318.1.1.12.3.3.1.1.4.{}'
    # see: https://www.apc.com/us/en/faqs/FA156142/
    OID_LOAD = '1.3.6.1.4.1.318.1.1.12.2.3.1.1.2.1'
    OID_POWER_OFFTIME = '1.3.6.1.4.1.318.1.1.12.3.4.1.1.5.{}'


    # enum values for set commands
    #IMMEDIATE_ON = pysnmp.proto.api.v1.Integer(1)
    #IMMEDIATE_OFF = pysnmp.proto.api.v1.Integer(2)
    #IMMEDIATE_REBOOT = pysnmp.proto.api.v1.Integer(3)


    async def turn_on(self, outlet):
        '''Turns on a given outlet.

        Args:
            outlet (int): outlet to turn on.
        '''
        outlet = ((outlet - 1) % 8) + 1 # make sure the outlet is truncated to 1-8
        await self.set(self.OID_CONTROL.format(outlet), self.IMMEDIATE_ON)

    async def turn_off_immediate(self, outlet):
        '''Turns off a given outlet.

        Args:
            outlet (int): outlet to turn off.
        '''
        outlet = ((outlet - 1) % 8) + 1 # make sure the outlet is truncated to 1-8
        await self.set(self.OID_CONTROL.format(outlet), self.IMMEDIATE_OFF)

    async def turn_off_delayed(self, outlet):
        '''Turns off a given outlet.

        Args:
            outlet (int): outlet to turn off.
        '''
        outlet = ((outlet - 1) % 8) + 1 # make sure the outlet is truncated to 1-8
        await self.set(self.OID_POWER_OFFTIME.format(outlet), pysnmp.proto.api.v1.Integer(cfg.PDU_POWEROFF_DELAY))
        await self.set(self.OID_CONTROL.format(outlet), self.DELAYED_OFF)

    async def get_status(self) -> Sequence[bool]:
        '''Get status of all outlets.

        Returns:
            list: list of outlet status. True means on
        '''
        oids = [self.OID_CONTROL.format(i) for i in range(1,9)]
        status = await self.get(*oids)

        # convert to integers
        status = [int(val) for val in status]

        # map to boolean: 1 means on, 2 means off, anything else is an error
        if min(status) < 1 or max(status) > 2:
            raise RuntimeError('PDU outlet status out of range')
        return [val == 1 for val in status]

    async def get_current(self) -> float:
        '''Gets total current draw in Amps.
        '''
        val = await self.get(self.OID_LOAD)
        return float(val[0]) * 0.1
